---
title: Prêts
comments: false
---
Sous forme de prêts personnels, vous pouvez participer concrètement à l’avancement de ce projet global de permaculture humaine.

![](/images/uploads/1580301032833_illustration-prêt.jpg)  

Pour des prêts de 500 à 30 000 € avec un taux d'intérêt très faible, nous vous proposons une procédure simple, dont les conditions sont énoncées dans [ce document](https://docs.google.com/document/d/1SmuRGqJbSseQ1PdDikTR1klQ25XVyqqXa-YPqN2fGf0/edit?usp=sharing) qu'il vous suffit de remplir et de nous renvoyer. N'hésitez pas à [nous contacter](/page/contact) si quelque chose n'est pas clair.

Pour des prêts avec un meilleur rendement et à durée fixe, nous n'avons pas établi de conditions génériques et sommes prêts à discuter des conditions qui vous conviendraient. Là encore rendez-vous sur le [formulaire de contact](/page/contact) !

Nous envisageons aussi de monter dans le futur des financements participatifs sur des sites dédiés au _crowdfunding_. Vous ne les manquerez pas en suivant [notre newsletter](/page/contact) !

### Mais est-ce que c'est légal ?

Tout à fait, vous avez le droit d'accorder un prêt à un tiers, particulier ou société (nous sommes une SCI). Cela entraîne deux obligations :

*   si le prêt dépasse 760 €, le déclarer à l'administration fiscale _via_ un [formulaire 2062](https://www.impots.gouv.fr/portail/files/formulaires/2062/2016/2062_1391.pdf) à joindre à votre déclaration de revenus ;
*   déclarer avec vos revenus les intérêts perçus, sur lesquels vous serez normalement imposés.
