---
title: Formation CNV
subtitle: Module 2
date: '2020-03-18'
tags:
  - formation
  - vie de groupe
  - gouvernance
  - CNV
---
Le week-end des 14 et 15 janvier 2020 a été consacré pour la quasi totalité du groupe au module 2 de la formation à la [Communication Non Violente](https://www.cnvformations.fr/), communément appelée CNV.

Cette méthode, élaborée par Marshall Rosenberg, permet de communiquer en parlant de soi, d'aller au fond de ses ressentis pour y déceler les besoins fondamentaux qui se cachent derrière une émotion, pour pouvoir ensuite se mettre en lien avec ce qu'il y a de vivant dans une relation. Dans quel but? Celui de prendre soin de soi et des autres, celui de coconstruire une relation vraie et sincère.

![](/images/uploads/1587026133725_CNV_Module_2_3.jpg)  

[Le module 1](/post/2020-01-28-communication-non-violente/) nous a été dispensé en janvier. Entre temps, nous avons mis en place au sein du groupe quelques animations permettant de revenir sur les apprentissages pour tenter de les transformer en acquis et d'être fin prêts pour le module 2. C'est que passer de la théorie à la pratique, a fortiori de manière fluide et naturelle, ce n'est pas si simple ! Le module 2 nous a cependant permis de le faire, en nous reliant à ce qu'il se passe en nous, toujours, mais aussi, à présent, en essayant de nous relier aux ressentis et besoins de(s) l'autre(s) afin d'instaurer une communication dépourvue de jugements, se détachant de toute violence (envers la situation, envers l'autre ou envers nous-mêmes) pour aller voir ce qui se cache derrière. Deux paradigmes expliquent bien ce vers quoi notre société tend souvent et ce vers quoi la CNV pourrait nous permettre de tendre:

_Le paradigme chacal_ (celui que tout un chacun a en soi culturellement, à moins d'être un Saint)

![](/images/uploads/1587026188924_CNV_Module_2_1.jpg)  

_Le paradigme girafe_ (celui que propose l'application des principes de la CNV pour une communication au service des relations)

![](/images/uploads/1587026202060_CNV_Module_2_2.jpg)  

Nous remercions une fois de plus très chaleureusement Edith Tavernier, formatrice en CNV à [AT Conseil](http://www.atconseil.com/), ainsi que Benjamin Pont d'[Habitat et Partage](http://habitatetpartage.fr/) pour leur accompagnement à ces deux week-ends de formation.
