---
title: Panification
date: '2023-05-04'
tags:
  - portrait
---

Introduction
------------

_Bzzzzzzzz_. Oui, c'est bon, j'arrive. C'est le four qui fait ce bruit à la fin de la cuisson du pain, mais on ne peut pas lui en vouloir, c'est moi qui ai lancé ce programme de cuisson il y a presque une heure. Là, je sors la douzaine de pains du four, une odeur agréable se dégage, la croûte du pain croustille, chante, et je sais que je vais faire le bonheur des papilles de l'écolieu, de ses habitants, de ses volontaires venus en chantier participatif ou en wwoofing, ou des participants à des événements organisés sur le lieu. Ceux qui ont déjà passé un moment à l'écolieu m'ont déjà vu rentrer dans la Salle Parapluie avec un beau plateau de pain bien chaud dégageant une odeur appétissante.

![](https://cloud.magnyethique.org/s/RQcAi6im4RPEeAa/preview)  

Motivation
----------

Je fais ça pour le plaisir, pour avoir du bon pain fait maison. J'ai fait mes premiers pas il y a quelques années quand j'ai discuté avec la boulangère d'une "boulangerie participative" (sic!). Comme je ne savais pas ce que ça voulait dire, elle m'avait expliqué que leur but était évidemment de vendre du pain mais aussi de partager la connaissance de la fabrication du pain au levain. Qu'à cela ne tienne, quelques semaines plus tard je m'étais retrouvé chez [**Les Champs du Pain**](https://www.champsdupain.fr/) à découvrir le monde du bon pain. Depuis, mon levain m'a suivi en voyage en Écosse et sur les véloroutes entre l'Allemagne et la France. Après l'arrivée à l'écolieu, j'ai reçu une piqûre de rappel par **Ben**, volontaire en chantier participatif et ancien boulanger conventionnel qui a repris cette activité au feu de bois et au levain dans le Nord de la France. C'est grâce à lui que j'ai acquis le coup de main actuel.

![](https://cloud.magnyethique.org/s/HqfrHyZH9eDm47P/preview)  

Fabrication
-----------

Je me mets à préparer ma pâte à pain dès potron-minet, autour de 06h15 du matin. Que faut-il pour un bon pain? De la bonne farine pour commencer. J'ai découvert en 2022 la farine de Rémi de la [**Ferme du Vernand**](https://www.vernand.net). Il a sélectionné deux variétés de froment -- Renan et Royal -- qui font mon bonheur. J'ai préparé mon levain la veille, et après quelques expériences, j'ai maintenant commencé à utiliser peu de levain pour permettre au pain de lever lentement, doucement, en dégageant les saveurs des céréales. Après quelques heures de levée entrecoupées de rabats de pâte, je commence à façonner les pains en fonction des commandes internes à l'écolieu. (C'est amusant de voir ce que l'on peut déduire de la commande de pain. Je sais en avance quand les parents d'un de nos co-habitants sont à l'ecolieu : en général, sa commande est triplée.) Deux heures de pointage et presque une heure de cuisson plus tard, je me retrouve avec le _bzzzzzzzz_ du début de ce récit et les pains chauds.

![](https://cloud.magnyethique.org/s/Lmy8tXDgyLiWAcs/preview)  

Discussion(s)
-------------

Nous le savons, faire son propre pain et mettre les mains à la pâte est un sujet en plein essor; et ce non seulement depuis les confinements. Les [intolérances au froment mais pas au petit-épeautre](https://www.uni-hohenheim.de/fileadmin/uni_hohenheim/Aktuelles/Uni-News/Pressemitteilungen/GrossesEinkornProjekt_engl.pdf), les problèmes de digestion après avoir mangé du pain industriel, entre autre, en résultent. Pourtant, les céréales (dont le pain) sont à la base de notre alimentation. Pour promouvoir le bien-fait du pain à fermentation lente (et au levain) ou pour assouvir la curiosité, mais aussi pour démontrer que le pain au levain **n'est pas** forcément lourd, dense et acide, j'accueille parfois des co-boulangers en devenir. En général, ce sont nos volontaires qui demandent à faire une session de boulange avec moi, et je leur partage le savoir qui m'avait été partagé, en espérant qu'ils répandent également l'amour pour le [`#RealBread`](https://www.sustainweb.org/realbread/).

![](https://cloud.magnyethique.org/s/XMYcASaW9SpWJWS/preview)  

Invitation
----------

Passe à l'écolieu, je serai ravi de trouver un moment pour discuter avec toi de la panification, mes sources d'inspiration comme [The Bread Code (en)](https://www.the-bread-code.io/), [Plötzblog de Lutz Geißler (de)](https://www.ploetzblog.de/), le groupe de recherche de l'[Universität Hohenheim (de)](https://weizen.uni-hohenheim.de/) autour du [Pr Longin (de)](https://weizen.uni-hohenheim.de/111445), le livre [Notre pain est politique](https://www.semencespaysannes.org/boutique/produits/notre-pain-est-politique.html), et tant d'autres.
