---
title: Stage et conférence-concert avec Marc Vella
subtitle: >-
  Stage "Rendre belles les fausses notes" de la vie samedi 18 et dimanche 19 mai
  2024, Concert-conférence vendredi 17 mai à 20h
date: '2023-11-03'
tags:
  - formation
  - rencontre
  - culture
---

Nous avons la joie d'accueillir pour la troisième fois le pianiste Marc Vella pour un week-end. Si vous ne le connaissez pas encore, vous pouvez aller le découvrir [sur son site](http://www.marcvella.com) ou nous faire confiance et vous inscrire les yeux fermés ! Nous apprécions énormément la façon qu'il a d'amener chaque personne, musicien·ne confirmé·e ou totalement débutant·e à prendre plaisir à improviser au clavier en seulement deux jours ; et ceci n'est qu'une petite partie du programme !
L'essentiel est la façon dont il nous réconcilie à nous-même, nous amène avec sensibilité,générosité et bienveillance à oser sortir de nos habitudes et de nos croyances pour exprimer et jouer - avec la musique mais aussi dans la vie ! Chaque édition de ce stage a été un moment rare et intense, et nul doute que celui-ci suivra la même voie : si ça vous chante, inscrivez-vous vite, ces stages se remplissent rapidement.

![](https://cloud.magnyethique.org/s/FSXSBDpKsLbX5wc/preview)


### Logistique 

Le stage est limité à une quinzaine de personnes. Il se déroulera de 9h30 à 18h samedi et dimanche. 

Nous proposons à celleux qui le désireraient la possibilité de dormir sur place en dortoir. Pour profiter au mieux du temps de stage, nous proposons des repas végétariens à tendance biolocale pour samedi et dimanche midi et vous demandons de prévoir un pique-nique pour celui du samedi soir (accès possible à un frigo et cuisine).
Il est possible d'arriver dès le vendredi soir (entre 19h et 22h) si vous le désirez ; dans ce cas, prévoyez votre repas si besoin (accès à une cuisine et frigo possible).

### Transport   

La gare la plus proche est Amplepuis, à une heure de train de Lyon. 
Nous vous demandons dans le formulaire d'inscription de renseigner votre ville de départ, pour vous proposer éventuellement du covoiturage.
Nous prévoyons une navette depuis la gare : une vendredi vers 19h45 (juste à temps pour voir le concert-conférence !) et une autre samedi matin vers 9h. Si vous désirez en profiter, précisez-le sur le formulaire d'inscription.

### Tarif

Le prix se décompose en plusieurs parties : 
* 250€ pour les frais pédagogiques,
* 22€ pour les repas du midi
* une contribution libre aux frais engendrés par l'accueil : utilisation des salles, logement et petit-déjeuner si vous dormez sur place, valorisation du temps passé à organiser ce stage (communication, inscriptions et liens avec les stagiaires, préparation et des espaces), soutien au projet MagnyÉthique, et encouragement à la participation libre et consciente.

### Inscriptions

Pour vous inscrire, renseignez [ce formulaire](https://framaforms.org/inscriptions-stage-marc-vella-2024-1699005197) et envoyez un chèque d'arrhes de 80€ (qui ne sera pas encaissé sauf en cas de désistemnt de votre part dans les quinze jours qui précèdent le stage) à l'ordre de "association Les MagnyÉthiques" à cette adresse :
MagnyÉthique - stage Marc Vella - 213 chemin du château de Magny - 69550 Cublize

Attention : pour nous simplifier la gestion, notez que nous ne confirmerons pas la réception des arrhes : si vous avez rempli le formulaire et posté votre chèque, considérez-vous comme inscrit·e et faites confiance à la Poste ! Si d'aventure le stage était presque complet et que nous n'aurions pas reçu un ou deux chèques, nous vous contacterons à ce moment-là. 

N'hésitez pas à nous contacter si vous avez des questions à l'adresse suivante : [2024_marcvella@magnyethique.org](mailto:2024_marcvella@magnyethique.org)
