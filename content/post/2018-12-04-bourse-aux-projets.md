---
title: Bourse aux projets
date: '2018-12-04'
tags:
  - échanges
---
![](/images/uploads/image19.jpg)

Nos deux représentantes, Fanny et Noémie, sont rentrées pleine d'énergie et de motivation partagées avec les quelques 200 participants à la Bourse aux Projets de l'Habitat participatif qui avait lieu à Villeurbanne début décembre.

Au programme:

*   Accueil
*   Présentation de la soirée et présentation des structures : Habitat & partage, Cohabtitude, Colibri Lyon, Cologi, Habicoop
*   Carte géographique de la région sur laquelle apparaissaient les près de 50 projets régionaux, puis discussion avec les groupes et les particuliers par zone géographique
*   rencontres projets/particuliers autour des stands
*   repas et échanges libres
*   Final : en cercle + pépites puis photo finish

Un grand merci pour toutes les personnes enjouées qui sont venues, intéressées par l'habitat participatif en général ou par le nôtre en particulier. De telles rencontres d'une telle ampleur font chaud au cœur!

Merci aussi à l'excellente organisation qui nous a aussi permis de découvrir d'autres projets avec lesquels nous avons pu et pourrons échanger à l'avenir, voire créer des partenariats!
