---
title: "Newsletter 22 - \U0001F342 Permaculture - \U0001F6E0️Travaux"
date: '2020-11-05T00:00:00.000Z'
tags:
  - newsletter
  - avancement
  - biodiversité
---

[](#)

**Bonjour ami·es MagnyÉthiques**

[](#)

 

  
L’automne s’installe et apporte à la terre l’eau qui lui a tellement manqué l’été dernier.

 

[](#)

**Coloration de plan(t)s**

[](#)

 

Du samedi 17 au lundi 19 octobre, l’écolieu a accueilli une nouvelle fois [PermaLab](https://permalab.fr) pour une session de formation à la permaculture niveau 2. Nous avons eu la joie sincère de retrouver quelques stagiaires des sessions de l’automne ou de l’été dernier pour partager avec eux les contenus autour du maraîchage, des systèmes arboricoles et des animaux auxiliaires. Que d’informations motivantes ! De quoi donner des ailes à nos projets pour le terrain… Quelques travaux pratiques ont d’ailleurs permis d’exercer les nouvelles connaissances acquises directement avec des mises en pratique sur plan et la fabrication d’une [LiFoFer](https://terre-humanisme.org/wp-content/uploads/2019/05/guide-lff-2018.pdf).  
  
Après une journée de pause, sept MagnyÉth’ ont poursuivi les travaux autour du design permacole, accompagnés des conseils pointus d’Aurélia et Simon (PermaLab). Ils se sont concentrés principalement sur les terrains qui accueilleront le jardin-maraîcher dès ce printemps. Le plan prend formes et couleurs. Et in situ, une partie du terrain est prête, semée d’engrais vert pour être enrichie et recevoir nos premières vraies plantations dès le printemps prochain ; une petite pépinière a vu le jour pour semer, repiquer et prendre soin des végétaux qui constitueront nos premières haies fruitières, mellifères et bocagères. Si d’ailleurs vous avez des végétaux à dédoubler et à nous offrir, nous vous en serions très reconnaissants ! Il nous reste en tout cas encore un certain nombre d’études à poursuivre avant d’aboutir au design final.

 

[](#)

![](/images/uploads/2020-11-05--newsletter22_img1.jpg)

[](#)

 

  
Car le premier enseignement de la permaculture pourrait être celle de ce proverbe persan : “**La patience est un arbre dont la racine est amère, et dont les fruits sont très doux.**” Même si d’aucun·e parmi nous aurait déjà souhaité passer à l’action et planter des arbres, mieux vaut attendre et prendre soin des visions du groupe et des terrains pour assurer une croissance rapide et sans accroc ! La sagesse est cependant parfois difficile à atteindre ! ;-)

 

[](#)

  
**Truelle, scie et rigolade**

[](#)

 

Dans la semaine du 24 au 30, nous avons tenu notre chantier participatif de Toussaint comme annoncé. Encore une super équipe de volontaires joyeux, motivés, compétents, autonomes, n’en jetez plus ! Au menu : isolation des sous-pentes à la laine de bois avec un pare-vapeur et finition au placo, jointoyage à la chaux des murs décroutés cet été, électricité… Le chantier avance et nos compétences grandissent, notamment grâce à Clément de [Batisseco](http://www.batisseco.fr) qui a animé les deux premiers jours et a lancé le chantier avec brio.  
Merci à Anna, Cédric, Quentin, François, Pierre, Loreena, Vincent, Louise, Amy, Philippe, Eulalie, Antoine, Damien, Hélène, Karine, Marion, et à tous les MagnyÉth’ !  
Merci aussi à Laurianne et Mathieu, Zoé, Romain, Baptiste, Éléanor et Hélène qui, la semaine précédente, ont contribué à rafraîchir l’ancienne partie “restaurant”.

 
{{< load-photoswipe >}}

{{< gallery >}} 
{{< figure link="/images/uploads/2020-11-05--newsletter22_img2" caption="Isolation des sous pentes : laine de bois" >}} 
{{< figure link="/images/uploads/2020-11-05--newsletter22_img3" caption="Isolation des sous pentes : pare-vapeur" >}} 
{{< figure link="/images/uploads/2020-11-05--newsletter22_img4" caption="Jointoyage à la chaux" >}} 
{{< figure link="/images/uploads/2020-11-05--newsletter22_img5" caption="Jointoyage à la chaux" >}} 
{{< /gallery >}}
 



**Save the dates!**

[](#)

 

Les dates à retenir :

*   Vue la situation actuelle, nous allons hiberner et concentrer nos énergies sur les travaux et la poursuite du design.

 

[](#)

 

Avec l’automne, un nouveau confinement débute. Essayez de prendre l’air, malgré tout, captez les doux rayons du soleil qui rechargent si bien les batteries et prenez soin de vous ! Nous pensons à celles et ceux qui n’ont pas notre chance d’avoir de grands espaces : bon courage.

 

[](#)

![](/images/uploads/2020-11-05--newsletter22_img6)
