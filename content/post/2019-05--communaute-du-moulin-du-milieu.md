---
title: Communauté du Moulin du Milieu
date: '2019-05-13'
tags:
  - habitat participatif
  - échanges
bigimg:
  - desc: null
    src: /images/uploads/1558854629466_IMGP6840_resized.JPG
---
Katia, Pascal et leurs enfants ont été accueillis pendant plusieurs jours par les habitants du "[Moulin du Milieu](http://mittlere-muehle-tengen.de)" à Tengen, en bordure de Forêt Noire. Ce projet d'habitat participatif s'est concrétisé il y a trois ans et met petit à petit en place des méthodes d'organisation interne et de gouvernance que nos voyageurs ont pris plaisir à découvrir pour en tirer des enseignements pour nous.

  

Les 8 foyers se partagent un ancien moulin et 10 hectares de terrain alentours sur lesquels ils ont commencé à installer des moutons et un beau jardin en agroécologie dans lequel Katia et Pascal ont passé beaucoup de temps à aider et approfondir leurs connaissances et pratiques sur le sujet.

  

L'aspect de partage communautaire les a interpelé et va enrichir nos réflexions pour l'organisation quotidienne qui va être pour nous un des sujets brûlants de l'été !
