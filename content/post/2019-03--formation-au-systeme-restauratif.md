---
title: Formation au système restauratif
subtitle: 22 mars 2019
date: '2019-03-08'
tags:
  - formation
  - gouvernance
---
Les membres de notre commission gouvernance ont participé le vendredi 22 mars à une journée de formation "**Intégrer des pratiques restauratives : créer son système restauratif**" à Villeurbanne. Ils ont reçu de nouvelles pistes pour le développement interne du nôtre en répondant à la question: " Comment voulons-nous vivre nos conflits ? ". 

Qu'est-ce que le système restauratif? Initié et développé par Dominic Barter dans les années 90 pour faire tomber les tensions dans les favelas brésiliennes, le système restauratif se clôt par les cercles restauratifs lorsque cela est nécesaire. Ces cercles offrent un temps et un espace dans lesquels les conflits peuvent être abordés humainement: exprimés, entendus, vécus pour pouvoir être traversés et permettre à tous d'en sortir grandis.

La journée a commencé avec une réflexion autour de ce qu'est le conflit avant de passer à ses propres habitudes face au conflit et d'explorer les issues possibles pour sortir de ses automatismes. L'après-midi a clôt la journée avec une simulation de cercle restauratif particulièrement éclairante et enrichissante. Les discussions ont été vives dans la voiture pour rejoindre le reste du groupe pour un week-end de travail et de cohésion!
