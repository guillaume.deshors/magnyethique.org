---
title: Randonnée céleste avec Christèle Mady
subtitle: Vendredi 21 octobre 2022 à 20h30
date: '2022-09-11'
tags:
  - culture
---
![](https://cloud.magnyethique.org/s/itCgF5dzjDfjmgJ/preview)

Venez vous promener dans les étoiles vendredi 21 octobre 2022 à partir de 20h30 ! Au programme :

*   Tour des constellations à l'œil nu, quelques merveilles aux jumelles et au télescope
*   Les planètes présentes : Mars, Jupiter et Saturne
*   Chevauchée céleste avec le cheval ailé Pégase
*   Chasse pacifique aux Ourses, Dragon et galaxies
*   Légendes du ciel (mythologie)

À la fin de la soirée, vous pourrez reconnaitre les principales constellations d’automne, vous en saurez plus sur les étoiles et les planètes, vous aurez vu Jupiter et ses satellites, Saturne et ses anneaux ; vous aurez vu quelques objets du ciel profond, vous toucherez des yeux la grandeur de l’Univers !

Prévoir :
=========

Des habits chauds : pantalon chaud, anorak, bonnet, gants, tapis de sol ou sac de couchage, un transat si vous pouvez en amener un facilement. Eventuellement : une lampe frontale rouge, des jumelles, boisson chaude.

Et s'il pleut ?
===============

Si la météo n'est pas favorable, nous maintenons la soirée : Christèle a plus d'un ciel dans son sac et nous proposera des surprises astronomiques "indoor" !

Tarif :
=======

Prix libre et conscient

Inscriptions :
==============

Merci de nous prévenir de votre venue par mail à [2022etoiles@magnyethique.org](mailto:2022etoiles@magnyethique.org)

Enfants bienvenu·e·s !
