---
title: Arthropologia
subtitle: Préserver et favoriser la biodiversité
date: '2022-12-07'
tags:
  - biodiversité
  - permaculture
---
La conférence d’Hugues Mouret, directeur scientifique d’[Arthropologia](https://www.arthropologia.org/#), le 2 décembre 2022, a réuni une trentaine de personnes autour de la thématique de la biodiversité. L’association nous a mis à disposition [le](https://www.dropbox.com/s/74d7p9dt1ldtomb/Pollinisateurs_agir_cublize021222.pdf?dl=0) [diaporama de la conférence](https://www.dropbox.com/s/74d7p9dt1ldtomb/Pollinisateurs_agir_cublize021222.pdf?dl=0) – même si les explications d’Hugues Mouret valent leur pesant d’or et que le seul diaporama ne les remplacera pas.

![](https://cloud.magnyethique.org/s/gjpncWfjrq7iG5e/preview)

Voici également quelques explications complémentaires transmises par Hugues Mouret :

« Pour conserver (ou rétablir) une prairie riche en fleurs, qui servira à la fois de gîte et de couvert à tout un tas d’animaux, notamment les insectes : faucher plutôt que tondre, en décalé, avec exportation » :

*   « _**Faucher**_ » : Le fauchage couche simplement l’herbe dans toute sa longueur alors que la tonte broie tout et tue les animaux qui s'y trouvent. Le fauchage leur laisse la possibilité de fuire une fois au sol sans être détruits par la machine.
    
*   « _**en décalé**_ » : Le fait de ne pas tout faucher d’un coup mais par bandes successives demande certes plusieurs interventions, mais les bénéfices pour la biodiversité animale et végétale ne sont pas à négliger. La fauche successive permet l’étalement de la maturation de diverses plantes dans le temps (toutes ne sont pas précoces !) et permet ainsi que des végétaux plus tardifs puissent arriver à la fructification (graines), seul moyen de renouvellement de cette variété pour l’année suivante. Et comme certaines espèces d’insectes ne dépendent que de certaines variétés de végétaux, si on empêche la floraison de certains d’entre eux, on empêche également la présence des animaux associés.
    
*   « _**exportation**_ » : On ne laisse pas la matière se décomposer sur place et enrichir le sol, car les végétaux qui composent une prairie riche en biodiversité aiment les sols plus pauvres (Pensez aux prairies si belles de montagne !). Se rappeler que dans la nature, la matière prélévée l’est par l’alimentation de gros mammifères, ils mangent en emportant la matière avec eux et ne restent pas sur place à découvert.Il est donc également important pour conserver la diversité végétale des prairies de ne pas épendre sur toutes les prairies. Ce procédé favorise la quantité de fourage mais en diminue la qualité (la diversité) par un apport d’azote qui nuit à certaines plantes. Un bon complément à cette information par la [conférence de Gérard Ducerf](https://www.youtube.com/watch?v=P-1ltTj9xBk&feature=emb_imp_woyt) à laquelle plusieurs membres du groupe ont assisté en septembre dernier.
    

![](https://cloud.magnyethique.org/s/N2DSHw9a7T2iTgi/preview)

Voici le lien vers les **[Diagnostics pollinisateurs](ttps://www.arthropologia.org/expertise/pollinisateurs/diag_pollinisateurs#)** (pour le retrouver en numérique ou le faire tourner). Ce petit livret conçu par Arthropologia permet de voir ce que vous avez sur votre terrain et ce que vous pouvez y installer afin de préserver ou de favoriser l'installation des pollinisateurs.

Enfin, voici d'autres liens qui pourraient vous intéresser. Vous y retrouverez pas mal de points abordés lors de la soirée :

\- Série vidéo "[du vivant dans les champs](https://www.youtube.com/playlist?list=PLGSDRZ91g5gmRAnvGW8QLURg_QUI8sJLT)"

Il y a 5 épisodes de 5 à 10 miuntes, avec des témoignages de paysans, de scientifiques et de naturalistes :

- La mosaïque d’habitats,
- Les prédateurs et parasitoïdes
- Les recycleurs,
- Les pollinisateurs,
- Accueillir les auxiliaires
    

En 2021, nous avons fait une petite série avec la [Forêt Gourmande en Bourgogne](https://www.youtube.com/watch?v=qwWDCMnAuhU) : 3 épisodes de 15 min.

![](https://cloud.magnyethique.org/s/acfn67yq2FKZKAr/preview)  

Nous réinviterons avec grand plaisir Hugues Mouret à l’écolieu pour approfondir le sujet de la préservation de la biodiversité, avec une nouvelle conférence ou une sortie terrain. Si vous souhaitez soutenir l’association Arthropologia pour lui permettre de poursuivre ses actions pédagogiques ou de soutien scientifique pour freiner certains projets potentiellement nuisibles à la biodiversité, n’hésitez pas à leur [faire un don](https://www.helloasso.com/associations/arthropologia/adhesions/adhesion-arthropologia-2-2), mais aussi à partager leurs informations !
