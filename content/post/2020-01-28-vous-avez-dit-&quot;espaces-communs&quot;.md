---
title: Vous avez dit "espaces communs?"
subtitle: Colloque
date: '2019-11-24'
tags:
  - échanges
  - conférence
  - société
  - habitat-participatif
bigimg:
  - desc: null
    src: /images/uploads/1580226814433_colloque.jpg
---
Charlotte et Katia se sont rendues à St Etienne le vendredi 22 novembre 2019 pour y participer au colloque intitulé "Vous avez dit "espaces communs"?", organisé par l'École Nationale d'Architecture de St Étienne.

  

Ce fut l'occasion de prendre connaissance de nombreux projets mis en oeuvre grâce à la mise en commun de matériels, espaces et/ou forces vives, et de présenter le projet MagnyÉthique sur les plans de la gouvernance partagée et du tiers-lieu.

  

Une publication suivra, qui pourra être consultée sur la [Médiathèque de l'ENSASE](https://media.st-etienne.archi.fr/vous-avez-dit-espace-commun/), puis au printemps sur papier.
