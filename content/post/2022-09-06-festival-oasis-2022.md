---
title: Festival Oasis 2022
subtitle: Du jeudi 22 au dimanche 25 septembre 2022
date: '2022-09-06'
tags:
  - gouvernance
  - société
  - habitat-participatif
  - échanges
---
Caroline, Bastien et Bruno se sont rendus fin septembre au [Festival Oasis 2022](https://cooperative-oasis.org/vivre/les-evenements/) où ils ont beaucoup échangé autour de diverses pratiques dont certaines pourront peut-être être testées au sein de MagnyÉthique. Ces rencontres ont également permis de faire du lien avec d'autres personnes engagées dans des projets similaires au nôtre.

Voici un r[etour global sur ces rencontres](http://r.news.cooperative-oasis.org/mk/mr/xwneyRzT9CONIxKWx86mnJPJn-XC-WjRaTyT-WO_VcfAvbSN9Kj4P1ow8CRLZ59s7EsvP6eR7QQ-naIwfEGQIU5ct9V9s0KrOfQgFHYKye2pJTYGJ4Pltra5RiUAMU1YxD65Mk7I-B_QuIc), publié par la [Coopérative Oasis](https://cooperative-oasis.org).
