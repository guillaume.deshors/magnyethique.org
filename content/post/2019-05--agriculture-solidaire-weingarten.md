---
title: Agriculture solidaire Weingarten
date: '2019-04-12'
tags:
  - agriculture solidaire
  - agriculture biologique
  - wwoofing
bigimg:
  - desc: null
    src: /images/uploads/1558854129101_IMGP5429_resized.JPG
---
Katia, Pascal et leurs enfants ont passé une petite semaine comme volontaires pour aider le salarié employé par une association d'agriculture solidaire allemande (équivalent AMAP), à [Weingarten](https://www.gutesgemuese.de) près de Karlsruhe.  Des légumes sont produits pour les paniers hebdomadaires de 70 foyers. L'occasion pour eux de continuer à se former aux méthodes d'agriculture biologique et d'échanger avec les membres de l'association sur des thèmes de gouvernance et de médiation en cas de conflits puisqu'ils ont opté pour une gouvernance horizontale et se font accompagner en Communication Non Violente pour régler les conflits inhérents à tout projet de groupe.
