---
title: La Big Conf'
subtitle: Le GIEC par Avenir Climatique
date: '2023-03-06'
tags:
  - échanges
  - conférence
  - société
  - écologie
---
Samedi 4 mars 2023, nous avons eu le plaisir de servir chez nous de cobayes pour la "Big conf'" de trois jeunes ambassadrices de l'association [](https://avenirclimatique.org/)[Avenir Climatique](https://avenirclimatique.org) encore en formation dans l'[ACadémie d'Avenir Climatique](https://avenirclimatique.org/lacademy/).

![](https://cloud.magnyethique.org/s/pJNtdNNWLa2WNd6/preview)

Ainsi, nous avons eu droit à une bonne heure de conférence, menée conjointement par Julie, Elisabeth et Raphaëlle. Cette intervention permet de faire comprendre les liens entre énergies et climat, mis en évidence par les scientifiques du GIEC (Groupe d'Experts intergouvernemental sur l'évolution du climat). A partir des données du dernier rapport publié en français il y a presque un an, Avenir Climatique a élaboré une présentation en trois temps qui permet de présenter les résultats du GIEC, de les rendre tangibles pour tou.te.s, et d'aborder des pistes de solutions, préconisées également dans le rapport scientifique. Leur intervention, bien qu'en terrain conquis, a toutefois soulevé questions et remarques qui leur auront peut-être permis de faire leur baptême du feu et d'être prêtes à mener leur [Big Conf'](https://avenirclimatique.org/the-big-conf-2/) ailleurs, pour sensibiliser le plus grand nombre.

![](https://cloud.magnyethique.org/apps/files_sharing/publicpreview/q4HXtcZLTA45BTL?file=/2023/GIEC-2.png&fileId=25991&x=300&y=300&a=true)
![](https://cloud.magnyethique.org/apps/files_sharing/publicpreview/q4HXtcZLTA45BTL?file=/2023/Logo-AC-FB.png&fileId=25990&x=300&y=300&a=true)

Nous serons ravi.e.s de les recevoir à nouveau mais cette fois, nous le ferons savoir à l'extérieur et ouvrirons nos portes à qui voudra mieux comprendre des liens complexes et l'urgence à agir !
