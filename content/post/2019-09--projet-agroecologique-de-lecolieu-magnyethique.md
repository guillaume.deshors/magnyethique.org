---
title: Projet agroécologique de l'écolieu MagnyÉthique
date: '2019-09-20'
tags:
  - avancement
  - formation
  - permaculture
  - écologie
  - agriculture-biologique
  - société
  - générosité
subtitle: 'Nous lui avons donné des racines, offrez-lui des ailes !'
---
La commission agricole a élaboré durant tout l'été un projet agroécologique que le groupe a maintenant validé :

![](/images/uploads/1569227514456_Projet_permaculture-exterieurs_FLYERS_03.jpg)

Et déjà, nous y sommes !

*   Samedi prochain débute la première [formation permaculture](/post/2019-06-formation-permaculture-a-magny/) à l'écolieu
    
*   Vendredi dernier, nous avons lancé notre toute première [campagne de financement participatif](http://bluebees.magnyethique.org) dédiée à soutenir ce projet agroécologique
    

À présent, c'est entre vos mains que nous plaçons une partie de ce succès. Vous pouvez :

*   Nous aider financièrement en échange d'une contrepartie concoctée par un.e membre du groupe
    
*   Partager au maximum notre campagne à toutes celles et tous ceux qui seraient susceptibles de le faire
    

Merci d'avance ! Et au plaisir de vous recevoir sur nos 8 hectares d'espace protégé.
