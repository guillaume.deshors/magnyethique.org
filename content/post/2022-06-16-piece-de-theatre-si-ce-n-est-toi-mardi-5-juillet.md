---
title: Pièce de théâtre Si ce n'est toi - Mardi 5 juillet
subtitle: Représentation ouverte au public
date: '2022-06-16'
tags:
  - culture
  - théâtre
---
2077, en Europe. On a réaménagé les quartiers, supprimé les objets inutiles, effacé les singularités, et surtout aboli le passé. La mémoire, les souvenirs et l'art forment un ensemble de ruines dans lesquelles il n'est pas bon d'aller. Jams et Sara vivent en « couple » dans une maison où le mobilier est réduit au strict nécessaire : une table, deux chaises. Jams travaille comme flic dans cet univers aseptisé où les reliquats d'humanité sont traqués violemment. Sara, elle, garde les murs de la maison. Le monde a sa routine : des poussées massives de suicides ici et là, rien de bien alarmant. Si ce n'est des coups régulièrement frappés à la porte derrière laquelle il n'y a personne, et cette vieille qui se trimballe avec un tableau dans les ruines, et puis ce type qui se présente un soir comme étant le frère de Sara et ne veut plus repartir.

![](/images/uploads/1655733468296_Affiche-Si-ce-nest-toi.jpg)  

Placé en observateur, le spectateur peut faire l'expérience concrète et vivante de ce qui se passe quand on met une goutte d'eau dans de l'huile trop chaude, quand la barbarie technocratique échoue à anéantir la vie. Dans ce spectacle il est question d'inversions en permanence : inversions de tons, inversions morales, inversions métaphysiques. Ainsi, Si ce n'est toi est un spectacle au réalisme grotesque où les objets sont des personnes, et les personnes des objets, où les morts sont en réalité vivants, la maison et le corps siège de la société et les ruines le ferment de l'humanité. C'est un spectacle qui fait froid dans le dos et chaud au coeur. Car les errants et les souvenirs finissent toujours par frapper à la porte, et nous rappelle quelle petite musique c'est, de vivre.
