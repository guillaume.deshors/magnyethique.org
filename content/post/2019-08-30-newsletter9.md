---
title: Newsletter 9
date: '2019-08-30'
tags:
  - newsletter
  - avancement
---

Bonjour Ami.es MagnyÉthique 


Les vacances estivales touchent à leur fin, et elles ont été pleines de vie et d’événements ce dernier mois encore. Laissez-nous vous en offrir un aperçu.


![](/images/newsletters/9/demenagement.jpg)

       
### Bienvenue chez nous!
Après l’emménagement d’Yves, de Katia, Pascal et leurs enfants, de Noémie et de son fils en juin et juillet, le château a encore eu la joie d’accueillir les familles de Fanny et Guillaume puis de Bastien et Caroline ce mois-ci. Après l’organisation des emménagements dans les logements provisoires et du stockage des affaires mises de côté pour les appartements définitifs l’an prochain, nous plongeons dans celle du quotidien. Les enfants sont enchantés d’avoir des copines et copains à chaque fois qu’ils le souhaitent et profitent de ce temps passé ensemble pour s’en donner à coeur joie, faire quelques sottises et apprendre eux aussi la vie en communauté. Lundi prochain, ils seront huit à faire leur rentrée à l’école publique des Prés Verts à Cublize. Ils ont hâte d’élargir leur cercle d’amis à d’autres camarades du village et les parents du groupe s’organisent pour les navettes écolieu-village chaque matin et chaque soir : van, vélo-bus, navette avec un accompagnateur ? C’est le moment de finaliser tout cela pour être fins prêts dans une semaine. Nous pourrons vous raconter le mois prochain quelles options ont été retenues et/ou testées !

### Maçonnerie, tuyauterie, jardinage et… goûter!
Après les 12 volontaires de juillet, les chantiers d'août nous ont permis d’en accueillir 10 autres, sans compter les volontaires du groupe de lyonnais.es Éclat de voix déjà venus nous prêter main forte le week-end du 14 juillet et qui reviennent le week-end prochain en force ! De nombreuses tâches ont pu être bouclées ou au moins démarrées, telles que régler les fuites de plomberie ou sur les toits, refaire la maçonnerie d’un mur à la chaux, réparer la cabane-bâteau de pirate des enfants, préparer le jardin à accueillir les légumes d’automne et d’hiver, poursuivre la taille des thuyas, fabriquer des tringles pour rideaux de douche dans l’hôtel, faire divers travaux électriques pour avoir enfin de l’eau chaude dans la cuisine commune et se mettre en sécurité dans l’atelier, y voir clair dans le cellier, installer la future cuisine pro ainsi que l’atelier bois et outillage, aménager l’atelier couture et bricolage d’intérieur ainsi que le futur bureau partagé en tirant des fils pour internet, installer une cuisine dans la salle de bains (!) de Caroline et Bastien, sans oublier de faire quelques conserves de bons légumes d’été et des confitures pour le groupe et pour le financement participatif bluebees (voir ci-dessous). Bref : que d’actions réalisées grâce aux aides précieuses de nombreux volontaires, d’amis et de membres des diverses familles venus en renfort ! Sans vous tous, tout cela n’aurait pas été possible alors MERCI !


![](/images/newsletters/9/plomberie.jpg)


### “Saurez-vous résister aux champs MagnyÉthique ?”
La commission agricole a bossé dur sur le projet de permaculture de l’écolieu durant l’été et le groupe est sur le point de signer une convention avec le Permalab pour un accompagnement au design et à l’installation sur les terrains. Plusieurs agronomes sont venus début août pour apporter leurs savoirs dans ce but. Nous recherchons à présent de l’aide pour le financement de ce projet de longue haleine. C’est ainsi que la commission communication a aidé l’agricole pour élaborer ensemble un dossier déposé sur la plateforme de financement participatif Bluebees. Notre campagne est intitulée “Saurez-vous résister aux champs MagnyÉthique ?”. Nous serons reconnaissants à tous les donateurs (qui recevront tous une contrepartie), mais aussi à tous ceux qui voudront bien nous soutenir en relayant cette campagne à leurs proches ou sur les réseaux sociaux. Car le bouche à oreille reste ce qu’il y a de plus efficace à l’heure actuelle pour défendre et soutenir des projets comme le nôtre ! Alors à vous de jouer !

### Grande première dans la salle polyvalente
Vendredi et samedi dernier, une troupe d’acteurs amateurs a brûlé les planches de notre salle polyvalente en jouant deux pièces pour les volontaires qui avaient pu refaire le voyage jusqu’à nous ainsi que pour les adhérents à l’association MagnyÉthique. Une cinquantaine de personne en tout s’est déplacée et nous sommes ravis de cet engouement malgré une visibilité de l’événement moindre (nous ferons mieux l’an prochain !) Petits et grands ont été émus, ont ri et se sont rencontrés autour d’une pièce classique de Molière (bien que très peu connue) : George Dandin et d’un drame contemporain La compagnie sans commandeur d’Alberto Lombardo. Nous avons hâte de pouvoir proposer d’autres événements culturels pour contribuer à notre façon à contrer les a priori selon lesquels il n’y a d’offre qu’à la ville ! 




{{< load-photoswipe >}}

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/newsletters/9/theatre3.jpg" caption="La scène (George Dandin)" >}}
{{< figure link="/images/newsletters/9/theatre1.jpg" caption="Compagnie sans commandeur" >}}
{{< figure link="/images/newsletters/9/theatre2.jpg" caption="Une trentaine de personnes chaque soir dans le public" >}}

{{< /gallery >}}




### Fête des possibles 
Nos prochaines journées portes ouvertes du 15 septembre à 10h30 seront placées sous le signe de la Fête des Possibles ! Au programme :
- présentation du projet
- visite guidée des lieux
- pique-nique partagé (ouvert à tou.tes sans inscription)
De plus, la semaine suivante, le Quartier Métisseur nous fait l’honneur de nous proposer de tenir un stand lors de leur rendez-vous annuel dans le cadre de la Fête des Possibles : le Petit Bal des Possibles. 
Vous pourrez donc venir nous rencontrer chez eux, à Lamure-sur-Azergues, toute la journée du samedi 21 septembre, et profiter aussi des nombreuses conférences et activités organisées ce jour-là, mais aussi le vendredi et le dimanche ! 

### Dons de matériel
Si vous êtes encombrés par des objets qui ne vous servent plus, n’hésitez pas à consulter la [petite liste](/page/dons) non exhaustive d’objets qui nous seraient utiles ou à nous contacter pour nous faire des propositions !


### Save the dates !
Les dates à retenir :

 * dimanche 15 septembre à partir de 10h30 : Journée portes-ouvertes + repas partagé
 * samedi 21 septembre : Stand au Petit Bal des Possibles du Quartier Métisseur
 * du 28 septembre au 4 octobre : Formation permaculture
 * samedi 26 octobre à partir de 14h30 : Journée portes-ouvertes + goûter partagé

Bonne rentrée à tou.tes !

MagnyÉthique