---
title: Formation sur la pédagogie de coopération
sutitle: Semaine du 21 au 25 août 2017
tags:
  - formation
  - gouvernance
  - écologie
  - permaculture
date: '2017-08-25'
---
![](/images/uploads/image5.jpg)

Semaine du 21 au 25 août 2017.

\[Formation\]

Katia a participé à la formation d'Isabelle Peloux sur la pédagogie de coopération, au centre agroécologique des [Amanins](http://www.lesamanins.com/ "http://www.lesamanins.com/").

Des enseignements précieux autour du vivre ensemble, en milieu scolaire comme en organisation coopérative. Au programme, jeux coopératifs à objectifs divers, études de dynamiques de groupe et beaucoup de pédagogie individualisée et bienveillante, visite de l'École du Colibri et des jardins en permaculture du centre agroécologique.

Le lieu, initié par Pierre Rabhi et Michel Valentin, est également source d'inspiration dans son fonctionnement interne en gouvernance partagée et son rapport à son environnement, notamment grâce à la recherche d'autonomie alimentaire et énergétique.
