---
title: Stage Danse-contact-impro // REPORTÉ
subtitle: Du 26 au 29 mai 2023 avec Patricia Kuypers et Franck Beaubois
date: '2023-01-18'
tags:
  - stage
  - culture
  - danse
---

![](https://cloud.magnyethique.org/s/jiNN7Y23LiGkfQF/preview)


"Paysage de sensations"
=================================
### Stage résidentiel de contact-improvisation, facilité par Patricia Kuypers et Franck Beaubois ###

Dans cette forme de danse, l'autre est l'environnement mouvant avec lequel nous interagissons. Le toucher, l'écoute, le jeu avec la chute, le poids, la gravité emmène le solo, duo, trio ou autre combinaison vers une découverte des possibles de l'accordage, pour que s'invente sur le champ une danse ensemble.

Ainsi, l’exploration détaillée de la tactilité donne l’occasion d’intégrer la perception du partenaire et de nous-même dans une image élargie de l’environnement en mouvement. 
Diverses partitions nous invitent à redécouvrir et à puiser dans notre palette sensorielle pour, peut-être, percevoir autrement. Notre pratique se tiendra entre le jeu / l’engagement et la contemplation. La dimension collaborative du Contact Improvisation sera également au centre des scores.

Avec douceur - la dimension «  soft  » - nous éveillerons ensemble cette qualité de présence physique qui permet d’accueillir les invitations de la gravité. Nous traverserons des chemins d'attention propre au contact: support, flux du poids, désorientation, élan, pivot du centre en ayant recours à tout l’éventail tonique.

Enfin, échanger, et parfois rendre explicite quelque chose de l’expérience dansée, sera invité comme une autre dimension pour intégrer ce que nous découvrons.



* * *

Biographie :
-----------

Patricia Kuypers a découvert le Contact Improvisation lors de sa rencontre avec Steve Paxton, Nancy Stark-Smith, Lisa Nelson, Mark Tompkins et autres pionniers de cette forme de danse née aux Etats-Unis dans les années 70. Sa démarche artistique, nourrie de cette approche, a creusé des processus d'improvisation en relation avec des musiciens, plasticiens, ainsi que l'interaction avec la vidéo temps réel dans sa collaboration avec Franck Beaubois. Elle poursuit également une recherche sur l'improvisation qui s'actualise lors de performances, conférences dansées, et publications d'écrits, e.a. dans la revue Nouvelles de danse qu'elle a fondé en Belgique et à laquelle elle collabore ponctuellement. 
Franck Beaubois danseur, pratique le CI depuis plus de 25 ans, il a guidé/partagé des pratiques d’improvisation en danse en France et à l’étranger, dans des contextes aussi divers que les compagnies de danse, associations, universités, Cefedem, conservatoires, festivals, lycées, théâtres, musée, des hôpitaux, et un IME à destination des personnes autistes. Il développe une pratique hybride entre danse et vidéo temps réel, qui s'actualise dans des pièces comme « Delay versus duo » dont une version trio avec Lê Quan Ninh, des projets in situ « Panoramique », et récemment, la création « Entre bruits » ; pièces qui ont tourné régulièrement en Europe.

Logistique :
------------

Ce stage aura lieu à [MagnyÉthique](www.magnyethique.org) dans notre grande salle de 100 m² sur parquet, et il est proposé en résidentiel : logement sur place en camping, véhicule aménagé ou dortoir de six à huit lits. Les repas végétariens sont prévus du samedi midi au lundi midi. Pour le vendredi soir, nous vous proposons de venir avec quelque chose que vous avez envie de partager.
Le stage comprendra 18h d'atelier, et des jams en soirée selon l'énergie et l'envie des participant.e.s. 
L'accueil se fera à partir de 16h30 vendredi. Les ateliers sont prévus vendredi 18h30-20h30, samedi et dimanche 10h-13h et 15h30-18h30, et lundi 10h-14h. 
Le nombre de places est limité à 16.
Les chiens ne sont pas les bienvenus : si cela vous pose problème, il est impératif de nous contacter pour en discuter avant votre venue.

Tarif :
-------

Ce stage est proposé en participation financière libre et consciente. Celle-ci couvre les coûts pédagogiques (18h avec deux intervenant.e.s), le transport des intervenant.e.s, les repas et petits-déjeuner, la mise à disposition des espace de danse et de vie, les fluides, l'organisation du stage, le soutien au projet MagnyÉthique, et votre encouragement à la participation libre et consciente. Vous pouvez vous faire une idée approximative des dépenses engagées [ici](https://cloud.magnyethique.org/s/xNrXbdb7mM4cxKc/preview), et nous prendrons un temps durant le stage pour expliquer cette démarche.

Inscription :
--------------

Les inscriptions seront effectives après avoir rempli [ce formulaire](https://framaforms.org/inscription-stage-avec-franck-beaubois-et-patricia-kuypers-1677759122) et avoir envoyé un chèque d'arrhes d'environ un tiers de votre participation envisagée, à envoyer à :  

MagnyÉthique - stage contact-impro
213 chemin du château de Magny  
69550 Cublize

Ces arrhes servant à confirmer votre engagement et sécuriser l'effectif du stage. Il ne sera encaissé qu'en cas de désistement de votre part, sauf cas de force majeure.

Pour tout renseignement, contactez-nous à 
[2023contactimpro@magnyethique.org](mailto:2023contactimpro@magnyethique.org)
