---
title: Formation en permaculture
subtitle: Samedi 27 et dimanche 28 janvier 2018
tags:
  - formation
  - permaculture
date: '2018-01-28'
---
![](/images/uploads/image10.jpg)

Comme le nom de la formation l'indique, son contenu était vraiment basé sur les fondamentaux de la permaculture. Elle était donnée sur Paris par Eric Escoffier. Les participants étaient nombreux, signe que les choses bougent!

Fanny s'y est rendue. Les deux jours lui ont permis de se faire une idée des étapes de planification d'un projet en permaculture. Elle est également revenue avec de nombreuses références de livres et vidéos pour creuser le sujet.
