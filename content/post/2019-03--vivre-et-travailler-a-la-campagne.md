---
title: Vivre et travailler à la campagne
subtitle: Session d'information le 20 mars 2019
date: '2019-03-08'
tags:
  - réseau-local
  - société
  - échanges
---
Mardi 20 mars, plusieurs d'entre nous se sont rendus à une session d'information sur la thématique de la vie (professionnelle) à la campagne et notamment, dans le Beaujolais Vert. Ce fut une fois de plus l'occasion de rencontrer et de nous faire connaître des acteurs locaux et de trouver des pistes pour monter les différents projets professionnels, qu'ils soient collectifs ou individuels.

Les discussions avec les différentes personnes présentes ont été riches d'enseignements. Un grand merci aux organisateurs!

![](/images/uploads/1552036524771_Vivre&#32;et&#32;travailler&#32;à&#32;la&#32;campagne.jpg)
