---
title: Avancement du projet - fin juillet 2019
date: '2019-07-28'
tags:
  - avancement
---
Deux belles journées et soirées passées au château, tous ensemble, pour un week-end de cohésion et de travail.

Le samedi était sous le signe de l'ouverture, de la perception de soi et des autres au sein du groupe. Puis un jeu collectif en équipes avec les enfants avant de se laisser un peu aller en soirée autour d'une table et de jeux de société.

Dimanche était consacré à une plénière lors de laquelle nous avons pu aborder le sujet des "projets autour du projet", idées des uns ou des autres qui fédèrent plusieurs personnes et pourront aboutir à des activités pros ou amateurs. Nous avons enfin réfléchi à la place des enfants que nous avons intégrés à la réunion pour définir les règles incontournables de notre vie de groupe et auxquels nous proposerons dès la rentrée un conseil des enfants afin qu'eux aussi soient force de proposition.

Les commissions bossent encore activement:

\- Les _finances et statuts_ développent le pacte d'associés avec les plans de financement de chaque foyer et du groupe dans son ensemble.

\- La _gouvernance_ a élaboré une ébauche de système restauratif soumis aux remarques du groupe avant de pouvoir être détaillé dans divers documents supports et de pouvoir entrer en vigueur à la rentrée.

\- La commission _activités_ répond à différents contacts de professionnels souhaitant pouvoir établir une activité chez nous, organise les premiers événements qui vont avoir lieu sur place (théâtre, formation permaculture…) et essaie de trouver quel(s) modèle(s) pourrai(en)t fédérer tous les projets pros et/ou associatifs qui se tiendront sur place à l'avenir.

\- Les _aménagements_ travaillent dur également entre réunion avec l'architecte, visite du thermicien et prise de contact avec divers pro pour préciser les gros chantiers et les mettre en branle.

\- La commission _agricole_ qui a vu le jour en juin réfléchit aux aménagements extérieurs, à la question des animaux et prend les contacts pour les études de terrain nécessaires à la mise en place de la permaculture dans les mois qui viennent sur l'ensemble du domaine

\- _L'asso_ tente d'effectuer les changements en préfecture relatifs au déménagement et coordonne la logistique des chantiers participatifs.

\- La _commission communication_ gère quant à elle tous les contacts pour les chantiers participatifs, organise les futures journées portes-ouvertes ou présentations extérieures, ainsi que les diverses pages qui nous donnent de la visibilité.

Bref : beaucoup de boulot à Magny mais aussi de bons moments partagés et la satisfaction de voir notre projet devenir jour après jour réalité!
