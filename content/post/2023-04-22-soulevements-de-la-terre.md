---
title: Nous sommes les Soulèvements de la Terre
subtitle: On ne dissout pas un mouvement
date: '2023-04-22'
tags:
  - société
  - écologie
  - réseau-local
---

Alors que le ministre de l’Intérieur Gérald Darmanin a fait savoir qu’il engageait une procédure de dissolution du mouvement les Soulèvements de la Terre, des centaines d’intellectuels, de chercheurs et d’artistes manifestent leur indéfectible soutien au collectif et à ses luttes dans [une tribune](https://lessoulevementsdelaterre.org/blog/la-terre-le-feu-leau-et-les-vents-se-soulevent) parue initialement sur le site du nouvel obs.

Indigné par l'accaparement des biens vitaux et communs au profit d'une minorité, indigné par la répression policière et la violence illégitime, inquiet des dérives autoritaires du gouvernement, MagnyEthique s'associe à cette tribune et devient un [relais local](https://lessoulevementsdelaterre.org/blog/la-carte-des-points-relais-des-soulevements-de-la-terre) permettant de se renseigner sur les activités des Soulèvements de la Terre.


{{< citation  >}}

Nous sommes "la terre le feu l'eau et les vents", nous dit Édouard Glissant, poète et philosophe du "Tout monde".

Ces méga-éléments vitaux ne peuvent pas être dissous, matraqués, désintégrés. Ou alors nous mourons.

C'est pourtant la voie qu'a choisie notre gouvernement : pour défendre le modèle agro-industriel de captation/pollution des terres, de l'eau et de l'air, l’État est prêt à tuer, mutiler, estropier, autrement dit à employer ouvertement des techniques de guerre sur des civil.e.s.  Le projet de dissolution du mouvement des soulèvements de la terre relève de ce même registre de répression.

Tout cela n'est pas un banal maintien de l'ordre mais participe d'une stratégie claire de criminalisation des luttes écologiques pour faire perdurer un développement économique insoutenable à tous niveaux.

Au moment où le bouleversement des régimes climatiques de la Terre engage les communautés humaines à rediriger de manière pressante l'ensemble de leurs activités, nous ne pouvons plus laisser les entreprises multinationales et les gouvernements continuer à détruire les milieux qui nous nourrissent, dans lesquels nous respirons, nous travaillons, nous nous promenons, nous pensons, nous nous engageons, nous nous aimons. 

Nous ne pouvons pas non plus laisser les entreprises multinationales et les gouvernements combattre toutes celles et ceux qui dédient leur existence à prendre soin de ces milieux de vie.

Nous soutenons de corps et d'esprit les soulèvements de la terre. Nous continuerons à relayer leurs actions, à y participer. Nous en sommes. De la terre, du feu, de l'eau et des vents.

Les soulèvements de la terre sont inarrêtables. Vous le savez et nous le savons.

Face au changement climatique, à l’usage disproportionné de la force policière et du droit, ce ne sont plus les informations qui manquent mais le courage de les comprendre et d’agir en conséquence.
{{< /citation  >}}


Les premiers signataires :

Geneviève Azam, économiste&nbsp;; Jérôme Baschet, historien&nbsp;; Christophe Bonneuil, historien&nbsp;; Isabelle Cambourakis, éditrice&nbsp;; Yves Cochet, ancien ministre de l’Environnement&nbsp;; Philippe Descola, anthropologue&nbsp;; Emmanuel Favre, directeur de festival&nbsp;; Isabelle Fremeaux, autrice activiste&nbsp;; Sylvie Glissant, artiste&nbsp;; Barbara Glowczewski, anthropologue&nbsp;; Emilie Hache, philosophe&nbsp;; Malcom Ferdinand, chercheur au CNRS&nbsp;; François Jarrige, historien&nbsp;; Christophe Laurens, architecte, paysagiste&nbsp;; Baptiste Lanaspeze, éditeur&nbsp;; Sophie Gosselin, philosophe&nbsp;; Vincent Liegey, Ingénieur&nbsp;; Fanny Lopez, historienne de l’architecture&nbsp;; Bernard Loup, président du CPTG&nbsp;; Sébastien Marot, historien de l’architecture&nbsp;; Chloé Moglia, artiste, suspensive&nbsp;; Corinne Morel Darleux, autrice&nbsp;; Métie Navajo, écrivaine dramaturge&nbsp;; Frédéric Neyrat, philosophe&nbsp;; Thierry Paquot, philosophe&nbsp;; Geneviève Pruvost, sociologue&nbsp;; Josep Rafanell i Orra, psychologue&nbsp;; Agnès Sinaï, directrice Institut Momentum&nbsp;; Chantal T. Spitz, tahitienne, autrice&nbsp;; Isabelle Stengers, philosophe&nbsp;; Stéphane Tonnelat, ethnographe&nbsp;; Joëlle Zask, philosophe.

Vous pouvez consulter la liste complète des signataires et signer le texte de soutien [ici](https://lessoulevementsdelaterre.org/blog/la-carte-des-points-relais-des-soulevements-de-la-terre).

{{< youtube yS7GKZaMKAk >}}