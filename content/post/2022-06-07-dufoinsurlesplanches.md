---
title: Concert de Baklava vendredi 1er juillet à 19h
subtitle: Concert public dans le cadre du festival 'Du Foin sur les Planches'
date: '2022-06-07'
tags:
  - culture
  - échanges
  - concert
  - danse
---
Le festival local [Du Foin sur les Planches](http://courantdartbeaujolaisvert.fr/du-foin-sur-les-planches-2022/) nous fait cette année l’honneur de nous intégrer à deux reprises dans son programme, par ailleurs particulièrement éclectique et alléchant !

Nous sommes d’abord invités parmi d’autres très chouettes projets le dimanche 26 juin à 17h30 à présenter MagnyÉthique dans le cadre d’une table ronde dont le thème sera [« Les nouvelles manières d’habiter le Beaujolais Vert »](http://courantdartbeaujolaisvert.fr/du-foin-sur-les-planches-2022/#jp-carousel-2283). Ces échanges auront lieu au tout nouveau tiers-lieu d’Amplepuis : [l’Atelier](https://www.ateliertierslieu.org/).

Le vendredi suivant, 1er juillet, à partir de 19h, le festival viendra même chez nous ! Nous sommes ravi·e·s d’accueillir toute leur équipe d’organisation, mais aussi (et surtout) le groupe Baklava pour un concert de musique folk grecque.

![](/images/partenaires/Plaquette-DFSLP-planches-BAILLEUX_Page_03.webp)

![](/images/partenaires/Plaquette-DFSLP-planches-BAILLEUX_Page_12.webp)
