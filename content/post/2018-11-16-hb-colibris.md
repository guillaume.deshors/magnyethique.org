---
title: Bourse aux projets d'habitat groupé
subtitle: Organisée par les Colibris
tags:
  - échanges
date: '2018-11-16'
---
![](/images/uploads/image4.png)

Yves a participé à une autre rencontre présentation de différents projets d'habitats participatifs organisée par les Colibris Lyon. L'occasion de (re)découvrir d'autres projets: "Notre beau projet", "Grain&sens" et de présenter le nôtre "Les Fabriqués" et d'échanger.
