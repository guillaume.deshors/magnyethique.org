---
title: Nouveau logo
date: '2019-02-21'
tags:
  - avancement
bigimg:
  - desc: null
    src: >-
      /images/uploads/1550820914573_magny_ethique_logo_OK_Plan de travail 9
      copie 4.png
---
Nous avions lancé en décembre dernier un concours de créatrices et créateurs afin de trouver notre nouveau logo MagnyÉthique.

Nous tenons à remercier les 9 artistes qui nous ont fait pas moins de 16 propositions en répondant à notre appel de décembre! Nous avons procédé par [jugement majoritaire](https://jugementmajoritaire.net). Les résultats du premier tour ont été très serrés et hétéroclites. Quatre logos se détachaient du lot, ne donnant cependant pas entière satisfaction. Nous avons donc suggéré des améliorations à leurs créateurs avant de faire un second tour de votes au jugement majoritaire. C'est finalement le logo que vous pouvez admirez sur notre site qui est sorti en tête des votes. Il est beau, non!?

![](/images/uploads/1550820932013_magny_ethique_logo_OK_Plan de travail 9.png)  

Nous allons rapidement contacter avec les participants pour voir avec eux à quel moment ils veulent être nos hôtes. Encore MERCI à tous!
