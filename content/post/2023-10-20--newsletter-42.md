---
title: 'Newsletter 42 : à la poursuite d''octobre chaud...'
date: '2023-10-20T00:00:00.000Z'
tags:
  - newsletter
  - avancement
  - portrait
---

### Bonjour ami·es MagnyÉthiques

L’été est passé très vite, entre chantiers, vacances, festivals, une naissance et la sixième décennie d’une vie inaugurée en musique. La rentrée est elle aussi derrière nous, riche d’une programmation entre culture, permaculture et développement personnel.   
La pause hivernale va permettre la planification de la suite.   
En attendant, nous vous proposons de revenir sur les points forts des derniers mois et de vous présenter un autre habitant de l’écosystème MagnyÉthique. 

![](/images/uploads/2021-12-16--newsletter31_img1)

### SAVE THE DATES

### Les prochains ateliers, stages, événements…

*   **Mardi 7 novembre** : [Atelier chant avec Benjamin Campagna](/post/2023-10-08-atelier-chant/)  
     
*   **Samedi 18 et dimanche 19 mai 2024 :** Stage “Rendre belles les fausses notes de la vie” avec et par Marc Vella   
    \> Renseignements et pré-inscriptions sur [cette page](/post/2023-11-03-stage-marc-vella/)

  
 

![](/images/uploads/2022-01-22--newsletter32_img1)

### ENVIE D'AIDER ?

### Les prochains   
chantiers participatifs

**Les chantiers à venir en 2023 :**   
 

*   Du lundi 30 octobre au vendredi 03 novembre  
     
*   Plusieurs chantiers plantation sont prévus pour le verger conservatoire. Dates à confirmer...  
     

\> [Informations et inscriptions sur cette page](https://magnyethique.org/page/wwoofing_chantiers/)

### Constellation systémique

En juillet dernier, nous avons répondu à une sollicitation de troc lancée par Élisa Valmori et Sébastien Rosset, fondateurs de [Nouvel air](https://nouvel-air.org/fr).   
Leur proposition était d’animer chez des collectifs une [constellation systémique](https://nouvel-air.org/fr/constellations-d-organisations) en échange d’une immersion de quelques jours sur place. Nous avons eu la chance de bénéficier de leurs compétences et avons partagé, entre membres du groupe et voisins, une demi-journée de constellation d’organisation qui nous a ouvert des pistes de décryptage du fonctionnement de MagnyÉthique et d’en déceler certains freins, notamment autour de l’entité qu’est l’association. Nous avions conscience de la plupart de ces points mais la constellation a permis de les mettre en exergue d’une façon originale. À nous de les transformer à présent pour redonner du mouvement et de la fluidité à notre organisation ! 

Un grand merci à Élisa et Sébastien pour leur offre et les échanges qui ont pu avoir lieu lors de leur séjour !

### Ma petite planète

### Défi environnemental, challenge écologique.

Sur l’impulsion de Virginie, volontaire super dynamique venue en chantier cet été qui dans la vie travaille pour l’association “ma petite planète”, nous avons créé une “ligue” magnyéthique (MagnyPlanet) et participé à l’édition qui vient de se terminer.  
Le principe ? Pendant 3 semaines chaque participant (15 dans notre ligue, presque 17000 au total !) est invité à réaliser des défis écologiques du genre : réaliser une semaine sans viande, aller au boulot à vélo, dégivrer son congélateur, faire attention à sa consommation de données mobiles, prendre des douches de moins de quatre minutes, faire des courses en vrac…   
De très nombreux défis dans plein d’aspects de sa vie quotidienne avec à la clé quelques prises de conscience intéressantes. Chaque défi rapporte un certain nombre de points selon la difficulté. Nous finissons à la 140e place sur 1209 ligues, pas mal malgré du relâchement sur la fin !  
Pour beaucoup d’entre nous, on en ressort avec l’envie de conserver certains gestes initiés lors de cette session ou de s’améliorer sur les points qu’on n’a pas réussi à enfourcher cette fois-là.  
Un énorme merci à Virginie et à toute l’équipe de MPP !  
 

![](/images/uploads/2023-10-20--newsletter-42_img1)

### Le hangar prend des couleurs

![](/images/uploads/2023-10-20--newsletter-42_img2)

Session street art sur le hangar à l'initiative de Mathieu et Bastien et grâce à la présence et l’aide d’Adam, wwoofeur talentueux.

### Revue des chantiers de l'été

![](/images/uploads/2023-10-20--newsletter-42_img3)

Le parquet de la salle panoramique a eu droit à un gommage exfoliant dépuratif. L'esthéticien a oeuvré pendant que les Magnyéths de l'étage n'étaient pas là. Merci à lui et à son acolyte non présent sur la photo.

![](/images/uploads/2023-10-20--newsletter-42_img4)

Chez Bruno, c'est ici l'étape de l'isolation phonique du plafond qui est illustrée. C'est un plafond suspendu avec plusieurs épaisseurs d'isolants qui ont chacun leur rôle. On espère gagner en tranquillité ce que l'on perd en hauteur sous plafond.

![](/images/uploads/2023-10-20--newsletter-42_img5)

Chez Corinne, c'est aussi une histoire de plafond. La pose des fourrures (des rails, en gros), qui accueilleront l'isolant, et sur lesquelles nous fixerons le gypsolinium (du placo, en gros).

### Festival Oasis 2023

### Comment faire société ensemble ?

Claire, Anne et Cédric ont passé quelques jours au festival Oasis qui s'est tenu fin août à l'écolieu Ste Camelle en Ariège.  
4 jours de conférences, ateliers, rencontres, concerts, animations...   
Bref, 4 jours bien remplis qui donnent un aperçu de la vivacité du mouvement coopératif en France.  
Un chouette moment, au sein d'un chouette lieu, avec des gens chouettes.  
Si l'habitat participatif vous intéresse, nous vous recommandons vivement l'édition 2024 !  
 

![](/images/uploads/2023-10-20--newsletter-42_img6)

L’événement a pris la forme d’une université d’été pour mettre en avant le rôle politique que jouent les oasis dans les dimensions culturelles, sociales et économiques de la société à travers des témoignages d’habitantes et d’habitants d’écolieux mais aussi de sociologues, philosophes, économistes, artistes… lors de 3 conférences et 5 tables rondes.  
[\>> Résumé vidéos et photos de l'événement](https://cooperative-oasis.org/articles/festival-oasis-2023/)

![](/images/uploads/2023-10-20--newsletter-42_img7)

_Une bonne idée parmi tant d'autres : le lave main avec des infuseurs à tisane qui permettent d'économiser l'eau._

### Portrait : Guillaume, le bricolo-brasseur

Si vous le croisez à l’écolieu, entre château et hangar-atelier, vous le reconnaîtrez sans aucun doute : Guillaume est quasiment toujours équipé de son casque de chantier multi-fonctions et de sa ceinture-caisse-à-outils.

![](/images/uploads/2023-10-20--newsletter-42_img8)

Parce qu’avant tout, Guillaume est non seulement notre bricoleur le plus polyvalent, mais aussi le plus inventif, s’inspirant de lectures scientifiques et blogs low-tech divers et variés avec lesquels il passe de nombreuses heures nocturnes en recherche de solutions…  
 

La cause d’un tel comportement ? Une probable génétique entre Mac Gyver - trouvant une solution à tout défi quasi impossible -, et Dormeur - le poussant à inventer un système pour dormir plus longtemps même quand il est de corvée “poulettes”.  
Ainsi, il a entre autres installé un treuil maison pour aider à stocker du matériel à la grange sans le monter à l’échelle, élaboré [un escalier trois-quarts de tour balancé](https://magnyethique.org/post/2021-04-01-newsletter26/) pour l’appartement qu’il occupe avec sa famille, ou fabriqué une porte automatique à capteur solaire à partir d’une vieille perceuse pour notre célèbre Carapoule ! Et il faut bien avouer que nous bénéficions tou·te·s de ces inventions !  
 

Mais ses dons ne s’arrêtent pas là. Nous pourrions parler de lui comme de notre webmaster, ou pour ses qualités pratiques dans la commission aménagements. Mais son portrait ne serait que trop partiel si nous n’évoquions pas plutôt ses compétences de brasseur de l’écolieu. Eh oui, c’est également Guillaume qui nous régale depuis trois ans avec des bières différentes à chaque fois (volontairement ou non, d’ailleurs 😉), et il y en a pour tous les goûts et toutes les occasions !

