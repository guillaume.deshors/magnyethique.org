---
title: Dons
comments: false
---
Vous voulez nous soutenir mais ne savez pas comment ? Votre aide est la bienvenue et peut prendre des formes bien différentes !

![](/images/uploads/1580301014749_illustration-don.jpg) {{< helloasso "Faire un don en euros" "https://www.helloasso.com/associations/les-magnyethiques/formulaires/1" >}}

![](/images/uploads/1580301026942_illustration-parrainage-arbre.jpg) {{< helloasso "Parrainer un arbre" "https://www.helloasso.com/associations/les-magnyethiques/collectes/test-arbres/" >}}

Vous pouvez aussi faire un don de matériel :

\- plaques de cuisson

\- grands plats et casseroles pour cuisine collective

\- louches, cuillères en bois…

\- batteur et tout autre petit électroménager de cuisine

\- boîtes de rangement en métal, garde-manger, légumier

\- outillage, bottes, seaux…

\- charrettes et brouettes

\- bois de récupération ou outils pour l'atelier menuiserie

\- matériel de clôtures, barrières, portails…

\- tonte, broyat pour les espaces agroécologiques
