---
title: Chantiers participatifs
comments: false
---
{{< calendar urls="https://cloud.magnyethique.org/remote.php/dav/public-calendars/BbDyFaYXstpA8g24/?export" colors="#E4C441" tiny="true" >}}

Prochaines dates de chantier participatif pour 2025 :
-----------------------------------------------------

<ul class="fa-ul">
   <li><i class="fa-li fa fa-building-wheat"></i>       le samedi 22 février : demi-journée chantier fin de paillage verger et jardin (Inscription à cette adresse : 2024paillage@magnyethique.org)</li>
   <li><i class="fa-li fa fa-snowflake"></i> du lundi 24 au vendredi 28 février2025</li>
   <li><i class="fa-li fas fa-seedling"></i>   du lundi 28 avril au vendredi 2 mai</li>
   <li><i class="fa-li far fa-sun"></i>        du lundi 30 juin au vendredi 04 juillet</li>
   <li><i class="fa-li far fa-sun"></i>        du lundi 07 juillet au vendredi 11 juillet</li>
   <li><i class="fa-li far fa-sun"></i>        du lundi 14 juillet au vendredi 18 juillet</li>
   <li><i class="fa-li fas fa-sun"></i>        du lundi 4 au vendredi 8 août</li>
   <li><i class="fa-li fas fa-sun"></i>        du lundi 11 au vendredi 15 août</li>
</ul>

Vous êtes plutôt extérieur ou intérieur ? Peu importe, à l'écolieu, il y en a désormais pour tou·te·s !

*   Pour les premier·ère·s, nous sommes désormais présents sur la plateforme [wwoof.fr](https://wwoof.fr/fr/host/6494) et espérons recevoir de l'aide pour la gestion du maraichage et du verger. Jusqu'à 2 personnes en permanence peuvent être accueillies pour ce projet-là, de préférence sur des périodes de plusieurs semaines. Nous avons planté en décembre 2023, après conception réfléchie en guildes fruitières et comestibles, un verger-forêt conservatoire grâce au soutien précieux attribué par le Fonds Germe et l'agrémenterons de bassins naturels. Nous rêvons également de construire nous-mêmes un grand four à pain, une cabane de sudation, un bassin de baignade naturelle etc.
    
*   Pour les second·e·s, c'est sur la plateforme [Twiza.fr](https://fr.twiza.org/ecolieu-magnyethique-au-coeur-du-beaujolais-vert,zs3301,11.html), consacrée à des chantiers participatifs écologiques, que vous nous trouverez afin de venir pour 2 jours minimum nous donner un coup de main sur les chantiers intérieurs. 
    

Si toutefois vous êtes de passage et voulez en profiter pour nous épauler au quotidien sur un temps long (plusieurs semaines), [contactez-nous](https://magnyethique.org/page/contact/) et nous verrons ensemble ce que l'on peut vous proposer, le principal écueil étant la disponibilité des habitant·e·s pour encadrer des chantiers hors temps officiels.

Au menu, en vrac : recloisonnement après destruction de cloisons pour finir de réorganiser les espaces, isolation extérieure, intérieure et phonique, installation du chauffage collectif, poursuite de révision de l’électricité et de la plomberie, enduits et badigeons à la chaux, pose de béton ciré (à base de chaux aussi)…

Nous effectuons les rénovations des lieux appartement par appartement pour permettre aux habitant·e·s, à peu près dans l'ordre d'arrivée et dans la logique des emplacements dans les bâtiments, de s'installer dans leurs appartements définitifs, libérant ainsi la place de logements provisoires pour accueillir les nouveaux·elles habitant·e·s. Ainsi, les tâches peuvent sembler toujours les mêmes à celles et ceux qui nous suivent, car nous les réitérons effectivement appartement après appartement.

Ceci nous permet de gagner en compétence et … de fêter plus de crémaillères !

**Rétrospective**

Le bien acheté en juin 2019 est un ancien château déclassé dont les pièces les plus anciennes remontent au 13ème siècle. L’ensemble du bâtiment comprend 2 000m² et forme un carré autour d’une cour intérieure que nous voulons embellir. L’intérieur est en état très variable et doit être réorganisé pour les besoins du projet et en grande partie rénové et réaménagé.

![](/images/illustration-chantier.jpg)

De menus travaux ont été effectués en 2019 pour permettre les premières installations provisoires dans des conditions viables. Depuis 2020 nous réalisons une série de chantiers intérieurs et extérieurs. Pour les définir et en fixer les différentes étapes, nous sommes accompagnés par une architecte locale formée à la rénovation écologique et un thermicien mène des études pour nous permettre de prévoir au plus juste quels seront nos besoins et comment les gérer à l'avenir de la façon la plus écologique possible. Pour l'extérieur, nous sommes accompagnés et formés par un bureau d'étude et de formation à la permaculture pour designer notre futur environnement. Ces précieux partenaires sont tous favorables aux chantiers participatifs et nous conseillent dans cette direction.

En 2021, c'est la rénovation globale des deux premiers appartements, dont une première crémaillère est fêtée en fin d'année, l'autre début 2022. En extérieur, une expérimentation de maraichage concluante qui tend à devenir un vrai maraîchage pérenne, mais aussi la plantation d'environ 70 fruitiers pour un verger vivrier aux variétés sélectionnées.

En 2022, deux autres appartements sont terminés pour des emménagements en décembre 2022 et janvier 2023.

En 2023, nous avons réalisé la rénovation d'une salle ouverte au public et deux prochains appartements ainsi qu'une grande salle commune très lumineuse sont bien avancés à présent. À l'hiver 2023-24, c'est la plantation de la majorité du [verger conservatoire](https://magnyethique.org/post/2021-12-23-un-verger-conservatoire-a-l-ecolieu/) en guildes arborées qui nous a occupés sur les extérieurs, avec pas moins de 115 arbres et le triple d'arbustes fruitiers, mais aussi des aromatiques, plantes médicinales etc.

En 2024, nous avons terminé les deux appartements de Corinne puis de Bruno, et avons attaqué les deux suivants ; de plus nous avons aussi terminé la suite parentale du premier appartement terminé en 2021, qui était occupée à l'époque par un autre foyer. Pour l'hiver 2024-25, c'est la chaufferie collective qui sera enfin terminée et fonctionnelle (nous avons hâte !).

Nombre d'appartements terminés : 6 sur 14 !

**Le principe du volontariat en wwoofing ou chantier participatif :** un petit coin pour planter votre tente ou un lit dans le bâtiment (en fonction des besoins et dispos), un accès aux commodités, de quoi vous nourrir chaque jour (végé) contre quelques heures passées à nous aider dans les divers travaux extérieurs ou intérieurs.

Vos compétences : alors si vous êtes charpentier, électricien, plombier, maître chaux, maraîcher, jardinier amateur, ou autre, c'est top, vous aurez deux desserts 😉 ! Mais si vous êtes de bonne humeur, bricoleur, autonome ou rien de tout ça, mais motivé et curieux d'apprendre et un peu débrouillard, c'est le principal ! Nous essayons de mettre l'accent sur le côté humain et de créer une bonne ambiance de groupe, ainsi que de transmettre des compétences autant que possible.

**Note** : le passage par Twiza et l'adhésion (35€/an) est recommandée car elle ouvre le droit à une excellente assurance accident de la vie couvrant l'ensemble des chantiers que vous ferez, et ce tarif est vraiment intéressant.
