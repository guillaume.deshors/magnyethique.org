---
title: Concert de piano à 4 mains avant la projection
subtitle: Ce vendredi 24 mars à 20h
date: '2023-03-16'
tags:
  - culture
  - échanges
  - concert
bigimg:
  - desc: null
    src: /images/uploads/1679515806979_Piano.jpg
---
**Vendredi 24 mars, à 20h**, et avant [la projection du film "Les jours heureux"](/post/2023-03-16-projection-les-jours-heureux/), nous aurons le plaisir d'accueillir deux artistes locales pour un concert d'une trentaine de minutes de piano à 4 mains.

Concert gratuit, sans réservation. La projection du film est décalée de 15 minutes, à 20h45.

Venez nombreux !
