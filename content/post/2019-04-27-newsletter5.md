---
title: Newsletter 5
date: '2019-04-27'
tags:
  - newsletter
  - avancement
---

Bonjour Ami.es MagnyÉthique 


### Vivre et travailler à la campagne, une riche idée
Mercredi 20 mars au soir, à Lyon s’est tenue une rencontre sur le thème de “Vivre et travailler à la Campagne”. Celle-ci était organisée par Beaujolais Vert Votre Avenir, Les Monts du Lyonnais et l’incubateur Ronalpia, soit autant de raisons qui nous ont fait y participer. Trois d’entre nous étaient présents et nous avons eu le plaisir de retrouver deux personnes d’une précédente rencontre autour de l’habitat partagé - ils sont tout intégrés dans le groupe à présent. Ainsi, nous étions nombreux à aborder le projet MagnyÉthique parmis la trentaine de participants. 

Les deux personnes représentant les structures d’aide au développement d’entreprise nous ont montrés les accompagnements possibles pour des porteurs de projets ayant pour but le lien social, le commerce de proximité et la transition écologique. Elles étaient également là pour nous présenter  pépinières d’entreprise, incubateurs, tiers-lieux... (exemples avec nos voisins de Lamure-sur-Azergues du Quartier Métisseur et de l’espace de coworking de la Cordée) ou encore des “CAE - Coopératives d’activité et d’emploi” comme à Calad Impulsion à Villefranche sur Saône où l’on y retrouve des métiers issus de l’art, l’artisanat, le commerce, la formation, le textile, l’informatique...

A cette soirée, il y avait des personnes qui souhaitent s’installer au vert, d’autres qui ont en vue une région de prédilection, d’autres encore qui sont déjà établis dans la région. Parmi eux, de futurs voisins souhaitant adapter leur ferme-auberge : la ferme du chapi. Ils proposent des produits bio et un accueil de groupe. 
Nous ressortons de cette organisation des histoires plein la tête et bien heureux de ces nouvelles rencontres !

![](/images/newsletters/5/illustration-beaujolais-vert-news-5.jpg)

### Temps de travail et de cohésion dans la région
Fin mars également, nous avons partagé un beau week-end de travail et de cohésion tout proche du château de Magny, au gîte de groupe de Meaux-la-Montagne où nous avons été accueillis chaleureusement. Le soleil radieux fut l’occasion de prendre la mesure de la beauté du coin, de regarder les étoiles ensemble (jusqu’au lever de la pleine lune), de respirer le grand air et de se projeter ensemble sur les opportunités qu’offre le coin en terme de nature. La marche étant un bon moyen d’entrée dans les conversations variées parfois profondes et ce, l’air de rien, une belle balade nous a permis le samedi après-midi par petits groupes changeants d’approfondir notre connaissance les uns des autres. L’exercice d’écoute empathique proposé par la commission gouvernance en extérieur pendant cette balade y a également contribué. Le samedi soir, nous avons accueillis les derniers venus dans le groupe autour d’un petit jeu de connaissance et d’une soirée plus informelle avant une plénière intense le dimanche où nous étions pour la première fois au grand complet, sans ordinateur comme intermédiaire vers l’Allemagne, la Bretagne ou autre contrée : c’était très chouette ! La cuisine à la sauce auberge espagnole a aussi bien fonctionné malgré les interrogations de certains en amont. Nous avons pu nous concocter de succulents repas partagés tout le week-end à partir des plats ou ingrédients emmenés par les uns ou les autres.


{{< load-photoswipe >}}

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/newsletters/5/meaux-1_1200.jpg" caption="à table !" >}}
{{< figure link="/images/newsletters/5/Meaux-2_1200.jpg" caption="à table ! (bis)" >}}

{{< /gallery >}}

### Ce léger flottement qui nous porte
En attente de l’achat de Magny, le groupe oscille entre ne pas prendre trop d’engagements à cause des échéances incertaines et faire avancer le projet malgré tout en planifiant certaines activités à plus ou moins court terme, confiant que les choses suivent leur cours et que l’achat est imminent. Ainsi, la commission aménagements continue son travail sur les répartitions des espaces avec les conseils d’un architecte et la commission communication commence avec elle à penser sérieusement à l’organisation de chantiers participatifs dès cet été pour faire avancer les travaux et rencontrer du monde. 


### Bientôt les chantiers participatifs 
Si le coeur vous en dit, nous serons ravis de vous accueillir ! Gardez un oeil sur la page internet ou patientez jusqu’à la prochaine newsletter où nous serons certainement en mesure de vous en dire plus. Une fois que des dates précises nous seront données en vue de l’achat, la commission activités pourra aussi confirmer à certains groupes désireux de venir donner un coup de main et nous ayant contactés la possibilité de les recevoir cet été.

### Des étapes concrètes !
Suite aux portes ouvertes de l’école des Prés verts de Cublize qui ont eu lieu le samedi 30 mars et ont charmé les parents qui ont pu s’y rendre, les premières inscriptions d’enfants MagnyÉth’ ont été déposées. Nous sommes heureux de pouvoir bientôt participer de bien des façons à la vie de ce joli petit village du Beaujolais vert !

… Et, le meilleur pour la fin : c’est officiel depuis jeudi dernier, nous avons signé les statuts de la toute nouvelle SCI MagnyÉthique chez notre notaire, le dépôt auprès de l’administration est en cours. Alors il ne manque plus que la date pour signer l’acte d’achat de notre futur écolieu ! Nous avons hâte !


![](/images/newsletters/5/illustration-signature-SCI-news-5.jpg)


En attendant la prochaine newsletter qui devrait être pleine de bonnes nouvelles, nous vous souhaitons de doux moments de printemps, tous sens en éveil !

À bientôt !

MagnyÉthique