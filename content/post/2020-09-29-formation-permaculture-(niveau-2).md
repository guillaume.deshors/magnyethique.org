---
title: Formation permaculture (Niveau 2)
subtitle: Du samedi 17 au lundi 19 octobre 2020
date: '2020-09-28'
tags:
  - formation
  - permaculture
---
À notre demande, PermaLab nous concocte pour le mois d'octobre une formation permaculture de niveau 2 afin de nous apporter les connaissances supplémentaires approfondies qui pourront notamment nous être utiles pour avancer sur notre design.

Nous avons convenu que ce stage sera ouvert à toutes les personnes ayant déjà participé à l'écolieu aux deux formations dispensées par PermaLab à l'automne 2019 et à l'été 2020.

Le programme traitera la mise en oeuvre d'un système agricole à petite échelle en abordant :

\- les techniques de maraîchage (dont les serres et l'irrigation)

\- les systèmes arboricoles

\- les auxiliaires animaux

Et comme toujours, les journées alterneront entre ateliers théoriques et pratiques.

Si vous êtes intéressé·es, écrivez-nous directement par le [formulaire de contact](/page/contact/) !
