---
title: Ferme pédagogique des animaux et de la nature
date: '2019-04-28'
tags:
  - Agriculture biologique
  - permaculture
  - wwoofing
bigimg:
  - desc: null
    src: /images/uploads/1558854227735_IMGP6170_resized.JPG
---
Katia, Pascal et leurs enfants ont passé une semaine complète dans une [ferme pédagogique](https://www.natur-und-tiererlebnishof.de) passionnante en Allemagne où la responsable du lieu travaille en protégeant et cultivant d'anciennes races de légumes et d'animaux en voie de disparition car menacés par l'agriculture intensive.

  

Ils ont beaucoup appris sur les races, la permaculture et ont participé à de nombreuses activités auprès des animaux, au jardin ou en aidant la responsable de la ferme à encadrer des groupes d'élèves en visite. 

  

Une semaine passionnante donc qui donne du grain à moudre au réflexion de notre collectif en vue de la mise en place des activités agricoles à Magny.
