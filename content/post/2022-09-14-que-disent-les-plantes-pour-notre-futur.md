---
title: Que disent les plantes pour notre futur ?
subtitle: Conférence de Gérard Ducerf
date: '2022-09-14'
tags:
  - écologie
  - permaculture
  - conférence
  - biodiversité
---
Plusieurs membres de la commission agricole se sont rendus le samedi 10 septembre 2022 à la conférence donnée par Gérard Ducerf, invité par l'association [L'Ombre des mots](https://www.lombredesmots.fr) à Pouilly-le-Monial.

Gérard Ducerf, originaire de Saône-et-Loire, a acquis une renommée internationale parmi les botanistes en étudiant depuis plus de 40 ans les plantes sous un angle particulier : ce qu'elles nous disent de la constitution et de la santé de notre sol et de notre environnement. Au fil de ses travaux, il a publié de nombreux livres, notamment l'[encyclopédie des plantes bio-indicatrices en trois volumes](https://www.promonature.com/produit/pack-3-volumes-de-encyclopedie-des-plantes-bio-indicatrices), que la commission agricole a utilisée pendant le design permacole. Ces livres permettent de reconnaître les plantes, d'en découvrir la raison d'être dans l'optique d'un diagnostic de terrain, de savoir sans analyse chimique de quoi notre sol est constitué. Ainsi, il est possible ensuite de savoir comment le faire évoluer en douceur, si tel est notre souhait, ou de choisir des aménagements et plantations adaptées à ce type de terrain.

La conférence a été riche : les qualités de transmission de Gérard Ducerf a permis de (re)découvrir le monde passionnant de la flore spontanée avec de nombreux exemples concrets. Avec lui, pas de mauvaise herbe, juste des plantes qui nous parlent si on sait les écouter !

De quoi nous encourager à poursuivre la lecture de nos terrains pour voir, après 2 à 3 années, comment notre sol a évolué suite à nos actions, par exemple au niveau du jardin.

![](https://cloud.magnyethique.org/s/iHe6SBqTQXwCyMk/preview)

Et en bonus, voici la [vidéo de l'enregistrement de cette belle conférence](https://www.youtube.com/watch?time_continue=1&v=P-1ltTj9xBk&feature=emb_logo). Merci à l'équipe de L'Ombre des Mots pour sa mise à disposition !
