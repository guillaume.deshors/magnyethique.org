---
title: Atelier Philo
subtitle: Lundi 30 septembre 2024 à 20h00
date: '2024-09-07'
tags:
  - échanges
  - atelier
  - philosophie
---

## Atelier Philo

Venez découvrir ce que Laetitia et Brice (intervenants **Atelier Philo**) vous proposent.
Dans un cadre emprunt de bienveillance, nous apprendrons à dialoguer en accueillant et partageant nos différents points de vue, en réinterrogrant nos certitudes, coryances, définitions et préjugés, sur un thème défini et exploré ensemble.

Ensemble, cultivons une vision plus claire et plus inclusive !


## Atelier Découverte

- *Participation aux frais*&nbsp;: 10€
- *Informations et inscriptions*&nbsp;: [mandala.anne@gmail.com](mailto:mandala.anne@gmail.com), 06.63.76.55.39

![Atelier Philo](https://cloud.magnyethique.org/s/XRipf5ETdb6Pmb3/preview)