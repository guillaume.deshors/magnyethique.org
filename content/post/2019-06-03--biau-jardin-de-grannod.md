---
title: Le Biau Jardin de Grannod
subtitle: et l'atelier paysan
date: '2019-06-03'
tags:
  - échanges
  - agriculture biologique
  - permaculture
bigimg:
  - desc: null
    src: /images/uploads/biau_jardin_grannod.jpg
---
Guillaume a passé 3 jours au ["Biau jardin de Grannod"](http://www.biaujardindegrannod.com/), ferme de maraîchage en bio depuis 1979, dans le cadre d'une grande fête organisée par des copains et gentiment hébergée à la ferme dans un esprit de festival bio auto-géré. 

A cette occasion Matthieu l'exploitant actuel et son père Pascal l'ancien exploitant, jamais avares de transmission de connaissances, nous ont gratifié d'une visite extrêmement intéressante, en nous expliquant notamment la technique des ["planches permanentes"](http://www.biaujardindegrannod.com/node/12911) mise au point partiellement sur place, et qu'ils exploitent avec des outils créés sur place aussi, ainsi que les principes et l'application de la rotation des cultures entre les différentes familles de plantes et en alternance avec des engrais verts, temporaires et pluri-annuels.

Ces outils ont été mis au point dans le cadre d'Adabio autoconstruction, devenu [l'atelier paysan](https://www.latelierpaysan.org/) : une coopérative visant à concevoir ou améliorer les machines, bâtiments et outils des paysans et à donner les moyens à chacun de le faire chez soi, au travers du partage de plans en licences libres, d'entraide sur le forum très actif, de formations et d'ateliers où les participants réalisent ensemble leurs outils. Pascal est modestement cofondateur de ce réseau dont les mots-clés sont [DIY](https://fr.wikipedia.org/wiki/Do_it_yourself), Open-Source, éducation populaire et bien commun.

N'hésitez pas à consulter et à garder en marque-pages les liens présents dans cet article, ce sont des vraies mines d'or ! Un esprit de partage que nous espérons arriver à égaler.
