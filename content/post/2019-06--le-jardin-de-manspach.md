---
title: Le jardin de Manspach
date: '2019-06-03'
tags:
  - permaculture
  - écologie
  - agriculture solidaire
  - agriculture biologique
  - wwoofing
---
Notre famille franco-allemande toujours en route vers Magny s'est arrêtée une semaine pleine en Alsace en wwoofing au [Jardin de Manspach](http://jardindemanspach.e-monsite.com). L'occasion pour eux de découvrir une méthode de culture inattendue, inconnue mais passionnante: la MSV (= Maraîchage sur Sol Vivant).

  

![](/images/uploads/1560802933945_IMGP7116_resized.JPG)  

  

"Parce qu'il y a des imbéciles qui cultivent des sols morts?" diront d'aucuns? Et bien à en croire Fabrice: oui! 80% de la biomasse terrestre serait contenue dans le sous-sol, dont 90% dans les “veines” constituées par les galeries de vers de terre colonisées par du mycélium, essentiel à l’installation pérenne de végétaux (lire pour s'en convaincre l'excellent livre La vie secrète des arbres de Peter Wohlleben). Le fait de retourner la terre pour la cultiver détruirait toutes ces galeries, pas si faciles à reconstruire pour les vers de terre, impossible pour le mycélium à cause d’un retournement fréquent dans les jardins.

   

Le principe de MSV est sur la base de ces connaissances de ne pas retourner les sols, de cultiver en créant des galeries supplémentaires par l’enracinement de divers végétaux qui, lorsqu’ils sont couchés et couverts, vont non seulement créer de nouvelles galeries par la destruction de leurs racines, mais aussi enrichir le terrain en surface et en profondeur, créant de la biomasse et favorisant le développement de celle déjà présente. L’existence de galeries riches est essentielle pour la vie de sol car elles servent au passage de petits animaux, d’insectes ou autre, mais aussi du mycélium. Les excréments des divers animaux (dont les vers de terre) donnent du collant à la terre. 

  

On pourrait penser que les galeries cassent les mottes mais en fait, elles les créent grâce à la place pour le collant des excréments ainsi qu’une forte résilience à l’eau (voir _Slake Test_). De plus, un sol couvert est moins sujet à l’évaporation, les racines des végétaux ensuite “détruits” avant une plantation permettent de faire remonter les oligo-éléments qui aideront les plantations à se nourrir au plus vite et au mieux. D’où l'intérêt d’avoir des racines de taille et profondeur différentes et des végétaux de différentes familles pour des plantations diversifiées ensuite.

  

Mais en plus de cette grande nouveauté, nos franco-allemands ont découvert le guidage de cheval de trait. Et Fabrice aime en plus s'essayer à la géobiologie, organiser des concerts au jardin et un festival estival. De quoi donner plein d'envies!

  

Bref: hâte de tester tout cela à notre tour en nous installant sur notre écolieu!
