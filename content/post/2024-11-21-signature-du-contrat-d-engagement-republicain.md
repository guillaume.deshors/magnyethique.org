---
title: Signature du Contrat d’Engagement Républicain
subtitle: Des échanges riches avant une prise de décision
date: '2024-11-21'
tags:
  - échanges
  - gouvernance
  - société
---
L’association Les MagnyÉthiques a pris la décision de signer le Contrat d’Engagement Républicain (CER), formalité obligatoire pour recevoir la subvention attribuée par la COR dans le cadre de notre projet de verger conservatoire. Nous avons longuement échangé entre membres de l’association autour de ce document et nous souhaitons partager une synthèse de nos réflexions dans ce courrier.

  

*   Nous sommes ravis des échanges et de la rencontre qui ont eu lieu entre des membres de l’association et des représentants de la COR. Cette attribution de subvention est pour nous une marque de confiance et de soutien à notre projet à vertu écologique.
    

  

*   Nous avons discuté sur la notion de trouble à l’ordre public présente dans le contrat car cet encadrement spécifique est déjà prévu dans les dispositions constitutionnelles et les déclarations de droits, et cette mention supplémentaire nous a fait réfléchir. Cette clause particulière nous a semblé potentiellement discriminante pour des associations engagées, notamment sur les sujets environnementaux, car elle pourrait entraver leur accès à des financements et aussi limiter leur capacité d’action. Cela avait d’ailleurs été relevé par [Le Mouvement Associatif avant la mise en œuvre du CER](https://lemouvementassociatif.org/wp-content/uploads/2022/01/LMA_CP_03012022_decretCER.pdf). L’ordre public est en effet une notion qui dépend de l’interprétation des parties prenantes (représentants de l’Etat, collectivités, associations, …) et qui peut donner lieu à de fastidieux échanges juridiques... 
    

  

*   La signature de ce CER par Les MagnyÉthiques se fait donc avec une volonté d’équité de l’accès aux financements publics pour l’ensemble des associations.
    

  

*   Des membres ont évoqué des inquiétudes quant aux risques induits pour la liberté d’action associative en général. Celle-ci nous semble essentielle à notre démocratie.
    

  

*   Ces échanges nous ont permis de réfléchir plus globalement sur les subventions que nous sollicitons, les engagements liés à celles-ci peuvent être multiples et nécessitent d’être toutes discutées en groupe.
    

  

Ces réflexions ont permis d’approfondir nos échanges entre membres et de partager plus en profondeur autour de valeurs d’écologie, de démocratie, de liberté, d'éthique et d’engagement, avec toutes les nuances possibles dans ces concepts qui nous tiennent à cœur et que nous voulions partager ici.
