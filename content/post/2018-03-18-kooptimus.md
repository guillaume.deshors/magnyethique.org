---
title: Formation à la gouvernance partagée et à la CNV
subtitle: Samedi 17 et dimanche 18 mars 2018
tags:
  - conférence
  - gouvernance
  - CNV
date: '2018-03-18'
---
![](/images/uploads/image14.jpg)

Samedi 17 et dimanche 18 mars 2018

Katia, notre "Allemande" s'est rendue à une soirée atelier organisée par [KoOptimus](https://www.kooptimus.de "https://www.kooptimus.de") puis à une journée de [workshops](https://gfktagbonn.de "https://gfktagbonn.de") organisée par la fédération germanophone de Communication Non Violente lors de laquelle de nombreux acteurs très divers sont intervenus (dont KoOptimus). La 5ème édition a eu lieu cette année à Bonn.

Au programme du samedi soir, un atelier de 2h mêlant théorie et mise en pratique par jeu de rôles sur le thème: "Du combat à la convergence: trouver des solutions qui conviennent à tous". Une méthode d'élaboration et prise de décision élaborée par Miki Kashtan, très proche de celle au consentement mais pouvant être privilégiée lorsque les tensions relevant des attentes et besoins personnels pourraient être un frein à la réalisation d'un projet, puisque son élaboration est construite à partir de ceux-ci.

Et le dimanche, des ateliers théoriques et pratiques d'une heure et demi chacun autour de la santé, du développement personnel, de la CNV en éducation et, plus intéressant pour nous, la gouvernance partagée et la gestion de conflit au sein d'un groupe:

*   modérer des discussion efficacement en gérant les conflits à la racine : exposé époustoufflant de Andrea Mergel, l'une des grandes figures de la CNV allemande. Une méthode toute simple, prenant appui sur les neurosciences, inspirée de la CNV pour calmer les esprits en moins de deux dans une discussion conflictuelle.
*   "De quoi parlons-nous, en fait ?" Savoir aller à l'origine du conflit sans reproche.
*   Sociocratie : pour des réunions efficaces et des décisions solides et durables en équipe.
