---
title: Newsletter 8
date: '2019-07-30'
tags:
  - newsletter
  - avancement
---

Bonjour Ami.es MagnyÉthique 


Les jours s’écoulent depuis la date d’achat et nous effectuons jour après jour une plongée à couper le souffle dans la réalité et le concret du quotidien ! Laissez-nous vous conter un peu cela…


### Des urgences aux visions d’avenir...
À la remise des clés, il a d’abord fallu aux membres du groupe s’armer de patience pour pallier le manque de plan du château afin de démêler les circuits d’arrivée d’eau, d’électricité, d’eaux de pluie et eaux usées etc. Il nous a fallu un mois pour avoir électricité et eau dans toutes les parties du bâtiment sans être obligés d’éponger sans cesse les fuites venant des toits ou de la plomberie défectueuse! Entre temps, les quelques appartements “combo” étaient très prisés pour les douches chaudes ! ;-) 

Mais depuis, tout prend forme et les travaux ont pu débuter. Les chantiers participatifs permettent d’avoir une aide précieuse pour mener à bien les premières tâches que nous nous sommes fixées en concertation avec l’architecte. Ont déjà vu le jour un grand enclos pour les poules et son poulailler de luxe tout en récupération de matériaux trouvés ci et là dans et autour du château. La taille drastique des thuyas qui assombrissaient et rapetissaient désespérément la cour intérieure lui offre une deuxième jeunesse. Les trous abyssaux de l’allée menant au château ont été comblés, parce que nous ne pouvions nous résoudre à l’achat de 4x4. Les foins et le débroussaillage de certaines parcelles ont été effectués grâce à l’aide d’agriculteurs du village. Le nettoyage des terrains où jonchaient moult déchets est en cours mais sera un travail de longue haleine. Les zones à risque comme le puits ou les regards à nu ont été sécurisées. L’élaboration de l’aire de jeux pour les enfants a débuté et va se poursuivre car nos petits MagnyÉth’ le méritent bien vue l’aide indéfectible qu’ils nous apportent au quotidien. À venir aussi le décroutage des murs pour les préparer au drainage et à accueillir un enduit à la chaux destiné à les assainir, et bientôt le début des travaux de phytoépuration, l’élagage des tilleuls etc. Les plans des géomètres que nous allons recevoir cette fin du mois vont nous permettre d’avancer sur le réaménagement des intérieurs et de débuter progressivement ces travaux.

Côté potager, les choses prennent forme aussi, mêlant encore différentes techniques de jardinage. Malgré les plantations forcément tardives, nous avons déjà dégusté nos premiers radis et surveillons avidement la croissance des courgettes et le rougissement progressif des tomates. Nous sommes encore loin de l’autonomie mais il faut un début à tout ! Et en attendant, nous nous fournissons chez les trois agriculteurs bio de Cublize grâce à qui nous nous régalons de bons produits bio et locaux.

Un grand merci aux volontaires déjà venu.es et aux futur.es sans qui de telles avancées ne seraient possibles!!!

### Des rencontres !
Le mois de juillet a été riche en rencontres. Outre les nombreu.ses volontaires des chantiers participatifs, nous avons aussi eu le plaisir, voire l’honneur d’accueillir sur place diverses personnes.

Le jeudi 4 juillet, nous avons organisé le premier apéritif MagnyÉthique auquel nous avions convié des acteurs de la vie associative, politique, et entrepreneuriale locale. Ce fut une soirée riche en échanges, découvertes et conseils divers. Nous remercions toutes les personnes présentes pour leur ouverture dans l’expression de doutes ou mises en garde, mais aussi de leur accueil chaleureux et amical. Nous espérons refaire le point sur l’avancée de certains partenariats qui en découleront peut-être.

Deux jours plus tard, le samedi 6 juillet, une quarantaine de personnes ont bravé les orages pour venir nous rencontrer, découvrir le projet et visiter le château. Le repas partagé a été l’occasion de conversations à bâton rompu, où rêves et idéaux ont été partagés. Cette journée nous a permis de recruter des personnes intéressées par rejoindre le groupe. Les prochaines portes-ouvertes auront lieu les 15 septembre et 26 octobre pour celles et ceux qui n’ont pu venir précédemment.


### Côté cour ou côté jardin?
Une troupe de théâtre amateur viendra jouer à Magny les 23 et 24 août deux pièces de son répetoire : le drame contemporain La Compagnie sans commandeur d’Alberto Lombardo et la comédie George Dandin de Molière. La météo décidera du lieu de représentation mais nous décidons du public ! Nous aimerions donc profiter de cette occasion pour remercier les volontaires des chantiers participatifs de l’été et tous nos adhérents à venir partager avec nous ces belles soirées en perspectives (contactez-nous pour vous inscrire car les places sont limitées, adhésion possible sur place).

{{< load-photoswipe >}}

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/newsletters/8/compagnie-sans-commandeur.jpg" caption="affiche Compagnie sans commandeur" >}}
{{< figure link="/images/newsletters/8/George-Dandin.jpg" caption="affiche George Dandin" >}}

{{< /gallery >}}

### Save the dates!
Les dates à retenir :

  * du 29 juillet au 4 août : Chantier participatif n°4 
  * du 15 au 23 août : Chantier participatif n°5
  * vendredi 23 et samedi 24 août : Théâtre à Magny 
  * dimanche 15 septembre à partir de 10h30 : Journée portes-ouvertes + repas partagé
  * samedi 21 septembre : Stand au Petit Bal des Possibles du Quartier Métisseur
  * du 28 septembre au 4 octobre : Formation permaculture
  * samedi 26 octobre à partir de 14h30 : Journée portes-ouvertes + goûter partagé


Profitez bien de l’été et à bientôt !

MagnyÉthique


