---
title: MagnyÉthique accueille un jeune en service civique
date: '2021-12-01'
tags:
  - échanges
  - rencontre
  - société
  - réseau-local
---
Depuis mi-novembre, nous avons l’honneur de recevoir Maxime B. à l’écolieu dans le cadre d’un [service civique 100% transition](https://osonsicietmaintenant.org/100-pourcent-transition/) qu’il effectue auprès de l’[association Osons Ici et Maintenant.](https://osonsicietmaintenant.org) Pendant 6 mois, 3 jours par semaine, il nous prêtera main forte sur le lieu. Nous nous réjouissons de cette nouvelle expérience qui s’annonce constructive et enrichissante pour tou·te·s.

![](https://cloud.magnyethique.org/s/dWdsZC8wjPS3wJM/preview)
_Maxime avec Guillaume, qui est son tuteur pour le serice civique_

Voici le témoignage de Maxime, écrit après seulement quelques semaines parmi nous, dont la toute première vécue lors de la dernière semaine de chantier participatif de l'automne.

"_Le lieu d'accueil de l'association MagnyÉthique offre un écrin de verdure, un bol d'air frais. De son château perché, celui de Magny, on peut y observer un très beau panorama qui s'étend, ci et là, entres deux hauteurs. On y trouve un cadre qui se fait apaisant, reposant, qui invite à la méditation, réflexion et à l'inspiration. Un paysage qui donne à penser sérieusement à la question de l'écologie, de s'en soucier. Ce qui est l'une des priorités des MagnyÉths (les personnes qui vivent dans l'écolieu). Et ils ne ménagent pas leurs efforts pour rendre le lieu plus vert et douillet possible. Réutilisation de matériaux, utilisation de matériaux naturels, anti-gaspillage, anti-déchets, etc._

_De ma première expérience de chantier participatif à MagnyÉthique. J'en ai tiré que de bonnes conclusions. J'ai pû faire la rencontre et la connaissance de super personnes, que ça soit du côté des bénévoles, que du côté des MagnyÉths. L'ambiance était toujours au beau fixe. Il y avait de l'échange, de l'énergie. On avait à apprendre de tous, de leurs expériences et de leurs languages (personnes étrangères). Une bonne part d'humain. Ça donne la pêche, la motivation pour le boulot._

_Lors du chantier, et pas que, tu arrives avec tes bagages. Si tu ne sais pas faire les choses, aucun soucis, on t'y apprends. L'Association est dans cette envie constante de transmettre tous ses savoirs. On travaille dans un environnement bienveillant à chaque instant. Il n'y a pas de pression. Prendre son temps pour faire mieux, c'est okay. Quelques loupés, on t'explique pourquoi, faut juste être plus vigilant ensuite. Y a pas de stress à avoir, faut être motivé. Motiver à apprendre et aider une cause, une association, qui vaut la peine d'être aidée._

_Détail important, il faut pas avoir peur de se salir et de travailler le physique plus que le mental._

_À l'heure actuelle, j'ai été pris en service civique au sein de MagnyÉthique. J'en apprends toujours plus. Je suis satisfait d'être aussi bien tombé. Les gens sont toujours aussi gentils, bienveillants et dans l'envie de partager (astuces écologiques, diverses aspects extérieurs, BTP, autres)… Et je vais m'arrêter là, car, je peux tellement en dire._" Maxime B.

