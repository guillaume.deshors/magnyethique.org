---
title: Festival Sacrées Abeilles
subtitle: Restaurer notre lien au vivant
date: '2022-08-24'
tags:
  - biodiversité
  - écologie
  - permaculture
---
Katia et Pascal se sont rendus en famille à la toute première édition d'un festival très prometteur qui a eu lieu des 19 au 21 août derniers à Fontvieille (entre Arles et Avignon) : le festival [Sacrées abeilles](https://www.sacrees-abeilles.org).

![](images/uploads/1662470924060_SACREES-ABEILLES-05-1024x1024.png)![](images/uploads/1662470924060_SACREES-ABEILLES-05-1024x1024.png)![](https://cloud.magnyethique.org/s/TMxJpdbSGPezcBd/preview)

La première journée, un peu en dehors du festival, consistait à faire se rencontrer différents acteurs de l'apiculture au naturel et défenseurs de la biodiversité, notamment des pollinisateurs sauvages. Ce ne sont pas moins de 80 personnes des quatre coins de France et même au-delà des frontières qui se sont réunies autour de ce sujet pour échanger, discuter, créer du lien et se projeter vers l'avenir collectivement. En effet, vue l'urgence à agir face à l'effondrement de la biodiversité et à la fragilité inquiétante des pollinisateurs, ce groupe a discuté des possibilités de faire avancer ensemble la mise en lien, la communication, la pédagogie, dans un seul et même but : donner plus de visibilité aux actions ponctuelles menées ci ou là ! Une [association](https://www.sacrees-abeilles.org/qui-sommes-nous/) a été créée, qui se retrouvera en novembre pour poursuivre la réflexion et l'action.

Les deux journées de festival ont ensuite offert une richesse de programmation incroyable pour une première édition :

\- conférences

\- ateliers pratiques de ruches écologiques, apithérapie, reconnaissance des pollinisateurs sauvages (dont nocturnes)

\- théâtre, contes… pour jeune public

\- projection de documentaires

\- tables rondes et discussions

mais aussi

\- bal folk

\- librairie

…

Il y en avait pour tous les goûts et tous les âges, des plus novices aux plus connaisseurs.

![](https://cloud.magnyethique.org/s/2MK5YSoLzZ6i4H5/preview)  

![](https://cloud.magnyethique.org/s/LawWsZr5PB7xT3f/preview)  

![](https://cloud.magnyethique.org/s/RHo8N9Sj7maWr7q/preview)

Une chose est sûre : toute la famille est rentrée émerveillée, et un peu plus éveillée de ces quelques jours aux "Sacrées Abeilles". La pratique de l'apiculture à l'écolieu MagnyÉthique va évoluer en fonction de ces découvertes et apprentissages faits sur place : gestion respectueuse de la ruche et de l'essaim, solutions naturelles pour soutenir ses abeilles contre le varroa et d'autres maladies… Et l'envie de protéger le vivant dans son ensemble, sur place et ailleurs, a encore grandi.
