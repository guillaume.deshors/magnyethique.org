---
title: RDV Cosmos à l'écolieu MagnyÉthique
subtitle: Première retransmission sur le thème de l'agriculture, du sol et de la biodynamie
date: '2022-09-29'
tags:
  - conférence
  - société
  - permaculture
  - biodiversité
  - échanges
---
Nous sommes heureux·ses de pouvoir contribuer à une belle initiative de l'association [Cosmos - culture & écologie.](https://cosmos-culture-ecologie.com/?page_id=125)

*   **[L'assosiation _Cosmos - culture et écologie_](https://cosmos-culture-ecologie.com/?page_id=125)**

Elle propose des programmations culturelles et artistiques autour de l'écologie, dans son sens le plus large, pour tous les publics. Elle s'appuie sur l'art et la culture, en tant que vecteurs de connaissances, d'émerveillement et de récits, pour nourrir les liens au vivant et l'engagement.

*   [**RDV Cosmos**](https://cosmos-culture-ecologie.com/?page_id=75)

Les prochains Rendez-vous Cosmos - Film & Rencontre, en novembre et en décembre, auront lieu en ligne, à prix libre. Ils peuvent être également diffusés en direct dans des lieux culturels, associatifs, tiers-lieux, écolieux… La Maison de l'écologie à Lyon, Evreux Nature Environnement, l'écolieu MagnyEthique diffuseront ainsi le prochain Rendez-vous Cosmos.

Contact et inscription à la newsletter : Pauline De Boever / [cosmos.culture.ecologie@gmail.com](mailto:cosmos.culture.ecologie@gmail.com)

![](https://cloud.magnyethique.org/s/eWiSgCJEZGtd9Nx/preview)

L'écolieu MagnyÉthique a ainsi choisi pour la prochaine date de programmation de retransmettre en direct le 9 novembre prochain à 20h (début à l'écolieu à 19h45), la soirée autour du film [La Graine, les particules et la lune](https://cosmos-culture-ecologie.com/?page_id=1920) de Dune Dupuy. Le documentaire, sorti en 2021, traite de la question de l'agriculture, du sol et de la biodynamie, et dure 1h18.

Sa rediffusion sera suivie d'un temps de discussion avec la présence exceptionnelle de Lydia et Claude Bourguignon, microbiologistes, spécialistes et médecins des sols, et de la réalisatrice Dune Dupuy. Nous avons hâte de pouvoir profiter de leurs éclairages sur bien des points car nous sommes plusieurs à apprécier leurs travaux.

![](https://cloud.magnyethique.org/s/wbS9mrfBeQpp9K5/preview)

*   **Le film**

**Synopsis** : La planète Terre souffre, et Bouba vient de se faire plaquer. C’est alors qu’elle rencontre un groupe d’agriculteurs en biodynamie, prenant en compte l’action de forces invisibles. Une quête poétique qui entraîne la réalisatrice et le spectateur dans des mondes insoupçonnés, à la rencontre d’humains proposant d’élargir notre perception du vivant, et qui ouvre aussi à un autre regard sur le sol et l’agriculture : comment préserver le sol et quel est son rôle ? Quels sont ses liens et ceux des plantes, des animaux et des êtres vivants, avec le cosmos ?

Le film a été programmé aux Rencontres du cinéma documentaire de Lussas 2021 et au Festival Interférences 2021 à Lyon.

Extrait : [https://www.youtube.com/watch?v=2tzVRof0NbQ](https://www.youtube.com/watch?v=2tzVRof0NbQ)

Film accessible dès 10-11 ans.

*   **Les invités**

Nous aurons la joie d’accueillir deux invités exceptionnels : **Lydia Bourguignon**, Maîtres es sciences et technicienne en œnologie, et **Claude Bourguignon**, ingénieur agronome et docteur es sciences en microbiologie du sol, sont tous deux des spécialistes de renommée internationale du sol. Leur engagement a fait d’eux des figures de proue de la connaissance et de la préservation du sol, et plus largement de l’écologie. Ils ont créé en 2008 LAMS, un laboratoire d’analyse de sol spécialisé dans l’étude écologique de profil cultural pour restaurer la biodiversité des sols de terroir afin d’améliorer la qualité et la typicité des vins et des denrées agricoles. Ils ont publié récemment _Pourquoi ne faisons-nous rien pendant que notre maison brûle ?_ (Editions D’En bas).

**Dune Dupuy** a étudié le cinéma à l’Ensav à Toulouse et vit dans le Lot sur le Causse du Quercy. _La Graine, les particules et la lune_ est son premier film, co-produit par deux jeunes sociétés installées dans le Sud Ouest, l’Argent et Embrassez-vous. Son travail s’inscrit dans une approche transdisciplinaire du réel, motivée par la nécessité absolue de changer notre lien avec les autres vivants, les non-humains. Dune Dupuy dans _[La Dépêche](https://www.ladepeche.fr/2021/11/10/lot-la-realisatrice-dune-dupuy-pour-comprendre-le-monde-nous-avons-besoin-autant-de-science-que-de-poesie-9920265.php)_ : « Pour comprendre et apprécier le monde qui nous entoure, nous avons besoin autant de science que de poésie. Je pense qu’il est vital de changer notre lien avec les autres vivants, c’est-à-dire les plantes et les animaux avec lesquels nous devons cohabiter. Pour cela il est important de prendre le temps de les connaître. Et je crois que c’est l’émerveillement qui nous donne envie de prendre soin. »

*   **Informations pratiques**

Cette soirée est **ouverte à tou·te·s**, sur Lyon pour celles et ceux qui y sont, mais aussi chez nous, pour les voisin·e·s ! Accueil à l'écolieu à partir de 19h30. Introduction et règlement à 19h45.

L'entrée se fera sur la base du **prix libre**, les recettes seront partagées entre l'association _Cosmos - culture & écologie_ et la nôtre.  

**Contact** : cosmos@magnyethique.org
