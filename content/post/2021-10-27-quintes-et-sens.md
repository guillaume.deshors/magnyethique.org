---
title: Quintes et sens
subtitle: Comment Marc Vella nous a fait vibrer
date: '2021-10-27'
tags:
  - rencontre
  - formation
  - conférence
  - stage
---
Des quintes et des sens
=======================

Apprivoiser les fausses notes de la vie pour les faire résonner et les mêler à notre symphonie personnelle. Tel est ce que nous a enseigné Marc Vella le week-end dernier à l'écolieu.

La posture du maître et la grâce.
=================================

Après une soirée consacrée au visionnage du documentaire créé après la dernière caravane amoureuse du musicien et compositeur en Casamance, il nous a guidé·e·s vers la **quintessence** pendant tout le week-end. Des moments chargés de profondeur, d'émotions, mêlés de réflexions philosophiques et très personnelles.

Se réconcilier avec la musique ou l'instrument ; découvrir l'improvisation ; vivre un moment collectif fort ; prendre plaisir musicalement à plusieurs ; voir que l'on est capable de beau ; vérifier que l'on n'est pas nul·le ; rencontrer Marc Vella ; travailler sur soi ; tisser un autre lien avec la musique ; la curiosité… Voici quelques unes des raisons qui ont conduit les stagiaires du week-end autour de Marc Vella et du piano de l'écolieu. La plupart sont reparti·e·s comblé·es, tant sur le plan musical que personnel.

Une première journée consacrée à "**la posture du maître**" nous a permis de découvrir la musicalité de chacun·e, invité·e au piano à jouer une mélodie autour de la quinte de son choix. Et même les grand·e·s débutant·e·s ont fait des merveilles !

La seconde évoquait quant à elle "**la grâce**". Se donner confiance et créer tous sens ouverts pour se laisser glisser dans la création. Le talent de Marc Vella à notre service, il nous a invité·e·s à nous asseoir avec lui au piano pour improviser des quatre mains uniques et magnifiques, tantôt sombres, tantôt légers, mélancoliques , déchaînés ou enthousiastes. La vie sur des touches noires et blanches pour nous encourager à vivre chaque instant comme moment unique de création et de vie.

Un week-end inoubliable pour donner de l'élan et rendre sa vie plus belle, malgré - ou grâce - à ses fausses notes.
