---
title: La vie secrète des sols
subtitle: Conférence de Marc-André Sélosse
date: '2024-01-21'
tags:
  - permaculture
  - biodiversité
  - conférence
  - écologie
  - ''
---
En 12 janvier 2024, Katia et Pascal se sont rendus à la conférence de Marc-André Sélosse, spécialiste réputé des sols, de leur composition (minérale, végétale, animale, bactérienne ou microbienne, mycorhyzienne) à leur santé. En plus de faire partie actuellement des programmes de recherche internationaux les plus pointus pour faire avancer encore les connaissances et expériences dans ce domaine, Marc-André Sélosse, qui se défint lui-même comme un "passeur de science", est un pédagogue hors-pair, il arrive à expliquer et illustrer les enjeux actuels à des publics variés.

Un grand merci aux associations [Agir au Val d'Oingt](https://www.facebook.com/AgirValdOingt?__cft__[0]=AZXO2ZIXgvKWA4WQVSlCfjgXWBaOApEvTjL7PlTw4JtqchSZvNo0Pmfvr8-TicXJmMlVxSZWPF-ivHumZ7wvpHnOOXOaDp3b5SADH__U2stUmefaw4Wx2goG9tDryc7tYXFgDE5dWdXgU8Pn9vvMYjNsHLplsnbf4yTEJ5MoQguGmzCMt-E0e6iAcTENf4zi2nE&__tn__=-]K-R) et [Demain c'est ici et maintenant](https://www.facebook.com/demaincesticietmaintenant?__cft__[0]=AZXO2ZIXgvKWA4WQVSlCfjgXWBaOApEvTjL7PlTw4JtqchSZvNo0Pmfvr8-TicXJmMlVxSZWPF-ivHumZ7wvpHnOOXOaDp3b5SADH__U2stUmefaw4Wx2goG9tDryc7tYXFgDE5dWdXgU8Pn9vvMYjNsHLplsnbf4yTEJ5MoQguGmzCMt-E0e6iAcTENf4zi2nE&__tn__=-]K-R) (DIM pour les intimes), situées en aval de la vallée de l'Azergues, dans les Pierres dorées, pour l'organisation de cette conférence qui a déplacé plus de 300 personnes du secteur, des agriculteurs au simples jardiniers en passant par les viticulteurs et les élus.

Le 16 mars, DIM réitérera sa formation "Semis-semences" où Katia et Maxime s'étaient rendus il y a un an avec deux wwoofeurs pour apprendre comment réussir ses semis, puis récolter ses semences en fin de cycle végétal. Une grainothèque est également mise en place pour pouvoir les échanger ensuite.

La conférence est à (re)voir sans modération en ligne : [Marc-André Sélosse "La vie secrète du sol"](https://www.youtube.com/watch?v=_wfMrNm7WKM&t=3162s).
