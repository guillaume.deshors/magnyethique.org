![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

Projet gitlab portant le site https://magnyethique.org/

Pour mettre à jour le site, il existe 3 solutions, par ordre croissant de complexité / possibilités :

1. Utiliser l'interface d'administration SaleinaCMS : https://magnyethique.org/admin/
1. Utiliser le WebIDE de gitlab : https://gitlab.com/-/ide/project/guillaume.deshors/magnyethique.org/edit/master/-/
1. Cloner le projet en local avec git, travailler puis pusher de façon normale.

---

## 1. Interface d'administration

Cette solution est la plus simple d'utilisation et normalement assez auto-explicite. Il est possible de créer du contenu mais pas d'en supprimer. A chaque fois qu'on sauvegarde une modification, elle est "poussée" sur le site et apparaît en ligne environ 5 min plus tard.

## 2. WebIDE de gitlab

Cette solution permet de créer, modifier, et même de dupliquer/supprimer des fichiers. Les attributs des fichiers se trouvent dans les premières lignes constituant le bloc d'en-tête, le contenu se trouve après ces lignes. Il faut éditer en markdown (c'est très facile !) mais il existe un _preview markdown_ qui permet de voir le résultat.

Voici une spécification de la syntaxe markdown acceptable : https://sourceforge.net/p/hugo-generator/wiki/markdown_syntax/

Les différents éléments sont là :

 * page d'accueil : https://gitlab.com/-/ide/project/guillaume.deshors/magnyethique.org/blob/master/-/content/_index.md
 * pages : https://gitlab.com/-/ide/project/guillaume.deshors/magnyethique.org/tree/master/-/content/page/
 * posts : https://gitlab.com/-/ide/project/guillaume.deshors/magnyethique.org/tree/master/-/content/post/
 
Les fichiers finissant par **.en.md** sont en anglais, ceux finissant juste par **.md** en français.

On peut éditer plusieurs fichiers à la fois Une fois qu'on a fini de faire les modifications, il faut faire "commit" et choisir "commit to master branch" :

![image.png](./image.png)

Les modifications apparaissent en ligne environ 5 min plus tard.


## 3. Travail en local

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Commit
1. Push

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

Read more at Hugo's [documentation][].


[ci]: https://about.gitlab.com/gitlab-ci/
	
[hugo]: https://gohugo.io
	
[install]: https://gohugo.io/overview/installing/
	
[documentation]: https://gohugo.io/overview/introduction/
	
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
	
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
	
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains


## Comment récupérer les newsletters ou autres pages en HTML

1. retrouver [les newsletters](https://mail.google.com/mail/u/0/#search/newsletter+magny%C3%A9thique+%23)
2. prendre le lien de la version HTML et appeler ./importnewsletter.py <url>
3. entrer la date d'envoi quand demandé (ou la passer en paramètre)
4. supprimer toute la partie inutile du début et de la fin
5. examiner la sortie du script, notamment les alertes ; vérifier les images si doublon ; les diminuer si trop grosses (alerte dans la sortie du script si >1Mio) ; vérifier les liens
6. relire et enjoy :)
