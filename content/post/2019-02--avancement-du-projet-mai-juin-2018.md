---
title: Avancement du projet - mai-juin 2018
date: '2018-06-23'
tags:
  - avancement
---
Les mois derniers ont été riches en enseignements pour l'ensemble du groupe. Les différents accompagnements dont nous avons bénéficié en mai ont contribué à mettre beaucoup de choses à plat. Un certain nombre de foyers a pris la décision de mettre fin à son engagement dans le projet, pour diverses raisons. Nous avons apprécié les moments partagés qui nous ont tous fait grandir et leur souhaitons bonne route: au plaisir de les recroiser!

  

  

Un noyau de quatre foyers reste actif et travaille à présent au rythme d'une soirée par semaine (les mercredis soirs) en conférence et d'une journée de travail par mois. Des week-ends travail et cohésion auront lieu au moins 3 à 4 fois par an pour soigner les relations humaines, en présenciel.

  

  

Il s'agit à présent de retrouver un lieu, en élargissant la zone géographique des recherches. Mais l'essentiel du projet d'origine est conservé: nombre de foyers souhaités à terme (environ 10), partage, écologie, permaculture, artisanat, ouverture sur l'extérieur. Tout sera adapté au nouveau lieu.

  

  

Nous tenons à nous excuser auprès des personnes qui ont continué à nous contacter pendant cette période difficile, pour nous encourager parfois ou pour nous signaler leur intérêt de participer. La situation ne nous a pas permis de répondre, nous allons à présent tenter de rattraper notre retard. Merci en tout cas pour vos messages et peut-être à bientôt!
