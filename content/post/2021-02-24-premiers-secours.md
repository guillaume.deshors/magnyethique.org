---
title: Formation premiers secours
subtitle: Atelier d'initiation / rafraîchissement et Formation certifiante
date: '2021-02-03'
tags:
  - formation
  - atelier
---
Les premiers secours débarquent à l'écolieu, sous deux formes différentes.

### Session « enfants » / « rafraîchissement »

Date : samedi 13 mars 2021 après-midi

Thèmes abordés : malaise, prévention et évacuation incendie, ...

Tarif libre

### Formation certifiante PSC1 (Adulte) :

(12 participants maxi, priorité aux [adhérents](/page/adherer) de l'association)

Date : samedi 20 mars 2021 

Horaires: 9h-12h - 13h-17h

Dans cette session, les thèmes basiques suivants seront abordés :

*   L'alerte
*   L'inconscient qui respire
*   L'inconscient qui ne respire pas
*   Le DSA (défibrillateur semi automatique)
*   La personne qui s'étouffe
*   Les plaies. brûlures. hémorragie.

Participation à la formation : _coût 549€ à diviser par le nombre de participants._


Pour s'inscrire à l'une de ces sessions : envoyer un mail à 202103_formationpremierssecours@magnyethique.org
