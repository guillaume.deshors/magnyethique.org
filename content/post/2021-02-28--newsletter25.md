---
title: "Newsletter 25 - 🦋 Arrivées 🐛, Formations CNV 🦒 / 1ers secours"
date: 2021-02-28
tags: 
  - newsletter
  - avancement
---


[](#)

**Bonjour ami·es MagnyÉthiques**

[](#)

 

Février aura hésité entre tapis de neige et tapis de fleurs. Quoi qu’il en soit, les prémices du printemps se font sentir, réjouissent les mains vertes du jardin et adoucissent les températures à l’intérieur du bâtiment dès l’ouverture des fenêtres par nos bricoleurs.

 

[](#)

  
**Effets collatéraux des confinements ?**

[](#)

![](/images/uploads/2021-02-28--newsletter25_img1)

L’année 2020 semble avoir poussé de nombreuses personnes à la réflexion sur la situation climatique et l’urgence à agir. Certain·es ont choisi de franchir le pas et de changer de vie. Pour nous, elle a été synonyme de nombreuses sollicitations avec des personnes qui envisagent la transition, mais aussi d’accueils au sein du projet.  
C’est dans cette droite lignée que nous sommes heureux de vous annoncer la récente éclosion d’Agnès, nouveau papillon parmi les papillons MagnyÉthiques. Elle prépare déjà son arrivée au printemps.


![](/images/uploads/2021-02-28--newsletter25_img2)

Deux autres foyers encore chrysalides finalisent leurs réflexions, notamment avec la commission aménagements pour savoir quels espaces provisoires pourront être occupés par qui, en attendant la rénovation de leurs appartements définitifs respectifs. Ainsi, nous espérons fort assister à trois emménagements dans les mois qui viennent.

[](#)

![](/images/uploads/2021-02-28--newsletter25_img3)

L’année 2020 semble avoir poussé de nombreuses personnes à la réflexion sur la situatioCôté chenilles et œufs, là aussi, beaucoup de mouvement ! À tel point que nous avons dû inaugurer à notre dernière plénière en présence le [“parcours couveuse”](https://drive.google.com/file/d/15UY9uHQmZ2mMeNNX1STc1jyrDaaafRH2/view?usp=sharing) que nous avions élaboré l’été dernier en prévision d’un jour lointain, où nous serions peut-être presque au complet.

[](#)

 

Lointain ? Oui, et bien finalement, les événements ont accéléré les choses, et nous y voilà, sans même avoir eu le temps de nous en apercevoir. Nous verrons bien où les chemins de chacun·e les mèneront. Mais nous nous réjouissons que notre projet trouve un tel écho et que tant de personnes participent concrètement au changement de société !

Et si vous êtes curieux·se de découvrir les récent·es membres, tout apparaît petit à petit sur [notre site](/page/habitatparticipatif/) !

 

[](#)

![](/images/newsletters/6/travaux.jpg)

[](#)

**“Il faut toute la vie pour apprendre à vivre.” Sénèque**

[](#)

 

L'écolieu foisonne de petit·es et de grand·es acrobates, explorateur·rices, chercheurs·ses, expérimentateurs·rices, aventuriers·ères et le bricolage devient une activité à plein temps ! Pour rassurer les troupes à l’avenir, nous avons choisi de suivre deux **formations aux 1ers secours** qui se passeront à l’écolieu.

En plus des marteaux, visseuses, scies et meuleuses, de nouveaux outils pratiques pour l’assistance aux personnes en détresse dans l’attente des secours n’auront bientôt plus de secret pour nous !

**Un atelier de découverte des premiers secours destiné aux enfants** pourra aussi servir de rappel pour les personnes déjà détentrices du PSC1 mais ayant envie d’un rafraîchissement. **Cet atelier aura lieu l’après-midi du samedi 13 mars**.

**Le PSC1 (formation certifiante) se tiendra le samedi 20 mars**. Cette formation est ouverte à d’autres personnes que les membres du groupe dans la limite des places disponibles. Si vous êtes intéressé·e, toutes les infos sont [là](/post/2021-02-24-premiers-secours/).

C’est ensuite avec grand plaisir que nous retrouverons Edith Tavernier, formatrice en **Communication Non Violente les 17 et 18 avril** prochains. Nous l’avons invitée à revenir animer un module 1 de CNV pour - entre autres - nos membres n’ayant pas pu en bénéficier l’an dernier. Cette formation est également ouverte aux personnes extérieures au groupe. Pour plus d’informations, c’est par ici !

 

[](#)

![](/images/uploads/2021-02-28--newsletter25_img4)

[](#)

**“Des p’tits trous, des p’tits trous, encore des p’tits trous !”**

[](#)

 

ôté travaux, tant en intérieur qu’en extérieur, nous faisons en effet des trous, plus ou moins petits d’ailleurs. Dans la terre, dans les murs…

 

Après les 25 premiers “grands” arbres fruitiers plantés en janvier, ce sont 48 autres arbres et arbustes, ainsi que quelques petits fruitiers qui ont pris place sur nos terrains en février ! Merci aux nombreux·ses donateurs·rices à [notre campagne de don d’arbres](http://helloasso.com/associations/les-magnyethiques/collectes/test-arbres) ou ayant fait des dons en nature ! Et au printemps, place aux fleurs !

 

Quant aux avancements en intérieur : des trous aussi, pour modifier des cloisons, poser une verrière, passer les fils électriques dans les nouveaux plafonds isolés… Mais aussi, et ça fait plaisir, on commence à reboucher quelques trous ! Un trou rebouché dans le plancher ici, une porte posée là...

 

[](#)

 

![](/images/uploads/2021-02-28--newsletter25_img5)

 

Dessin de Cassandre G., volontaire de l’été 2020. Merci Cassandre !

 

[](#)

 

Et pour toutes les personnes qui n’ont pas encore eu l’occasion de nous rendre visite, sachez que les chantiers offrent une belle opportunité de visiter et de s’imprégner du lieu, de rencontrer les membres du projet tout en étant témoin d’une plénière hebdomadaire et en approfondissant les sujets qui vous intéressent, que ce soient les montages juridiques, la gouvernance partagée, la permaculture ou les bricolages low tech ! Alors n’hésitez plus, ce printemps, vous avez [plein de possibilités de venir](/page/wwoofing_chantiers/) assouvir votre curiosité en nous donnant un bon coup de main !

 

[](#)

**Save the date**

[](#)

 

Les dates à retenir :

*   [du 1er au 23 mars](/page/wwoofing_chantiers/) : chantier participatif menuiserie / charpente
*   [le 13 mars 2021](/post/2021-02-24-premiers-secours/) : formation certifiante aux premiers secours
*   [le 20 mars 2021](/post/2021-02-24-premiers-secours/) : atelier d’initiation aux premiers secours pour enfants et rappel pour adultes déjà certifiés.
*   [17 et 18 avril 2021](/post/2021-02-24-formation-cnv/) : formation CNV module 1
*   [du 19 au 23 avril](/page/wwoofing_chantiers/) : chantier participatif
*   [du 31 mai au 25 juin](/page/wwoofing_chantiers/) : chantier participatif menuiserie / charpente (suite)

 

[](#)

![](/images/uploads/2020-10-01--newsletter21_img2.jpg)

[](#)

 

En attendant que vous trouviez une occasion de (re)venir nous voir, nous vous espérons en bonne santé. Ouvrez grandes vos fenêtres pour vous aérer et vos oreilles pour goûter aux délices symphoniques des oiseaux qui s’échauffent déjà la voix pour le printemps !

 
