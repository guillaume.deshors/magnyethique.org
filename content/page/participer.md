---
title: Calendrier global
date: '2019-01-04T23:06:50.000Z'
---
Voici le calendrier global de tous les événements proposés par MagnyEthique :

{{< calendar urls="https://cloud.magnyethique.org/remote.php/dav/public-calendars/BbDyFaYXstpA8g24/?export,https://cloud.magnyethique.org/remote.php/dav/public-calendars/CjTXxEd5BDFzW5NG/?export,https://cloud.magnyethique.org/remote.php/dav/public-calendars/ERn8LszmJBBpsGwH/?export,https://cloud.magnyethique.org/remote.php/dav/public-calendars/FbymRCJoMt8rPg7p/?export,https://cloud.magnyethique.org/private-calendar" colors="#E4C441,#FFA07A,#87CEFA,#3CB371,lightgrey" 
>}}


Légende des couleurs :

*   {{< liencouleur "#E4C441" "../wwoofing_chantiers" >}}Chantiers participatifs{{< /liencouleur >}}
*   {{< liencouleur "#FFA07A" "../culture" >}}Événements culturels{{< /liencouleur >}}
*   {{< liencouleur "#87CEFA" "../formations" >}}Formations et ateliers{{< /liencouleur >}}
*   {{< liencouleur "#3CB371" "../visites" >}}Visites et présentations{{< /liencouleur >}}
*   {{< liencouleur "lightgrey" "" >}}Événements privés (salles probablement réservées){{< /liencouleur >}}