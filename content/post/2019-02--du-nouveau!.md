---
title: Avancement du projet - juillet 2017
date: '2017-07-18'
tags:
  - avancement
---
Beaucoup de mouvement ces derniers temps!

Le groupe s'est reformé après quelques départs. Un noyau de personnes engagées et motivées poursuit, accompagné à présent par de nouveaux-venus tout aussi engagés et motivés: c'est très prometteur! Nous sommes à présent 9 foyers dans l'aventure.

Après un RDV chez le notaire lundi dernier pour discuter des statuts de la SCI, nous voilà donc lancés!

Le groupe s'est rencontré à plusieurs reprises ces dernières semaines (en chair et en os, via skype…) avant de se réunir à l'ombre du grand platane du parc pour faire connaissance avec chacun en direct tout en observant les enfants prendre possession des lieux qu'ils ont découvert ensemble avec une joie immense.

Nous avons discuté de divers sujets essentiels et reformé les commissions en charge de faire des propositions à tous à la prochaine réunion et de faire avancer notre beau projet collectif.

*   La **commission statuts et finances** va se concentrer sur le bouclage de la SCI dans le mois qui vient, la reconstitution du bureau de l'association des Prés-fabriqués,.
    
*   La **commission finances** va proposer différents modèles de financements de la SCI puis du projet immobilier, prendre contact avec des courtiers pour négocier un emprunt collectif où tous les habitants seront gagnants, monter des dossiers de demandes de subventions…
    
*   La **commission cohésion** va nous concocter un calendrier et des actions pour que l'humain reste un point fort du groupe dans ces moments d'intense paperasse qui se profilent.
    
*   La **commission communication** va continuer à faire vivre le projet vers l'extérieur (et l'intérieur!) afin de gagner quelques foyers supplémentaires, de concrétiser les pistes de partenariats futurs et d'en trouver d'autres et que tous les participants soient au fait des avancées concrètes.
    
*   Quant à la **commission architecture**, elle est dans un premier temps mise en suspens jusqu'à l'achat du lieu qui reste fixé à novembre. Les échéances approchent, on entre dans le vif du sujet dans l'excitation générale!
