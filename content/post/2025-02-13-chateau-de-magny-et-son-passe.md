---
title: Le château de Magny et son passé
subtitle: Appel à témoignages
date: '2025-02-13'
tags:
  - échanges
  - culture
  - histoire
  - rencontre
  - réseau local
---
L’association municipale [Cublize et son histoire](https://cublize.fr/fr/as/242569/212253/cublize-et-son-histoire) a décidé cette année de diriger ses recherches sur le château de Magny, et notamment sur l’époque où il aurait d’abord été sanatorium pour enfants malades avant de devenir un grand centre de colonie de vacances. Notre association Les MagnyÉthiques est touchée à l’idée d’un travail commun qui nous permettra de connaître le passé du lieu et de partager ces connaissances lors des journées du patrimoine de septembre.

  

Pendant que les spécialistes vont fouiller les archives pour glaner des informations, nous nous mettons en quête de témoignages de personnes étant venues sur le lieu à l’époque des colonies de vacances au siècle dernier. Ce [questionnaire](https://framaforms.org/appel-a-temoignages-chateau-de-magny-1739440066) est à leur disposition avant d’organiser entre eux, les membres de Cublize et son histoire et nous une rencontre riche en échanges de savoirs. Merci de nous aider en le relayant !

  

Et si vous êtes en possession de documents ou de connaissances pouvant être utiles, vous pouvez les adresser à l’une ou l’autre des associations. Tout document vous sera bien sûr rendu ensuite !

![](https://cloud.magnyethique.org/s/SQQa7jsHYPkzKRS/preview)
