---
title: La fibre arrive à MagnyÉthique
date: '2023-04-27'
tags:
  - avancement
---
Jusqu'ici, on avait la fibre écolo... maintenant ça y est on a la fibre tout court&nbsp;! Pas écolo internet&nbsp;? Oui, c'est sûr, on a tous nos contradictions. En attendant on est un paquet à en dépendre au quotidien pour notre travail, nos réunions projet et bien sûr notre vie perso, et on peut vous dire qu'on attendait l'arrivée de la fibre depuis un moment car jusqu'ici ce n'était pas grandiose. Elle était promise courant 2019 ; ça a presque été tenu...

On s'est dit que c'était l'occasion de vous partager des infos sur la solution de connexion qu'on utilisait depuis bientôt 4 ans, qui nous a rendu de fiers services, et qu'on abandonne quand même avec joie car ça avait aussi quelques inconvénients, et surtout que ça ne pouvait pas faire de miracles avec un ADSL très faiblard et une 4G en dents de scie. Attention, la suite de cet article est relativement technique et n'intéressera sans doute pas grand-monde.

Nous avions donc mis en place **OpenMpTcpRouter**. Qu'est-ce que c'est ? Pour faire court, c'est le moyen de grouper plusieurs connexions internet comme si ce n'en était qu'une, en bénéficiant d'une meilleure vitesse et d'une meilleure résilience. Meilleure vitesse car les paquets réseau passent par la connexion physique la moins chargée à tout moment. Meilleure résilience car si l'une des connexions devient inopérante, alors les autres prennent  le relais. Et oui, chez nous ça tombe... souvent. Un arbre sur la ligne ADSL une fois, mais surtout 4G trop chargée et inopérante à intervalles très réguliers, intempéries qui contrarient le signal, ADSL qui se coupe plusieurs fois par semaine pour des raisons inconnues, etc.

Voici un schéma de principe du système :

![](https://cloud.magnyethique.org/apps/files_sharing/publicpreview/q4HXtcZLTA45BTL?file=/2023/2023.03.03%20openmptcprouter/openmp-diagram.png&fileId=25501&x=1920&y=1080&a=true)

Pour être plus concret, nous avions mis en place :
  * un abonnement ADSL chez un opérateur
  * un abonnement 4G chez un autre
  * un serveur chez nous sur lequel est installée la partie 'client' de OpenMpTcpRouter. Au début, un Raspberry Pi 4, mais il saturait et nous ralentissait, et ensuite un vieil ordinateur portable.
  * un serveur quelque part sur internet sur lequel est installée la partie 'serveur' de OpenMpTcpRouter. Dans notre cas, un VPS (_Virtual Private Server_) loué à OVH, situé à Gravelines.

Le concept est donc d'avoir un lien virtuel entre notre réseau et le VPS, et autant de liens physiques qu'on le souhaite, deux en l'occurrence. Notre IP vue de l'extérieur était celle du VPS, et c'est une des limites du système, car les plages d'IP de ce genre de VPS sont souvent présentes dans des listes noires à cause d'usages illégaux réalisés par d'autres personnes au travers de VPS, ce qui fait qu'on avait encore plus souvent que les autres des CAPTCHA, voire des bloquages purs et simples sur certains sites, typiquement bancaires, mais pas seulement. Il est toutefois possible de poser des exceptions pour que certains protocoles ou certaines adresses ne passent que par l'une des connexions, en direct.

Quelques détails techniques en plus : le DHCP des box 4G et ADSL était désactivé, et c'est notre routeur OpenMpTcpRouter qui servait de DHCP, passerelle, et DNS pour tout notre réseau interne.

OpenMpTcpRouter est basé sur OpenWrt et propose une interface de supervision vraiment pratique, qui nous donne l'état des liens physiques et le ping courant. En général, quand ça rame, on y voit tout de suite ce qui est en orange et qui pose problème.

![](https://cloud.magnyethique.org/apps/files_sharing/publicpreview/q4HXtcZLTA45BTL?file=/2023/2023.03.03%20openmptcprouter/openmp-luci.png&fileId=25491&x=1920&y=1080&a=true)

OpenMpTcpRouter est développé à peu près exclusivement par une seule personne, un français (pseudo Ysurac), et nous avons fait un don pour le remercier de tout ce travail placé en Open Source. Cf. https://www.openmptcprouter.com/

et la suite ?
----

La suite c'est donc la fibre. Nous allons continuer à mutualiser une seule connexion pour tout notre réseau local ; internet est un probablement le poste de charges où nous faisons les meilleures économies d'échelle ! Nous avons pris un abonnement professionnel qui fait automatiquement un fallback sur 4G en cas de perte de la fibre, ce qui reprend la partie 'résilience' de notre solution précédente.

L'avantage d'avoir une bonne connexion et normalement une IP fixe, c'est que nous devrions avoir la possibilité d'auto-héberger quelques services, ce que nous ne pouvions pas faire jusque-là. On vous partagera peut-être ça un jour.
