---
title: Communication Non Violente
subtitle: Module 1
date: '2020-01-18'
tags:
  - formation
  - vie-de-groupe
  - gouvernance
  - CNV
bigimg:
  - desc: null
    src: /images/uploads/1580227721670_CNV Module 1.jpg
---
Le week-end des 10 et 11 janvier 2020, presque tous les membres du groupe MagnyÉthique ont participé sur l'écolieu à leur premier module de formation à la [Communication Non Violente](https://www.cnvformations.fr), communément appelée CNV. 

  

Cette méthode, élaborée par Marshall Rosenberg, permet de communiquer en parlant de soi, d'aller au fond de ses ressentis pour y déceler les besoins fondamentaux qui se cachent derrière une émotion, pour pouvoir ensuite se mettre en lien avec ce qu'il y a de vivant dans une relation. Dans quel but? Celui de prendre soin de soi et des autres, celui de coconstruire une relation vraie et sincère.

  

Le module 1 est consacré à mettre des mots sur les sentiments et les besoins qui leurs sont liés dans diverses situations, qu'elles soient agréables ou désagréables, pour pouvoir les exprimer et se mettre en lien avec l'autre. Pas si facile de se défaire de tous les jugements, reproches et autres travers de pensée et de communication que nous a inculté notre culture depuis des siècles ! Ces deux jours de formation ont été pour certains une découverte, pour d'autres un entrainement précieux pour pouvoir s'approprier ce mode de communication qui nous sera sans aucun doute d'une grande aide dans le groupe.

  

Nous remercions chaleureusement Edith Tavernier, formatrice en CNV à [AT Conseil](http://www.atconseil.com), ainsi que Benjamin Pont d'[Habitat et Partage](http://habitatetpartage.fr) pour leur accompagnement à ce premier week-end de formation et attendons avec impatience le module 2 qu'ils nous dispenseront en mars.
