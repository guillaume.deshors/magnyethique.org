---
title: '26 au 29 mai  2022 : Stage Danse-contact-impro et Axis Syllabus'
subtitle: Avec Antoine Ragot (Berlin)
date: '2022-03-01'
tags:
  - stage
  - culture
  - danse
bigimg:
  - desc: photo Antoine
    src: /images/uploads/1646308911498_photo antoinerecadre2.jpg
---
"Les sens, l’essence de la danse"
=================================
### Stage résidentiel de contact-improvisation, facilité par Antoine Ragot ###

Jams, cours, exercices de perceptions, partnering, improvisations de groupe, pratiques somatiques, discussions... Voilà une opportunité de s'engager pour quelques jours dans une aventure collective, réenchantée par une attention aiguisée et fluide, la reconnaissance et le jeu avec les forces universelles créatrices de mouvement sur terre... et pour ceux qui le souhaitent les mains sur terre et les pieds en l'air!

Nous utiliserons la force analytique de l’Axis Syllabus et sa faculté à révéler nos capacités de mouvement au service de cette pratique sociale et jubilatoire qu’est le contact improvisation.

Le Contact-Improvisation a émergé de la scène de danse post-moderne du New-York des années 60 comme pratique performative. Elle s’est transformée pour devenir aujourd’hui une danse plus sociale et plus populaire. Sa forme s’improvise, s’invente et se déforme spontanément, nourrie par les informations sensorielles et motrices qui émergent de l’intra-action, avec son/sa/ses partenaire(s) et l’environnement.



* * *

Biographie:
-----------

Antoine Ragot est investi dans la pratique et la recherche dans une variété de domaines allant de la danse contemporaine aux pratiques somatiques en passant par le contact-improvisation, les coordinations athlétiques et l'improvisation. Il produit et co-pilote un cycle annuel d’éducation au mouvement au Lake Studios à Berlin depuis 2015.

Pour plus de détails, consultez son site internet: [www.movementartisans.net](www.movementartisans.net)

Logistique :
------------

Ce stage aura lieu à [MagnyÉthique](www.magnyethique.org) dans notre grande salle de 100 m² sur parquet, et il est proposé en résidentiel : logement sur place en camping ou dortoir de six à huit lits. Les repas végétariens sont prévus du jeudi midi au dimanche midi.

Participation financière libre et consciente : un détail des dépenses engagées vous sera communiqué à votre inscription (ou sur demande). Le nombre de places est limité à 16.

Renseignements et inscriptions : 2022contactimpro@magnyethique.org. Votre inscription est effective à réception d'un chèque d'arrhes de 80 €, à envoyer à :  

MagnyÉthique - stage contact-impro

213 chemin du château de Magny  

69550 Cublize
