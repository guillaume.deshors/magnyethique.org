---
title: La Trame Verte et Bleue
subtitle: Ou comment assurer la continuité écologique des réservoirs de biodiversité
date: '2021-03-25'
tags:
  - formation
  - permaculture
  - écologie
  - biodiversité
---
[La Trame Verte et Bleue](https://player.vimeo.com/video/468116056)

![](/images/uploads/1616703279073_TVB.webp)  

Un nouveau MOOC gratuit a été organisé par [Tela Botanica](https://mooc.tela-botanica.org) de janvier à mars 2021 autour de la Trame Verte et Bleue.

Mais c'est quoi, la Trame Verte et Bleue ? Il s'agit du nom donné à des espaces assurant la continuité écologiques entre les différents réservoirs de biodiversité présents sur un territoire (qu'il soit national, régional, départemental, communal ou individuel).

La démarche est la suivante :

*   Il s'agit d'abord d'observer pour connaître les espèces fauniques et floristiques présentes et à protéger,
*   apprendre à connaître leurs comportements.
*   Il faut ensuite reconnaître les installations humaines qui peuvent être défavorables à leurs déplacements, car pour qu'il y ait de la vie, il faut du mouvement.
*   En fonction de cette analyse, des mesures peuvent finalement être prises pour restaurer ou favoriser le mouvement des espèces et assurer ainsi leur pérénité.

On parle alors de corridors pour la biodiversité. Ces jonctions entre les réservoirs sont particulièrement importants car ce qui fait la richesse d'un réservoir de biodiversité n'est pas seulement ce qu'il contient, mais le nombre de connexions à d'autres réservoirs qui lui permettront de perdurer en continuant à s'enrichir.

La trame verte correspond au monde arboricole, la trame bleue à tout ce qui est aquatique ou milieu humide. Il existe aussi une trame noire assurant la continuité sombre pour la vie nocture, mais aussi une trame marron qui s'intéresse aux sols etc.

Cette formation s'adressait à la fois à des particuliers, des associations et des élu·es. C'est au titre des deux premiers que Katia a suivi ce MOOC, approfondissant ainsi ses connaissances sur la nature et faisant le plein de trucs et astuces qui pourront nous servir à boucler le design en permaculture en tenant compte de tous les habitants et usagers du terrain !
