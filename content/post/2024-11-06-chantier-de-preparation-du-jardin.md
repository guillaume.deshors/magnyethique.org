---
title: Chantier de préparation du jardin et du verger pour l'hiver
subtitle: Dimanche 24 et vendredi 29 novembre 2024
date: '2024-11-06'
tags:
  - permaculture
  - rencontre
  - chantier participatif
---
Deux jours pour préparer en collectif le jardin et le verger conservatoire à passer l'hiver.

**Au programme** : récupération de foin dans une vieille grange voisine que les propriétaires veulent vider, paillage et soin du verger conservatoire. Mais aussi échanges, rencontre, discussions, repas partagé.

Si vous avez envie de contribuer au projet, de discuter autour de l'écolieu, l'habitat participatif, l'agroécologie, de découvrir la conception en permaculture des terrains et notamment du verger, bienvenu·e·s !

Inscription nécessaire pour prévoir repas et goûter : 2024paillage@magnyethique.org (N'oubliez pas de préciser la date SVP !)  

![](https://cloud.magnyethique.org/s/AsK6Ab6n7GixAiL/preview)
