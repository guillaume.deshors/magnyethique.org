---
title: Amis et partenaires
---
{{< citation qui="Coopérative Oasis (2023)" couleur="#809360" imgderriere="/images/partenaires/coop-oasis.jpg" >}}

Depuis décembre 2022, nous sommes soutenus par la Coopérative Oasis tant financièrement, avec un prêt de 150&nbsp;000€, qu’humainement, puisque la Coopérative nous propose accompagnements et conseils. Cette aide va nous permettre de faire avancer les gros chantiers, notamment celui du chauffage central que nous espérons en place pour l’hiver prochain, et de poursuivre les réflexions de plusieurs commissions en bénéficiant de points de vue extérieurs et de partages d’expérience.

{{< /citation >}}

{{< citation qui="Avenir Climatique (2021)" couleur="#428a9a" imgderriere="/images/uploads/1630441973843_cropped-Logo-AC-simple-1.png" >}}

Si on devait résumer le ressenti des participant·es de l’UEDAC 2021, ce serait par les mots “incroyablement magique”. Cette semaine fut riche de rencontres, d’échanges, d’apprentissages, de rires, de joies… partagées au château de MagnyÉthique.

Ah au fait, UEDAC signifie Université d'Été Décontractée d’Avenir Climatique. C’est un camp d’été bas carbone où une quarantaine de membres de l’association Avenir Climatique, jeunes et moins jeunes, se retrouvent pour parler climat, changement de société, résilience, etc…

Et l’ingrédient crucial pour réaliser une belle UEDAC, c’est le lieu : c’est la première étape, autour de laquelle se dessine tout le camp, qui lui donnera une tonalité unique puisqu’il change tous les ans. Et en 2021, c’était donc l’UEDAC de Magny.

Depuis le début, les relations avec elleux ont été simples et faciles. L’énergie déployée par Bruno, Pascal et Caroline, nos interlocuteurices, pour s’assurer que tout se préparait et se passait bien, était incroyable. Iels ont tout mis en œuvre pour nous faciliter notre séjour sur place, parer aux imprévus, et nous soutenir face aux cataclysmes (bon, un gros orage, mais en camping, c’est rude). Iels ont mis à notre disposition tout le matériel nécessaire : une cuisine fonctionnelle pour nourrir 40 personnes, un rétroprojecteur, l’accès aux douches en matinée, des outils, un dortoir après l’orage… Côté chantier participatif, les encadrants.e.s et la bonne humeur étaient bien là .

Leur expérience de la gestion des conflits, à laquelle Bruno nous a initié pendant la semaine, a permis de définir des règles simples pour que la cohabitation se passe sans problème : nous nous sentions chez nous, profitant des lieux mis à notre disposition, sans (trop) empiéter sur leur espace vital, selon un savant équilibre entre partage et intimité.

Au final, nous avons partagé de nombreux beaux moments : les chantiers participatifs, un dîner à la chandelle (merci l’orage), et une soirée sur la terrasse des tilleuls (liste non exhaustive). Tant et si bien qu’il est difficile de croire aujourd’hui que nous n’avons partagé qu’une semaine avec les MagnyÉthiques ! Peut-être car depuis, cette superbe semaine tourne en boucle dans nos têtes…

Le groupe de l'UEDAC 2021 ([Avenir Climatique](https://avenirclimatique.org) est principalement composée [d’étudiant.e.s et de jeunes actif.ve.s](https://avenirclimatique.org/les-acteurs/) qui ont envie d’agir d’une manière concrète et optimiste pour les enjeux énergie/climat.) {{< /citation >}}

{{< citation qui="PermaLab (2021)" couleur="#fe0000" imgderriere="/images/uploads/1630441323263_permalab-baseline.jpg" >}}

MagnyÉthique est un collectif que nous, PermaLab, bureau d’étude et de formation, accompagnons depuis presque ses débuts au château de Magny à Cublize lors de l’été 2019. L’ambitieux projet du collectif d’habitants d’investir cette immense demeure pour en faire un lieu d’habitat, de transition, d’accueil, de culture et de biodiversité a trouvé écho dans nos activités d’accompagnement au design et à la mise en œuvre de projets en permaculture. En effet, autour de la bâtisse s’étendent près de 8ha que le collectif veut valoriser dans un projet agricole essentiellement d’autoproduction et de régénération, basé sur les principes de l’agroforesterie et de l’agroécologie. Plusieurs membres du collectif ont créé la commission agricole pour se former et travailler sur la création du projet et l’aménagement des terrains. Mais c’est bel et bien tout le collectif qui s’est initié à la permaculture afin de comprendre la démarche lors de deux formations d’une semaine, ouvertes au public, assurées par PermaLab en 2019 et 2020. Le travail de design suit son cours, avec les premières mises en culture d’un espace de maraîchage pour la consommation des habitants. Le projet murit actuellement l’implantation de haies fruitières, de vergers diversifiés conservatoires et d’une partie de reforestation. La gestion de l’eau est aussi une priorité avec l’objectif de collecter les eaux de pluie et de ruissellement pour les conserver à la parcelle, mais aussi accueillir plus de biodiversité par la présence de mares permanentes ou semi-permanentes. Ce travail de design impliquant, supervisé par PermaLab, permet aux habitants de s’approprier leur futur projet sur tous ces aspects techniques, mais aussi collaboratif avec divers acteurs de la région (agriculteurs, associations,…).

Le rendu du design en début d’automne 2021 à l’ensemble du collectif d’habitantS permettra d’acter les prochains travaux ou plantations collectives sur les prochaines années. Àl’image de nombreux projets que PermaLab accompagne, nous serons aussi présent au besoin pour coordonner la mise en œuvre aux côtés des habitants et volontaires.

Grâce à ce travail, MagnyÉthique souhaite devenir un lieu ressources dans les années à venir où chacun pourra venir observer la biodiversité cultivée et naturelle, se former aux approches d’agroécologie et de permaculture, voire s’impliquer dans le développement des divers projets agricoles possibles sur le lieu. Diverses formations thématiques, assurées par PermaLab ou d’autres acteurs, et un jour par les « MagnyÉhs » eux-mêmeS, auront lieu dans les prochaines années, avec comme support le terrain aménagé et l’expérience du projet accumulée.

Simon, de [PermaLab](https://permalab.frhttps://permalab.fr) (Bureau d'études et de formation en permaculture, agriculture régénérative et hydrologie régénérative) {{< /citation >}}

{{< citation qui="Le quartier métisseur (2020)" couleur="#fcdd08" imgderriere="/images/partenaires/quartiermetisseur.png" >}}

Bonjour MagnyEthique. Permets nous de te tutoyer.

Bienvenue à bord, heureux de te voir ici en Haut Beaujolais tout vert.

Heureux de te voir prendre place en haut de cette colline au creux de ce château, auquel on s’était habitué vide. Merci de le remplir de vie, de rires d'enfants, de rideaux colorés aux fenêtres et de pleins de projets.

Curieux de ce que tu vas y construire, nouvelles formes d'habitat, de vivre ensemble, de vivre avec.

Merci d’être là Magnyéthique, avec nous. La vie nous attend.

Amitiés et embrassades

Tereska et Wladek Rozwadowska Potocki

Pour [Quartier Métisseur](http://quartiermetisseur.mystrikingly.com/) et autres (Lamure sur Azergues) {{< /citation >}}

{{< citation qui="La ferme du Suchel (2020)" couleur="#5f9c3e" imgderriere="https://cloud.magnyethique.org/s/yHNKKjz6oLBLDzn/preview" >}}

Je ne saurais dire si c'est cette impression d'arriver dans un lieu un peu hors du temps qui vous interpelle avant même votre arrivée, si ce sont les petits panneaux faits maison à l'entrée qui vous disent que vous serez très certainement bien accueillis, si ce sont ces voix d'enfants jouant dans une cour qui résonnent à nos oreilles comme un appel à aller vers les choses les plus enthousiasmantes de la vie, si c'est l'ambiance chaleureuse que l'on retrouve quand on rencontre les cohabitants, ou encore si c'est ce potentiel immense que développe ce lieu et les gens qui construisent ce projet (avec de solides pierres de gouvernance partagée, de permaculture globale…), mais ce qui est certain, dans tout cela, c'est que la cohérence globale donne le rythme à ce projet des plus complet !

La route s'annonce encore longue avant d'avoir un projet abouti pour toutes les familles (les travaux sont immenses), mais la route compte autant que la destination et le collectif semble avoir les épaules pour une réussite, sur place, ou avec leur réseau de partenaires !

Alexandre, de [la ferme du Suchel](https://lesuchel.tumblr.com/) (Projet global en permaculture, habitat partagé et maraîchage, sur la commune de Valsonne, Beaujolais vert)" {{< /citation >}}

{{< citation qui="Beaujolais Vert Votre Avenir (2020)" couleur="#eb6f07" imgderriere="/images/partenaires/bvva.png">}}

Nous qui cherchons à soutenir et accompagner l’accueil de nouveaux habitants, de nouveaux projets, pour soutenir et développer la vie de nos villages en Beaujolais, on peut dire que vous êtes pile poil les personnes que l’on souhaite accueillir !

Votre projet s’inscrit tout juste dans les intentions que nous portons :

*   recréer des activités dans des lieux qui étaient vides : c’est ce que vous faites en redonnant vie au Château de Magny
*   devenir un éco-territoire et soutenir une économie locale qui va dans le sens du développement durable : c’est votre projet d’éco-lieu !

Alors nous sommes à vos côtés pour vous aider à relever ce défi participatif, professionnel et écologique si inspirant et modélisant pour notre territoire !

[Beaujolais Vert Votre Avenir](https://www.beaujolais-vertvotreavenir.com) est une démarche portée par les intercommunalités du Pays Beaujolais. Une équipe dédiée à ce programme anime et met en réseau les acteurs locaux pour créer un véritable écosystème autour de l’accueil et de l’accompagnement des projets et des familles dans les village. {{< /citation>}}
