---
title: 'Présentation du projet, échanges et visite'
subtitle: Fête des possibles & Journée européenne de l'habitat participatif
date: '2021-07-17'
tags:
  - échanges
  - rencontre
---
Nous organisons une journée de visite et rencontre "Portes ouvertes" le **dimanche 26 septemble 2021 de 14h30 à 17h30**.

Cet événement s'inscrit dans le cadre de deux de plus grandes ampleur :

*   [La Fête des Possibles 2021](https://fete-des-possibles.org)
*   [Les journées européennes de l'Habitat Participatif](https://www.habitatparticipatif-france.fr/?JourneesPortesOuvertesEuropeennesDeLHabit)

![](/images/uploads/1630055609932_HPFJPO_JPO_2021_20210618095430_20210618095506.jpg)  

**Au programme à l'écolieu :**  
Présentation du projet, échanges et **trois visites à 14h30, 15h30 et 16h30**.

[**Inscription obligatoire**](https://docs.google.com/forms/d/e/1FAIpQLScec8bms4PhKbP6Akc9rgEUa4Uo5JI6rkAfYMUPRpJ4gmYuMQ/viewform). Ceci nous permettra de vous accueillir au mieux. Merci !
