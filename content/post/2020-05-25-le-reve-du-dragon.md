---
title: Le rêve du Dragon
date: '2020-05-15'
tags:
  - vie de groupe
  - formation
  - échanges
  - gouvernance
  - permaculture
---
Caroline, Katia, Manon et Mathieu ont participé début mai à une conférence/atelier organisée à distance par Julien Berlusconi, motivologue, autour d'une méthode de conception de projet appelée « [Rêve du Dragon](https://revedudragon.org) ».

![](https://lh5.googleusercontent.com/wFJTzFRZMyrXujPmLh6pRdmlB6YjFM9MUEcxoR0RwiflJ6otR-SR_lgBynaZx65Y-1PWKrlrFW126LriOFd81ASsq1Lw3A6kUf6Miq8zc8-mFOJS5G1PCcV9frpnyGtSRM5jnhWS)

Cette méthode a été créée dans les années 90 par John Croft, australien, qui s'est inspiré de la culture ancestrale aborigène et de nombreuses recherches autour de la permaculture, l'éducation nouvelle etc, pour proposer une réponse face aux enjeux écologiques, économiques et humains et à la problématique suivante : Comment favoriser la réussite de projets au service du changement et de la résilience ? 

  

_Le rêve du dragon_ a vocation de permettre la création d'organisations humaines collaboratives où se développent l'entraide et l'intelligence collective ; d'aider l'individu à prendre soin de lui, à se réconcilier avec son intuition et sa créativité, et enfin de soutenir le vivant, en participant à rétablir l'équilibre entre l'humain et la nature.

  

La méthode propose de s'appuyer sur 4 étapes fondamentales dans la création de projet : le rêve, la planification, l'action et la célébration. 

En donnant toute leur place aux étapes du rêve et de la célébration, par exemple, l’enthousiasme et la motivation sont renforcées dans les groupes et les actions plus durables. 

Nous mesurons, dans notre groupe MagnyÉthique, tout l'intérêt de mettre en place des moments réguliers de bilan et de célébration afin de prendre conscience de ce qui se transforme en soi et dans le groupe, des nouvelles compétences acquises...

  

La méthode permet aussi de nous confronter aux « dragons » : nos peurs et nos croyances limitantes, qui peuvent être à l'origine de conflits dans un groupe !

  

Pour finir l'atelier, nous avons eu l'opportunité de vivre un « cercle de rêves » dans lequel nous avons partagé chacun notre tour nos rêves les plus fous ! 

Car combien de rêves ne voient pas le jour parce qu’on n'ose pas en parler ?

  

Matrice du rêve du dragon

![](https://lh6.googleusercontent.com/4h1KtMV_BWFecKFK0cHO6wMcGsDKTiRUSOjnf4iS5R3iND0xs6-v0iI_Oc8UzsxAexJ1BSRgWGgIvZSCulr-h_tEhM1KWFiOLUPUhVSz-a-JFCHa_MpMTyef3yQdVkCJ8RXct9gb)
