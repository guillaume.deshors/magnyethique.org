---
title: Formation gouvernance
tags:
  - formation
  - gouvernance
date: '2018-05-19'
---
![](/images/uploads/image16.jpg)

Tristan Rechid, riche de son expérience démocratique dans la ville de Saillans, est formateur à l'animation de la démocratie locale au sein de l'association [Démocraties Vivantes](https://www.democratiesvivantes.com "https://www.democratiesvivantes.com"). C'est en cette qualité qu'il est intervenu pour nous aider à animer une journée de travail lors de laquelle il nous a présenté certains outils que nous ne connaissions pas encore (l'ordre du jour par tension) et nous donner des conseils concrets et individualisés pour pallier certaines difficultés d'organisation. 

Sa présence, dans une phase si difficile et particulière dans laquelle le groupe initial des _Fabriqués_ s'est scindé, a été précieuse pour fixer de nouveau objectifs et un calendrier des tâches. Merci encore!
