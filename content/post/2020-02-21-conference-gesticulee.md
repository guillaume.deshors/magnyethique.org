---
title: "Sculpteurs de mondes"
subtitle: Conférence gesticulée (12 mars)
date: '2020-02-21'
tags:
  - conférence
  - culture
  - société
---

MagnyEthique a le plaisir d'accueillir Christian Lefaure **le 12 mars 2020 à 19h** pour sa [conférence gesticulée](http://www.valleeducousin.fr/spip.php?article561) 'Sculpteurs de mondes - L'expérience d'un marcheur colporteur'.

{{< load-photoswipe >}}
{{< gallery hover-effect="grow" caption-effect="affiche" >}}
{{< figure link="/images/uploads/sculpteurs_de_mondes.png" caption="Affiche" >}}
{{< /gallery >}}

Vous êtes invités à venir nombreux avec ou sans réservation. Participation libre, buvette, adhésion obligatoire à l'association Magnyethique (à partir de 2€).

{{< citation qui="Christian Lefaure">}} 

4850 km à pieds, après mes 70 ans, en France à la rencontre d’alternatives dans tous les
domaines : depuis le paysan boulanger et la ferme de permaculture, jusqu’aux fab-labs et à
un lycée public innovant en passant par les monnaies locales complémentaires, la
production d’énergie  citoyenne, les espaces-de-travail partagés, un logiciel de dialogue
pour tous les acteurs de la vie locale, les éco quartiers avec habitat participatif, des
entreprises sans hiérarchie, les associations ville en transition, une EPHAD et des
associations éco responsable(s)…

Les questions que je me posais et que je leur posais :
Comment marche votre initiative aujourd’hui ? Quels sont les moteurs ? Quels sont les
freins ? 

J’ai appelé cette conférence gesticulée
‘Sculpteurs de monde’ ; l’expérience d’un ‘marcheur - colporteur’.

Elle allie témoignage du marcheur, témoignage du sculpteur (je sculpte sur bois), et
réflexions sur notre société et notre avenir lors d’une présentation avec une dimension
théâtrale. Elle décrit quelques unes des 200 expériences alternatives que j’ai rencontrées et
cherche à répondre aux questions précédentes ; elle témoigne aussi de la richesse de toutes
ces rencontres recherchées ou totalement inopinées.
 
Ces 4850 km sur 11 mois, m’ont amené en particulier à m’interroger sur la notion de
‘communs’ que j’ai croisée un peu partout et qui ré émerge en ce moment et sur les outils
juridiques à notre disposition pour appréhender et gérer ces ‘communs’.
La question clef à se poser à l’issue de la conférence pourrait être :
‘Sous quelles conditions un autre monde est-il possible ?’ ‘Sous quelles conditions ces
expériences vont-elles rester des expérimentations ou sont-elles déjà les prémisses d’un
autre monde ?’

{{< /citation >}} 
