---
title: Salon Primevère
subtitle: Du vendredi 23 au dimanche 25 février 2018
tags:
  - conférence
  - gouvernance
  - société
date: '2018-02-25'
---
![](/images/uploads/image13.jpg)

Le salon Primevère de l'alter-écologie a présenté sa 32ème édition à Eurexpo Lyon. Certains d'entre nous s'y sont rendus chercher des réponses, contacts et autres inspirations. Voici un extrait en rapport direct avec le groupe seulement car bien d'autres choses étaient intéressantes et transposables, que nous ne développerons cependant pas ici.

Un [programme](http://salonprimevere.org/salon_programme "http://salonprimevere.org/salon_programme") riche et varié:

*   l'association Scicabulle présentait un atelier sur l'animation participative, proposant d'étudier la démarche et le sens. Un Processus proposé en 4 étapes :

1\. Exister : jeux brise glace pour être reconnu en tant que personne à part entière dans le groupe sans trop d'implication

2\. Participer : Brainstorming autour de ce que sont (ou pas) "animer" et "participer". Études des différents niveaux de participation (8 étapes) et savoir quel niveau choisir en fonction du public et des objectifs de la réunion. Différents outils ont été présentés pour rendre une réunion plus participative : débats mouvants, binaire ou sur un axe, brainstormings avec post it…

3\. Interroger : donner la parole à chacun avec différents outils. Utilisation d'un sablier pour un temps de parole égal et une visibilité du temps.

4\. Construire : "World Café" : réflexion en petits groupes sur un thème qui change de groupe en groupe dans un temps imparti, les réactions des groupes implémentent la réflexion, puis porte-parole de chaque groupe.

*   plusieurs intervenants ont parlé vendredi soir lors d'une conférence sur les coopératives immobilières.

Ce fut aussi l'occasion de rencontrer d'autres oasis de vie, de se renseigner auprès d'habitat et partage, habicoop, cohab'titude (mise en réseau d'habitat participatif et personnes ressources en région lyonnaise). De nombreuses pistes donc, concrètes et pour tisser des liens avec d'autres organisations.
