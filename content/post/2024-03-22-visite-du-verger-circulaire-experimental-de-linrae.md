---
title: Visite du verger circulaire expérimental de l'INRAE
date: '2024-03-22'
tags:
  - biodiversité
  - agriculture biologique
  - permaculture
  - recherche scientifique
  - échanges
subtitle: Verger zéro phyto
---
Le jeudi 21 mars, sur invitation de nos partenaires [Les Croqueurs de Pommes du St Genois](https://vergersaintgenois.com), Katia a pu se rendre en leur compagnie à Gotheron, dans la Drôme. C'est là qu'est situé un grand centre de recherche en agronomie de l'INRAE, portant notamment sur la culture des fruitiers.

Depuis plusieurs décennies, l'INRAE tente de trouver des moyens pour limiter les interventions phytosanitaires sur les cultures de fruitiers. D'abord par sélection variétale, puis en optimisant les moments d'intervention permettant de réduire les traitements à 40 par an en biologique, à 45 en conventionnel. Mais même en biologique, les traitements ont des effets néfastes, notamment sur la santé des sols.

![](https://cloud.magnyethique.org/s/bDzLtwXjq5PMcsp/preview)

En 2015, l'INRAE s'est donc lancé dans une nouvelle expérimentation avec un verger circulaire "zéro phyto", d'abord réfléchi et conçu pendant 3 ans avant d'être planté et surveillé dans les moindres détails, réadaptés en fonction des aléas depuis maintenant 6 ans.

Le groupe des Croqueurs de pommes y a organisé une visite pour pouvoir comprendre des phénomènes, piocher des idées, s'inspirer. La visite, particulièrement généreuse, a permis d'aborder de nombreuses questions : variétés, associations végétales, alternance des cultures dans l'espace, installations de biodiversité favorisant les prédateurs des parasites…

Une visite particulièrement riche à lire plus en détail [ici](https://docs.google.com/document/d/1WYc_dPqc10neHfz2ek6Y9wVEN3U5p1hwde4E0GlUma8/edit?usp=sharing) pour les curieux·ses : de quoi réfléchir, s'inspirer pour mener les vergers des Croqueurs de Pommes et de MagnyÉthique à l'aune de ces enseignements.
