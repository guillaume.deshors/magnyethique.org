---
title: Willkommen
date: '2019-02-03'
---
Herzlich willkommen auf der Seite unserer News!
Zur Zeit bitten wir euch, uns etwas Zeit für die Bearbeitung der
dringenden Aufgaben im Schloss zu lassen.
Daher können wir uns nicht um Übersetzungen unserer Neuigkeiten kümmern.
Testet eure Französischkenntnisse oder nutzt einen 
Übersetzer, zum Beispiel https://www.deepl.com/translator#fr/de.
