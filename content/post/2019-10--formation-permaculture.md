---
title: 'Formation permaculture, deuxième édition'
subtitle: Du samedi 25 avril au vendredi 1er mai 2020 - reportée du 4 au 10 juillet 2020
date: '2019-10-25'
tags:
  - permaculture
  - formation
---
Nous avons le plaisir de vous annoncer qu'une seconde session de formation permaculture sera organisée par notre nouvel écolieu au printemps. Elle aura lieu grâce à l’intervention du [Permalab](http://permalab.fr/).

Quand?
======

Du samedi 25 avril au vendredi 1er mai 2020 de 9 à 18h chaque jour.

Où?
===

À l'écolieu au de Magny à Cublize (69).

Intervenants
============

Chan Sac Balam, formateur/designer en permaculture (PermaLab), Simon Ricard, agronome (PermaLab) et Samuel Bonvoisin, agronome (PermaLab), co-fondateurs de l’Oasis de Serendip (26).

Contenu de la formation
=======================

Il est possible de s’inscrire soit pour l’initiation seulement, soit pour l'initiation et l'approfondissement. L'initiation correspond aux deux premiers jours de la formation donnée sur le week-end. Le contenu est centré sur la découverte de l’éthique et des principes de la permaculture, ainsi que des méthodes de conception utilisées. Une grande part des contenus de ce module d’initiation est théorique.

Les cinq jours d’approfondissement de la semaine alterneront moments de théorie et de pratique. À l’issue de ce stage vous saurez :

*   Ce qu’est la permaculture, et ce qu’elle n’est pas (initiation) !
*   Manipuler les principes de la permaculture, et les utiliser dans votre propre contexte (approfondissement).
*   Comment débuter un projet de production vivrière sur un terrain, quelle que soit sa taille.
*   Comment maximiser les ressources en eau, et les utiliser sur votre terrain.
*   Quand et comment installer et gérer des arbres sur votre terrain.
*   Comment favoriser la vie d’un sol, et sa fertilité.
*   Comment produire vos propres purins et autres préparations permettant de soigner vos plantations.
*   Comment s’inspirer des motifs présents dans la nature pour concevoir des (éco-)systèmes.
*   Comment utiliser les outils de la permaculture humaine pour organiser votre vie.
*   Comment utiliser les outils de la permaculture humaine pour favoriser le vivre ensemble.

Hébergement
===========

Possibilité d’hébergement en camping ou en dortoir sur place, ou dans des gîtes à proximité selon le confort souhaité (n’hésitez pas à nous contacter pour en savoir plus!).

Repas
=====

L’organisation du stage prend en charge les repas pendant la durée de la formation. Les repas de midi seront préparés par un cuisinier, ceux du soir en gestion collective. Ils seront végétariens et préparés à base de produits locaux et/ou bio.

Tarif
=====

Notre groupe MagnyÉthique a choisi de faire bénéficier à certains de ses membres de cette formation dirigée par le Permalab afin de pouvoir ensuite mettre en place une gestion responsable et raisonnée des 8 hectares de l’écolieu. Nous avons décidé d’en faire profiter des personnes extérieures sur la base d’un prix libre et conscient. Chaque participant sera libre de donner la somme qu’il pensera être adaptée en son âme et conscience à la formation et à l’accueil qu’il aura reçus. Un temps dédié à l’explication de ce processus et des coûts du stage aura lieu le dernier jour de stage. Si vous le désirez, nous pouvons vous envoyer un document qui présente de façon plus complète notre démarche pour le prix libre et conscient ainsi que le prévisionnel des frais engendrés par la formation.

Places et Inscriptions
======================

La formation initiale (2 jours) peut accueillir en tout 15 personnes extérieures au groupe.

La formation pratique (2+5 jours) peut en accueillir 10. Les personnes s’inscrivant à la semaine complète seront prioritaires.

Formulaire de pré-inscription à remplir [sur ce lien](https://forms.gle/3fF3YUPdDFKNKnso7)
