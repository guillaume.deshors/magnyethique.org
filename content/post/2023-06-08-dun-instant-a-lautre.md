---
title: Spectacle D'un Instant à l'autre
subtitle: Vendredi 16 juin 2023 à 18h
date: '2023-06-08'
tags:
  - culture
  - spectacle
  - résidence
---

![](https://cloud.magnyethique.org/s/gXmgS8Tea9jcEK8/preview)

# D'un Instant à l'Autre #
## spectacle tout public et multidisciplinaire ##
## vendredi 16 juin 2023 à 18h ##


Un trio aux facettes multiples invite le public à s’interroger sur sa relation au temps. Théâtre, musique, manipulation d'objets et marionnettes colorent des tableaux absurdes, profonds et poétiques. Un spectacle tout public de 40 minutes, léger et humoristique, où l'on ne voit pas passer le temps sauf quand on le tient entre ses mains !

Cette création vient tout juste d'être produite et montée dans le cadre des paniers culturels, proposés par [Ouvrir l'Horizon AURA](https://www.ouvrirlhorizon-aura.fr/).
Emprunt du modèle de la distribution alimentaire en circuit court, les professionnel.les du spectacle se produisent à proximité de leur espace de vie, et participent à l’élaboration d’un écosystème artistique. Comme pour les paniers de légumes des AMAP, le public aura la surprise de la découverte du spectacle.  
Lien facebook de l’association : https://www.facebook.com/Paniersartistiques 

Ce sera la première réprésentation !

Avec Marjolaine Larranaga (jeu, marionnette,musique) Bruno Labouret (jeu, jongleries,  musique) Jérémy Lhuillier (régie, création sonore, jeu, musique)
Regard extérieur : Lucie Philippe

Jauge limitée, réservation indispensable par mail à [2023panierculturel@magnyethique.org](mailto:2023panierculturel@magnyethique.org)
