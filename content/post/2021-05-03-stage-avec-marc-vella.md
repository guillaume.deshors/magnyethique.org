---
title: Stagiaire dans une formation de Marc Vella
subtitle: '"Rendre belles les mauvaises notes de la vie"'
date: '2021-05-08'
tags:
  - formation
  - stage
  - conférence
  - culture
  - musique
---
Bruno a participé les 3 et 4 mars derniers au stage de [Marc Vella](https://www.marcvella.com/FR/marc-vella.html) à la [Ferme de Peuton](https://fermedepeuton.com) (Mayenne). Voici le retour enthousiaste qu'il en a fait :

"_J'ai très envie de partager ce que j'ai vécu durant le stage effectué tout récemment avec le pianiste improvisateur Marc Vella._

_Prenant la relation à la musique et au piano comme point de départ, Marc accompagne chacun·e dans son rapport à soi-même, avec une grande finesse et une infinie bienveillance. Ces deux jours ont été riches de nombreux moments de grâce et d'émotion, et nous avons découvert des clés concrètes pour prendre plaisir au clavier tout de suite, même en l'absence totale de connaissances musicales. Je recommande vraiment ce stage à tous et toutes, musicien·ne ou non : il est à la fois une ouverture fertile sur l'univers de la musique improvisée et un enseignement joyeux à accueillir les " fausses notes " de la vie._

_Je suis enchanté à la perspective de le faire venir à MagnyÉthique prochainement pour qu'encore plus de personnes profitent de sa pédagogie !_"

  

* * *

  

Et pour vous faire envie, voici la présentation qui était faite du stage de mars, avant de vous annoncer bientôt celui qui se tiendra à l'écolieu à l'automne !

"Qui, dans sa vie, n'a pas subi ou fait une fausse note au point d'éprouver colère et culpabilité ? En effet, qui n'a pas été maladroit, insuffisant, défaillant ? Qui n'a pas manqué de discernement ? Qui n'a pas été blessé, offensé ? Et si toutes ces fausses notes étaient une chance ? Et si elles étaient notre plus grande aventure donnant un sens à notre vie ?

La manière dont nous digérons l'histoire de notre vie détermine son rayonnement.  
Vous recevrez des clés d'être précieuses qui vous aideront à rendre belles les fausses notes de la vie.

Un musicien est avant tout quelqu'un qui entre en amour avec le silence.

![](https://web.archive.org/web/20201204071736im_/http://fermedepeuton.com/wp-content/uploads/2019/10/marc-vella-shop.jpg)

Stage dans lequel nous apprendrons à nous libérer de nos peurs et de nos croyances limitantes. En très peu de temps, des personnes n'ayant jamais fait de musique pourront improviser au piano et les

professionnels formatés par l'apprentissage auront une plus grande liberté d'expression.physique ou l'âge. Débutants bienvenus."
