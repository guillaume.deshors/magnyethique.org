---
title: Concert du Boris Vian Pataphysique 4tet
subtitle: Vendredi 6 septembre 2024 à 20h
date: '2024-08-25'
tags:
  - concert
  - culture
---
Le Boris Vian Pataphysique 4tet, c'est du jazz et de la fougue : tout un poème !

Le groupe viendra à l'écolieu le vendredi 6 septembre 2024 pour un concert de fin de (p)résidence après une semaine de travail dans les locaux du [Quartier Métisseur](https://quartiermetisseur.mystrikingly.com). Un concert sans aucun doute pétillant et loufoque, participation au chapeau pour les musiciens.

  

![](https://cloud.magnyethique.org/s/JYdKfa3YTDqfncz/preview)

Le concert sera suivi par un boeuf musical : ramenez vos instruments et chauffez vos voix !

Buvette sur place gérée par les MagnyÉth' avec la bière de la microbrasserie _Amalthée_ du Quartier Métisseur.
