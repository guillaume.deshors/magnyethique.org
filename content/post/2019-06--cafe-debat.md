---
title: Café-débat à la MJC d'Amplepluis
date: '2019-06-01'
tags:
  - échanges
  - promotion
  - réseau local
---
La MJC d'Amplepluis nous a fait l'honneur de nous inviter pour leur dernier café débat de l'année 2018-2019. Nous y avons présenté notre projet: qu'est-ce que l'habitat participatif? Qu'est-ce qu'un écolieu? Quel est la spécificité du nôtre? Comment envisage-t-on son fonctionnement? Que voulons-nous proposer au niveau local?

Notre intervention a eu lieu le samedi 8 juin 2019 face une une quinzaine d'intéressé.es et/ou curieu.ses Elle était suivie, comme tous les cafés-débats proposés par la MJC, d'un repas partagé qui a été particulièrement chaleureux et nous a permis d'approfondir les discussions avec les un.es et les autres.

Merci aux personnes qui ont fait le déplacement et surtout à la MJC de nous avoir consacré toute une séance!
