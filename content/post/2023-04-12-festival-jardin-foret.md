---
title: Festival Jardin-Forêt
subtitle: Deux conférences rediffusées en direct à l'écolieu MagnyÉthique
date: '2023-04-12'
tags:
  - biodiversité
  - permaculture
  - conférence
  - écologie
  - réseau-local
---
**[Les Alvéoles](https://www.alveoles.fr)** est une association de passionné·e·s de permaculture qui a décidé pendant le confinement de transmettre pour donner plus d'élan à des pratiques respectueuses du vivant. Le [festival Jardin-Forêt](https://alveoles.fr/event/festival-jardin-foret-2-2023-04-18-2023-04-22-43/register) qui a vu le jour en novembre dernier pour sa première édition fait partie de cette démarche :

_Si, il y a quelques années, certain.e.s d’entre nous se demandaient “à quoi ressembleront nos jardins avec les changements climatiques ?”, chaque rapport du GIEC vient nous confirmer, si nous en avions encore besoin, que demain est déjà là._

_Dans ce contexte, nous sommes de plus en plus nombreuses et nombreux à entreprendre de faire évoluer nos pratiques jardinières pour donner plus de place à l’arbre. Alors pour dynamiser et accompagner ce mouvement, l’équipe des Alvéoles organise une nouvelle fois du 18 au 22 avril prochain un festival en ligne 100% dédié au jardin agroforestier._

![](https://cloud.magnyethique.org/s/w6atPortjeA4eoi/preview)

Des membres de la **commission agricole MagnyÉthique** suivent régulièrement les formations proposées par _Les Alvéoles_ et ont voulu répondre à leur appel d'élargir encore le cercle en proposant la diffusion de certaines conférences de la deuxième édition du Festival Jardin-Forêt, à qui veut se joindre à eux en salle restaurée.

Ils ont choisi deux conférences, et donc deux dates :

*   **Mardi 18 avril 2023 de 20h30 à 22h : Conférence d'Hervé Covès "La Métamorphose de notre monde"**

Hervé Covès, ingénieur agronome et moine franciscain, est un pure amoureux de la nature. Il prône la plantation d’arbres pour restaurer la vie des sols. Pour lui, il est temps d’aider les agriculteur·ice·s à restaurer la vie des sols afin que la nature puisse reprendre ses droits et faire ce qu’elle a toujours su faire : laisser pousser. Dans cette conférence, il partagera sa vision si positive du monde : "_La vie est belle !_", répète-t-il si souvent, afin de teinter d'espoir les changements en cours et de rendre à chacun·e sa force créatrice.

*   **Jeudi 20 avril 2023 de 18h à 19h30 : Conférence de Franck Nathié "La résilience alimentaire et le jardin forêt"**

Franck Nathié mène depuis 1999 des recherches en permaculture. Actuellement basé à Simplé, dans le nord-ouest de la France, il a fait de son terrain un laboratoire : gestion de l’eau, pépinière, synergie végétale, forêt comestible, apiculture, synergie humaine… Il y développe une vision holistique de la permaculture. Il a créé [Forêt nourricière](https://www.laforetnourriciere.org), centre de formation et de recherche à la permaculture.

**Arrivée** possible 15 minutes avant le début de chaque conférence.

**Tarif** : gratuit ou à prix libre et conscient dont le bénéfice sera partagé entre Les Alvéoles et MagnyÉthique

**Buvette** sur place
