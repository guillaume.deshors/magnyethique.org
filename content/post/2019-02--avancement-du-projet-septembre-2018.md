---
title: Avancement du projet - septembre 2018
date: '2018-09-30'
tags:
  - avancement
---
C'est le mois de la rentrée et après un été riche en rencontres avec les personnes qui nous avaient sollicités, nous avons engagé les visites de biens immobiliers.

  

  

Nous sommes à présent en mesure de relancer les rencontres publiques. Le dimanche 30 septembre, nous avons été ravis de rencontrer un bon nombre d'intéressés avec qui les échanges ont été parfois très intéressants. 

  

  

Nous nous sommes présentés, avons expliqué le projet, nos critères de recherche immobilière et le candidat potentiel sur lequel nous travaillons dur actuellement en vue, nous l'espérons, d'un prochain achat.
