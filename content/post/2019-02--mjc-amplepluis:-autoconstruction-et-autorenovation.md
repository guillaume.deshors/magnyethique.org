---
title: 'MJC Amplepluis: autoconstruction et autorénovation'
date: '2019-02-09'
tags:
  - aménagements
  - réseau-local
---
Samedi 9 février 2019, Noémie, Charlotte et Marie se sont rendues à la conférence-débat organisée par la [MJC d'Amplepluis](http://www.mjc-amplepuis.fr) autour du thème: "**Mettre un projet d’éco-construction et/ou d’éco-rénovation à sa portée : trouver l’équilibre entre faire soi-même et faire-faire**".

  

L'occasion une fois encore de faire des rencontres intéressantes, notamment d'artisans locaux proposant des accompagnements à l'auto-construction ou auto-rénovation. De nouvelles pistes pour les rénovations à venir.

  

Une fois du plus, nous sommes ravis de voir que la région offre de belles opportunités et qu'elle fait preuve d'un dynamisme indéniable en matière écologique.

  

Merci la MJC pour ces rencontres-débats mensuelles toujours très intéressantes!
