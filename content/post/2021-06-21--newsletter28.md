---
title: "Newsletter 28 - \U0001F64B‍♂️ Appel à volontaires chantiers participatifs rénovation juillet et août \U0001F528"
date: '2021-06-21T00:00:00.000Z'
tags:
  - newsletter
  - avancement
  - biodiversité
---
 

**Bonjour ami·es MagnyÉthiques**

[](#)

 

Nous poursuivons les chantiers participatifs en juillet et en août et nous cherchons activement des volontaires pour venir nous aider et partager des moments avec nous.

 

[](#)

![](/images/uploads/2021-06-21--newsletter28_img1)

[](#)

 

 

[](#)

**Qu'est-ce qu'un chantier participatif ?**

[](#)

C'est un évènement durant lequel des gens se retrouvent pour travailler ensemble, bénévolement et dans la convivialité.

Dans notre cas, nous sommes en train d'avancer plus particulièrement sur 2 appartements.

Au programme : montage de cloisons, électricité, plomberie, sous couche d'enduit puis enduit chaux sable, finitions ou encore, installation de VMC, cuisines et salle de bain...

Que tu sois bricoleur expérimenté ou que tu n'ai jamais touché à un marteau ou une truelle, c'est possible de venir participer !

![](/images/uploads/2021-06-21--newsletter28_img2)

[](#)

**On mange quoi et on dort où ?**

[](#)

![](/images/uploads/2021-06-21--newsletter28_img3)

Bien sûr, on vous accueille, on vous loge et on vous nourrit :-)

On mange bien ! à base de produits frais et bio.

C'est un des rôle du chantier que de faire à manger pour tous les partcipants et en général on se régale :-) Les repas sont pris en commun avec les habitants et les volontaires.

On s'adapte aux régimes particuliers (allergies, végétariens, végans…) et chacun.e en profite pour cuisiner et faire découvrir ses recettes préférées.

Pour dormir, nous avons 2 dortoirs et il possible de venir poser sa tente ou son camion.sur notre terrain.

[](#)

**Quelles sont les autres activités que les travaux ?**

[](#)

 

Bien sûr, il y a des occasions de :

*   Découvrir la région aux alentours (lac des sapins, balades dans le Beaujolais Vert...)
*   Faire des soirées jeux / musiques / discussions...
*   Échanger autour de notre habitat partagé
    
*   Participer à la vie quotidienne
*   Se détendre, lire, profiter de la nature, du jardin...
*   Faire de belles rencontres :-)

Tu peux donc venir aussi bien seul qu'en couple, en groupe et même en famille.

 

[](#)

**C'est quand ? Où m'inscrire ?**

[](#)

Les périodes des chantiers pour cet été :

*   3 semaines du **5 au 23 juillet**,
*   2 semaines du **9 au 20 août**

A toi de choisir si tu viens 3 jours, une semaine ou même 3 semaines.

  
Maintenant que tu as super envie de venir participer, tu peux t'inscrire via Twiza :  
 

 

[**T'inscrire au chantier via Twiza**](http://fr.twiza.org/ecolieu-magnyethique-au-coeur-du-beaujolais-vert,zs3301,11.html)

![](/images/uploads/2021-06-21--newsletter28_img4)

[](#)

 

 Ou nous envoyer un mail à [twiza@magnyethique.org](mailto:twiza@magnyethique.org) en retour de ce mail 📧

Et n'hésites pas à partager ce mail avec des personnes de ton entourage qui seraient intéressées pour venir à nos chantiers participatifs.

Merci pour votre soutien.

Les Magnyéth'

 

[](#)

![](/images/uploads/2021-06-21--newsletter28_img5)

[](#)

 
