---
title: Forum de Zegg - stage de base
subtitle: Du 7 au 10 mai 2023 
date: '2023-02-27'
tags:
  - stage
  - culture
  - formation
  - gouvernance
---

![](https://cloud.magnyethique.org/s/QWFr6wKH8FyRaFP/preview)


# Forum de ZEGG : stage de base #
## du 7 au 10 mai 2023 ##


Le Forum de Zegg est l'outil de facilitation qui a soutenu le processus de coexistence de la communauté ZEGG depuis sa création en 1978, offrant un espace pour la transformation culturelle, la gestion des émotions, la prévention et la résolution des conflits, et la créativité.

Le cours de base est une expérience conviviale de 4 jours, conçue pour rencontrer notre essence, se nourrir des miroirs de la diversité du groupe et approfondir les différentes modalités de
l'outil Forum de Zegg.

Le forum vise à créer une transparence sur les processus émotionnels des personnes au sein d'un groupe, en expérimentant différentes manières de partager, en accueillant la diversité et l'authenticité de chacun.e.

L'écovillage allemand ZEGG (Centre expérimental pour la création sociale et culturelle) a été créé pour expérimenter et établir les bases d'une culture sans violence et en coopération avec la nature. Les valeurs qui sous-tendent le travail du Forum sont la transparence et la confiance.


À qui s'adresse-t-il ?
---

* Individus ou groupes intéressé.e.s à générer des relations plus authentiques dans leur environnement.
* Personnes à la recherche d'une expérience de connaissance de soi dans la coexistence.
* Personnes qui souhaitent accéder à la formation à la facilitation de Forum de Zegg.

Qu'allons-nous faire ?
-----------

* Approfondir notre compréhension de la technique du Forum et de ses différentes modalités
* Explorer des moyens créatifs et transparents de communiquer les uns avec les autres
* Mieux comprendre nos émotions et celles des autres
* Utiliser des pratiques saines pour donner et recevoir du feedback
* Honorer, se montrer et grandir à partir du pouvoir de notre vulnérabilité.
* Prendre du plaisir au sein d'un groupe dans un espace sûr et de confiance.
* Célébrer qui je suis, qui nous sommes, célébrer la vie

Logistique :
------------

Ce stage aura lieu à [MagnyÉthique](www.magnyethique.org) en résidentiel : logement sur place en camping, véhicule aménagé ou dortoir de six à huit lits. Deux cuisiniers nous prépareront les répas végétariens. Il est possibles que des temps de stage soient organisés en soirée.

L'accueil se fera à partir de 18h samedi 6 au soir (pas de repas prévu samedi soir). Le stage débutera dimanche à 9h et terminera mercredi à 17h.
Le nombre de places est limité à 25.
Les chiens ne sont pas les bienvenus : si cela vous pose problème, il est impératif de nous contacter pour en discuter avant votre venue.

Formatrices : 
-----


### Ena Rivière Feder :
Facilitatrice de groupe, formée par l'IIFACE, facilitatrice de FORUM-ZEGG depuis 2018 et en formation en Process Work et Somatic Experiencing.
Elle vit actuellement à ZEGG et coordonne et facilite la formation à la facilitation du Forum ZEGG en Espagne et en France.
### Barbara Stützel :
Psychologue, chanteuse, avec une formation et une expérience dans le monde du théâtre. Plusieurs
séjours et travaux intenses en Amérique latine. Membre de ZEGG depuis 2001. Outre l'art, sa passion est d'ouvrir des espaces où les vérités humaines trouvent leur place et contribuent ainsi au développement et à l'épanouissement personnel.

Tarif :
-------

Ce stage est proposé en participation financière libre et consciente. L'inclusion est l'une de nos valeurs les plus importantes, c'est pourquoi ce cours a un prix échelonné.
Tu peux payer ce que tu veux en fonction de tes moyens. Cependant, pour que le cours soit inclusif tant pour les participant·e·s que pour les formatrices, nous vous donnons un prix suggéré afin que vous puissiez vous faire une idée de la valeur du cours. Si ta situation financière le permet, n'hésite pas à soutenir économiquement
pour que tout le monde puisse y participer.
Quelques indications à adapter à vos possibilités : 

* Coût pédagogique : prix suggéré 400€, prix solidaire 150€, prix soutien: 800€
* Repas : entre 55 et 90 € (7 repas de dimanche midi à mardi midi, plus petits-déjeuners)
* Hébergement et espaces de travail : 35 à 80 €

Inscription :
--------------

Les inscriptions se font en remplissant _[ce formulaire](https://framaforms.org/inscription-stage-de-base-forum-de-zegg-mai-2023-1674751037)_. Votre inscription sera effective après avoir complété ce formulaire et avoir réglé un acompte par virement, chèque ou HelloAsso (infos dans le formulaire):  

Pour tout renseignement ou question : _[2023zegg@magnyethique.org](mailto:2023zegg@magnyethique.org)_

