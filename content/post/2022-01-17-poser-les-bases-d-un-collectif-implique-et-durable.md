---
title: Poser les bases d'un collectif impliqué et durable
date: '2022-01-17'
tags:
  - formation
  - gouvernance
  - habitat participatif
  - vie de groupe
---
Caroline et Bruno (deux membres du groupe MagnyÉthique) vous proposent deux jours de stage en résidentiel au sein de l'écolieu autour du thème "Poser les bases d'un collectif impliqué et durable".
 
Les 12 et 13 mars, venez découvrir et vivre des outils qui favorisent confiance et coopération au sein de vos équipes, dans le milieu associatif ou professionnel, au sein d'un habitat participatif …
 
Vous y expérimenterez un ensemble cohérent de pratiques issues de la gouvernance partagée, de la Communication NonViolente, de l'intelligence collective et des systèmes restauratifs. Deux jours pour partir du bon pied dans vos aventures collectives ou pour pérenniser vos expériences !
 
Ce stage est proposé pour une quinzaine de personnes maximum, en participation libre et consciente.
 
![](https://cloud.magnyethique.org/s/iAtpZyEjxbqJ53z/preview)
 
### Objectifs : ###

* Découvrir des outils de gouvernance partagée et d’intelligence collective, et au-delà comprendre leurs intérêts, usages et limites.
* Acquérir une compréhension des enjeux et postures qui soutiennent leur mise en œuvre.
* Découvrir le système restauratif (au sens de Dominic Barter) de prévention et régulation des tensions au sein d’une communauté. En connaître les modalités d’élaboration et de mise en œuvre.
* Améliorer ses capacité de communiquer, identifier les obstacles à une communication inter-personnelle authentique et bienveilllante.

### Compétences : ###

* Être capable de présenter à un groupe les principaux outils de gouvernances partagée : décision par consentement, élection sans candidat, chapeaux de Bono, et de les animer.
* Avoir des éléments pour animer une réflexion collective sur les tensions et initier un système restauratif au sein d’une équipe.
* Exprimer un feed-back (positif ou négatif) au service d’une relation. Être en mesure de proposer à un groupe quelques outils pour favoriser une communication sincère et confiante.

### Détails pratiques : ###

Logement en dortoir d'une demi-douzaine de lits (merci de prévoir draps et literie, sac de couchage, …). Les repas des midis seront fournis, et la préparation de celui de samedi soir est intégrée au stage.
 
Votre inscription sera validée à la réception d'un chèque d'arrhes de 50 € (qui vous sera rendu lors de votre paiement) à l'ordre de Association MagnyÉthique, à envoyer à :
 
MagnyÉthique - stage Bases du Collectif  
 
Chemin du château de Magny  

69550 Cublize

N'hésitez pas à nous contacter si vous avez des questions par mail à [2022collectif@magnyethique.org](mailto:2022collectif@magnyethique.org) ou au téléphone au 06 62 28 36 08.
