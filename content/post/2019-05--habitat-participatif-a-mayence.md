---
title: Habitat participatif à Mayence
date: '2019-04-02'
tags:
  - habitat participatif
  - échanges
  - agriculture solidaire
bigimg:
  - desc: null
    src: /images/uploads/1558854092022_IMGP5071_resized.JPG
---
Katia, Pascal et leurs enfants ont passé quelques jours chez des habitants de l'habitat partagé Layenhof à Mayence en Allemagne. L'occasion d'échanger sur l'historique de leur projet créé en 1994, de voir ce qui fonctionne ou pas et d'essayer d'en comprendre certaines raisons. Les discussions ont été intenses et ont permis de parler sur la gouvernance, la conciliation entre privé et commun etc. Bref: de quoi alimenter les réflexions du groupe par des expériences et des avis extérieurs !

  

C'est aussi à cette occasion qu'ils ont pu visiter une AMAP (Solidarische Landwirtschaft) allemande et d'en étudier l'organisation.
