---
title: Avancement du projet - janvier 2019
date: '2019-01-19'
tags:
  - avancement
---
La fin d'année a été chargée et la nouvelle commence sur les chapeaux de roues! Avant d'en dire plus:

**Bonne année 2019 à tous!**

Décembre a été un mois de partage:

*   journée portes-ouvertes du 29 à en donner le vertige tellement vous étiez nombreux à venir: intéressés, curieux, en recherche de partenariats pro ou culturels! Merci, c'est super encourageant!
    
*   une session de fin d'année emprunte d'ouverture, de construction de confiance mutuelle, d'esprit constructif, de belles rencontres qui donnent foi dans le projet et dans l'humain au sein et autour de ce projet
    

Janvier?

*   **Commission coordination**: définir le calendrier jusqu'à l'été (au moins) en y plaçant tous les RDV de travail, de cohésion, avec des partenaires extérieurs etc.
*   **Commission gouvernance**: poursuivre l'intégration de nouveaux membres (déjà une nouvelle famille de papillons!), former les membres du groupe en interne aux modes de fonctionnement MagnyÉthique, élaborer un système restauratif adapté à notre groupe, trouver les bonnes personnes qui pourront nous accompagner et/ou nous former cette année sur cette dernière thématique
*   **Commission statuts et finances**: finalisation des statuts de la SCI en vue de la signature d'achat au plus tard en avril
*   **Commission aménagements**: poursuivre les études et l'organisation du début des travaux, répartir les espaces et placer les communs
*   **Commission communication**: terminer l'élaboration du nouveau site, envoyer la 2ème newsletter (pour vous y inscrire: par ici), fixer de nouveaux rendez-vous publiques, remplir les dossiers de subventions, définir le mode de choix du futur logo (voir fenêtre ci-dessous) et changer tous les documents actuels pour l'y intégrer
*   **Commission activités**: étudier les différentes possibilités d'intégration de projets pros, associatifs et culturels sur le lieu, chercher des partenaires
*   **Commission asso**: changer les statuts de l'asso (nom, adresse…), lancer la campagne d'adhésions 2019 (si vous voulez nous soutenir, par ici!)

Bref: du boulot, du concret, et une motivation au sommet!!!
