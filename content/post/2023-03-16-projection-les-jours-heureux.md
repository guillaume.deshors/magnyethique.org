---
title: Projection du film "Les jours heureux" de Gilles Perret
subtitle: En soutien aux grévistes de la réforme des retraites
date: '2023-03-16'
tags:
  - société
  - culture
  - échanges
---

**Vendredi 24 mars, à 20h45**, suivant une initiative des [Mutins de Pangée](https://www.lesmutins.org/des-films-en-soutien-aux-caisses-2593), Magnyethique organise la projection du film "[Les jours heureux](https://www.cinemutins.com/les-jours-heureux)" de Gilles Perret. Ce film de 2013 est malheureusement plus que jamais d'actualité. La projection est à prix libre et la recette sera versée à une [caisse de grève](https://caisse-solidarite.fr/). 

![](https://cloud.magnyethique.org/apps/files_sharing/publicpreview/q4HXtcZLTA45BTL?file=/2023/les_jours_heureux.jpg&fileId=25978&x=1920&y=1080&a=true)

## Synopsis ##

Entre mai 1943 et mars 1944, sur le territoire français encore occupé, seize hommes appartenant à tous les partis politiques, tous les syndicats et tous les mouvements de résistance vont changer durablement le visage de la France. Ils vont rédiger le programme du Conseil National de la Résistance intitulé magnifiquement : « Les jours heureux ».

Avec : Raymond Aubrac : résistant, Robert Chambeiron : secrétaire du CNR, Daniel Cordier : secrétaire de Jean Moulin, Jean-Louis Crémieux-Brilhac : résistant, Stéphane Hessel : résistant, Léon Landini : Résistant FTP MOI - Président du PRCF, Laurent Douzou : historien, Nicolas Offenstadt : historien, Christophe Ramaux : économiste, François Bayrou, Jean-François Copé, Nicolas Dupont-Aignan, François Hollande et Jean-Luc Mélenchon.

Ce programme est encore au cœur du système social français puisqu’il a donné naissance à la sécurité sociale, aux retraites par répartition, aux comités d’entreprises... Ce film vise à retracer le parcours de ces lois, pour en réhabiliter l’origine qui a aujourd’hui sombré dans l’oubli. Raconter comment une utopie folle dans cette période sombre devint réalité à la Libération. Raconter comment ce programme est démantelé depuis, questionner la réalité sociale d’aujourd’hui, et voir comment les valeurs universelles portées par ce programme pourraient irriguer le monde demain.
