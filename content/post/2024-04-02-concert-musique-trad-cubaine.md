---
title: Musique traditionnelle cubaine “Son”.
date: '2024-05-01'
tags:
  - concert
  - culture
  - échanges
subtitle: 'Groupe Esencia Sonera, le vendredi 12 juillet 2024'
---
Du côté de la musique populaire, la concurrence est rude à Cuba, mais s’il y a un tresero (interprète de la guitare cubaine, le Tres), qui s’élève toujours plus dans sa virtuosité, son sens du rythme et sa sensibilité, c’est bien Onelio Arias, 55 ans, le « pulpo de Maisí », qui dès l’âge de 6 ans laissait courir ses doigts sur les différents instruments familiaux et reproduisait d’oreille les mélodies qu’il entendait à la radio. De sa jeunesse à la pointe orientale de son île natale, il a toujours gardé un sens inouï de l’adaptation à toutes les musiques, aussi à l’aise quand il s’agit de se lancer dans de folles improvisations que pour faire danser les foules.

![](https://cloud.magnyethique.org/s/Ky2jcEPnDqmy4Jg/preview)  

La formation du groupe Esencia Sonera naît d’une volonté de formaliser un groupe familial et de proposer des thèmes cubains revisités, entre le populaire, le jazz et le classique.

La formation se compose de Onelio Arias (Tres), Beatriz Arias (Violon), Thalia Arias (Trompette), Miledis Milán (Percussions) et Lazhar Cherouana (Guitare).

En concert au château le **vendredi 12 juillet à 20h**

* * *

Atelier rythmes cubains **jeudi 18 juillet** clôturé par une jam session à 19h. Les inscriptions seront ouvertes dès l'arrivée des artistes.
