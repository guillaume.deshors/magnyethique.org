---
title: Avancement du projet - avril 2018
date: '2018-04-14'
tags:
  - avancement
---
Le groupe, après une phase de latence source de doutes, frustrations et conflits divers, relance la machine. Les va-et-vient auprès des banques pour tenter de répondre à toutes les exigences (Coopérative d'habitants ou SCI(A), financement...) ont mis le groupe en tension. Une médiation a débuté, certains membres nous ont malheureusement quittés mais nous amorçons une nouvelle phase de formation active dans diverses directions pour nous offrir les ressources pour rebondir (voir "actualités").

  

  

Des initiatives personnelles nous ont permis de tester plusieurs méthodes d'animation lors des dernières rencontres. Un profond travail de la commission gouvernance a remis de l'ordre dans nos modes de fonctionnement pour être plus efficaces au sein des commissions - et donc dans le travail de l'ensemble du groupe, les tâches sont toutes redéfinies, réparties et portées de manière collective. Les longs week-ends d'avril/mai seront placés sous le signe de la cohésion et des formations de groupe (voir "journées de travail et de cohésion"). En tout, ce sont 12 journées que nous allons partager intensément, lors desquelles nous allons nous former à la CNV (Communication Non Violente), au traitement des non-dits, approfondir nos connaissances autour de la gouvernance et tisser du lien entre tous les membres (chrysalides et papillons ainsi que quelques chenilles de passage).

  

  

Nous pesons les pour et contre des solutions pour rebondir face aux difficultés de financements, quitte à en passer par la recherche d'un autre lieu qui permettrait de réaliser le cœur de notre projet à moindre frais (moins de travaux, plus grand terrain, garder le même lieu mais différemment?). Toutes les pistes sont évoquées, étudiées, et un nouveau souffle est amorcé. Tous les membres restants sont particulièrement actifs au sein des diverses commissions, telles les abeilles au printemps après un dur hiver!

  

  

Nous espérons grâce à tout cela dépasser les tensions humaines naturelles qui ont fait surface dans cette phase délicate, passage malheureusement obligé de la plupart des groupes, semble-t-il.

  

  

Dans l'attente, les journées porte-ouverte sont mises entre parenthèses au moins jusqu'à juin. Ce qui ne nous empêche pas d'accueillir de nouveaux intéressés qui pourraient, s'ils le souhaitent, se joindre au groupe et contribuer à son nouvel élan. Si vous êtes de ceux-là, voyez notre calendrier de rencontres et contactez-nous pour nous rejoindre sur une journée, faire notre connaissance et celle du projet qui nous unit!

  

  

L'aventure continue!
