---
title: Forum de Zegg
subtitle: >-
  TRANSFORMATION CULTURELLE - GESTION EMOTIONNELLE DE GROUPE - EPANOUISSEMENT
  PERSONNEL
date: '2022-03-30'
tags:
  - formation
  - gouvernance
bigimg:
  - desc: null
    src: /images/uploads/1664452666886_ZEGG1.png
---
Bruno a d'abord découvert lors des Rencontres Oasis, puis creusé la méthode du **Forum de Zegg** au printemps à l'[Espace Rivoire](https://www.espacerivoire.fr). Voici son témoignage sur une expérience marquante et inspirante, qu'il fera probablement découvrir au groupe MagnyÉthique au fur et à mesure de son approfondissement :

> "J'ai connu le Forum de Zegg lors de la Rencontre des Oasis en 2021, où j'avais suivi un atelier d'une journée pour le découvrir. Le Forum de Zegg est une pratique élaborée à Zegg en Allemagne, collectif créé dans les années 90 pour expérimenter une transformation sociétale indissociable d'un travail sur soi.
> 
> Pour le décrire simplement, il s'agit de s'exprimer non pas assis sur sa chaise en périphérie du cercle mais debout au centre du cercle, avec un binôme de facilitateurices qui soutiennent l'expression et la prise de conscience de la personne au centre par différentes invitations : par exemple s'exprimer en courant, en marchant en arrière, en répétant un geste, en chantant, … Après être passé au centre, il y a la possibilité de recevoir des " miroirs " : les témoins peuvent à leur tour venir au centre pour partager offrir leur ressenti au service de la personne qui s'est exprimée.
> 
> J'ai été séduit par cette approche holistique, au-delà des mots et de l'intellect, en faisant la part belle au non-verbal et aux émotions. J'ai été bluffé par l'efficacité du processus, l'intensité vécue et la pertinence des propositions des facilitatrices, et j'ai eu envie d'aller plus loin pour mieux en comprendre le fonctionnement.
> 
> J'ai donc fait au printemps le " module de base " sur quatre jours proposé par deux habitantes de Zegg, et mon apprentissage a dépassé mes espérances : en plus de la pratique de différents types de forum, nous avons expérimentés différents jeux de connexion, de prise de confiance, qui visent à accroître notre capacité d'expression authentique. Nous avons eu aussi des apports théoriques multiples sur les relations de groupes, sur les émotions, la connaissance de soi, … Quatre jours denses et riches, qui m'ont permis de toucher la puissance de transformation du processus et sa pertinence dans un collectif.

![](https://cloud.magnyethique.org/s/SJMpCa9JtKSCAtJ/preview)

> Et je viens de me décider à prolonger encore pour me lancer dans la formation à la facilitation de ces forums. Je ne sais pas encore comment et quand j'aurai l'élan et la possibilité d'en animer, mais je sais que ce sera l'occasion pour moi de progresser et d'acquérir des outils précieux à ramener à MagnyÉthique !"
> 
> Si vous voulez plus d'informations, il y a une page web pour le forum de Zegg en France : [https://forumzegg.fr/](https://forumzegg.fr/)
