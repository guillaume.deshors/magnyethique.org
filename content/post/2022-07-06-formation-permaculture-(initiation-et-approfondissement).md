---
title: Formation permaculture (initiation et approfondissement)
subtitle: Les 15-16 puis du 17 au 20 octobre 2022
date: '2022-07-12'
tags:
  - formation
  - permaculture
---

Nouvelle session de formation
=============================

Nous avons le plaisir de vous annoncer qu’une quatrième session de formation permaculture sera organisée par notre écolieu en octobre. Elle aura lieu une nouvelle fois grâce à l’intervention de [Permalab](https://permalab.fr).

![](https://cloud.magnyethique.org/s/ym7y8YfJQiRD6E8/preview)  

Quand?
======

Du samedi 15 au jeudi 20 octobre 2022 de 8h30 à 18h chaque jour.

Où?
===

À l’écolieu au Château de Magny à Cublize (69).

Intervenants
============

Aurelia Diesnis, formatrice/designer en permaculture, Caroline Julien, formatrice, Coline Curinier, Designer et facilitatrice.

Contenu de la formation
=======================

Il est possible de s’inscrire soit seulement à l’initiation, soit à l’initiation et à l’approfondissement. L’initiation correspond aux deux premiers jours de la formation globale, donnés sur le week-end. Le contenu est centré sur la découverte de l’éthique et des principes de la permaculture, ainsi que l’initiation à la conception en permaculture. Ce module d’initiation est théorique avec des temps de co-conception et des ateliers pratiques.

2 jours d’initiation à la Permaculture
--------------------------------------

À l’issue de ces deux jours vous saurez ou connaîtrez :

*   ce qu’est la permaculture, et ce qu’elle n’est pas !
    
*   manipuler les principes de la permaculture, et les utiliser dans votre propre contexte
    
*   adopter l’état d’esprit du designer en permaculture
    
*   concevoir une feuille de route et comment démarrer un projet en permaculture
    
*   des outils de conception et intégrer les bases pour avoir envie d’aller plus loin
    

4 jours d’approfondissement
---------------------------

Les quatre jours d’approfondissement alterneront moments de théorie et de pratique (notamment à partir des plans de vos projets personnels). Ils s'articuleront autour des grandes thématiques "**L'eau**", "**Le sol**", "**L'arbre**", "**La biodiversité**".

À l’issue de ce stage vous saurez :

*   comment débuter un projet de production vivrière sur un terrain, quelle que soit sa taille.
    
*   comment maximiser les ressources en eau, et les utiliser sur votre terrain.
    
*   quand et comment installer et gérer des arbres sur votre terrain.
    
*   comment favoriser la vie d’un sol, et sa fertilité.
    
*   comment produire vos propres composts et vos propres biofertilisants permettant de soigner le sol et les plantes.
    
*   comment vous inspirer des motifs présents dans la nature pour concevoir des (éco-)systèmes.
    
*   comment créer un environnement favorisant la biodiversité à tous les étages en vous appuyant sur le vivant
    

Hébergement
===========

Possibilité d’hébergement en dortoir sur place, ou dans des gîtes à proximité selon le confort souhaité (n’hésitez pas à nous contacter pour en savoir plus !).

Repas
=====

Les repas du week-end se feront sur le principe de l’auberge espagnole.

L’organisation du stage prend en charge les repas pendant la durée de la formation d’approfondissement. Les repas de midi seront cuisinés et servis sur place (10€ le repas), ceux du soir en gestion collective à partir des ingrédients mis à disposition du groupe (5€ le repas). Ils seront végétariens et préparés à base de produits locaux et/ou bio.

Tarif
=====

Notre groupe MagnyÉthique a choisi de faire bénéficier à certains de ses membres de cette formation dirigée par PermaLab afin de pouvoir poursuivre la mise en place d’une gestion responsable et raisonnée des 8 hectares de l’écolieu. Nous avons décidé d’en faire profiter des personnes extérieures sur la base d’un prix libre et conscient. Chaque participant·e sera libre de donner la somme qu’il pensera être adaptée en son âme et conscience à la formation et à l’accueil qu’il·elle aura reçus (hébergement, petit déjeuner, collation, utilisation des salles, frais pédagogiques…). Un temps dédié à l’explication de ce processus et des coûts du stage aura lieu le dernier jour de stage. 
Si vous voulez des informations plus détaillées, vous pouvez consulter les documents suivants :

[Prix libre et conscient pour le week-end d'initiation](https://docs.google.com/document/d/12KtUHck4Wu-1ewIuRBDr8ik6XeFRsjIifgOYxBLfOXs/edit)

[Prix libre et conscient pour les six jours](https://docs.google.com/document/d/188LA_jtb9T48PDaXMh7mZ85kw3WOrOZfKUDrst4yUEo/edit)

Places et Inscriptions
======================

La formation initiale (2 jours) peut accueillir jusqu’à 20 personnes.

La formation pratique (2+4 jours) peut en accueillir 15. Les personnes s’inscrivant à la semaine complète seront prioritaires.

Formulaire de pré-inscription à remplir [sur ce lien](https://framaforms.org/magnyethique-pre-inscription-a-la-formation-permaculture-doctobre-2022-1657473402)

Covoiturage :
=============

Pour proposer une place dans votre voiture ou en demander une, RDV ici : [lien covoiturage](https://www.togetzer.com/index.php?view=organiser&slug=u9yv2g)
