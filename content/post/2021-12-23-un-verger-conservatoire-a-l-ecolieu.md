---
title: Un verger conservatoire à l'écolieu fin 2022
date: '2021-12-23'
tags:
  - avancement
  - permaculture
  - société
  - écologie
  - wwoofing
  - biodiversité
bigimg:
  - desc: Plantation
    src: /images/uploads/1640252056364_Plantation_verger.jpg
---
Pas une, mais deux bonnes nouvelles avant les fêtes, comme deux beaux cadeaux de Noël faits à l'écolieu.

Lundi, l'organisation [WWOOF France](https://wwoof.fr/fr/) a accepté notre candidature d'hôte. Nous allons donc pouvoir accueillir dès 2022 deux WWOOFeur.euse.s à l'écolieu pour gérer conjointement le maraichage et le soin aux arbres.

![](https://cloud.magnyethique.org/s/7SQPxx3Q2waef7o/preview)

Et à propos d'arbres, le [Fonds Germe](http://www.fonds-germes.org) nous a octroyé une aide conséquente de 8200 € pour le futur verger conservatoire ! Une aide précieuse qui nous permettra de planter à l'hiver 2022-23 les 115 arbres et près de 300 arbustes fruitiers, médicinaux et mellifères de variétés rares et/ou anciennes que nous avions prévus sur les plans suite au design permacole. Nous sommes ravis et très touchés de cette attribution que nous voyons comme un encouragement très net à poursuivre sur la voie que nous avons choisie.

![](https://cloud.magnyethique.org/s/MZ4dD7bkmHqgwWM/preview)

Cette année, nous avons déjà planté environ 300 arbres sur les terrains, en haies fruitières de subsistance pour le groupe et en haies bocagères et mellifères pour favoriser la biodiversité sur l'écolieu. Ces plantations ont pu être réalisées grâce à la sueur de volontaires pour la partie concrète, mais aussi grâce au soutien de nombreux donateurs ayant répondu à notre financement participatif de l'an dernier. La dotation du Fonds Germe va nous offrir la possibilité de planter encore, mais aussi de nous former à la taille et à la greffe des arbres fruitiers, à l'apiculture, et de nous procurer le matériel adapté aux soins de tous ces végétaux.

L'objectif final est multiple, tant écologique que social, et répond à un idéal pour l'avenir :

*   animer sur le lieu des ateliers de sensibilisation à la biodiversité
*   nourrir les personnes de passage à moindre coût (pour permettre à tous de participer, quelles que soient leurs accès à l'argent et développer ainsi le prix libre)
*   fournir des greffons de variétés végétales rares et anciennes à qui voudra, comme nous, les protéger pour les perpétuer

**Pour en savoir plus sur le Fonds Germe :**

*   [Choix de banque](https://cloud.magnyethique.org/s/X5ZYn5egnECwayo)
*   [Fonds Germe](https://cloud.magnyethique.org/s/afQSYTHPz7Xo28w)
