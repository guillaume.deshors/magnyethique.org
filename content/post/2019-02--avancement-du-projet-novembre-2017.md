---
title: Avancement du projet - novembre 2017
date: '2017-11-25'
tags:
  - avancement
---
Ce week-end, nous avons fait un point sur l'administratif. Le dossier est long à monter, l'achat est retardé. Il aura probablement lieu début 2018. Un peu déçus (nous espérions prendre possession des lieux autour du réveillon et cherchons donc un autre lieu où nous retrouver). Mais ceci ne remettant rien en question sur l'ensemble du projet, nous sommes toujours aussi motivés.

Une nouvelle commission a donc vu le jour qui allonge la liste des cinq commissions déjà existantes:

La commission activités va entamer la réflexion sur l'utilisation des espaces ouverts au public: école, coworking, crèche parentale, café associatif, ateliers et conférences, ouverture à la professionnalisation de certains domaines de notre écohabitat (transformation des surplus du potager et formation), recherche et finalisation de partenariats, utilisation d'une monnaie locale. La liste des pistes est surement encore longue mais ce sont les premiers projets qui nous animent.

Il restera à chaque porteur de projet la latitude de le gérer tel qu'il l'entend dans le cadre que définira la commission.
