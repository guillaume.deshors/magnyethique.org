---
title: Chantier participatif plantation - Théorie et pratique
subtitle: Dimanche 27 novembre et 9h30 à 16h
date: '2022-10-26'
tags:
  - biodiversité
  - permaculture
  - rencontre
  - écologie
  - échanges
---
Le dimanche 27 novembre 2022, nous invitons de 9h30 à 16h les personnes intéressées à venir nous aider sur un chantier participatif dédié à l'arboriculture, avec deux grands axes :

*   densification de haies bocagères
*   suite d'arpentage en "lignes clés" (méthode d'agencement des terrains pour capter l'eau de pluie, appelée "keyline") pour préparer la plantation du [verger conservatoire](https://magnyethique.org/post/2021-12-23-un-verger-conservatoire-a-l-ecolieu/) qui débutera cet hiver.

![](https://cloud.magnyethique.org/s/PXQ3Ty8tG4SsqYw/preview)

Lors de cette journée, nous vous proposerons quelques explications autour de la partie arboricole de notre design en permaculture et des techniques choisies (ou non) pour créer des haies bocagères et gérer la question hydrologique tellement cruciale pour l'avenir, notamment à l'aide de la "keyline".

Le repas de midi sera pris en charge par l'écolieu et partagé. Merci de vous inscrire via [ce lien](https://framaforms.org/plantation-haie-bocagere-1666888972) pour nous permettre de prévoir votre repas et de vous communiquer les dernières informations.
