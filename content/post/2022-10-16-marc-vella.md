
---
title: Stage et conférence-concert avec Marc Vella
subtitle: Samedi 18 et dimanche 19 mars 2023
date: '2022-10-16'
tags:
  - atelier
  - conférence
  - culture
  - stage
---


![](https://cloud.magnyethique.org/s/QfWQqfD2T5d7raJ/preview)  

[Marc Vella](https://www.marcvella.com/) est pianiste, prix de Rome en composition en 1999 et concertiste improvisateur. Son parcours singulier l'a amené à fonder "[l'école de la fausse note](https://ecoledelafaussenote.com/)" , et à monter des "Caravanes amoureuses" qui l'ont emmené à travers le monde avec son piano à queue pour partager la joie de la musique et du vivre ensemble. Nous avons le privilège de l'accueillir sur deux jours pour un stage et une conférence-concert.

Conférence-concert "le maître et la grâce" samedi 18 mars 2023 à 20h
======================

Marc Vella nous propose une soirée qui allie musique et témoignage : comment avoir foi en nous-même ? Comment rester enthousiaste ?
Cette soirée est encore à confirmer ; pour l'instant elle est prévue à MagnyÉthique, à 20h.
Tarif : au chapeau, prix indicatif 10€/personne.

Stage "Rendre belles les fausses notes de la vie" les 18 et 19 mars 2023
====================================

Ce stage est ouvert à tous et toutes, sans prérequis musical. Nous avons déjà accueilli Marc Vella à l'automne 2021 et nous avons été emballés par ce qu'il nous a fait vivre. Au-delà d'outils concrets et véritablement accessibles quel que soit le niveau musical, c'est surtout une posture et un état d'esprit qu'il nous transmet par son accompagnement sensible, généreux et profondément humain.
"Jouer sa propre musique pour savourer son intériorité. Un enseignement qui conduit à plus d'amour envers soi et les autres, qui connecte à l'instant présent, écrin de la grâce".

Et vous toucherez du doigt que "la grâce n'attend qu'une chose, c'est de vous visiter" !


Infos pratiques : 
==================

Le stage est limité à une quinzaine de personnes, et aura lieu de 9h à 18h samedi et dimanche.

Nous proposons à celleux qui le désireraient la possibilité de dormir sur place en dortoir. Pour profiter au mieux du temps de stage, nous incluons dans le stage deux repas végétariens à tendance biolocale pour samedi et dimanche midi. Pour le samedi soir, nous vous proposons une auberge espagnole : amenez vos spécialités et ce qui vous fera plaisir de partager.

En ce qui concerne le prix, nous vous demanderons 250€ pour les frais pédagogiques, 20€ pour les repas que nous proposerons, et une contribution libre aux frais engendrés par l'accueil : utilisation des salles, temps passé à l'organisation, logement et petit-déjeuner le cas échéant, soutien au projet MagnyÉthique et à la participation libre et consciente ; soit une part fixe de 270 € et une part laissée à votre libre appréciation.

Accès : 
=====

MagnyÉthique est proche de la ville d'Amplepuis, accessible en train et bus. Nous proposerons une navette depuis la gare d'Amplepuis vendredi soir vers 20h et samedi matin vers 8h15. Cochez la case correspondante dans le formulaire d'inscription !
Nous vous invitons aussi à covoiturer en inscrivant vos offres et demandes sur [ce site](https://www.togetzer.com/covoiturage-evenement/50l8m7).


Renseignements et inscriptions :
===============================

Pour vous inscrire, il suffit de remplir [ce formulaire](https://framaforms.org/inscription-marc-vella-2023-1668445359). 

Votre inscription sera enregistrée après réception d'un chèque d’arrhes de 80€ à envoyer à : 
MagnyÉthique - stage Marc Vella - 213 chemin du château de Magny - 69550 Cublize

N'hésitez pas à nous contacter si vous avez des questions !
[202303_marcvella@magnyethique.org](mailto:202303_marcvella@magnyethique.org)
