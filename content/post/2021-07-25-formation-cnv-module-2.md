---
title: Formation CNV - Module 2 // ANNULÉE
subtitle: Samedi 4 et dimanche 5 septembre 2021
date: '2021-06-15'
tags:
  - formation
  - CNV
---
**FORMATION ANNULÉE / REPORTÉE**

  

Les 4 et 5 septembre 2021, Edith Tavernier, formatrice agréée en CNV (Communication Non Violente), revient à l'écolieu pour encadrer le module 2, suite logique du module 1 qui a eu lieu en avril.

![](/images/uploads/2021.09_StageCNV2.png)

Formation à la Communication Non Violente selon le processus de Marshall B. Rosenberg

Un art de vivre avec soi-même et les autres
===========================================

La Communication Non Violente peut vous aider dans tous les domaines de votre vie. Que ce soit en couple, en famille, dans le milieu professionnel ou associatif, cette méthode élaborée par Marshall Rosenberg permet de prendre soin des liens qui nous relient tout en réussissant à exprimer nos émotions pour qu’elles soient entendues.

Edith Tavernier interviendra une nouvelle fois à l'Écolieu pour former quelques membres du groupe et d’autres personnes intéressées à la pratique de la CNV. La pédagogie est basée sur une alternance entre des apports théoriques et des temps de mise en pratique en petits groupes. Elle s’appuie sur l’expérimentation et l’exploration de situations relationnelles apportées par les participants.

À l’issue de la formation, vous recevrez un certificat afin de pouvoir la faire valoir.

Modalités pratiques :
=====================

Horaires : samedi 9h30 - 18h, dimanche 9h - 17h30.

Accueil : Si vous nous en faites la demande, nous pouvons vous héberger samedi soir (voire vendredi soir), en dortoir ou camping. Prévoyez dans la mesure du possible sac de couchage, drap, oreiller pour nous simplifier la logistique !

Repas : les repas de samedi et dimanche midi sont à amener par vos soins (frigo disponible sur place, attention pas de micro-onde disponible !). Nous proposons un repas chaud, végétarien à tendance biolocale le samedi soir sur inscription.

Prix : le coût pédagogique est de 230 €, auxquels s'ajoutent une participation pour la logistique. Nous avons décidé cette fois-ci de proposer celle-ci à prix libre et conscient, afin de favoriser l'accessibilité au plus grand nombre. Cette participation est nécessaire pour couvrir les frais engendrés par l'organisation de ce WE, l'utilisation des salles de formations, le grignotage mis à disposition, éventuellement le repas du samedi soir, petit-déjeuner et le logement (en dortoir d'une demi-douzaine de lits), ainsi que votre éventuel soutien au projet MagnyÉthique dans l'accueil de telles formations, et votre encouragement à la pratique du prix libre ! Nous prendrons quelques minutes dimanche pour en parler plus en détail et présenter les frais effectivement engagés. Nous espérons que cette formule rejoigne vos aspirations d'ouverture, de liberté et de responsabilité !

Pour vous inscrire, contactez-nous à 2021cnv@magnyethique.org en précisant également votre ville de départ et mode de transport pour des éventuels covoiturages, et faites parvenir un chèque d’arrhes de 70 € à : MagnyÉthique - formation CNV - 213 chemin du château de Magny - 69550 Cublize

N’hésitez pas à nous contacter si vous avez des questions !
