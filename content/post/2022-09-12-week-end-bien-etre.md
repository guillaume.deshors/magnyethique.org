---
title: Week-end Bien-être
subtitle: Samedi 8 et dimanche 9 octobre 2022
date: '2022-09-12'
tags:
  - atelier
  - conférence
  - réseau local
  - gouvernance
  - société
---


![](https://cloud.magnyethique.org/s/G8SL3AL53Jej3od/preview)  

Samedi 8 octobre à 18h : "Conférence immersive"
======================

Nous vous proposons une "conférence immersive" sur le thème du bien-être : un moment convivial d'échange et d'intelligence collective sur un thème qui nous est cher ! Nous poursuivrons la soirée avec une auberge espagnole.

Dimanche 9 octobre : Journée bien-être
====================================

Quelques membres du projet MagnyÉthique et des intervenant·e·s des environs que nous avons envie de vous faire rencontrer proposeront divers ateliers. Venez découvrir leurs pratiques et vous faire du bien ! (Programme sous réserve, des modifications sont encore possibles)


![](https://cloud.magnyethique.org/s/pWCNjfAZNkYkWca/preview)  


Renseignements et inscriptions :
===============================

Pas d'inscription à l'avance ! Tout se fera sur place : venez donc un peu en avance, ce sera l'occasion de papoter.
Si vous souhaitez plus d'informations, ou nous prévenir de votre venue, écrivez-nous à [2022bienetre@magnyethique.org](mailto:2022bienetre@magnyethique.org)
