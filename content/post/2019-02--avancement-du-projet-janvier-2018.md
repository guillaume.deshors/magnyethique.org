---
title: Avancement du projet - janvier 2018
date: '2018-01-01'
tags:
  - avancement
---
C'est ensemble que le groupe a commencé cette nouvelle année, dans les locaux d'Ardelaine en Ardèche, lors d'une session de trois jours qui a été riche, en décisions communes et contacts humains.

  

  

Les derniers jours de 2017 nous ont réservé une surprise bien inattendue, d'abord un peu déstabilisante pour la Commission Finances surtout mais après discussion tous ensemble, nous sommes tous ravis! Le projet ne se fera peut-être pas par un montage de SCI / SCIA, mais sous la forme d'une Coopérative d'habitants. Cette nouvelle perspective nous emballe par sa simplicité et pour les valeurs qu'elle induit en terme de non-propriété, d'usage du système bancaire etc. La date de l'achat reste fixée pour ce début d'année. Nous avons hâte!

  

  

En attendant, les commissions travaillent dur, dans la joie et la bonne humeur:

  

Outre la Commission finances qui a été mise au grill, la Commission Communication nous a préparé de magnifiques festivités. Une charte plus détaillée ainsi qu'un rafraichissement du processus d'intégration a occupé la Commission Gouvernance cette fin d'année. Nous nous sommes tous pris à rêver, petits et grands, autour des plans idéaux des Fabriqués imaginés et présentés par les uns et les autres et qui vont servir de base à la Commission Aménagements pour que les rêves deviennent (si possible) réalité.

  

  

Vous l'aurez compris: les perspectives 2018 s'annoncent riches et passionnantes!

  

  

A tous ceux qui nous suivent, nous souhaitons 12 mois du même bonheur! 

  

  

Bonne année 2018 à tous!
