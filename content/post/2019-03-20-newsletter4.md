---
title: Newsletter 4
date: '2019-03-20'
tags:
  - newsletter
  - avancement
---

Bonjour ami.es MagnyÉthiques,

En attendant la signature officielle de l’acte de vente, nous nous occupons de manière constructive, tout en conservant une attention élevée, car les délais sont très rapprochés. Enfin, nous vous rassurons tout de suite, c’est pour notre plus grand plaisir de voir aboutir ce beau projet.

### Balades au salon Primevère
Une partie du groupe s’est retrouvée fin février au salon Primevère où ils ont pu boucler l’ordre du jour de la plénière en présence qui avait lieu une semaine plus tard. En effet, l’informatique a ses limites, elles sont virtuelles et il n’y a rien de mieux que le direct et la convivialité !
Tandis que certains ont assisté à des conférences, d’autres ont tenu des stands en lien avec leurs activités personnelles. En résumé, nous étions une nouvelle fois cette année dans les allées à la recherche d’informations.

### Vis ma vie de MagnyÉth’
Les dernières personnes à avoir intégré le processus se sont manifestées fin janvier (dans notre circuit d’intégration, on entre en tant que ‘graine’ pour terminer ‘papillon’, vous pouvez consulter la procédure [ici](/page/ressources/). De nouvelles graines ont donc assisté à leur première plénière début mars. Ces plénières se déroulent sur une journée complète et ne laissent pas beaucoup de place à la sieste, au grand regret de notre participant espagnol !
C’est durant ces grandes journées que l’on essaie d’aborder les sujets délicats, ceux qui prennent du temps, ceux qui nécessitent l’avis de l’ensemble des membres. En général, lorsqu’arrive 17h00, on ne pense qu’à une chose : se coucher tôt le soir !... pour imaginer de nouvelles solutions :-) 

Un des sujets qui nous a occupé ces dernières semaines, par exemple, était la SAFER (Société d’Aménagement Foncier et d’Établissement Rural), par laquelle tout porteur de projet agricole DOIT passer avant de s'établir. Nous avons eu une première rencontre fort agréable avec eux et sommes ressortis du rendez-vous avec satisfaction et fierté. S’entendre dire que notre projet dépasse leur cadre existant nous a gonflé à bloc. 
Il nous restait à prendre la décision de s’associer ou pas avec eux.
Dans le groupe MagnyÉthique, constitué de 15 adultes, les discussions vont bon train. La décision de signer un cahier des charges avec la SAFER n’est pas acquise. Il faut dire que la durée est engageante : un contrat de 15 ans, ce n’est pas à prendre à la légère!
Nous avons avancé sur cette proposition mais la décision ne se fera pas cette fois-ci. 
En effet, il est rare que le consentement se fasse en une fois. La moindre objection motivée peut faire évoluer les propositions. Nous sortons tout de même de la plénière en ayant listé les questions à poser et choisi notre porte-parole auprès de la SAFER afin d’apporter des éclaircissements à nos interrogations. 

Les 11 enfants présents ce jour-là, pendant ce temps, ont continué à faire connaissance ensemble, accompagnés de leur nounou du jour.

En attendant la suite des aventures, voici quelques photos inédites…

{{< load-photoswipe >}}

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/newsletters/4/IMG_1818__1200.jpg" caption="" >}}
{{< figure link="/images/newsletters/4/IMG_1841__1200.jpg" caption="" >}}
{{< figure link="/images/newsletters/4/IMG_1842__1200.jpg" caption="" >}}
{{< figure link="/images/newsletters/4/IMG_1855__1200.jpg" caption="" >}}
{{< figure link="/images/newsletters/4/IMG_7894_1200.jpg" caption="" >}}


{{< /gallery >}}
