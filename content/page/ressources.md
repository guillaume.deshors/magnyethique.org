---
title: Ressources
---
Cette page liste une série de documents qui ont pour but de vous permettre de nous connaître mieux ou de servir de ressource pour d'autres projets.

*   Notre processus d’intégration
    *   [En version texte](https://docs.google.com/document/d/1hfD1wQl6CxieZg4tdZrkPTDgDbcefhRTnYal_IYKsgM/)
    *   [En version logigramme](https://drive.google.com/file/d/1JLIqsTqx96nu-XOE_gGUeIxcFVVDIPMZ/view?usp=sharing)
*   [Gouvernance partagée](https://drive.google.com/file/d/1LRHJ3LgA4Nayg3tg27b7rXPX8XvBfQ70/view?usp=sharing)
*   [Charte et valeurs](https://docs.google.com/document/d/1YQLaqlpVMlZLPR42CnL4b_k1264Vh63ahgZwyzLoj54/edit?usp=sharing)
    *   _Nota bene_ Cette charte doit bientôt être remise à jour, nous la considérons obsolète. Un travail de réécriture a commencé.
*   Aspects juridiques
    *   [Présentation synthétique des statuts et finances](https://docs.google.com/presentation/d/1esvQ6Wou8buOUasfyFT_JnWdtdtxJFyVevbwu8hKcE4/edit?usp=sharing)
    *   [Statuts de la SCI](https://docs.google.com/document/d/1EUmDc2pq3hkVAS5Y3Ua-6vjNT8eSxCTjGgEzhU0VazI/edit?usp=sharing) (anonymisés)
*   Design en permaculture (en cours) :
    *   [Étude du site](https://docs.google.com/presentation/d/1cK5Py20qZLfi7YqOROm0FB_gapqP3KXvG3E4IuYCYkc/edit#slide=id.g7211b6808c_0_440) (2020)  
    *   [Design permacole de l'écolieu MagnyÉthique](https://docs.google.com/presentation/d/1HyINueyQ4z6-rAJk1xaLbX-zqbn6YLCtmSvpjir0Xz0/edit#slide=id.p) (septembre 2021)  
*   [Conférence Kaizen “l’habitat participatif”](https://www.kaizen-magazine.com/sound/habitats-participatifs-reves-collectifs/)
