---
title: Journée portes-ouvertes
subtitle: Le 6 juillet 2019
date: '2019-06-06'
tags:
  - échanges
  - promotion
  - portes-ouvertes
---
![](/images/uploads/1559825426620_affiche-porte-ouverte-Magny-600px.jpg)

Nous aurons le plaisir de vous accueillir le samedi 6 juillet 2019 à 10h30 pour nos prochaines portes ouvertes.

Au programme:  
\- présentation du projet  
\- visite guidée des lieux  
\- pique-nique partagé (ouvert à tou.tes sans inscription)

Intéressé.es par l'habitat participatif en général, l'écolieu MagnyÉthique en particulier ou simples curieu.ses, vous êtes les bienvenu.es ! Merci de nous contacter via l'onglet "[Contactez-nous!](/page/contact/)" pour vous inscrire afin que nous puissions nous organiser au mieux !
