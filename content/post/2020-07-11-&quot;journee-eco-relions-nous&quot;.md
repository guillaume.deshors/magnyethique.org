---
title: '"Journée Éco-relions-nous"'
date: '2020-06-27'
tags:
  - échanges
  - conférence
  - société
  - habitat-participatif
  - atelier
---
Le samedi 27 juin 2020, Anne et Katia ont été honorées de représenter le groupe MagnyÉthique à la première journée de rencontre "Éco-relions-nous" organisée par un petit groupe d'amis engagés sur Lyon. 

  

Cette journée s'est déroulée au parc Blandan et a permis à 40 personnes d'échanger autour de l'habitat participatif, de ses tenants et aboutissants, des enjeux, des difficultés et des moyens de les surmonter. En fin de matinée, Clément, coréalisateur du documentaire [Écovillages, en quête d'autonomie](https://www.youtube.com/watch?v=Wy_jvp5wn8I), est venu présenter ce beau documentaire, avant de nous laisser la parole pour parler de MagnyÉthique.

  

Une animation fluide, des échanges sincères et quelques ateliers l'après-midi pour creuser différents sujets, dont les méthodes de gouvernance partagée, retour d'expérience de MagnyÉthique.

  

Merci pour cette magnifique journée de rencontre et au plaisir de participer à la prochaine !
