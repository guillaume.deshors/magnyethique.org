---
title: "Nosferatu -- ciné-concert"
subtitle: "Le 19 janvier 2025 à 17h00"
date: '2025-01-03'
tags:
  - concert
  - culture
---

L'écolieu MagnyÉthique vous propose un **ciné-concert** autour du film Nosferatu de Murnau (1922!), la première adaptation du mythe de Dracula au cinéma, avec une **composition inédite** pour un trio à cordes : violon, violoncelle et contrebasse. La partition repose sur les connexions sentimentales ou amoureuses entre les personnages.

Le film ayant fêté son centenaire il y a trois ans, venez le (re)découvrir chez nous à l'écolieu.

**Ciné-concert gratuit à participation libre.**

![](https://cloud.magnyethique.org/s/GTFdMogcwoQCgLp/preview)