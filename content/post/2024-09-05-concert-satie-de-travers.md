---
title: Concert "SATIE de travers"
subtitle: Dimanche 15 septembre à 17h
date: '2024-09-05'
tags:
  - concert
  - culture
---

Dimanche 15 septembre à 17h, nous aurons le plaisir d'accueillir Benjamin Campagna et Éric Noyer pour leur spectacle "SATIE *de travers*".


![](https://cloud.magnyethique.org/apps/files_sharing/publicpreview/YQExzQ7M4GHa4sm?file=/&fileId=44136&x=1920&y=1080&a=true&etag=636c2e9ec86ed38695f9841c54d92a10)

Participation aux frais libre. Réservation à l'adresse libererlavoix@benjamin-campagna.fr

N'hésitez pas à venir après nous avoir rencontré à 15h pour notre [journée porte ouvertes](../2024-09-05-visite-septembre-2025) !