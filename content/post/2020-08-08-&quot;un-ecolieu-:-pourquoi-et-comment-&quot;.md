---
title: '“Un écolieu : pourquoi et comment ?”'
subtitle: 'Projection et débat, visite et rencontre - le samedi 12 septembre 2020'
date: '2020-08-08'
tags:
  - échanges
  - conférence
  - promotion
  - portes-ouvertes
  - rencontre
  - habitat participatif
bigimg:
  - desc: null
    src: /images/uploads/1596883075943_La voie des éco-lieu.jpg
---
**"Un écolieu : pourquoi et comment ?"**

Après-midi organisé sur la thématique des écolieux :

*   14h : projection du documentaire "[La Voie des Écolieux](https://www.youtube.com/watch?v=TTp6rNViTUM)" par [IdéActes](https://www.ideactes.com)
*   15h : débat animé par les réalisateurs Fanny BOËGEAT & Yoan SVEJCAR
*   16h : goûter
*   16h30 : visite et rencontre avec MagnyÉthique
*   18h : fin

Inscription obligatoire par [ce formulaire](https://docs.google.com/forms/d/e/1FAIpQLSc-m6gaLoos5cgskegALKwaTmVfh-OeCZiDaaqadf8bCTaGqg/viewform) (afin d'organiser l'événemet en respectant les normes sanitaires imposées). Un message précisant les mesures prises sera envoyé aux participants avant l'après-midi.

Prévoir de la monnaie pour le chapeau des réalisateurs et le goûter à prix libre

Goûter proposé par MagnyÉthique dans le respect des normes sanitaires

Cette journée s'inscrit dans le cadre de deux événements du mois de septembre :

*   [Fête des Possibles](https://fete-des-possibles.org)
*   [Journées européennes de l'Habitat participatif](https://www.habitatparticipatif-france.fr/?HPFJPO)

![](/images/uploads/1597311835228_ldofbceglffghgkm.png)

![](/images/uploads/1597219466507_JpOGroupes_JPObandeau_2020petit_2020.jpg)

**Synopsis**

_En 2018, Fanny Boëgeat et Yoan Svejcar sont partis en tour du monde pour en apprendre plus sur les écovillages et les personnes qui y vivent.  
_  

_Ils ramènent de leur grand voyage un film qui rapporte les témoignages des habitant·e·s d'écolieux de 9 pays différents : Irlande, Canada, Etats-Unis, Bolivie, Chili, Nouvelle-Zélande, Australie, Inde et France._

_Des interviews inspirantes qui présentent la vision du monde de ces personnes qui ont choisi de changer radicalement leur mode de vie pour créer des lieux de vie plus en harmonie avec la Terre._

_Ces écovillageois·es nous parlent avec le cœur de leurs peurs, leurs colères ou leurs tristesses face à l'état du monde, mais aussi et surtout de ce qui les motive à agir, de leurs espoirs, de la montagne d'alternatives qui s'offrent à nous et des enseignements qu'ils et elles ont tiré de toutes leurs années d'expérience passées à construire un monde plus résilient !_

_Ce documentaire se veut une source d'inspiration pour tou·te·s celles et ceux qui désirent participer à la création d'un monde plus heureux et harmonieux, dans le respect du vivant._

![](https://static.wixstatic.com/media/f97503_42880696011642059369c164d62cf7b6~mv2.jpg)

**Tarif de la journée :** chapeau pour les réalisateurs, prix libre pour le goûter et possibilité d'adhésion sur place à l'association _Les MagnyÉthiques_
