#!/usr/bin/python3

# appel : importnewsletter.py <url brevo> <date d'envoi>
# avec date au format yyyy-mm-dd

from markdownify import markdownify as md
import fileinput, re, sys, urllib, urllib.request, shutil, os



def imageimport(matchobj):
    global index
    # import image and alert about the size
    url = matchobj.group(1)
    print("  -> image trouvée : " + url)
    path1 = 'static/images/uploads/tempimage'
    req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    with urllib.request.urlopen(req) as response, open(path1, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)
    print("stockée dans " + path1)
    file_size = os.path.getsize(path1)
    imagepath = 'static/images/'
    for root, directories, files in os.walk(imagepath, topdown=False):
        for name in files:
            s = os.path.getsize(os.path.join(root, name))
            if (s == file_size and name != 'tempimage'):
                print("!!!! La taille est identique à l'image " + os.path.join(root, name))
                # remove tempimage
                os.remove(path1)
                return "![](/" + root.replace('static/', '', 1) + '/' + name + ")"  
    if (file_size > 1000000):
        print("!!!!! attention, la taille de l'image est de ", file_size/1024, "Kio")
    # image new name
    path2 = 'static/images/uploads/' + os.path.basename(filename)[:-3].replace(' ', '_') + '_img' + str(index)
    index += 1
    print('image renommée en ' + path2)
    # rename image
    os.rename(path1, path2)
    return "![](/" + path2.replace('static/', '', 1) + ")" # URL

def correctlink(matchobj):
    # parse link and translate if it is a redirection
    url = matchobj.group(2)
    name = matchobj.group(1)
    try:
        print("  -> lien trouvé : " + url)
        with urllib.request.urlopen(url) as response:
            html = response.read().decode('utf-8')
            url2 = re.search("top.location=\\\'(.*)\'", html, flags=re.M).group(1).replace('\\', '')
            print("lien de renvoi décodé : " + url2)
            return "["+ name + "](" + url2 + ")" 
    except AttributeError:
        print("!!!!! une exception s'est produite : le lien ne contient pas de lien de renvoi au format attendu")
        return "["+ name + "](" + url + ")"
    except urllib.error.HTTPError:
        print("!!!!! une exception s'est produite : lien non fonctionnel")
        return "["+ name + "](" + url + ")"

################# début du script
print("Démarrage du script d'import de newsletter")

# combien d'arguments ont été passés ?
if (len(sys.argv) > 1):
    url = sys.argv[1]
else:
    url = input("Entrez l'URL de la newsletter (lien Brevo) : ")


if (len(sys.argv) > 2):
    date = sys.argv[2]
else:
    date = input("Entrez la date d'envoi de la newsletter au format yyyy-mm-dd : ")

if (not re.match('\d{4}-\d{2}-\d{2}', date)):
    print("Format de date incorrect, arrêt du script")
    sys.exit()

# Aller chercher le HTML de la newsletter
data = urllib.request.urlopen(url).read().decode('utf-8')

# trouver le titre et le numéro de la newsletter
m = re.search('<title>(.*)</title>', data)
title = m.group(1)
m2 = re.search('#(\d+)', title)
number = m2.group(1)
print("Numéro détecté : {}".format(number))

# nettoyer le code HTML de toutes les balises qui embetent le traducteur
data = re.sub('</?tr.*?>', '', data)
data = re.sub('</?td.*?>', '', data)
data = re.sub('</?table.*?>', '', data)
data = re.sub('</?th.*?>', '', data)
data = re.sub('</?tbody.*?>', '', data)

# traduire en markdown le HTML
markdown = md(data)

# créer le fichier md
filename = "content/post/{}--newsletter{}.md".format(date, number)


with open(filename, 'w') as file:
    file.write(
"""
---
title: '{}'
date: '{}'
tags:
  - newsletter
  - avancement

---

""".format(title, date))
    file.write(markdown)


#### partie mise à jour des liens et des images

index = 1

print("lecture du fichier " + filename + "...")


with open (filename, 'r' ) as f:
    content = f.read()
    # import and handle all images
    content = re.sub('!\[\]\((https?://.*?)\)', imageimport, content, flags = re.M)
    # parse links and translate them if they are redirections
    content = re.sub('\[(.+?)\]\((https?://.*?)\)', correctlink, content, flags = re.M)
    # remove all 'soft hyphens' (Hexa : c2 ad)
    content = re.sub('(\u00ad)', '', content, flags = re.M)
    # replace all multiple occurrences of line feeds by only two line feeds
    content = re.sub('\n{2,1000}', '\n\n', content, flags = re.M)
    # replace all #### by ###
    content = re.sub('####', '###', content, flags = re.M)

with open(filename, "w") as myfile:
    myfile.write(content)


