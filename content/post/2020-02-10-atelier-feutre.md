---
title: Atelier feutre
date: '2020-02-10'
tags:
  - formation
  - atelier
bigimg: []
---
Pour le tout premier atelier de l'écolieu, Claire a proposé le dimanche 9 février 2020 un bel atelier de travail du feutre.

La journée a été divisée en deux :

*   la première partie de l'atelier, de 10 à 11h30, a permis à 4 adultes et 4 enfants de se familiariser avec la technique de "feutrage à plat" ou "feutrage mouillé". Pour cela, chaque participant a pu se confectionner une jolie fleur en feutre grâce à ses conseils.

![](/images/uploads/1581323654700_IMGP8053_resized.JPG) ![](/images/uploads/1581323663179_IMGP8056_resized.JPG) ![](/images/uploads/1581323699120_IMGP8068_resized.JPG)

*   Seules deux adultes ont poursuivi l'atelier pour cette première à l'écolieu. Elle ont repris la technique de feutrage apprise le matin pour se confectionner des pièces bien plus travaillées : un béret pour l'une, un sac pour l'autre. Claire les a guidées à travers toutes les étapes du travail : de la confection des supports le matin jusqu'à la finalisation de l'objet en fin d'après-midi. Et c'est très réussi !

![](/images/uploads/1581323894218_IMGP8083_resized.JPG) ![](/images/uploads/1581323900621_IMGP8085_resized.JPG)

Vivement les prochaines dates pour que celles et ceux qui n'ont pu en profiter puissent le faire !
