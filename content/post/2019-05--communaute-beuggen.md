---
title: Communauté Beuggen
date: '2019-05-03'
tags:
  - habitat participatif
  - échanges
bigimg:
  - desc: null
    src: /images/uploads/1558854321756_IMGP6396_resized.JPG
---
Katia, Pascal et leurs enfants ont passé quelques jours avec les membres de la [Communauté de Beuggen](https://www.kommunitaet-beuggen.org) sur les rives du Rhin à la frontière germano-suisse. La communauté existe depuis 13 ans cette année, s'est constituée autour d'un objectif commun fort : organiser des fêtes et célébrations œcuméniques pour permettre à différentes branches du christianisme de communier ensemble.

  

L'enseignement principal qu'ils ont retiré de leur visite est l'importance primordiale de travailler au "Je" et au "Nous" pour pouvoir prendre soin des relations au sein du groupe sans lesquelles la communauté ne pourrait fonctionner et perdurer dans le respect des individus comme du collectif.
