---
title: Welcome!
date: '2019-02-03'
---
Welcome to our English readers; this is the first post in English -- and
will probably the only one for some time. 
Please understand that we are currently working on hot topics in and 
around the Castle. 
Hence, we are currently not able to translate posts into English.
However, you can challenge your French skills on the French mirror or use
a translator such as https://www.deepl.com/translator#fr/en.
