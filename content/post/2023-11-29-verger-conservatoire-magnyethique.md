---
title: Verger conservatoire MagnyÉthique
date: '2023-11-29'
tags:
  - biodiversité
  - permaculture
  - chantier participatif
  - wwoofing
---
Les 25 et 26 novembre derniers, un gros chantier de plantation a permis de réaliser l’étape la plus visible et symbolique du projet : planter 475 végétaux, parmi les plus grands qui vont constituer le verger conservatoire MagnyÉthique.

Ce projet, qui a germé il y a 3 ans ½ lors du design en permaculture, a ensuite dû être imaginé, conçu, préparé :

*   Des recherches ont été faites sur les [variétés fruitières](https://docs.google.com/spreadsheets/d/1zikFHMQZd-GEDVR-uyW0z8BANE-InUaOi4uoDsuukpc/edit#gid=0), les porte-greffes et sur la création de [guildes avec des végétaux de toutes tailles](https://docs.google.com/spreadsheets/d/1T29RIU_mwHkaF3Na9Emx1p9K3zze7h_aJN9nX9ZUT7A/edit#gid=0) ayant chacun leur(s) utilité(s) afin de faire [notre choix](https://docs.google.com/spreadsheets/d/1o0tHfJae_7Syv4MIIbey6OhcgAJgPH_FEgHUxXWLDRk/edit#gid=0) d'essences.
*   Entre temps, plus de 90 arbres fruitiers ont déjà été plantés, ainsi qu'une [haie bocagère](https://magnyethique.org/post/2022-10-26-chantier-participatif-plantation-theorie-et-pratique/) recréée
*   Une réflexion a été menée grâce et avec PermaLab pour trouver une gestion hydrologique viable et résiliente qui devrait permettre à nos arbres de grandir et produire sans irrigation
*   Au printemps 2023, notre voisin et ami Joseph (Symphonie des Vergers) a tracé dans la prairies les baissières qui accueilleront les plantations selon la conception hydrologique en lignes clés ("keyline-design")
*   Ces baissières ont été ensemencées en mycorhyzes grâce à la ferme à champignons qui a accepté que nous récupérions le substrat plus assez productif et grâce à la patience de nos wwoofeurs et wwoofeuses successives
*   Le tout a été paillé pour éviter la trop importante repousse de la prairie
*   La [dotation conséquente du Fonds Germe](https://magnyethique.org/post/2021-12-23-un-verger-conservatoire-a-l-ecolieu/) il y a deux ans nous a permis l’achat de la majorité des plants. D’autres ont été et seront encore patiemment semés puis greffés, ou bouturés afin de terminer l’implantation prévue sans dépasser le budget

![Joseph nous creuse les sillons.](https://cloud.magnyethique.org/s/yoDZXyJgZa8R42L/preview)

**Les objectifs de ce verger ?**

Un havre de **biodiversité** avec un étalement des floraisons sur 12 mois (si si !).

Des guildes relativement denses offrant **gîte et couvert** à toute une faune et permettant aux arbres de gérer leur **évapotranspiration** en créant des conditions hydrologiques bénéfiques

Des **fruits**, gros et petits pour contribuer à **adapter nos tarifs d’accueil** de stagiaires et artistes en résidence afin que l’aspect financier ne soit pas un frein.

Un lieu pour **tester des pratiques agroforestières et agroécologiques** et y mener des temps de **sensibilisation** et formation autour de la biodiversité animale et végétale, de l’entretien d’un verger.

Et _last but not least_ : pouvoir mettre à disposition de quiconque en fera la demande **boutures ou greffons** de végétaux de jardin-forêt et de fruitiers locaux et/ou anciens pour qu’émergent un peu partout des haies comestibles et fruitières !

![](https://cloud.magnyethique.org/s/L2XyrJKEPypa8Nq/preview)

**Un grand merci** à [PermaLab](https://permalab.fr) pour nous avoir accompagnés dans l'émergence de ce grand projet, aux [Croqueurs de Pommes du St Genois](https://vergersaintgenois.com) pour leurs conseils, à [Symphonie des Vergers](https://symphonie-des-vergers.fr) pour l'aide mécanique et les conseils de pros, à nos fournisseurs pour avoir réussi à livrer la grande majorité des végétaux dans les temps, avec un merci particulier à Valentine du Jardin Comestible pour sa patience à répondre depuis 3 ans à nos questions et ses conseils toujours personnalisés, au [Fonds Germe](http://www.fonds-germes.org) qui nous a permis de planter l’essentiel des strates en une fois, aux wwoofeur·euse·s de l'année 2023 pour leur aide dans la laborieuse mais précieuse préparation du terrain, aux 20 volontaires venu·e·s en chantier participatif (via notre réseau local, [Wwoof France](https://wwoof.fr/fr/how-it-works), [Twiza](https://fr.twiza.org) ou [Les Alvéoles](https://alveoles.fr)) pour prêter main forte aux MagnyÉths et ce, malgré le froid.

<div class="row">
<div class="col-md-8 col-sd-12">
<img src="https://cloud.magnyethique.org/s/ipm3Ho4Jzd8nox6/preview">
</div>
<div class="col-md-4 col-sd-12">
<img src="https://cloud.magnyethique.org/s/3xEpFFdgcGK24cJ/preview"><img src="https://cloud.magnyethique.org/s/w65a8xpfa4N9wwD/preview"><img src="https://cloud.magnyethique.org/s/ddgJDBnf2DTM3LZ/preview">
</div>
</div>

**Les perspectives** : bichonner tout ce petit monde pour que la plantation à la Ste Catherine leur permette de bien prendre racines. Au printemps, ajouter les vivaces. Et continuer à chercher des financements pour creuser les mares prévues dans la gestion hydrologique du lieu, que nous souhaitons autonome et résiliente.
