---
title: Hymne de l’UEDAC 2021
subtitle: ''
tags: []
---


*Sur l’air de “Le Retour à La Terre” des Fatals Picards*


### Légende

Groupe 1 : les citadin.e.s

Groupe 2 : les Magnyéthicien.ne.s


### Paroles

(1) Magny fais tes valises et les miennes aussi

Nous quittons Amplepuis pour le paradis

J'ai trouvé le T2 dont nous rêvions tant

Dans une jolie rue à côté d'un Auchan

### 

(2) Mais qu'est-ce que tu racontes ici on est si bien

On a des toilettes sèch' et des douch' le matin

Un château du treizième dans un parc de huit hectares

Pour planter nos tent' c'est vraiment le panard


###  

(1) On s'est gelé les miches pendant toute la nuit

On s'est cramé la gueule sous le soleil de midi

Si tu veux une chasse d'eau c'est toi qui la fais

Si tu veux une crème solaire c'est toi qui la fais

### 

(2) Pour trouver des légum' il faut marcher 30 mètres

(1) Pour trouver un bar il faut faire 50 km

(2) Le premier embouteillage est à 120 km

(1) Mais à six heures le coq a chanté

###  


Elle est pas belle la vie à l'UEDAC de Magny ?

La main dans la main à imaginer demain

Elle est pas belle la vie à l'UEDAC de Magny ?

La main dans la main

Mais faites' attention au chien

###  

(2) Alors on est pas bien la nuit sous les étoiles

En plus tous les repas c'est d'la cuisine trois étoiles

(1) Couscous trop épicé, fromage pour les vegan

On a des pêches pourries et mêm' pas de bananes

(2) Ici il y a d'la vie, des gamins éveillés

"Haut les mains s'il te plaît", tu s'ras quand même mouillé,

Nos hôtes' sont chaleureux, débrouillards et heureux

Plus tard je voudrais vivre dans un écolieu

(1) Trois réus par semaine pour savoir si tu vas bien,

Si tu veux lire un mail, faut aller dans l'jardin,

S’tu veux avoir une chaise c'est toi qui la prends,

Des fois t'as pas le droit c'est l'repas insolent

(2) Moi sur les chantiers j'ai l'impression de renaître,

On a viré les ronces sur au moins 200m

(1) On a raclé les murs sur mêm' pas 2cm

Et puis l'orage est arrivé !

###  

--(Coupure)--

###  

Elle est pas belle la vie à l'UEDAC de Magny ?

La main dans la main à imaginer demain

Elle est pas belle la vie à l'UEDAC de Magny ?

La main dans la main

Et toujours le ventre plein

###  

(2) Jouer chanter débattre on partage nos passions

Et si jamais t'as soif fais toi couler une pression

Si tu veux des mots d'amour y'a le crieur tous les jours,

C'est ouf ces paysages t'as pas ça dans ta cour

(1) Si tu veux rien louper faut dormir moins de 6h

Pour ramener tout le monde il faut 25 chauffeurs

T'es plus chaud qu'le climat mais bien moins que la douche

Sauf cell' qui sont en bas, quand t'en reviens tu te mouches

(2) Dans la bistroposphère on a refait le monde

Et tous ces jeux de mots c'est des génies qui les pondent

(1) Si tu veux d' la lumière c'est toi qui la fais

Si tu veux un parasol c'est toi qui le fais

(2) On a que des gens bien dans un rayon d'800 mètres

(1) On a r'trouvé les tentes à 18km

(2) Ici on s'autogère on n'a ni dieu ni maître

Et tu r'prendras bien du rosé !

###  

Elle est pas belle la vie à l'UEDAC de Magny ?

La main dans la main à imaginer demain

Elle est pas belle la vie à l'UEDAC de Magny ?

La main dans la main

Franch'ment on ne manque de rien

La main dans la main

Je reviendrai l'an prochain !


{{< load-photoswipe >}}
{{< gallery >}} 
{{< figure link="/images/partenaires/programme_uedac_2021.jpg" caption="Le programme de l'UEDAC" >}} 
{{< /gallery >}}