---
title: Traverser les conflits grâce au cercle restauratif
date: '2019-11-04'
tags:
  - gouvernance
  - formation
---
Samedi 26 octobre, Noémie, Caroline et Katia se sont rendues à St Clément sur Valsonne ou l’habitat participatif Solydaire a réuni différents membres de 4 habitats participatifs ainsi que d’autres engagés dans un projet d’école démocratique.

Nous étions 14 à bénéficier d’une formation intense dispensée par Céline Meunier et Marie-Dominique Texier. La commission gouvernance les avaient déjà rencontrées au printemps lors de la journée sur la création de son système restauratif. D’inspiration directe de la CNV, le cercle restauratif permet lors d’un conflit complexe de prendre soin du lien entre les personnes et d’écouter les ressentis et les intensions pour une compréhension profonde de ce qui vit en chacun.e.

Un grand merci aux deux formatrices qui sont venues dans le Beaujolais Vert pour nous tous, aux membres des autres groupes pour les échanges. Nous espérons pouvoir bientôt nous retrouver pour des sessions informelles de pratique qui permettront à tou.te.s de s’approprier l’outil au mieux.
