---
title: Bourse aux projets d'habitat groupé
subtitle: Organisée par les Colibris
tags:
  - échanges
date: '2018-03-06'
---
![](/images/uploads/image4.png)

Guillaume a participé à une rencontre présentation de différents projets d'habitats participatifs organisée par les Colibris Lyon. L'occasion de découvrir d'autres projets: "Les Bricolos Ecolos", "Notre beau projet", "L'Astrolabe", "Grain&sens", "Projet d'Alexandre" en plus du nôtre "Les Fabriqués" et d'échanger autour des recherches de terrain, de participants ainsi que des montages financiers et statutaires notamment.
