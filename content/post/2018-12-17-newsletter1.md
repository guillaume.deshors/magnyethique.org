---
title: Newsletter 1
date: '2018-12-17'
tags:
  - newsletter
  - avancement
---

Bonjour ami·es MagnyÉthique !

Vous avez montré de l’intérêt pour notre projet d’habitat partagé à Cublize au cours des derniers mois et par la rédaction de cette première newsletter, nous lançons notre première communication ciblée.
Nous espérons que cet avancement vous plaira autant qu’à nous.

### Rapide historique, « Les Fabriqués »

Les premières personnes qui constituent le groupe se connaissent depuis une voire deux années. Elles avaient à cœur de rassembler les savoirs-faires de chacun pour monter un collectif ouvert sur l’extérieur. Elles souhaitaient avoir suffisamment de terrain pour viser une certaine autonomie alimentaire via la permaculture.

Le premier projet n’a pas pu aboutir, dû entre autre à la frilosité des banques. 

### À présent, « MagnyÉthique »

Le nouveau groupe est plus fort de ses expériences passées et prêt à investir dans un lieu localisé dans le Beaujolais Vert. 

C’est ainsi que quatre foyers ont signé la promesse de vente pour l’achat du Château de Magny, à Cublize, le 27 novembre 2018.

*(2 images : vue de haut et vue globale)*

Avec ce nouveau projet, un nouveau nom a été adopté au consentement par le groupe : 

**MagnyÉthique**

En effet, beaucoup d’entre nous se sentent attiré·es par ce lieu d’une façon très magnétique…

A présent que l’achat est réglé et le nom trouvé, il nous reste à entrer dans le vif du sujet ! 

### Au programme

Dès la réception des clés, nous commencerons les travaux – l’isolation, le chauffage et l’assainissement sont une priorité. Parallèlement, nous abordons les sujets divers comme les activités, l’aménagement intérieur qui en découle et très rapidement viendra l’aménagement extérieur avec un grand espace de permaculture.

Aujourd’hui, ce sont les neurones qui travaillent dur ! Dans un avenir proche, ce seront nos mains et nos muscles...
… Et les vôtres si vous souhaitez nous rejoindre !... Car oui, il y a encore quelques places. Et les besoins sont larges. Nous organiserons dès le printemps 2019 des chantiers participatifs, car il nous faudra démultiplier nos forces.

*(image du lieu en couleur pour la répartition des espaces)*

Plus d’infos sur le site : http://hameau-les-fabriques.fr *nota bene : ce site n'est maintenant plus en ligne ; il a été remplacé par celui que vous avez sous les yeux !*

Vous pouvez constater que les ambitions sont grandes ! 
Si nous souhaitons continuer à avancer sans l’aide des banques, ce projet a besoin d’une large participation de chacun. Tant pour la matière grise que pour les financements et aussi la mise en œuvre (=les biscotos !!)

### Aujourd’hui, nous avons besoin d’un nouveau logo !

A vos crayons, pinceaux, claviers ! Échange matière grise et talent contre week-end découverte sur le lieu. Consultez le document joint pour toutes les informations et participez dès maintenant.

### Pour nous soutenir

Vous pouvez devenir sympathisant·e en adhérant à l’association par voie postale et prochainement à partir du nouveau site en création. 
Peu importe le montant, il aura une utilité de taille : location des salles de réunion, réalisation du logo, création d’un nouveau site internet, achat de documentations, adhésions à d’autres associations en lien avec les différents projets…

Nous vous tiendrons au courant de l’avancée en envoyant une newsletter régulière. 
Dès que le collectif sera officiellement propriétaire, nous organiserons une visite des lieux mensuelle.

Alors nous vous disons à tou·te·s, à très bientôt ! 





