---
title: Ateliers et formations
subtitle: ''
sections: ["Ateliers à venir", "Ateliers passés"]
tags: []
---
{{< calendar urls="https://cloud.magnyethique.org/remote.php/dav/public-calendars/ERn8LszmJBBpsGwH/?export" colors="#87CEFA" tiny="true" >}}

Cette page présente le calendrier des ateliers, stages et formations animés par le projet ou se passant sur le lieu.

{{< liste_evenements titre_avenir="Ateliers à venir" titre_passes="Ateliers passés" >}}
{{< evenement 
    date="2025-01-18T14:30:00"
    dateFin="2025-01-18T17:30:00"
    titre="[Fresque de la biodiversité](https://www.fresquedelabiodiversite.org/#participer)"
    afficheHeure=true
>}}
{{< evenement 
    date="2025-01-29T14:30:00"
    dateFin="2025-01-29T17:00:00"
    titre="[Kiffe tes premières règles, duo <strong>mère-fille</strong>](post/2024-12-09-atelier-kiffe-tes-1eres-regles/)"
    afficheHeure=true
>}}
{{< evenement 
    date="2025-02-26T14:30:00"
    dateFin="2025-02-26T17:00:00"
    titre="[Kiffe tes premières règles, duo <strong>père-fille</strong>](post/2024-12-09-atelier-kiffe-tes-1eres-regles/)"
    afficheHeure=true
>}}
{{< evenement 
    date="2024-11-28T20:00:00"
    dateFin="2024-11-28T22:00:00"
    titre="[Atelier philo](/post/2024-11-19-atelier-s-philo)"
    afficheHeure=true
>}}
{{< evenement 
    date="2024-12-14"
    dateFin="2024-12-15"
    titre="[Stage d'électricité](/post/2024-11-06-stage-d-electricite-avec-un-pro)"
>}}
{{< evenement 
    date="2024-09-30T20:00:00"
    titre="[Atelier Philo](/post/2024-09-07-atelier-philo)"
    afficheHeure=true
    description="animé par Laetitia et Brice"
>}}
{{< evenement 
    date="2024-04-22T20:00:00"
    titre="[Fresque de la biodiversité](/post/2024-04-07-fresque-de-la-biodiversite)"
    afficheHeure=true
    description="animée par Michael Klöpper, animateur de plusieurs fresques dans le Grand-Est"
>}}
{{< evenement 
    date="2023-12-15T13:30:00"
    dateFin="2023-12-15T16:30:00"
    afficheHeure=true
    titre="atelier théorique et pratique **permaculture** \"**la haie bocagère**\""
    description="[Sur inscription](https://www.ouestrhodanien.fr/semaine-europeenne-du-developpement-durable-2023/?fbclid=IwAR04gNmjtMsia_ClRRZVLkYDP2Lz16zWZ2sOWBL5FRYDOeNIkoNzQalEzw8) (amelie.corgier@c-or.fr)"
>}}
{{< evenement 
    date="2024-01-28T09:30:00"
    dateFin="2024-01-28T17:00:00"
    afficheHeure=true
    titre="[Journée autour du piano](https://magnyethique.org/post/2023-11-28-journee-autour-du-piano/)"
>}}
{{< evenement 
    date="2024-05-18T09:30:00"
    dateFin="2024-05-19T18:00:00"
    afficheHeure=true
    titre="[stage avec Marc Vella \"Rendre belles les fausses notes de la vie\"](https://magnyethique.org/post/2023-11-03-stage-marc-vella/)"
>}}
{{< evenement 
    date="2023-11-23T13:30:00"
    dateFin="2023-11-23T16:30:00"
    titre="atelier théorique et pratique **permaculture** \"**la haie fruitière**\""
    description="[Sur inscription](https://www.ouestrhodanien.fr/semaine-europeenne-du-developpement-durable-2023/?fbclid=IwAR04gNmjtMsia_ClRRZVLkYDP2Lz16zWZ2sOWBL5FRYDOeNIkoNzQalEzw8) (amelie.corgier@c-or.fr)"
>}}
{{< evenement 
    date="2023-11-07"
    titre="[atelier de **chant** avec Benjamin Campagna : libérer la voix](https://magnyethique.org/post/2023-10-08-atelier-chant/)"
>}}
{{< evenement 
    date="2023-10-14"
    dateFin="2023-10-15"
    titre="[Pratiquer la **CNV** avec les enfants et les ados](https://magnyethique.org/post/2023-05-11-pratiquer-la-cnv-avec-les-enfants-et-ados/)"
    description="avec Paul-Georges Crismer, formateur certifié du CNVC"
>}}
{{< evenement 
    date="2023-09-30"
    dateFin="2023-10-01"
    titre="[stage de **chant** \"exprimer par la voix son plein potentiel\" avec Marianne Sébastien](https://magnyethique.org/post/2023-05-29-stage-chant-marianne-sebastien/)"
>}}
{{< evenement 
    date="2023-09-23T10:00:00"
    dateFin="2023-09-23T12:00:00"
    titre="atelier **permaculture** \"Plantations d'automne, les vivaces au jardin. Échanges et mise en pratique.\""
    description="[Sur inscription](https://www.ouestrhodanien.fr/semaine-europeenne-du-developpement-durable-2023/?fbclid=IwAR04gNmjtMsia_ClRRZVLkYDP2Lz16zWZ2sOWBL5FRYDOeNIkoNzQalEzw8) (evenements@c-or.fr ou 04 74 05 06 60)"
>}}
{{< evenement 
    date="2023-09-09"
    dateFin="2023-09-10"
    titre="[atelier **CNV** \"Pacifier sa relation à l'argent\"](https://magnyethique.org/post/2023-05-11-stage-cnv-relation-a-l-argent/)"
    description="avec Paul-Georges Crismer, formateur certifié du CNVC"
>}}
{{< evenement 
    date="2023-06-24"
    dateFin="2023-06-25"
    titre="[Week-end \"occupons-nous des conflits avant qu'ils s'occupent de nous\"](https://magnyethique.org/post/2023-05-09-occupons-nous-des-conflits-avant-quils-ne-soccupent-de-nous/)"
>}}
{{< evenement 
    date="2023-05-15"
    dateFin="2023-05-21"
    titre="second module de formation à la facilitation du **Forum de Zegg** (stage réservé aux personnes ayant suivi le premier module)"
>}}
{{< evenement 
    date="2023-05-07"
    dateFin="2023-05-10"
    titre="[stage de base de **Forum de Zegg**](https://magnyethique.org/post/2023-02-27-stage-de-base-forum-de-zegg/)"
>}}
{{< evenement 
    date="2023-03-18"
    dateFin="2023-03-19"
    titre="[Rendre belles les fausses notes de la vie](https://magnyethique.org/post/2022-10-16-marc-vella/)"
    description="formation avec Marc Vella"
>}}
{{< evenement 
    date="2022-10-17"
    dateFin="2022-10-20"
    titre="[approfondissement et pratique(s) de la **permaculture**](https://magnyethique.org/post/2022-07-06-formation-permaculture-initiation-et-approfondissement/)"
>}}
{{< evenement 
    date="2022-10-15"
    dateFin="2022-10-16"
    titre="[initiation à la **permaculture**](https://magnyethique.org/post/2022-07-06-formation-permaculture-initiation-et-approfondissement/)"
>}}
{{< evenement 
    date="2022-10-09"
    titre="[Journée **bien-être**](https://magnyethique.org/post/2022-09-12-week-end-bien-etre/)"
>}}
{{< evenement 
    date="2022-10-08"
    titre="[Conférence immersive sur le **bien-être**](https://magnyethique.org/post/2022-09-12-week-end-bien-etre/)"
>}}
{{< evenement 
    date="2022-02-26"
    dateFin="2022-02-27"
    titre="[stage d'**improvisation vocale** collective](https://magnyethique.org/post/2021-11-07-stage-d-improvisation-vocale-collective/)"
>}}
{{< evenement 
    date="2022-03-12"
    dateFin="2022-03-13"
    titre="[Poser les bases d'un collectif impliqué et durable](https://magnyethique.org/post/2022-01-17-poser-les-bases-d-un-collectif-implique-et-durable/)"
>}}
{{< evenement 
    date="2021-10-23T09:00:00"
    dateFin="2021-10-24T18:00:00"
    titre="[**Rendre belles les fausses notes de la vie**](/post/2021-05-09-stage-avec-marc-vella-ecole-de-la-fausse-note/)"
    description="formation de et par Marc Vella"
>}}
{{< evenement 
    date="2021-08-08"
    dateFin="2021-08-14"
    titre="[Université d'été décontractée d'Avenir Climatique](/post/2021-07-25-avenir-climatique/)"
>}}
{{< evenement 
    date="2021-04-17"
    dateFin="2021-04-18"
    titre="[**Module 1 de CNV**](/post/2021-02-24-formation-cnv/)"
>}}
{{< evenement 
    date="2021-03-20T09:00:00"
    dateFin="2021-03-20T17:00:00"
    afficheHeure=true
    titre="[PSC1 (**Prévention et Secours Civique niveau 1**)](/post/2021-02-24-premiers-secours/)"
    description="formation certifiante"
>}}
{{< evenement 
    date="2021-03-13"
    titre="[Atelier d'initiation aux premiers secours pour des enfants](/post/2021-02-24-premiers-secours/) et rafraîchissement pour adultes détenteurs d'un PSC1"
>}}
{{< evenement 
    date="2020-10-15"
    dateFin="2020-10-18"
    titre="**Formation permaculture (niveau 2)**"
    description="réservée aux stagiaires ayant passé le niveau 1"
>}}
{{< evenement 
    date="2020-08-27"
    dateFin="2020-09-01"
    titre="[Danse-contact-impro](/post/2020-07-27-danse-contact-impro/)"
>}}
{{< evenement 
    date="2020-07-06"
    dateFin="2020-07-10"
    titre="**approfondissement et pratique(s) de la permaculture**"
>}}
{{< evenement 
    date="2020-07-04"
    dateFin="2020-07-05"
    titre="**initiation à la permaculture**"
>}}
{{< evenement 
    date="2020-03-14"
    dateFin="2020-03-15"
    titre="[**Module 2 de CNV**](/post/2020-04-16-formation-cnv/)"
>}}
{{< evenement 
    date="2020-02-09T10:00:00"
    dateFin="2020-02-09T17:00:00"
    titre="Atelier feutre"
    description="animé par Claire<br>![](/images/uploads/1581323173119_IMGP8052_resized.JPG)"
>}}
{{< evenement 
    date="2020-01-11"
    dateFin="2020-01-12"
    titre="[**Module 1 de CNV**](/post/2020-01-28-communication-non-violente/)"
>}}
{{< evenement 
    date="2019-09-28"
    dateFin="2019-09-29"
    titre="**initiation à la permaculture**"
>}}
{{< evenement 
    date="2019-09-30"
    dateFin="2019-10-04"
    titre="**approfondissement et pratique(s) de la permaculture**"
>}}
{{< /liste_evenements >}}
