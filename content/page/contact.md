---
title: Contact
subtitle: ''
tags: []
---
Merci de l'intérêt que vous portez au projet MagnyÉthique !

Pour être tenu·e informé·e de nos projets, plusieurs options s'offrent à vous :

*   Vous êtes curieux·se des avancées du groupe : vous pouvez nous suivre en [vous inscrivant à la lettre d'information](https://my.sendinblue.com/users/subscribe/js_id/2w4x5/id/1). Cette lettre est diffusée environ tous les deux mois.
*   Vous habitez près de chez nous et souhaitez être informé·e des événements que nous proposons : vous pouvez [vous inscrire à notre liste d'information locale](https://7e684666.sibforms.com/serve/MUIEABLi9JV7oNhfccpwZ8n_5-Ca8jN6BSXe7CmgrRBKhA8TyOHsDgvIr40yvn822cT-R462T-nO4cSla9ZYvcpxrbCYtonQpcJ1u0TkpVOoyEwAmsayD36grwGrR_Hjdj4GDdR2SJrOy887bZLYxvYrs7EzkayXFdZ2bdDUHm24qLPjAnQZ3ErRNTg8cuw6NxhVFXsrJlQD7mMT).
*   Nous mettons aussi quelques informations sur [Facebook](https://www.facebook.com/MagnyEthique/#).
*   Et bien sûr vous pouvez aussi consulter notre site que nous essayons de tenir à jour, en allant sur notre page [Actus](/post/).

Actuellement les MagnyÉth's sont très investi·e·s dans les multiples missions d’avancement du projet (travaux de rénovation, maraîchage, accueil de volontaires, soin aux relations dans le groupe…) et les sollicitations sont nombreuses. Nous ne sommes par conséquent pas en mesure de donner suite spécifiquement à chaque mail et même si c'est à regret car le contact personnel nous plaît, nous vous proposons de vous rediriger de manière ciblée afin de pouvoir continuer à traiter au mieux les demandes.

*   Vous cherchez des informations sur le groupe, les montages juridiques et financiers. Notre site regorge d'informations et nous faisons notre possible pour qu'il reste à jour : prenez le temps de le lire ! Un grand nombre de demandes nous parviennent dont les réponses sont déjà présentes. Toutefois, pour les groupes en formation qui souhaitent échanger sur notre expérience, une option vous est dédiée ci-dessous, sans garantie de délai de réponse.
*   Vous souhaitez participer à un chantier participatif, une option vous est dédiée dans le formulaire ci-dessous ! Alternativement contactez-nous _via_ [Twiza](https://fr.twiza.org/ecolieu-magnyethique-au-coeur-du-beaujolais-vert,zs3301,11.html) (l'inscription y est payante mais donne droit à une assurance avantageuse couvrant les accidents de chantier).
*   L'agroécologie/permaculture vous intéresse et vous souhaitez en découvrir plus en mettant les mains dans la terre pour au moins trois semaines ? Proposez-nous votre aide en tant que [Wwoofer](https://wwoof.fr/fr/host/6494) !
*   Vous souhaitez nous contacter à propos d'une activité à nous proposer ou à faire sur place, utilisez l'option correspondante du formulaire pour joindre la commission idoine.
*   Vous êtes intéressé·e pour rejoindre le groupe. Hélas pour vous, nous ne sommes pas actuellement à la recherche de nouveaux membres et, sauf départ, ne devrions pas l'être avant quelques années, quand nous y verrons plus clair sur les derniers travaux. Surveillez l'onglet "[Visite et présentation](/page/visites/)" du site sur lequel nous mettons les prochaines dates ou inscrivez-vous à la [lettre d'information](https://my.sendinblue.com/users/subscribe/js_id/2w4x5/id/1) pour recevoir toutes les infos dont les prochaines dates de rencontre.
*   Pour toute autre question, vous pouvez utiliser le formulaire ci-dessous mais soyez indulgent·e, nous n'avons plus les moyens d'être réactifs·ves en ce moment (pensez semaines plutôt que jours…)

{{< contact >}}

* * *

Notre adresse : 213, chemin du Château de Magny, 69550 Cublize.

{{< carte_chateau >}}
