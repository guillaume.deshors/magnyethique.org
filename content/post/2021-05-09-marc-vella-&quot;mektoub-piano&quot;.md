---
title: Marc Vella - "Mektoub Piano" ou "La Caravane amoureuse en Casamance"
subtitle: Projection conférence le vendredi 22 octobre 2021 à 20h00
date: '2021-07-18'
tags:
  - conférence
  - culture
  - rencontre
  - concert
---
La caravane amoureuse :
=======================

"[_Le cœur de l’homme, patrimoine de l’humanité_](https://www.caravaneamoureuse.com/FR/accueil.html)"

_Le commandant Cousteau partait en expédition avec la Calypso explorer les océans pour en montrer la beauté. La Caravane amoureuse, elle, part en expédition avec des bus pour explorer les profondeurs humaines et mettre à l’honneur par le film documentaire les femmes et les hommes qui s’engagent pour que notre monde aille vers davantage de paix, de solidarité et de conscience environnementale._

_Associations, entreprises, politiques : Présidents de Régions, députés, élus … beaucoup soutiennent ce projet._

_Ensemble, nous choisissons de mettre en avant le Coeur de l’Homme, patrimoine de l’humanité._"

Marc Vella, à la veille de la formation qu'il proposera le week-end des 23 et 24 octobre 2021 à l'écolieu, nous fait également l'honneur de tenir conférence sur son voyage époustouflant d'humanité "piano au dos" en Casamance. De quoi ravir et combler des envies d'horizons et de musique de tou·te·s !

![](/images/uploads/1620567228147_MagnyéthiqueConférence_MVella.jpg)  

La conférence aura donc lieu le vendredi 22 octobre à 20h à l'écolieu MagnyÉthique. Pour vous inscrire, merci d'écrire à l'adresse précisée en bas de l'affiche.
