---
title: Avancement du projet - octobre 2017
date: '2017-10-29'
tags:
  - avancement
---
La création de la SCI "Les Fabriqués" et l'achat des lieux sont imminents: nous trépignons d'impatience dans l'attente des propositions de dates des notaires!

Après notre réunion de septembre et un intense week-end sous le soleil ardéchois qui vient de s'achever, la répartition en commission a évolué: 

*   La **commission coordination** va piloter le projet en animant le groupe en fonction du calendrier dont elle aura une vue d'ensemble.
*   La **commission finances**, en lien désormais avec un courtier, poursuit le travail autour du financement pendant que le courtier négocie un emprunt collectif pour la SCI et les emprunts individuels. Le rôle de cette commission porte également sur la finalisation des statuts de la SCI et la mise à jour de ceux de l'association des "Prés Fabriqués" qui reprendra prochainement du service.
*   La **commission communication** est constituée de deux pôles: la communication et la cohésion en interne; la communication vers l'extérieur. Les résultats des derniers mois sont très concluants: le groupe se soude et se fédère autour de conversation de fond, nous avons obtenu une jolie visibilité dans les médias et nous voulons donc poursuivre dans ce sens!
*   La **commission gouvernance** peut s'appuyer sur d'intenses échanges que le groupe a eu sur les valeurs pour préciser la charte du groupe et a débuté un travail sur la gouvernance, incluant un règlement et des méthodes de prises de décision. Travail vaste et passionnant qui nécessite de la formation.
*   Quant à la **commission aménagements**, elle entre en scène à l'approche de l'achat du bien pour commencer à réfléchir aux aménagements intérieurs et extérieurs. Ainsi, les discussions au sein du groupe vont permettre de guider le travail de l'architecte pour les intérieurs et la commission va pouvoir planifier et lancer les travaux extérieurs afin de planter, par exemple, les fruitiers au plus tôt et d'en profiter dès le début des chantiers participatifs! Peut-être bénéficierons-nous à terme d'un bassin de baignade naturel comme celui que nous avons pu voir ce week-end (voir photo).

Le groupe s'agrandit de mois en mois de personnes nous venant parfois de lointains horizons. La technologie a permis à ses membres de profiter de ces journées pour partager des moments conviviaux, créer du lien, connaître les individualités qui constitueront le groupe et pour amorcer les réflexions et actions concrètes qui nous font avancer chaque jour vers notre cap. Et ce, malgré les frontières et parfois même les océans qui nous séparent.
