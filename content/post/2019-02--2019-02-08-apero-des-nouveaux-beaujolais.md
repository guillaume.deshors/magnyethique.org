---
title: Apero des nouveaux Beaujolais
date: '2019-02-08'
tags:
  - échanges
  -  réseau local
bigimg:
  - desc: Affiche Beaujolais Vert Votre Avenir
    src: /images/uploads/1549054934225_BVVA.jpg
---
Vendredi 8 février, nous étions invités par l'équipe de [Beaujolais Vert Votre Avenir](https://www.beaujolais-vertvotreavenir.com) à venir les rencontrer eux, ainsi que d'autres acteurs, tout nouveaux ou moins du territoire. Cette 3ème édition avait lieu à [La Cordée](https://www.la-cordee.net/cordee/beaujolais/lamure-sur-azergues/), espace de travail partagé de Lamure-sur-Azergues et était organisée en appui avec le réseau des Villages d'accueil.

Bastien et Yannick, qui s'y sont rendus pour le groupe, en sont revenus ravis. Ils ont été accueillis chaleureusement par de nombreuses personnes venant à leur rencontre avec de nombreuses questions et des envies de partenariats et de partage. Nous sommes heureux de pouvoir entrer dans une communauté d'acteurs locaux soudés et si actifs!

Vivement le pique-nique prévu cet été auquel nous viendront plus nombreux!
