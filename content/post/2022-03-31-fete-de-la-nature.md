---
title: Fête de la nature - Jardin de biodiversité
subtitle: Dimanche 22 mai 2022 de 14h15 à 17h15
date: '2022-03-31'
tags:
  - biodiversité
  - permaculture
  - écologie
---
![](/images/uploads/1648759728940_Fete-nature-2022-1.jpg)

Visite de l'écolieu MagnyÉthique avec focus sur les 8 hectares de terrain pour voir les **installations** mises en place (ou prévues) pour **favoriser la biodiversité**.

**On ne peut protéger que ce que l'on connaît**. D'où l'importance de se pencher sur ces milieux, de les observer, de comprendre qui y vit et pourquoi. 

*   nichoirs à passereaux et hirondelles,
*   pierriers pour reptiles,
*   bassin naturel pour amphibiens,
*   hôtels à insectes,
*   haies bocagères

Venez découvrir et observer qui y trouve gîte et/ou couvert, apprendre comment faire pour  créer dans vos jardins et quelle est la place la plus appropriée ! 

La visite pourra se terminer par un petit atelier de **confection d'une mangeoire ou abreuvoir pour la faune.**

Merci de vous inscrire en amont pour permettre une adaptation optimale du programme au nombre de personnes (limité) et aux âges des visiteurs.

Inscription obligatoire à l’adresse suivante : [2022fetenature@magnyethique.org](mailto:2022fetenature@magnyethique.org)
