---
title: Chantier création de mares de biodiversité
subtitle: Samedi 16 novembre 2024
date: '2024-10-11'
tags:
  - biodiversité
  - chantier
  - rencontre
  - écologie
---
Le 16 novembre 2024, en partenariat avec la LPO, nous mettrons en place sur les terrains 3 mares de biodiversité spécifiques pour les amphibiens (tritons, crapauds, salamandre…) et plus spécifiquement pour le petit crapaud "Sonneur à ventre jaune" qui bénéficie d'un programme de protection.

Venez nombreu·x·ses nous aider dans ce chantier qui sera non seulement utile mais en plus très fun !

Inscriptions sur [l'agenda de la LPO](https://auvergne-rhone-alpes.lpo.fr/agenda/creation-de-mares-pour-le-sonneur-a-ventre-jaune-chantier-nature/)

![](https://cloud.magnyethique.org/s/PegcwW5MrAkxtL8/preview)
