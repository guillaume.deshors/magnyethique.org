---
title: Poser les bases d'un collectif impliqué et durable
date: '2022-08-04'
tags:
- formation
- gouvernance
- habitat participatif
- vie de groupe
---
Caroline et Bruno (deux membres du groupe MagnyÉthique) vous proposent deux jours de stage en résidentiel au sein de l'écolieu autour du thème "poser les bases d'un colectif impliqué et durable".

Les 22 et 23 octobre, venez découvrir et vivre des outils qui favorisent confiance et coopération au sein de vos équipes, dans le milieu associatif ou professionnel, au sein d'un habitat participatif.

Vous y expérimenterez un ensemble cohérent de pratiques issues de la gouvernance partagée, de la Communication Nonviolente, de l'intelligence collective et des systèmes restauratifs. Deux jours pour partir du bon pied dans vos aventures collectives ou pour pérenniser vos expériences !

Ce stage est proposé pour une quinzaine de personnes maximum, en participation libre et consciente (voir plus bas).

![](https://cloud.magnyethique.org/s/YTiybqWm9xoH48s/preview) 


### Objectifs : ###

* Découvrir des outils de gouvernance partagée et d’intelligence collective, et au-delà comprendre leurs intérêts, usages et limites.
* Acquérir une compréhension des enjeux et postures qui soutiennent leur mise en œuvre.
* Découvrir le système restauratif (au sens de Dominic Barter) de prévention et régulation des tensions au sein d’une communauté. En connaître les modalités d’élaboration et de mise en œuvre.
* Améliorer ses capacité de communiquer, identifier les obstacles à une communication inter-personnelle authentique et bienveilllante.


### Compétences : ###

* Être capable de présenter à un groupe les principaux outils de gouvernances partagée : décision par consentement, élection sans candidat, chapeaux de Bono, et de les animer.
* Avoir des éléments pour animer une réflexion collective sur les tensions et initier un système restauratif au sein d’une équipe.
* Exprimer un feed-back (positif ou négatif) au service d’une relation. Être en mesure de proposer à un groupe quelques outils pour favoriser une communication sincère et confiante.

### Détails pratiques : ###

Logement en dortoir d’une demi-douzaine de lits (merci de prévoir draps et literie, sac de couchage, ….) ; possibilité de camper ou de dormir dans votre véhicule. Les snackings et repas végétariens à tendance biolocale seront fournis, ainsi que le petit-déjeuner (pain et confitures maisons, thé, café, fruits). Repas de vendredi soir en mode auberge espagnole.
Gare la plus proche : Amplepuis (trains depuis Lyon ou Roanne) Nous pouvons venir vous y chercher si vous nous le demandez, vendredi vers 19h ou samedi vers 9h. 
Accueil samedi matin à partir de 9h pour un début du stage à 9h30 ; fin prévue dimanche 17h.

### Prix libre et conscient : ###

Nous souhaitons à la fois proposer un stage accessible à tous et toutes, et recevoir une rémunération qui nous semble juste. Celle-ci vient valoriser :
* le temps passé lors du stage et des visios (2x20h)
* le temps de préparation et d’élaboration du stage (2x8h)
* le temps consacré à l’organisation (16h)
* Notre investissement dans les diverses formations continues que nous suivons pour approfondir les compétences que nous partageons
* Une part (10 à 20%) versée à l’association MagnyÉthique pour faire vivre notre écolieu

Quelques chiffres pour vous donner une idée de ce que nous attendons au global :
* Si nous récoltons moins de 1200 €, nous sommes dans le rouge et nous considérons que nous sommes perdants : cela nous fera réfléchir à maintenir ce type d’activité.
* Si nous récoltons 1800 €, nous sommes à l’équilibre.
* Si nous récoltons plus de 2200 €, nous aurons de quoi investir dans des formations et améliorer les conditions d’accueil du lieu.

Vous pouvez nous soutenir en incitant vos relations à venir : plus nous serons de participant·e·s, plus cet objectif financier sera facile à atteindre !

### Inscription ###

Renseignez ce formulaire :
https://framaforms.org/poser-les-bases-dun-collectif-implique-et-durable-1662375144
 
Votre inscription sera validée à la réception d'un chèque d'arrhes de 50 € (qui vous sera rendu lors du stage) à l'ordre de Association MagnyÉthique, à envoyer à :
 
MagnyÉthique - stage Bases du Collectif  
Chemin du château de Magny 
69550 Cublize

Nous ne confirmerons pas la réception des arrhes : si vous l'avez envoyé, considérez que c'est bon ! 
N'hésitez pas à nous contacter si vous avez des questions par mail à [2022collectif@magnyethique.org](mailto:2022collectif@magnyethique.org) ou au téléphone au 06 62 28 36 08.
