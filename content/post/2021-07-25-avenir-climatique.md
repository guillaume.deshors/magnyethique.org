---
title: Avenir Climatique - Université d'été décontractée
subtitle: du 8 au 14 août 2021
date: '2021-06-14'
tags:
  - échanges
  - écologie
  - société
---
Cet été, nous avons l'honneur d'accueillir la belle équipe d'[Avenir Climatique pour son université d'été décontractée](https://avenirclimatique.org/luniversite-decontractee-davenir-climatique-2/) !

![Photo de groupe](https://avenirclimatique.org/wp-content/uploads/elementor/thumbs/groupe-nv6mu1951njkowers8eyd7xjc7yzdydyi705dspbgw.jpg)

Qui sont-ils ?

Un groupe de jeunes encadrants dynamiques, engagés à faire connaître et comprendre les enjeux énergétiques et climatiques actuels.

Comment ?

*   avec des ateliers explicatifs du dernier rapport du GIEC
*   par de nombreux échanges avec les partiipants
*   en rencontrant des acteurs engagés dans l'écologie localement
*   en vivant une semaine participative avec gouvernance partagée
*   …

Si vous voulez à la fois vivre une super semaine à leurs côtés et venir nous rencontrer, il est toujours temps de vous inscrire !
