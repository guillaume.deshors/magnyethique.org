---
title: "Atelier «\_Kiffe tes 1ères règles »"
subtitle: Mercredis 29 janvier et 26 février 2025 après-midi
date: '2024-12-09'
tags:
  - échanges
  - atelier
  - société
---
Manon, habitante de l’écolieu, Éducatrice Menstruelle et Naturopathe, propose des ateliers pour découvrir et apprendre à aimer les règles. Quelle étape importante de la vie des jeunes filles, riche en émotions, questionnements, représentations et quelle grande étape de la vie des parents !

Ces ateliers s’adressent aux jeunes filles de 10 à 14 ans ayant leurs règles ou non, accompagnées de leur mère ou de leur père.

Entre temps de partages et d’écoute, jeux autour de dessins et de maquettes de vulve, de vagin et d’utérus, présentation de toutes les protections féminines existantes, constitution collective de la « trousse des 1ères règles », etc., les jeunes filles repartiront informées et en confiance pour accueillir leurs règles ! La relation mère-fille ou père-fille est renforcée, on se connaît mieux, un langage commun est développé, on est plus outillé pour la suite !

![](https://cloud.magnyethique.org/s/zQFn4LYcn2N4CwE/preview)  

Deux événements prévus dans la salle d’activités de l’écolieu :

*   mercredi 29 janvier 2025 de 14h30 à 17h en duos **mères-filles**
    
*   mercredi 26 février 2025 de 14h30 à 17h en duos **pères-filles** – parce que les pères aussi sont de précieux alliés du cycle menstruel !
    

Tarif : 40€ par binôme + adhésion à prix libre à l'association MagnyÉthique

Infos et réservations : manon@femenessence.fr ou 06 16 13 42 26

\+ d’infos sur la page Facebook « FemenEssence» ou sur le site www.femenessence.fr
