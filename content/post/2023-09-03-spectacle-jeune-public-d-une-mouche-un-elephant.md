---
title:  D'une mouche un éléphant
subtitle: Spectacle très jeune public le samedi 23 septembre 2023 à 17h
date: '2023-09-03'
tags:
  - spectacle
  - portes ouvertes
  - rencontre
  - culture
---
Ce samedi 23 septembre à 14h, nous accueillerons une nouvelle édition de nos portes ouvertes. Venez nombreux pour visiter notre lieu de vie, échanger autour de l'habitat participatif et comprendre comment nous fonctionnons. Si vous êtes juste curieux, ça marche aussi !

Pour clore cette demi-journée Visite et Rencontres, nous avons l'honneur d'accueillir une artiste locale que nous apprécions tout particulièremnt : Lucie Carbonne. 
Elle viendra jouer chez nous en avant première son tout nouveau spectable musico-poéto-acrobatique : **D'une mouche un éléphant.**

Lucie tient plusieurs fois par an à Grandris des stages de cirque et spécifiquement de monocycle
auxquels trois des enfants du château participent avec grand plaisir depuis trois ans !
Elle se produit aussi en spectacle avec son monocycle et son accordéon, voici [son site](https://lucie-monocycle.com/).
Ce nouveau spectacle est à destination des enfants à partir de deux ans.

**Spectacle gratuit**. Il est possible de rester après la visite ou de venir juste pour le spectacle.

![](https://cloud.magnyethique.org/s/qSydQcKcGWqbiDc/preview)  

{{< citation >}} 
« D’une mouche un éléphant » est un spectacle circassien et
musical à destination du jeune et du très jeune public, à partir
de deux ans.
Des papiers en couleur, un monocycle, un accordéon : l’artiste
pirouette, virevolte, tourbillonne. Sur son vélo à une roue, elle
peut tout faire, même jouer de l’accordéon, même plier des
origamis.
Entre équilibre et déséquilibre, elle crée un ami de papier et
partage avec lui son univers onirique et musical où la poésie
s’est affranchie des mots.
Tout en douceur, une ode à l'amitié et au partage, une invitation
à ajuster son regard, à ne pas, face au moindre imprévu ou à la
moindre différence, « faire d’une mouche un éléphant ».

{{< /citation >}} 
  

