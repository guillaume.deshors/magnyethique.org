---
title: Initiation aux musiques à danser avec Camille Stimbre
date: '2024-07-24'
tags:
  - culture
  - musique
  - danse
subtitle: Du 28 au 30 août 2024 de 09h30 à 13h00
---

🎻 Camille Stimbre (violoniste dans Bargainatt, Arquèthi, Duo Cam&Léon, ...) propose aux musicien.ne.s une initiation aux musiques à danser de bal trad/bal folk. 

💃 À partir de mélodies choisies, nous tâcherons d'identifier les éléments constitutifs de la danse étudiée, ceux qui font que c'est agréable à danser ou non, et nous mettrons en pratique. La danse sera naturellement utilisée pour aider à l'apprentissage par le corps. 

🎵 Au rythme d'une session par matinée, nous travaillerons la scottish, la bourrée à deux temps, la bourrée à 3 temps, la mazurka... et plus si on a le temps. 

🪗 Pour clôturer le stage (option), nous animerons la première partie de bal avec le Duo Azare au Quartier Métisseur le vendredi 30 août au soir. (Bal trad & folk au Quartier Métisseur)

![](https://cloud.magnyethique.org/s/PdzLWs8B42kfSgT/preview)

🧑‍🤝‍🧑 Ce stage est ouvert à tous niveaux sauf grands débutants.

📆 Dates&nbsp;: les 28, 29, 30 août 2024 de 9h30 à 13h  

💸 Prix&nbsp;: 120€ (40€/jour) (tarif négociable pour les petits budgets)

💁 Plus d'information&nbsp;: 0629875847, [kmillestimbre@gmail.com](mailto:kmillestimbre@gmail.com) et sur [Facebook](https://www.facebook.com/share/3hDvZ1qXbsSPMSXg/)
