---
title: >-
  Stage de chant avec Marianne Sébastien - exprimer par la voix son plein potentiel 
  COMPLET - INSCRIPTION SUR LISTE D'ATTENTE
subtitle: 30 septembre et 1er octobre 2023
date: '2023-05-29'
tags:
  - stage
  - culture
  - formation
---


![](https://cloud.magnyethique.org/s/m6FtyexjPtakdZe/preview)

Nous sommes ravis d'accueillir Marianne Sébastien, qui se définit elle-même comme "accoucheuse de voix".
Fondatrice de ["Voix Libre"](www.voixlibres.org), elle intervient dans de nombreux pays et notamment en Amérique du sud pour permettre à chacun.e de se révéler et d'agir pour le changement de soi et du monde ! Vaste programme...

## Logistique : ##

Actant le succès et l'expérience de Marianne Sébastien, nous avons monté la jauge de ce stage jusqu'à 25 personnes. Il se déroulera de 10h à 16h30 samedi et dimanche ; nous vous accueillons dès 9h30 samedi.
CE STAGE EST COMPLET : nous vous invitons à remplir le formulaire ci-après pour la liste d'attente, et nous vous recontacterons en cas de désistements ou pour vous avertir d'une prochaine venue de Marianne Sébastien dans la région. 

Il y a des possibilités d'hébergement sommaires sur place (dortoir, camping, véhicule aménagé) et des solutions plus confortables en gîte ou chambre d'hôtes existent à proximité.

Les repas du midi seront végétariens à tendance biolocale et servis sur place ; pour celui du samedi soir nous vous proposons une auberge espagnole : venez avec ce que vous aimez partager, plat salé ou dessert pour quatre à six personnes. Pour le petit déjeuner nous aurons le pain et les confitures confectionnés sur place ; thé, café, tisanes seront aussi disponibles. S'il vous faut quelque chose de spécifique nous vous invitons à l'apporter.

Vous pouvez arriver dès le vendredi soir ; merci de nous le signaler dans le formulaire d'inscription plus bas, et de noter que le repas du vendredi soir n'est pas prévu. 

## Transport : ##

Nous vous invitons à privilégier les transports collectifs. Nous sommes à une douzaine de kilomètres de la gare SNCF d'Amplepuis, et nous proposerons une navette depuis la gare vendredi soir vers 20h45 et samedi matin vers 9h. Merci de nous en faire la demande sur le formulaire d'inscription.

Si vous souhaitez utiliser votre véhicule, pensez à vous inscrire sur [Togetzer](https://www.togetzer.com/covoiturage-evenement/r3vzen) pour permettre à d'autres de profiter de votre trajet.


## Tarif : ##

Le prix du stage est de 250 € pour la partie pédagogique, 22 € (2x11€) pour les deux repas, et participation libre et consciente pour le logement, le petit-déjeuner, l'utilisation des salles, l'organisation du stage et le soutien au projet MagnyÉthique.

Nous avons envie que ce stage soit aussi accessible aux personnes ayant de faibles revenus : si vous souhaitez en faire la demande, merci de nous contacter.

## Inscription : ##

Votre inscription sur liste d'attente se fait via [ce formulaire](https://framaforms.org/inscriptions-stage-chant-avec-marianne-sebastien-1686141528).
Nous garderons également vos coordonnées pour vous avertir d'un prochain stage avec Marianne Sébastien dans la région.

Pour tout renseignement ou question, n'hésitez pas à nous contacter à [2023chant@magnyethique.org](mailto:2023chant@magnyethique.org) 