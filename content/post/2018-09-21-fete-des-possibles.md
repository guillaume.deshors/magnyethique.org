---
title: Fête des possibles
subtitle: 21 et 22 septembre 2018
tags:
  - échanges
  - conférence
date: '2018-09-21'
---
![](/images/uploads/image6.png)  

Dans le cadre de la [Fête des possibles](https://fete-des-possibles.org/rdv/forum-des-habitats-partages-et-solidaires/ "https://fete-des-possibles.org/rdv/forum-des-habitats-partages-et-solidaires/") lors de laquelle nous avions proposé une rencontre l'an dernier à Clérieux, Yves s'est rendu cette année à Crest pour le Forum des Habitats participatifs et solidaires. L'accent était mis sur les habitats participatifs qui accueillent des personnes en difficulté, principalement handicapées. Présentation de l'un d'eux mais aussi des conférences abordant différents aspects de la question.
