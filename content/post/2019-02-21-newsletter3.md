---
title: Newsletter 3
date: '2019-02-21'
tags:
  - newsletter
  - avancement
---

Bonjour ami.es MagnyÉthiques,

L’année 2019 n’a pas commencé depuis si longtemps et pourtant, tellement de choses se sont déjà passées! 

### Travail et plaisir

Les commissions travaillent d’arrache-pied, dans la bonne humeur. Les plénières sont très efficaces grâce à ce travail en amont et à la méthode de prise de décision de gestion par consentement avec laquelle tout le monde est à présent assez familiarisé pour que nous trouvions notre rythme de croisière. Une toute nouvelle formation interne prévue dans le processus d'intégration des nouveaux membres y a bien contribué et le travail commun est autant efficace qu’agréable.

Niveau rencontres aussi, les choses avancent bien et nous commençons à tisser des liens intéressants et enrichissants avec les acteurs du Beaujolais Vert. Quel dynamisme dans ce petit coin de pays! Nous sommes ravis de pouvoir y apporter humblement notre pierre prochainement. 

Deux rencontres récentes nous ont tout particulièrement apporté, dont nous aimerions vous parler un peu: 

Vendredi 8 février au soir, Yannick et Bastien, représentants de MagnyÉthique se sont rendus à Lamure-sur-Azergue pour participer à un apéro organisé par **‘Beaujolais Vert Votre Avenir’**, syndicat mixte du Beaujolais, dans les locaux de l’espace de coworking La Cordée. Une quarantaine de personnes nouvellement installées dans la région étaient présentes. Ce fut une foultitude de belles rencontres et de retours d’expériences trèèèèès intéressants ! Durant cette soirée, nos deux initiateurs (qui n’étaient pas intéressés que par l’apéro convivial) ont pu échanger avec un maire, un agriculteur bio, des informaticiens (tout open source), des personnes du Quartier Métisseur, d’autres tournées vers l’autoconstruction, l’environnement, et aussi avec des habitants du château de Longeval. Celui-ci est situé à moins de 15 km de Cublize et a été en refait à 70 % en auto construction pour accueillir des yogistes de tous horizons.

Ils en ont tous des choses à nous transmettre, n’est-ce pas ?

![dsfdsf](/images/newsletters/3/image1.jpg)

Le lendemain, c’est Charlotte, Marie et Noémie (autres représentantes) qui ont assisté au ‘**ça me dit café-Débat**’ à la MJC d’Amplepuis. Il était question d’auto-construction et d’éco-rénovation - trouver un équilibre entre faire soi-même et faire-faire. Quatre éco-rénovateurs, en tant que professionnels ou de façon personnelle, ont donné un aperçu de leurs savoirs.

La MJC accueillait et présentait les participants. Elle est un bel exemple d’accélérateur d’initiatives locales.
 
Il va sans dire que plus nous avançons et plus nous découvrons une région grandement engagée écologiquement et solidaire. Elle a tout pour nous séduire !



### Faire-part

Les sujets touchant à l’identité du projet lancés à l’automne dernier touchent enfin au but. Nous sommes ravis dans cette lettre de vous annoncer la naissance de deux petits nouveaux qui nous sont chers!

Commençons par notre tout nouveau logo:


![logo](/img/logo-full-marges.svg)

N’est-il pas beau !?

Nous tenons à remercier les 9 créateurs qui nous ont fait pas moins de 16 propositions en répondant à notre appel de décembre! Nous avons procédé par jugement majoritaire (lien: https://jugementmajoritaire.net). Les résultats du premier tour ont été très serrés et hétéroclites. Quatre logos se détachaient du lot, ne donnant cependant pas entière satisfaction. Nous avons donc suggéré des améliorations à leurs créateurs avant de les repasser au jugement majoritaire pour choisir finalement ce logo-ci.

Nous reprendrons rapidement contact avec les créateurs pour voir avec eux à quel moment ils veulent être nos hôtes. Encore MERCI !

Le deuxième bébé du mois? Peut-être encore un peu prématuré, nous allons lui apporter quelques soins. Il va encore être affiné, peaufiné et complété au fur et à mesure mais l’accouchement d’un site est long et l’essentiel y est déjà alors nous vous laissons aussi le découvrir : 

Les impatients y trouveront dans la catégorie “événements” un calendrier où surveiller autrement que par la newsletter nos futures dates de rencontres. Très prochainement y apparaîtront aussi les dates des premiers travaux participatifs et stages. Mais vous pouvez dores et déjà retenir la date du samedi 6 juillet à laquelle nous ouvrirons à nouveau les portes du château pour les intéressés et/ou les curieux!


En attendant les prochaines surprises dans notre quatrième lettre, nous vous envoyons de bonnes ondes chargées de prémices printanières!

À bientôt!

MagnyÉthique 