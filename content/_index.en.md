---
title: MagnyÉthique
---

{{< chateau >}}  

The MagnyÉthique project (ecoplace at Magny Castle in Cublize, Rhône, France), founded in June 2019, is a global project articulated around permaculture, in its social as well as agricultural acception.

The founders and participants to this project wish to favor in and near the location mixity, cooperation and cohesion of systems around 4 complementary poles of realisation :

*   {{< liencouleur "#4b892f" >}}Cohousing{{< /liencouleur >}}
*   {{< liencouleur "#743068" >}}Coworking{{< /liencouleur >}}
*   {{< liencouleur "#34618e" >}}Public space{{< /liencouleur >}}
*   {{< liencouleur "#8c6334" >}}Handicraft and agroecology{{< /liencouleur >}}

In order to satisfy these needs, an ecological, social, cultural, economical and solidary dynamic has been put in place. 
Let's hope that our project will be the reference on a local, regional, national and why not international plan.

Our name "MagnyÉthique" is composed of two elements:

* "Magny" is the name of the castle where we have settled,
* the word "éthique" (= ethics) defines a reflection on behaviours 
  to be adopted in order to bring humanity to the world. 
  It tends to achieve a societal ideal through the application of committed behaviours.

Furthermore, MagnyÉthique is a pun as it is pronounced like "magnétique" which means magnetic.
We would like to attract -- like a magnet -- a lot of people, be it driven by curiosity or
by the will of joining us for a short-time or a long-term commitment.

As you'll find out, the english and german versions of this site have been removed, as we found out that we barely find time to maintain the french version. Pease refer to the french version in cas you need information, use an automatic translation tool, or contact us directly. Thanks !