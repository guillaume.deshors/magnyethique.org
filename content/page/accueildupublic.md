---
title: Accueil du public
comments: false
---
L’aile du Château de Magny anciennement allouée à l'hôtellerie et la restauration va être réhabilitée et permettra l’accueil journalier et/ou en hébergement sur place de groupes pour des activités spécifiques.

![](/images/uploads/1580300990666_illustration-accueil-public.jpg)

Capacités
=========

Notre priorité pour les travaux est la rénovation des appartements d'habitation. Cependant, l'accueil de public est une valeur fondatrice du projet et certains espaces du lieu peuvent déjà accueillir des groupes pour des stages, cours, résidences ou autres séminaires. Nous avons rafraîchi la "salle restaurant" de 50 m2 environ pour en faire la "salle restaurée" : lumineuse, dotée d'un poêle à bois, d'un piano, d'un vidéo-projecteur et d'un écran, elle jouxte une cuisine qui peut être mise à disposition de groupes extérieurs.

_C'est là que nous accueilli Christian Lefaure pour sa conférence gesticulée "Sculpteurs de Mondes", le chœur Homnium pour des répétitions, ou encore Marc Vella pour son stage "rendre belles les fausses notes de la vie" ...

![](/images/uploads/1630592418616_Salle_restaurée.JPG)

Au-dessus, la grande salle d'activité mesure près de 100 m2. Avec son sol en parquet, ses poutres apparentes, elle se prête à tout type d'activités physiques.

_Nous y avons accueilli quelques résidences de compagnies de théâtre, une rencontre de danse-contact-improvisation, des stages de CNV…_

![](/images/uploads/1630592506465_Rencontre_CI.jpg)

Pour des événements qui se déroulent sur deux jours ou plus, nous avons la possibilité de faire dormir sur place une quinzaine de personnes dans deux dortoirs, et plus sous tente.

_L'université d'été d'Avenir Climatique a réuni chez nous une quarantaine de participant·e·s durant une semaine en août 2021, et nous avons aussi accueilli deux étapes de l'Alter Tour en juillet 2020 avec près de 80 cyclistes._

![](/images/uploads/1630592560282_compoMagny1appla.jpg)

* * *

Usagers de l'espace public de l'écolieu
=======================================

Association MagnyÉthique

[  
![](https://secure.gravatar.com/avatar/8f90472d138389d8199631e08e5769de?s=200&r=pg&d=mm#floatright)](https://permalab.fr/)

*   [PermaLab](https://permalab.fr/) est venu donner une première [formation permaculture](/post/2019-09-formation-permaculture/) au mois de septembre 2019 et est revenu en [juillet et octobre 2020](/post/2019-10-formation-permaculture/).

{{< clear >}}

[![](/images/uploads/AT-CONSEIL_logo-VF_2021_invers%C3%A9.jpg#floatright)](https://atconseil.com/)

*   Édith Tavernier d'[AT Conseil](https://atconseil.com/) a réalisé plusieurs formations CNV.

{{< clear >}}

[![](/images/uploads/logo-h&p-2021.jpg#floatright)](https://habitatetpartage.fr/)

*   [Habitat et partage](https://habitatetpartage.fr/), société coopérative d'intérêt collectif, spécialisée dans le développement et l'accompagnement de groupes et projets d'habitat participatif, est venu faire un séminaire professionnel.
