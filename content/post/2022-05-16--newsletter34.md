---
title: "Newsletter 34 : Biodiversité, chantiers et festivités : c'est le programme printemps, été."
date: 2022-05-16
tags:
  - newsletter
  - avancement
---

### Bonjour ami·es MagnyÉthiques
­

Le printemps se poursuit, dans la continuité de tout ce qui avait été initié et que nous avions eu plaisir à vous conter dans notre dernière lettre. De beaux projets prennent forme. Laissez-nous vous les conter.

­
![](/images/uploads/2021-12-16--newsletter31_img1)

­
### SAVE THE DATES Les prochains ateliers, stages, événements…

­

*   **Dimanche 22 mai 2022**   
    [Visite et atelier “Jardin de biodiversité” dans le cadre des Journées de la Nature](https://magnyethique.org/post/2022-03-31-fete-de-la-nature/) en tant que Refuge LPO  
     
*   **Du jeudi 26 au dimanche 29 mai 2022**   
    [Stage Danse-contact-impro et Axis Syllabus](https://magnyethique.org/post/2022-03-01-26-au-29-mai-2022-stage-danse-contact-impro-et-axys-syllabus/) (complet)

*   **Dimanche 26 juin 17h-22h** au tiers-lieu “L’Atelier d’Amplepuis”  
     La Cotonnerie et le Château de Magny seront parmi les principaux invités autour de la Table ronde festive et fertile :  
    « Les nouvelles manières d’HABITER le Beaujolais Vert »

*   **1er juillet**   
    Concert Baklava (Bal folk grec) au château de Magny.  
    Organisé avec [Courant d'Art](http://courantdartbeaujolaisvert.fr/)

*   **Samedi 8 octobre**   
    Soirée conférence & ateliers   
    **dimanche 9 octobre**   
    Journée bien-être au château de Magny (programme en préparation)…  
     
*   **Samedi 15 et dimanche 16 octobre 2022**   
    Atelier formation "Poser les bases d'un collectif impliqué et durable"  
    Informations et inscriptions par e-mail : [\[email protected\]](/cdn-cgi/l/email-protection#90a2a0a2a2f3fffcfcf5f3e4f9f6d0fdf1f7fee9f5e4f8f9e1e5f5beffe2f7afe3e5f2faf5f3e4add9fee3f3e2f9e0e4f9fffee3b5a2a0bdb5a2a0e2f5fee3f5f9f7fef5fdf5fee4e3b5a2a0b5a3d1b5a2a0c0ffe3f5e2b5a2a0fcf5e3b5a2a0f2f1e3f5e3b5a2a0f4b6b3a3a9abe5feb5a2a0f3fffcfcf5f3e4f9f6b5a2a0f4e5e2f1f2fcf5)

­
![](/images/uploads/2022-01-22--newsletter32_img1)

­

### ENVIE D'AIDER ? Calendrier des prochains chantiers participatifs



*   Du **lundi 4** au **vendredi 22 juillet 2022**  
     
*   Du **lundi 8** au **vendredi 19 août 2022**  
      
    [\+ d'infos sur les modalités d'inscription](https://magnyethique.org/page/wwoofing_chantiers/)



### En direct des chantiers  
Objectifs : deux crémaillères de plus à l’été

­

Notre chantier participatif du printemps s’est tenu du 25 avril au 6 mai. Les inscriptions tardives nous ont fait craindre une faible participation, et finalement nous avons eu quatre volontaires la première semaine, et une douzaine la seconde !  
Nous avons fait des cloisons, de l’électricité, de la plomberie, de la sous-couche et de l’enduit, pas mal de rénovation de fenêtres en bois, un chantier remplacement de fenêtre, un chantier plafond…   
Les différents talents culinaires, musicaux et techniques des volontaires ont pu s’exprimer pleinement lors des différents temps, et nous ont bien épatés.  
 

![](/images/uploads/2022-05-16--newsletter34_img1)

­

Comme toujours, nous terminons chaque journée de chantier par un temps de météo et nous clôturons la semaine avec un temps de bilan un peu plus long, pour que tout le monde se sente bien et pour avoir un retour sur ce qui marche et ce qui peut être amélioré. Ces temps sont en général très appréciés (malgré un scepticisme initial de certains) et participent à faire entrer l’humain dans le quotidien du chantier.

­

![](/images/uploads/2022-05-16--newsletter34_img2)

­

Merci à Christophe, Balthazar, Odyle, Éloïse, Jonathan, Nadège, Aurore, Marine, Caroline, Marjolaine, Grégory, Flora et Laëtitia !



­

### Pendant ce temps… À vos marques, prêts, bzzzz !!!

­

Le samedi 7 mai dernier, Agnès, Corinne et Katia ont participé à une initiation à l’apiculture chez Félix Gravant ([**The Place to bee**](https://theplacetobee.fr/stages-dinitiation/)) en vue de l’installation de ruches à l’écolieu. Les essaims sont commandés pour mi-juin, le temps d’entretenir le matériel transmis à Katia par son père. 

­

![](/images/uploads/2022-05-16--newsletter34_img3)

­

### Á 5, Touché !

­

Au même moment, les gars de l’écolieu s’initiaient au Touch Rugby à cinq   (Rugby Toucher) au tournoi des Sapins à Amplepuis. Et malgré leur niveau débutant, sous les encouragements des petits MagnyÉths et de quelques autres supporters, ils ont réussi à marquer 6 essais dans l’après-midi.   
Bravo à l’équipe des “Bras Cassés” ! 

­

![](/images/uploads/2022-05-16--newsletter34_img4)

­

![](/images/uploads/2022-05-16--newsletter34_img5)

­


### Jardin de biodiversité

­

À l’occasion de [**la Fête de la Nature**](https://fetedelanature.com/edition-2022) 2022, qui aura lieu partout en France du 18 au 22 mai, Katia propose un jeu de piste pédagogique sur les terrains de l’écolieu, refuge LPO depuis deux ans.   
Les participant·e·s y découvriront la biodiversité et les aménagements pouvant la favoriser. 

­
Pour clore la visite, un petit atelier manuel de confection de mangeoire ou d’abreuvoir à passereaux et/ou insectes sera proposé. Pour avoir plus de détails sur cette demi-journée, [**RDV sur le site de la Fête de la Nature**](https://fetedelanature.com/mon-compte/mes-manifestations/edition-2022/visite-jardin-de-biodiversite)**.**  
Inscription obligatoire à l’adresse suivante pour permettre la préparation du matériel : [**\[email protected\]**](/cdn-cgi/l/email-protection#c8faf8fafaaeadbcada6a9bcbdbaad88a5a9afa6b1adbca0a1b9bdade6a7baaff7bbbdaaa2adabbcf581a6bbabbaa1b8bca1a7a6edfaf88eed8bfbed8989bcadedfaf8acadedfaf8a4a9edfaf8a6a9bcbdbaadedfaf8faf8fafa) 

­
­

![](/images/uploads/2022-05-16--newsletter34_img6)



### Les deux “Maxime”

­

Nous effectuons ce mois-ci la transition des Maxime à l’écolieu.   
Notre Maxime Service Civique a terminé sa mission chez nous le jeudi 5 mai. Un grand merci à lui pour son aide, sa curiosité, sa motivation, ses réflexions partagées. Nous lui souhaitons beaucoup de bonheur pour la suite !  
Et depuis avril, nous accueillons un nouveau Maxime en nos murs, ancien ingénieur et maraîcher, il vient découvrir notre projet et nous apporter son savoir-faire et son expérience pour le maraîchage. Des conseils précieux pour nous qui nous lançons plein de bonne volonté mais peu aguerris dans la belle aventure de la culture de la terre ET de l’accueil de wwoofeur·euse·s ! Voici donc une image de notre équipe actuelle préparant la plantation des tomates.  
 

![](/images/uploads/2022-05-16--newsletter34_img7)

­

### Noces de froment


En début de mois prochain, cela fera trois ans que nous aurons acheté ce qui est aujourd’hui devenu bien plus qu’une grande maison partagée : lieu de convivialité, de rencontres, d’activités et de formation, de découvertes, d’apprentissage ou d’évolution.   
Les questions de pandémie ne nous ont permis de fêter les premier et deuxième anniversaires que très timidement. Nous avons eu envie cette année d’une vraie célébration. Nous ne parlerons pas de cercle restreint puisque l’invitation de nos proches à tou·te·s débouche sur une assemblée d’une centaine de personnes. Mais pour cette occasion, c’est avec nos familles et amis proches, dans un grand bain intergénérationnel, que nous avons choisi de partager un week-end entier. L’occasion aussi de faire connaissance avec tou·te·s, de mieux nous connaître en instaurant ce cadre intime.  
Mais parce qu’un anniversaire se doit d’être fêté, nous allons nous offrir de chouettes moments en accueillant pour l’occasion divers artistes qui viendront animer notre week-end et qui contribueront ainsi à rendre ce troisième anniversaire inoubliable.

Prenez le temps de vous immerger dans une nature luxuriante, remplie de couleurs, d’odeurs, de sons. Nous espérons vous voir prochainement, lors de l’une ou l’autre manifestation estivale.

­

­