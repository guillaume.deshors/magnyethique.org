---
title: Journée portes ouvertes & Fête des possibles
subtitle: Dimanche 15 septembre 2019
date: '2019-07-24'
tags:
  - portes-ouvertes
  - échanges
  - promotion
---
Nous aurons le plaisir de vous accueillir le dimanche 15 septembre 2019 à 10h30 pour nos prochaines portes ouvertes qui se tiendront dans le cadre de la Fête des Possibles.

  

Au programme:

\- présentation du projet

\- visite guidée des lieux

\- pique-nique partagé (ouvert à tou.tes sans inscription)

  

Intéressé.es par l’habitat participatif en général, l’écolieu MagnyÉthique en particulier ou simples curieu.ses, vous êtes les bienvenu.es ! Merci de nous contacter via l’onglet “Contactez-nous!” pour vous inscrire afin que nous puissions nous organiser au mieux !
