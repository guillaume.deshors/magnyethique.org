---
title: MagnyÉthique
---
{{< chateau >}}

Le projet MagnyÉthique, écolieu au Château de Magny à Cublize (69550) depuis juin 2019, est un projet global dont les piliers trouvent leur fondement dans la permaculture, aussi bien sociale qu'agricole.

{{< dernieresactus >}}

En tant qu'initiateurs et participants à ce projet, nous souhaitons favoriser sur et autour du lieu la mixité, la coopération et la cohésion des systèmes, et ce sur quatre pôles :

*   {{< liencouleur "#4b892f" "page/habitatparticipatif" >}}Habitat participatif{{< /liencouleur >}}
*   {{< liencouleur "#743068" "page/espace_pro" >}}Espace professionnel partagé{{< /liencouleur >}}
*   {{< liencouleur "#34618e" "page/accueildupublic" >}}Espace d’accueil de public{{< /liencouleur >}}
*   {{< liencouleur "#8c6334" "page/agroecologie" >}}Agroécologie{{< /liencouleur >}}

Pour mener ce projet à bien, une dynamique écologique, sociale, culturelle, économique et solidaire forte est mise en place. 

Notre raison d'être est de _“**partager un lieu de vie intégré à son environnement, rêver et co-construire son écosystème résilient et ouvert.**”_

Notre nom, “MagnyÉthique” est composé de deux éléments :

*   Magny fait bien sûr référence au lieu sur lequel nous nous sommes installés,
*   le mot éthique définit une réflexion sur des comportements à adopter dans le but d'apporter de l'humanité au monde. Elle tend à atteindre un idéal sociétal par l'application de comportements engagés.

Sans oublier le fait que nous espérons pouvoir attirer “tel un aimant” de nombreuses personnes, curieuses ou engagées à nous rejoindre ponctuellement ou pour plus longtemps!
