---
title: Événements culturels
subtitle: ''
tags: []
---
{{< calendar urls="https://cloud.magnyethique.org/remote.php/dav/public-calendars/CjTXxEd5BDFzW5NG/?export" colors="#FFA07A" tiny="true" >}}

Cette page présente le calendrier des événements culturels animés par le projet ou se passant sur le lieu.

{{< liste_evenements >}}
{{< evenement
    date="2025-04-12T17:00:00.000Z"
    afficheHeure=true
    titre="Concert de fin de rencontre Twiza"
    description="**Anaïs & Stuart** nous font l'honneur d'un concert pour clore la rencontre Twiza."
>}}
{{< evenement
    date="2025-05-18T14:00:00.000Z"
    afficheHeure=true
    titre="[Calamity Jeanne](https://linktr.ee/calamityjeanne)"
    description="Pour récompenser les (presque) trente-six heures naturalistes, le groupe lyonnais Calamity Jeanne nous font voyager en musique."
>}}
{{< evenement 
    date="2025-06-07T20:00:00.000Z"
    afficheHeure=true
    titre="Bal folk avec [Zamaka](https://zamakaduo.wixsite.com/zamaka)"
    description="Un événement spécial pour le lendemain de nos six ans: un bal folk! Ça va danser la bourrée, le branle, la valse, la polka, la mazurka, et peut-être même des danses farfelues. Nous avons hâte d'y être!"
>}}
{{< evenement 
    date="2025-02-02T16:30:00.000Z"
    afficheHeure=true
    titre="[Spectacle Reflet -- **en rodage**](/post/2025-01-24-reflet) !"
    description="La troupe du spectacle REFLET vous convie à une représentation de sortie de résidence."
>}}
{{< evenement 
    date="2025-01-19T17:00:00.000Z"
    afficheHeure=true
    titre="[Nosferatu -- ciné-concert](/post/2025-01-03-nosferatu) !"
    description="Nous accueillons un trio à cordes pour mettre en musique le film Nosferatu de Murnau de 1922."
>}}
{{< evenement 
    date="2024-12-01T16:00:00.000Z"
    dateFin="2024-12-01T17:30:00.000Z"
    afficheHeure=true
    titre="Pièce de théatre Pasiphaé"
    description="tragi-comédie [Pasiphaé](/post/2024-10-24-piece-de-theatre-pasiphae) de Fabrice Hadjadj qui revisite le mythe antique de la mère du Minotaure par La Compagnie Arnold Schmürz"
>}}
{{< evenement 
    date="2024-08-28T09:30:00.000Z"
    dateFin="2024-08-30T13:00:00.000Z"
    afficheHeure=true
    titre="Initiation aux musiques à danser"
    description="Notre voisin Camille Stimbre propose un stage de musiques à danser à l'écolieu. 👉️ [infos ici](/post/2024-07-24-initiation-aux-musiques-a-danser)"
>}}
{{< evenement 
    date="2024-07-12T19:45:00.000Z"
    afficheHeure=true
    titre="Musique traditionelle cubaine \"Son\""
    description="Le groupe cubain **Esencia Sonera** nous fait l'honneur d'un concert à l'écolieu. 👉️ [infos ici](/post/2024-04-02-concert-musique-trad-cubaine)"
>}}
{{< evenement 
    date="2024-06-29T15:00:00.000Z"
    afficheHeure=false
    titre="Vélotopie"
    description="Dans le cadre du festival **Du foin sur les planches**, nous accueillons une étape de la **Vélotopie**."
>}}
{{< evenement 
    date="2024-05-26T15:00:00.000Z"
    afficheHeure=true
    titre="concert Iels en Voix et conférence sur l'eau"
    description="Tarif conseillé 12€ ou plus ! [infos ici](/post/2024-04-05-concert-iels-en-voix)"
>}}
{{< evenement 
    date="2024-05-05T15:00:00.000Z"
    afficheHeure=true
    titre="Voyage au cœur du piano"
    description="atelier-conférence avec l'accordeur Benjamin Charrière. Prix libre. [infos ici](/post/2024-04-29-voyage-au-coeur-du-piano)"
>}}
{{< evenement 
    date="2024-05-17T20:00:00.000Z"
    afficheHeure=true
    titre="conférence-concert avec Marc Vella"
    description="Tarif 10€ ou plus ! [infos ici](/post/2023-11-03-stage-marc-vella)"
>}}
{{< evenement 
    date="2023-09-30T18:00:00.000Z"
    afficheHeure=true
    titre="Projection à prix libre d'un documentaire d'une heure sur le travail de [Marianne Sébastien](https://magnyethique.org/post/2023-05-29-stage-chant-marianne-sebastien/), en présence de Marianne Sébastien pour la discussion qui suivra"
>}}
{{< evenement 
    date="2023-09-23T00:00:00.000Z"
    titre="[D'une mouche un éléphant](https://magnyethique.org/post/2023-09-03-spectacle-jeune-public-d-une-mouche-un-elephant/)"
    description="spectacle circassien dès tout jeune âge"
>}}
{{< evenement 
    date="2023-06-16T00:00:00.000Z"
    titre="[D'un instant à l'autre](https://magnyethique.org/post/2023-06-08-dun-instant-a-lautre/)"
>}}
{{< evenement 
    date="2023-06-03T00:00:00.000Z"
    titre="[conférence gesticulée \"Un Rubik's Cube dans les Urnes ou comment résoudre le casse-tête démocratique\"](https://magnyethique.org/post/2023-05-11-conference-gesticulee-democratie/)"
>}}
{{< evenement 
    date="2023-04-15T00:00:00.000Z"
    titre="[soirée vocale : sortie de résidence, chanson](https://magnyethique.org/post/2023-04-07-soiree-vocale/)"
>}}
{{< evenement 
    date="2023-03-24T00:00:00.000Z"
    titre="[récital à quatre mains et projection du film \"Les jours Heureux\", en soutien à une caisse de grève](https://magnyethique.org/post/2023-03-16-projection-les-jours-heureux/)"
>}}
{{< evenement 
    date="2023-03-16T00:00:00.000Z"
    titre="[concert-conférence de Marc Vella](https://magnyethique.org/post/2022-10-16-marc-vella/)"
>}}
{{< evenement 
    date="2023-03-18T00:00:00.000Z"
    titre="[conférence gesticulée \"un jongleur se déballe\"](https://magnyethique.org/post/2023-01-18-conference-gesticulee/)"
>}}
{{< evenement 
    date="2022-12-02T00:00:00.000Z"
    titre="[conférence d'Hugues Mouret \"Protéger les pollinisateurs sauvages : gîte et couvert\"](https://magnyethique.org/post/2022-10-23-conference-hugues-mouret-proteger-les-pollinisateurs-sauvages/)"
>}}
{{< evenement 
    date="2022-11-09T00:00:00.000Z"
    titre="[rediffusion en direct de l'événement RDV Cosmos](https://magnyethique.org/post/2022-09-29-rdv-cosmos/) dédié à la santé des sols (documentaire + discussion avec spécialistes)"
>}}
{{< evenement 
    date="2022-10-21T00:00:00.000Z"
    titre="[\"Randonnée céleste\"](https://magnyethique.org/post/2022-09-11-randonnee-celeste/)"
>}}
{{< evenement 
    date="2022-08-19T00:00:00.000Z"
    titre="[\"Atapa & Stuart O'Connor\", concert folk-rock-poétique](https://magnyethique.org/post/2022-07-27-concert-folk-rock-poetique/)"
>}}
{{< evenement 
    date="2022-06-05T00:00:00.000Z"
    titre="[\"Et si ce n'est toi\", pièce de théâtre](https://magnyethique.org/post/2022-06-16-piece-de-theatre-si-ce-n-est-toi-mardi-5-juillet/)"
>}}
{{< evenement 
    date="2022-07-01T19:00:00.000Z"
    titre="[Concert de Baklava dans le cadre du festival \"Du Foin sur les Planches\"](https://magnyethique.org/post/2022-06-07-dufoinsurlesplanches/)"
    afficheHeure=true
>}}
{{< evenement 
    date="2021-10-22T00:00:00.000Z"
    titre="[Conférence projection de Marc Vella](/post/2021-05-09-marc-vella-quotmektoub-pianoquot/) \"Mektoub Piano\" ou \"La Caravane amoureuse en Casamance\""
>}}
{{< evenement 
    date="2020-09-12T00:00:00.000Z"
    titre="[Ciné-échange](/post/2020-08-08-quotun-ecolieu-pourquoi-et-comment-quot/) autour du documentaire \"La Voie des écolieus\" par IdéActes"
>}}
{{< evenement 
    date="2020-03-12T19:00:00.000Z"
    titre="Conférence gesticulée [\"Sculpteurs de mondes\" de Christian Lefaure](/post/2020-02-21-conference-gesticulee/)"
    afficheHeure=true
>}}
{{< evenement 
    date="2020-08-24T00:00:00.000Z"
    titre="[Théâtre à Magny](/post/2019-07-du-theatre-au-chateau-/)"
    description="adhérents de l'association"
>}}
{{< evenement 
    date="2020-08-23T00:00:00.000Z"
    titre="[Théâtre à Magny](/post/2019-07-du-theatre-au-chateau-/)"
    description="adhérents de l'association"
>}}
{{< /liste_evenements >}}
