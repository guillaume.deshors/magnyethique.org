---
title: Design permacole
date: '2021-09-25'
tags:
  - permaculture
  - écologie
  - biodiversité
subtitle: Quand les extérieurs se dessinent
---
Samedi 25 septembre 2021, la commission agricole a présenté au reste du groupe le rendu du **design agricole** effectué ces deux dernières années.

![](/images/uploads/1634760469447_Design.jpeg)  

Grâce aux formations et à l'accompagnement de [PermaLab](https://permalab.fr), les membres de la commission ont acquis des connaissances sur les **principes et diverses méthodes de la permaculture**, ont observé les terrains, précisé les envies et besoins de l'ensemble du groupe pour développer les projets qui semblaient pertinents sur l'écolieu, les agencer de façon pertinente et complémentaire sur les plans, monter des dossiers d'étude autour de chacun d'entre eux. Et ils ont enfin fait des propositions précises au groupe.

Ainsi, nous avons à présent non seulement une image claire des richesses et lacunes de nos terrains, et des possibilités qu'ils nous offrent. Mais surtout, nous avons abouti à une **projection collective** de ce que nous souhaitons y faire. Ainsi, certains terrains seront destinés à l'**autonomie alimentaire** du collectif, d'autres à une production qui s'adressera à l'accueil de public. Certains lieux seront plutôt calmes tandis que d'autres offriront des possibilités d'**accueil de public en extérieur**…

Un mot d'ordre dans tout cela : le **respect de la biodiversité** que nous souhaitons non seulement **conserver mais aussi favoriser et valoriser**. Par exemple, en réhabilitant et créant des mares naturelles, des haies, des lisières riches et variées. La création d'un grand verger conservatoire devrait également permettre de sauvegarder des variétés rares, locales, et/ou anciennes tout en offrant sur place un beau terrain de jeu à des temps de sensibilisation à la faune et la flore. Un petit projet apicole trouvera sa place dans cet ensemble, ainsi qu'une probable champignonnière. Et les espaces d'activités humaines se fondront dans un écrin de verdure où faune et flore resteront à l'honneur.

**Bref : tout un programme ! **

Et nous avons à présent une idée assez précise des investissements financiers, humains et matériels qui nous ont permis de définir les étapes organisationnelles jusqu'à la création de ce bel ensemble.

Encore un grand merci à [PermaLab](https://permalab.fr) pour son expertise, son accompagnement pertinent, sa patience parfois et pour les liens que nous avons pu tisser avec chacun de ses membres ! Vivement les futurs chantiers participatifs pour mettre en place tous ces éléments pour un écolieu fantastique !
