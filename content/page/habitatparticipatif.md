---
title: Habitat participatif
comments: false
---
Le projet MagnyÉthique mettra à disposition un appartement individuel complété par des espaces de vie communs pour environ 14 foyers intergénérationnels et interculturels. Le tout dans un esprit d’économie, de partage des espaces et d'écologie.

![](/images/uploads/1580301020030_illustration-habitat-participatif.jpg)

Capacités
=========

L’esprit de l’habitat participatif à l'écolieu MagnyÉthique est d’offrir à chaque foyer une zone privative permettant de se retirer et d’aspirer à son calme d’une part, mais aussi de nombreux espaces communs favorisant l’esprit collectif. Nous pensons que le partage des espaces et de certains mobiliers et matériels qui vont avec est un acte écologique, allant à l’encontre de la surconsommation des biens immobiliers, mobiliers et matériels. Les bâtiments de l'écolieu offrent un potentiel énorme pour l’habitat participatif, appartements en RDC, aux 1er et 2nd étages. La répartition de quelques appartements va être revue pour améliorer la diversité des offres et ainsi la diversité des foyers (de la personne seule à la famille nombreuse). Ainsi, nous aurons à disposition environ 14 logements entre 35 et 110m² habitables. À cette surface purement privative s’ajoutent les espaces communs : grande salle commune avec bureau associatif, bar et cuisine semi-professionnelle ; buanderie ; chambres d’amis ; bibliothèque et ludothèque, salle de musique ; caves, cellier ; divers ateliers ; sans oublier l’espace permacole commun visant à atteindre une certaine autonomie alimentaire !

La commission aménagements est toujours en train d'étudier la répartition des espaces. Voici l'état des lieux actuel des appartements attribués et des possibilités dans les espaces restants :

{{< tableau_appartements >}}

De nombreuses options sont toujours à l'étude, certains espaces peuvent encore être regroupés, divisés ou finalement avoir une vocation collective. Pour vous aider à vous faire une idée, voici un schéma positionnant approximativement les zones les unes par rapport aux autres (le coin le plus proche étant plein sud) :

![](/images/dessin_plan_batiment.png)

Vous souhaitez nous rejoindre ? Patience ! L'effectif actuel du groupe ne nous permet pas encore d'envisager de nouveaux accueils.

Habitants et futurs habitants
=============================

Notre groupe a développé un [processus d'intégration](https://docs.google.com/document/d/1hfD1wQl6CxieZg4tdZrkPTDgDbcefhRTnYal_IYKsgM/) pour permettre à chaque (futur) membre d'entrer progressivement dans le projet avant un engagement définitif.

Actuellement, le groupe est composé de :

{{< load-photoswipe >}}

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/pictogrammes/picto-papillon.jpg" caption="11 foyers soit 18 papillons adultes et 12 jeunes papillons" >}}

{{< figure link="/images/pictogrammes/picto-chrysalide.jpg" caption="aucun foyer à ce jour" >}}

{{< figure link="/images/pictogrammes/picto-chenille.jpg" caption="2 foyers soit 4 chenilles adultes et 1 jeune chenille" >}}

{{< /gallery >}}

[Qui sommes-nous ?](/page/quisommesnous/)
