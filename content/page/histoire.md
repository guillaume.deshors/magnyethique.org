---
title: Histoire du château
comments: false
---


{{< load-photoswipe >}}
{{< gallery >}} 
{{< figure link="/images/uploads/1551878642157_IMG_7819.JPG" caption="Le château de Magny" >}} 
{{< /gallery >}}


{{< citation >}} 

Cette ancienne maison forte des XVe-XVIIe siècles est arrivée jusqu'à nos jours sans modification d'aucune sorte.
Le massif corps de bâtiment central couvert d'une haute toiture de tuiles plates est dominé par une tour carrée, élancée, qui lui est accolée. Sur ce même côté du bâtiment, l'arc imposant de la porte d'entrée principale est demeuré intact et semble défier le temps...

Jean de Nagu est seigneur de Magny en 1356. Alix de Nagu l'apporta en dot à Antoine des Serpents en 1502. En 1677, la propriété passe à la famille de La Rochefoucauld qui la vend, en 1726, à la famille de Vauban. Le 14 août 1795, Magny fut vendu comme Bien National. Il appartient aujourd'hui [date du livre à préciser] au Syndicat du personnel municipal de Lyon, qui l'a transformé en colonies de vacances.

{{< /citation >}}

Formidable de s'inscrire à la suite de tels noms et de dates aussi anciennes. Quand notre petite histoire rencontre la grande... 
Nous en savons un peu plus que ça sur l'histoire de notre beau bâtiment, et comptons vous le faire partager dès que possible ; par exemple nous avons trouvé sur internet de nombreuses cartes postales des années 1900. 

Toutefois nos données sont très parcellaires, et nos ressources assez pauvres, aussi bien en temps qu'en connaissances et compétences historiques. C'est pourquoi nous lançons un **appel à volontariat** : si un passionné d'histoire avait envie de faire des recherches sur l'histoire du château, ses habitants et ses environs, nous serions heureux de publier ici le fruit de son travail. 