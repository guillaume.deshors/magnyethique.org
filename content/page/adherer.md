---
title: Adhérer
---
MagnyEthique, c'est deux entités distinctes : une association loi 1901, et une SCI. La SCI possède le foncier, tandis que l'association gère la vie sur place, les chantiers participatifs, organise les événements culturels et régit le processus d'intégration dans l'habitat participatif. Vous trouverez des détails sur ce fonctionnement dans [nos ressources](../ressources) si vous voulez aller plus loin.

Il est nécessaire d'être adhérent à l'association pour participer à nos événements. Cela vous donne aussi le droit de participer à nos assemblées générales. Si vous souhaitez nous soutenir, vous avez la possibilité d'adhérer en ligne _via_ HelloAsso : {{< helloasso "Adhérer en ligne" "https://www.helloasso.com/associations/les-magnyethiques/adhesions/formulaire-d-adhesion-a-magnyethique-2" >}}

Merci !
