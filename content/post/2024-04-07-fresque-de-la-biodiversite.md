---
title: Fresque de la biodiversité
subtitle: Fresque de la biodiversité
date: '2024-04-07'
tags:
  - biodiversité
  - atelier
  - écologie
---
L'écolieu accueillera prochainement Michael Klöpper, animateur de multiples fresques (du climat, du numérique, de l'économie circulaire…). Il animera **le lundi 22 avril de 20 à 23h** la **[fresque de la biodiversité](https://www.fresquedelabiodiversite.org)**, permettant aux intéressé·e·s de découvrir ensemble les enjeux et pressions autour de la biodiversité se basant sur le rapport de l'IPBES au travers d'un atelier ludique, collaboratif, visuel et accessible à tous.

« _Le taux actuel d'extinction des espèces dans le monde est supérieur à la moyenne des 10 derniers millions d'années, et ce taux s'accélère._ » IPBES 2019

![](https://cloud.magnyethique.org/s/YX4tnjmPgaJeE5t/preview)  

**3 heures d'atelier**

• 20 minutes : comprendre les écosystèmes

• 1 heure : jeu de cartes

• 40 minutes : place à la créativité

• 1 heure : discussion autour de l'atelier

**5 modules**

• Définition de la biodiversité

• Les services écosystémiques rendus

• L'impact de l'Homme

• Les 5 grandes pressions

• Les conséquences

Atelier à **tarif libre**. Inscription [ici](https://framaforms.org/fresque-de-la-biodiversite-1712473757) (faites vite, les places sont limitées à 14 participant·e·s !)

![](https://cloud.magnyethique.org/s/gJMAt9La9JXHSJR/preview)
