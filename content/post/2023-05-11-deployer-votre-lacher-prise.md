---
title: Stage "déployez votre lâcher-prise"
subtitle: 4 et 5 juillet 2023
date: '2023-05-11'
tags:
  - formation
---

![](https://cloud.magnyethique.org/s/bPJXP8kqBiL5okf/preview)


Nous accueillons ce stage proposé par entre autre Lucie Puigblanque, que nous avons rencontrée en avril dernier puisqu'elle était venue lors de la résidence vocale "Mêlons les mots". Nul doute que ce stage sera un bon moment pour les participant.e.s !

Les inscriptions se font par téléphone au 06 73 14 86 88 ou par mail à [sophie.rimmele@lilo.org](mailto:sophie.rimmele@lilo.org)