---
title: Qui sommes-nous&nbsp;?
comments: false
---
{{< citation qui="Claire" annee="1964" imgdevant="/img/claire1.jpg" imgderriere="/img/claire2.jpg" couleur="slateblue" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}} J'arrive à un moment de ma vie où la maison en RA est devenue trop grande et éloignée de mon emploi actuel de médiatrice culturelle dans la SCOP Ardelaine avec une certaine polyvalence, deux grands enfants.

Vivre dans un lieu où s'entrecroisent des valeurs sur du partage avait déjà interpellé ma façon de m'organiser quand les enfants étaient plus jeunes. Alors pourquoi pas coconstruire aujourd'hui pour les jours et ans à venir, un lieu de vie « humanimé » avec des espaces partagés? « Promacteur »: « consommacteur » autrement. {{< /citation >}}

{{< citation qui="Katia et Pascal avec Leya, Elinor et Adriel" couleur="#FF0040" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}} Notre famille est composée de Katia, Pascal, et de nos trois enfants Leya (2011), Elinor (2013), Adriel (2015).

Nous nous sommes rencontrés dans le cadre d'une association franco-allemande encadrant des rencontres pour jeunes des deux nationalités. Nous y avons appris la gestion du conflit en travail d'équipe binational, et l'importance de la communication comme base du vivre ensemble. Ces principes régissent aujourd'hui encore notre vie de famille. Nous sommes sensibles à l'éducation positive, avons pratiqué avec nos trois enfants l'hygiène naturelle infantile, le portage en écharpe et tendons autant que possible depuis plusieurs années vers le zéro déchet. Cultiver la terre, cueillir le sauvage et transformer ou conserver s'inscrit dans cette dynamique. Lorsque nous voyageons, nous partons avec nos enfants en semi-itinérant ou en wwoofing afin d'être au cœur des cultures à découvrir. Pour nous, la vie est un enseignement et apprendre un moteur !

Depuis que nous avons rejoint le groupe, de nouvelles perspectives s'ouvrent à nous, en lien étroit avec nos convictions et idéaux. Nous sommes heureux pour nos enfants et nous-mêmes de bénéficier au quotidien des richesses humaines, naturelles, écologiques, créatives et en diverses compétences qu'offrent le projet, son environnement naturel et social, ses habitant·e·s et partenaires. Notre aventure la plus folle (si on exclut MagnyÉthique, bien sûr) a été d'avoir quitté l'Allemagne à vélo pour rejoindre l'écolieu au printemps 2019 : https://transitionride.home.blog {{< /citation >}}

{{< citation qui="Katia" annee="1981" imgdevant="/img/katia1.jpg" imgderriere="/img/katia2.jpg" couleur="#FF0040" >}}

Passionnée avant tout de nature, j'adore son observation qui éveille tous mes sens. J'ai grandi entourée d'animaux domestiques et sauvages sur lesquels j'approfondis sans cesse mes connaissances, me suis formée en 2020 à la botanique, suite logique de mon goût des « mauvaises herbes » et de mon habitude à confectionner mes propres tisanes et macérats contre les petits maux quotidiens ou juste pour le plaisir. J'ai débuté en octobre 2022 une formation en 3 ans d'herbaliste pour approfondir ce sujet. Je me passionne pour la permaculture agricole et humaine, pour la pédagogie. En tant que prof et maman, je me suis beaucoup intéressée et formée aux pédagogies alternatives. J'aime pratiquer quand j'en ai le temps la musique (quelques restes au piano, et plus récemment violoncelle et chant), les sports plutôt de plein-air (rando, escalade, parapente), les loisirs créatifs, la lecture, les voyages, l'interculturel, j'adore partager connaissances et passions avec d'autres et propose depuis 2024 diverses activités jusque-là souvent bénévoles dans un cadre professionnel : [https://lherbierdagape.fr](https://lherbierdagape.fr) {{< /citation >}}

{{< citation qui="Pascal" annee="1983" imgdevant="/img/pascal1.jpg" imgderriere="/img/pascal2.jpg" couleur="#FF0040" >}}

Je suis né et ai grandi en Allemagne avant de tomber amoureux de la France pendant mes études. J'adore pratiquer le vélo, mais aussi randonner, le rugby à 5, le trail et la course à pied dans notre bel environnement, lire, observer le ciel et les étoiles, jouer de la flûte à bec ou de la cornemuse de chambre. J'aime mon travail d'ingénieur mais souhaiterais lui donner plus de sens ! Les contacts humains me motivent énormément. En 2017, j'ai participé à une journée d'initiation à la fabrication du pain au levain en Chartreuse. Depuis, je me défends plutôt bien et ai appris à connaître et travailler les céréales de variétés anciennes, en m'inspirant de la slowfood. C'est ainsi que deux fois par semaine et pour des événements de l'écolieu, vous pouvez sentir la bonne odeur du pain cuit sur place, avant de le déguster. {{< /citation >}}

{{< citation qui="Fanny et Guillaume avec Alice, Léonard et Tom" couleur="yellowgreen" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}} Nous sommes un couple avec trois enfants : Alice (2013), Léonard (2015) et Tom (2024). Proche des idées de la décroissance, nous essayons de limiter notre impact environnemental.

Depuis 2012 nous avons participé à plusieurs projets pour créer ou rejoindre des habitats groupés urbains car nous étions intéressés par les aspects convivialité, ouverture sur l'extérieur et mutualisation de certains espaces. Vers la fin de la décennie notre projet a évolué et nous avons cherché un projet plus au vert avec suffisamment d'espace pour pouvoir gagner en autonomie et en consommation locale (atelier bois, permaculture…)

Depuis août 2019 nous habitons à l'écolieu et avons emménagé depuis fin 2022 dans notre appartement définitif dans le coin sud ! {{< /citation >}}

{{< citation qui="Guillaume" annee="1981" imgdevant="/img/guillaume1.jpg" imgderriere="/img/guillaume2.jpg" couleur="yellowgreen" >}}

Ingénieur en informatique, je me pose chaque jour plus de questions sur l'avenir et sur mon rôle dans la société. Je ressens le besoin de faire mieux cadrer mes activités professionnelles avec les valeurs que j'ai développées, notamment de sobriété et de recherche du bien commun.

J'aimerais participer à une société à plus petite échelle, fondée sur l'entraide et la bienveillance, et développer davantage d'autonomie alimentaire et énergétique en se basant autant que possible sur les principes du low-tech (si j'osais résumer : simple, réparable, sobre). Je souhaite partager ma vie entre bon temps, activités militantes, et travail rémunéré, en essayant de faire converger les trois autant que possible !

J'aime chanter, me déplacer à vélo, travailler le bois, réparer des objets, fabriquer des choses ou des bons plats, bien manger… et malgré tout l'informatique (à chacun ses contradictions). {{< /citation >}}

{{< citation qui="Fanny" annee="1985" imgdevant="/img/fanny1.jpg" imgderriere="/img/fanny2.jpg" couleur="yellowgreen" >}} Ingénieure en informatique depuis 15 ans, j'ai ressenti le besoin de vivre dans un environnement moins technologique, plus proche de la nature. J'avais envie d'apprendre à faire un maximum de choses par moi-même, en particulier en ce qui concernait l'alimentation. Pour l'instant, je monte en compétences sur les chantiers d'aménagements intérieurs.

Je cherchais un lieu intergénérationnel où mes enfants auraient des camarades de jeux et des adultes de tout âge qui leur apprendraient des valeurs plus humaines que celles véhiculées par la société de consommation actuelle.

Sinon en vrac : j'aime rencontrer des gens, faire des jeux de sociétés, cuisiner, fabriquer des choses et collectionner des boucles d'oreilles! {{< /citation >}}

{{< citation qui="Caroline et Bastien avec Louise et Arthur" couleur="#0080FF" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}} Nous sommes un couple avec 2 enfants Louise (2012) et Arthur (2014) qui viennent de Villeurbanne (69). Nous nous sommes rencontrés pendant nos études secondaires.

Nous partageons des valeurs autour de l'écologie, du lien social et faisons de notre mieux pour éduquer nos enfants avec une parentalité positive. Nous avons participé en famille à un jardin participatif pendant 2 ans ce qui a apporté à toute la famille une meilleure conscience des efforts à faire pour produire ce que nous mangeons.

Nous avons découvert le principe de l'habitat participatif en 2014 et depuis cela nous est apparu comme évident qu'il s'agirait de notre futur mode d'habitat. Nous avons participé à la bourse de l'habitat participatif au mois de décembre 2018 où nous avons rencontré des membres du groupe MagnyÉthique et nous y avons découvert le projet dans son ensemble. Nous avons ensuite rapidement intégré le groupe en janvier 2019 pour décider définitivement de venir habiter au château de Magny dès cet été.

Nous croyons vraiment que l'habitat participatif est une réponse à plusieurs problématiques actuelles : écologique, « un village pour élever des enfants », entraide, mutualisation des espaces, permaculture… {{< /citation >}}

{{< citation qui="Caroline" imgdevant="/img/caro1.jpg" imgderriere="/img/caro2.jpg" annee="1984" couleur="#0080FF" >}}

Initialement ingénieure, j'ai travaillé dans la maintenance ferroviaire, une
1ère reconvertition à la menuiserie depuis 2017, ensuite une formation de
coaching professionnel et un projet de boutique de créateurs. Je fais un peu
tous ces métiers en parallèle ou du moins j'utilise toutes les compétences.

En 2018, je disais que je n'étais pas prête à habiter à la campagne, en tout cas
pas à plus de 30 min de Lyon mais le projet de MagnyÉthique est passé par là et
je me suis vue prête et enthousiaste pour y déménager. J’ai vraiment besoin de
me sentir plus en lien avec la nature et avec les autres.

Le projet MagnyÉthique a été un véritable coup de cœur pour moi : les personnes
qui le composent, l’accueil que nous y avons reçu et également la gouvernance
qui y est vraiment très bien construite et en même temps évolutive en fonction
de ce qui se passe au sein du groupe et de son évolution.

J’aime participer à des projets avec des groupes (cela tombe bien ;-)),
apprendre de nouvelles choses, travailler le bois, animer des ateliers de
découverte pour tous les âges.

Après plusieurs années dans le projet et à découvrir notre écosystème, j'ai
comme projet de racheter une boutique de créateurs à Amplepuis.

Activités de [coaching](https://www.linkedin.com/in/caroline-dufau-seguin) et [menuiserie](https://www.cleabois.fr) ([Facebook](https://www.facebook.com/caroline.dufauseguin)) {{< /citation >}}

{{< citation qui="Bastien" annee="1984" imgdevant="/img/bastien1.jpg" imgderriere="/img/bastien2.jpg" couleur="#0080FF" >}}

J'ai toujours eu une forte sensibilité sur les questions d'écologie qui s'est affinée avec beaucoup de lecture autour de l'effondrement, du low-tech, de la décroissance. La rencontre avec MagnyÉthique s'est inscrite dans cette dynamique. Ce projet représente pour moi une mise en pratique ambitieuse de ces principes, rendue possible (même si ça reste un challenge) par la force du collectif.

J'ai une formation d'ingénieur informatique et je suis actuellement directeur informatique en temps partagé dans des associations du médico-social. Je pense que l'informatique doit être au service de l'homme (et non l'inverse…) et apporter du positif, faciliter les usages, tout en ayant une approche « low-tech » du juste usage. Grâce à ce métier, je contribue à ma manière via les associations que j'accompagne aux actions sociales.

J'aime : m'amuser et jouer à tous types de jeux de cartes et de société, faire des LEGOs avec mes enfants, chercher des solutions à des problèmes complexes, déguster des bonnes bières avec des pizzas, faire du sport (du basket au golf en passant par le badminton)… {{< /citation >}}

{{< citation qui="Bruno" imgdevant="/img/bruno1.jpg" imgderriere="/img/bruno2.jpg" annee="1973" couleur="#FF00BF" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}}

Mon parcours professionnel d'artiste (jongleur, comédien, musicien, et autres !) m'a sensibilisé aux joies et défis du « faire ensemble » : création de spectacles, collectif autogéré, vie en compagnie, organisation de festivals… Petit à petit je me suis interessé à différents outils d'intelligence collective, de gouvernance partagée, de prise de conscience et de déconditionnement. Je me suis notamment formé en Communication NonViolente, à la facilitation des Forums de Zegg, et à la danse-contact-improvisation que je transmets sur Roanne.

Aujourd'hui je suis en reconversion professionnelle et je me forme au métier de musicien intervenant en milieu scolaire, pour exercer une activité plus ancrée sur le territoire en restant dans la transmission et le domaine artistique.

Je vois dans MagnyÉthique un magnifique espace d’expérimentation et de co-apprentissage, qui rejoint mon rêve d’un lieu ouvert et inspirant, lieu d’accueil, de formation et de croisements, de culture dans tous les sens du terme.

www.blabouret.eu (site artistique, pas vraiment à jour)

{{< /citation >}}

{{< citation qui="Agnès" annee="1963" couleur="coral" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}} Une enfance à la ferme, la sobriété de fait et la convivialité au quotidien. L’échange de services, de compétences, la solidarité et la différence accueillie en richesse comme valeurs reçues, vécues.

Une fille de 31 ans et un fils de 33 ans profs de chant et de guitare et artistes.

Pas de diplômes, des emplois dans l’éducation populaire, animation de rue, quelques détours avant un job de bibliothécaire où tout était à construire pour un public pluriculturel.

De voyages (échanges de compétences, vie chez l’habitant), en petits boulots et wwoofing agricoles, forums d’habitats participatifs… en quête de vivre autrement, avec d’Autres. Reprise en mars 2019 d’un emploi de fonctionnaire en médiathèque, comme dans un bocal, cernée de WIFI, de hiérarchie de déshumanisation et consumérisme programmés. Rencontre des MagnyÉthiques la deuxième quinzaine de juillet 2019 et l’envie de rejoindre cette aventure humaine et éthique. {{< /citation >}}

{{< citation qui="Manon et Mathieu avec Ava" couleur="slateblue" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}} Nous étions Lyonnais pendant plusieurs années, dans le joli quartier de la Croix Rousse. Aujourd'hui, nous avons pris notre envol vers la campagne Beaujolaise…

Dès le début de notre rencontre, l'habitat partagé s'est révélé être une envie commune. Après avoir visité quelques lieux inspirants, nous avons découvert avec enthousiasme, fin 2018, le projet MagnyÉthique. Ce projet réunit à la fois nos besoins d'investir un lieu de vie proche de la nature et de rejoindre une dynamique de partage, d'intelligence collective, de créativité, au service du vivant.

En 2023, notre fille Ava a été la première enfant née au sein du projet.

{{< /citation >}}

{{< citation qui="Manon" imgdevant="/img/manon1.jpg" imgderriere="/img/manon2.jpg" couleur="slateblue" annee="1987" >}} J'ai toujours été passionnée par l'humain. J'ai étudié l'Anthropologie et la Sociologie, m'intéressant en particulier à la question des conditions de vie de populations nomades dans différents lieux en France : manouches, tsiganes, gitans, roms…

Puis je suis devenue assistante sociale pendant une dizaine d'années : plus que la recherche universitaire, l'envie de soutenir concrétement les personnes à accéder à leurs droits était mon moteur. Ayant besoin d'ouvrir mes horizons et de sortir des « sentiers battus » je suis devenue facilitatrice dans une école démocratique. Cette expérience m'a permis de remettre en question mes croyances, en particulier concernant l'éducation et de vivre pour la première fois au quotidien la gouvernance partagée avec des jeunes de tous les âges.

Enrichie par ces différentes expériences, je suis en pleine construction d'un nouveau projet professionnel à l'heure actuelle qui me permette d'être créative, soutenante et authentique dans ma relation au vivant. Le souhait de rejoindre le groupe MagnyEthique a beaucoup de sens pour moi, en particulier dans cet objectif de prendre soin des relations et de soi. J'ai très vite senti pouvoir m'épanouir dans ce projet où une large place est faite au développement de la qualité d'écoute, la confiance et tolérance, envers nous-même et les autres.

{{< /citation >}}

{{< citation qui="Mathieu" imgdevant="/img/mathieu1.jpg" imgderriere="/img/mathieu2.jpg" couleur="slateblue" annee="1987" >}}

« Croire en ses rêves » a toujours été un leitmotiv pour moi : « Si les autres y arrivent pourquoi pas moi ? ». Voilà ce que je me disais à l'âge de 10 ans après avoir redoublé mon année de CE1. C'est dans ces années là que l'on commence déjà à vouloir mettre les enfants dans des cases, ou du moins mettre une certaine pression avec cette fameuse question : « Qu'est-ce que tu veux faire comme métier plus tard ? ». Question à laquelle je répondais : « Je veux faire de la Bande Dessinée ! » Je vois encore d'ici les sourires un peu compatissants de mes professeurs. Qu'à cela ne tienne, l'intention était posée.

Une bonne trentaine d'années plus tard on peut dire que mon objectif a été tenu avec une dizaine de participation sur différents ouvrages aussi bien en illustration qu'en BD. J'ai pu également m'épanouir dans l'enseignement du dessin et de l'art séquentiel aussi bien en école d'art que dans les MJC ou encore lors de multiples interventions scolaires. Sur mon chemin professionnel, j'ai également rencontré le spectacle vivant, avec du dessin en direct sur scène, avec « l'Ardoise Magique© » ou encore La « Battle BD© ». La vie nous réserve de belles surprises lorsqu'on se laisse guider par son intuition.

C'est par ailleurs la bande dessiné qui m'a guidée sur les sentiers de l'habitat partagé en 2013 avec l'album « Joanne Lebster – Le début d'un nouveau monde » (éds RJTP). Celui-ci m'a laissé entrevoir un monde avec plus de partages, où la coopération remplace la compétition, un monde où ralentir ne rime pas avec régression, mais plutôt avec bien-être et équilibre. Une nouvelle intention était posée « vivre autrement ». Aujourd'hui un nouveau chapitre s'écrit sur l'écolieu MagnyÉthique. {{< /citation >}}

{{< citation qui="Corinne" imgdevant="/img/corinne2.jpg" imgderriere="/img/corinne1.jpg" couleur="#FF0040" badge="/images/pictogrammes/picto-papillon_mini.jpg" annee="1967" >}}

Fille d’agriculteur, ayant passé mon enfance et adolescence entre la ferme et le pensionnat, j’ai souvent imaginé, « quand je serai grande », vivre dans une grande maison avec un grand jardin et beaucoup de monde, famille, ami.e.s, gens de passage… Un lieu plein de vie et d’effervescence ! C’est à un peu plus de 50 ans que, sur mon parcours de vie j’ai trouvé ce lieu : un cours de Qi Gong, une retraite organisée par la professeure de Qi, une rencontre avec une habitante de Magny et j’ai mis un, puis deux pieds, puis toute ma personne dans le projet MagnyÉthique. Deux ans plus tard, j’emménageais.

Depuis quelques temps, je sentais que le « comment j’allais vivre le reste de ma vie » deviendrait essentiel pour mon épanouissement personnel et pour être en harmonie avec mes valeurs. Maman de deux grands enfants - Julie, 25 ans à l’époque, en fin d’étude, jeune globe trotter en projet de voyage de l’autre côté de l’Atlantique, Charlie, 17 ans, encore au lycée mais proche de quitter le foyer - sensible aux questions de l’écologie et soucieuse d’adopter un mode de vie plus solidaire et respectueux de l’environnement, il était incohérent pour moi de continuer à vivre seule dans un appartement de 100 m2 et de surcroit, sans espace extérieur pour planter quelques légumes.

J’aime à penser que nos pensées, nos intentions, lorsque le moment est venu, lorsqu’elles arrivent à maturité, nous mènent vers les « bonnes » rencontres, dans le sens de « justes rencontres ». A nous de saisir les opportunités et d’y mettre l’énergie nécessaire pour réaliser nos rêves : la grande maison avec un jardin s’est transformée en un grand château avec 8 hectares de terrain et pré, et beaucoup de monde : des familles avec et sans enfants, des volontaires, jeunes et moins jeunes, des gens de passage en quête de mieux être et de vivre autrement, des artistes … une belle faune humaine qui s’enrichit les uns les autres. La permaculture en pratique !

Profession : animatrice socioculturelle, ludothécaire, j’ai travaillé longtemps dans des structures d’accueil et de loisirs pour les enfants, puis à mon compte et en itinérance, comme ludothécaire, proposant des temps de jeu à un public âgé de quelques mois à 99 ans, avec comme principale motivation de favoriser les rencontres, les échanges et la convivialité entre joueurs et non joueurs, entre petits et grands, en famille ou avec des inconnu.e.s.

Mon dernier emploi dans un foyer d’hébergement d’urgence pour mères isolées avec jeunes enfants, en plus de la richesse de l’expérience et des rencontres avec ces mamans animées d’une force de vie (et de survie), me conforte encore davantage dans la nécessité et l’urgence de sortir d’un mode de vie ultra-consommateur-individualiste et de développer à notre échelle et dans le monde des éco- lieux, des lieux de vie où l’humain et son environnement animal, végétal soient au cœur des préoccupations. {{< /citation >}}

{{< citation qui="Anne et Cédric" couleur="yellowgreen" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}}
Nous nous sommes rencontrés à l'écolieu alors que nous cherchions à rejoindre un projet collectif. Une rencontre pleine de profondeur et de sens.
{{< /citation >}}

{{< citation qui="Anne" imgdevant="/img/anne1.jpg" annee="1976" imgderriere="/img/anne2.jpg" couleur="yellowgreen" >}}

Sensibilisée ces dernières années à ma place dans ce monde, tant par les petits gestes du quotidien, que par ce que j’ai à offrir, je suis heureuse de tout simplement participer à la Vie. Attentive aux justes relations, à la compréhension du vivant, à l’inclusivité des différences permettant à chacun de nourrir l’espace d’ensemble. Je suis à l'écoute des énergies qui influencent nos attitudes et cherche à améliorer ce qui est à ma portée, en nourrissant l'Essentiel et mettre en partage ce qui m’habite comme : les pratiques du Tai chi, du Qi Gong, de la méditation.

Le métier que je pratique aujourd'hui m’amène à me questionner sur les éducateurs que nous sommes chacun·e·s les un·e·s avec les autres. Maman de 2 grands enfants (maintenant jeunes adultes) je me laisse inspirer, surprendre par leur façon d’être au monde.

Et en questions ouvertes, que j'essaie de me poser chaque matin :)… Si nous étions une source d’inspiration pour l’autre, quelle version de nous-même serions-nous prêts à Être (ou ferions-nous l’effort d'Être) ? Quelles qualités observons-nous chez l’autre qui viennent éveiller un élan, une ressource ? {{< /citation >}}

{{< citation qui="Cédric" imgdevant="/img/cédric1.jpg" imgderriere="/img/cédric2.jpg" couleur="yellowgreen" annee="1975" >}}

Je suis arrivé à MagnyÉthique par hasard et j'y suis resté par choix.  Ceci dit,
le hasard je l'ai certainement provoqué quelque part.  Cela faisait plusieurs
années que je me renseignais sur les modes d'habitats alternatifs.  J'avais
suivi le MOOC « comment créer une oasis » et rapidement compris que je n'avais
pas une personnalité initiatrice de ce type de projet.  Au mois d'août 2020, une
amie me parle du château de Magny et du collectif qui s'y crée. Après une porte
ouverte et un chantier participatif, je m'engage début 2021 dans le processus
d'intégration.  Quatre ans plus tard, (oui j'ai mis un certain temps à pondre ma
biographie) je suis heureux de faire partie de ce beau projet, beaucoup plus
ambitieux que ce que j'avais imaginé il y a quelques années. Je rêvais de petits
habitats légers et modulables, j'ai embarqué sur un paquebot au final.

Ces vingt dernières années, mon parcours personnel et professionnel s'est
déroulé sous le signe de la quête de sens. Mon arrivée à l'écolieu coïncide avec
une sorte de tournant et je place les années à venir sous le signe de la
co-création et de la quête d'authenticité.  Cela mériterait d'être développé,
mais disons qu'il s'agit pour moi d'explorer ce qu'implique la notion de
responsabilité individuelle, ingrédient de base pour vivre de plus justes
relations.

J'ai un tempérament plutôt solitaire, rêveur et casanier. Le fait de m'inscrire
dans un projet collectif m'amène à progressivement sortir de mes petites
habitudes, même si je replonge volontiers dans ma grotte quand il le faut.

### J'aime/j'aime pas

- J'aime l'absurde, les jeux de mots insupportables, le tiramisu et Pink
Floyd (entre autres).
- Récemment j'ai beaucoup aimé la série Arcane.
- J'aime contempler les paradoxes de notre monde.
- Je n'aime pas avoir les pieds mouillés dans des chaussures.
- J'aime les dîners entre amis, les œufs au plat le matin, les capybaras et
la philosophie taoïste.
- Je ne suis ni optimiste ni pessimiste pour le futur de l'humanité.
- J'aime la beauté des petites choses du quotidien.
- J'espère passer de longues années dans ce bel endroit avec ces gens bons.


{{< citation qui="François" avec="Isao" couleur="#0080FF" annee="1979" badge="/images/pictogrammes/picto-chenille_mini.jpg" >}}

Le collectif est pour moi le mode de vie adapté à notre planète et notre avenir. Il permet de s'adapter aux changements environnementaux et sociaux, permettant plus de résilience. C'est aussi le meilleur environnement pour élever un enfant.

Ancien Parisien, j'ai doucement effectué ma migration vers la campagne pour retrouver un peu de calme et de sérénité. Quand j'ai découvert MagnyÉthique cela a été comme une évidence que ce lieu et ce groupe représentait ce que je cherchais depuis longtemps.

Ancien chef de projet informatique, à la suite d'un bore out, je me suis formé à la permaculture. Au cours de cette formation, j'ai décidé de donner plus de sens à mon activité professionnelle en devenant homme à tout faire dans la région.

[www.carasupp.fr](https://carasupp.fr)
 {{< /citation >}}

{{< citation qui="Sarah et Stéphane avec Thémis, Taël et Ayuko" couleur="#FF00BF" badge="/images/pictogrammes/picto-papillon_mini.jpg" >}}

Nous sommes une famille de 5 personnes avec Thémis (2014), Taël (2017) et Ayuko (2020).

Notre rencontre démarre au lycée. Notre amitié se poursuit dans les études dans la même ville et la relation amoureuse vient avec la colocation.

Les voyages, surtout une année en Australie, nous sensibilisent davantage à l’écologie. La naissance des enfants nous ouvre la porte de la “parentalité associative”, cette manière de vivre avec d’autres familles nos valeurs éducatives à travers la crèche, l’école ou le scoutisme laïque ou les oppositions locales…

L’habitat partagé a été un projet qui s’est construit petit à petit dans notre esprit avant de vraiment devenir un objectif vers 2019. S’en est suivi quelques visites de lieux sans trouver celui qui nous correspondait complètement.

Nous connaissions quelques habitants du château et avons ensuite entamé en 2021 notre processus de transformation, depuis l'œuf, la chenille, la chrysalide pour finir par intégrer le château en tant que papillon à l’été 2022 et cela à travers des fêtes, des réunions, des chantiers… des partages de moments de vie en tout genre.

{{< /citation >}}

{{< citation qui="Sarah" couleur="#FF00BF" annee="1984" >}}

Je me définis comme une amoureuse des lieux sauvages, en particulier de la mer, qui aime bien apprendre et enseigner.

Les lectures, les discussions et les changements naturels que l’on perçoit me montrent la nécessité d’aller vers une transition écologique, une expression qui reflète de multiples définitions mais du coup qui peut être vécue heureusement de bien des manières différentes. Et c’est la manière de MagnyÉthique qui résonne en moi et en notre couple. Pour moi, c’est un retour dans une campagne riche de culture et de liens sociaux, de possibilités d’observer et de vivre avec des espèces variées, de retrouver la joie du voyage et de l’exotisme au sein même de mon lieu de vie, d'expérimenter joyeusement des idées pour vivre différemment.

{{< /citation >}}

{{< citation qui="Stéphane" couleur="#FF00BF" annee="1985" >}}

Devenu écolo par alliance, j’ai complètement adhéré à tous ces principes depuis ma rencontre avec Sarah. Le fait d’avoir des enfants n’a fait que renforcer pour moi un besoin de nature et de sérénité.

Ingénieur en informatique de formation, je souhaite réduire à terme mon engagement professionnel pour me consacrer à des activités locales (agriculture, associatif…), et pourquoi pas envisager une reconversion (on verra dans quoi).

J’aime faire du sport (volley, badminton, course à pied, etc.), lire des mangas et jouer à des jeux de société.

{{< /citation >}}

{{< citation qui="Laurianne et Romain" couleur="slateblue" badge="/images/pictogrammes/picto-chenille_mini.jpg" >}}

Bio à venir : deux jeunes chenilles prennent des forces avant de peut-être fabriquer de solides cocons ! ;-) {{< /citation >}}
