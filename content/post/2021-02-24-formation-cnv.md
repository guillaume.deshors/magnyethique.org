---
title: Formation CNV
subtitle: Samedi 17 et dimanche 18 avril 2021
date: '2021-02-24'
tags:
  - formation
  - CNV
---
**Formation à la Communication Non Violente selon le processus de Marshall B. Rosenberg**

**Un art de vivre avec soi-même et les autres**

  

===

La Communication Non Violente peut vous aider dans tous les domaines de votre vie. Que ce soit en couple, en famille, dans le milieu professionnel ou associatif, cette méthode élaborée par Marchall Rosenberg permet de prendre soin des liens qui nous relient tout en réussissant à exprimer nos émotions pour qu'elles soient entendues.

Edith Tavernier interviendra une nouvelle fois à l'Écolieu pour former quelques membres du groupe et d'autres personnes intéressées à la pratique de la CNV. La pédagogie est basée sur une alternance entre des apports théoriques et des temps de mise en pratique en petits groupes. Elle s’appuie sur l’expérimentation et l’exploration de situations relationnelles apportées par les participants.

À l'issue de la formation, vous recevrez un certificat afin de pouvoir la faire valoir.

Pour vous inscrire, envoyez un mail à 202104_formationcnv@magnyethique.org

![](/images/uploads/1614159285371_formation-CNV-Flyer.jpg)
