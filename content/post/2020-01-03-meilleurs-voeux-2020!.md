---
title: Meilleurs voeux 2020!
date: '2020-01-03'
tags:
  - avancement
---
Après une riche et belle année 2019, nous sommes passés à 2020, qui se veut tout aussi prometteuse ! La nuit et le jour de Noël nous ont apporté deux petits poussins de soie, le réveillon du jour de l'an chaleur et amitié.

Nous vous souhaitons à toutes et tous nos meilleurs voeux de bonheurs, petits et grands, simples et profonds. Que 2020 vous apporte l'amour, la joie, l'aventure et la sérénité comme vous le souhaitez !

Au plaisir de vous retrouver à l'écolieu.

![](/images/uploads/1578061384337_carte-de-voeux-2020-Bass-def.jpg)