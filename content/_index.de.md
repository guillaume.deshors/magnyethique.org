---
title: MagnyÉthique
---
{{< chateau >}}

Das Projekt MagnyÉthique, Ökoort im Schloss von Magny (Cublize, Frankreich), existiert seit Juni 2019 und 
ist ein globales Projekt, dessen Grundlagen auf den Prinzipien 
sowohl der landwirtschaftlichen als auch auf der sozialen 
Permakultur beruhen.

Die Gründer des Projekts und seine Teilnehmer wollen vor Ort und um den Ort die Vielfalt, die Kooperation und den Zusammenhalt der Systeme
in vier komplementäre Richtungen fördern.

*   {{< liencouleur "#4b892f" "page/habitatparticipatif" >}}Cohousing{{< /liencouleur >}}
*   {{< liencouleur "#743068" "page/espace_pro" >}}Coworking{{< /liencouleur >}}
*   {{< liencouleur "#34618e" "page/accueildupublic" >}}Empfang von Gruppen{{< /liencouleur >}}
*   {{< liencouleur "#8c6334" "page/agroecologie" >}}Handwerk und Landwirtschaft{{< /liencouleur >}}


Dafür wird eine ökologische, soziale, kulturelle, ökonomische und solidarische Dynamik hergestellt, die sowohl lokal, als auch auf regionaler, nationaler und warum nicht internationaler Ebene als Vorbild dienen kann.

Unser Name, “MagnyÉthique” fungiert als Wortspiel: Er hört sich genauso an wie "magnétique" = "magnetisch" und besteht aus zwei Komponenten: 

* “Magny” steht für den Namen des Ortes unser Projektes, 
* “éthique” (= ethisch) beschreibt die Überlegung über Verhaltensweisen, die einer menschlichen Verbesserung der Welt zugrunde stehen. Sie strebt nach einem gesellschaftlichen Ideal durch die Verwirklichung engagierter Aktionen. 

Dazu hoffen wir wie ein Magnet engagierte und neugierige Menschen anzuziehen, damit sie für kurze oder längere Zeit zu uns kommen!
