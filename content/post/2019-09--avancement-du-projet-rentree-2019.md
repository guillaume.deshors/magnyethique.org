---
title: Avancement du projet - Rentrée 2019
date: '2019-09-16'
tags:
  - avancement
  - portes ouvertes
  - rencontre
  - échanges
---
L'été touche à sa fin, les travaux continuent par toutes petites touches, les familles prennent leurs marques dans les nouveaux milieux scolaire et d'activités locaux et la vie s'écoule paisiblement malgré tout à l'écolieu.

En interne au groupe, les fondements d'un **système restauratif MagnyÉthique** sont validés, ainsi que le lancement des études et accompagnements au design du **projet agricole**. Le **pacte d'associés** a été signé fin août, les projets d'**activités** sur place s'esquissent doucement. Les représentations **théâtrales** de fin août ont connu un véritable succès et appellent à une deuxième édition! La toute première **[formation](/post/2019-6-formation-permaculture-a-magny/)**, en permaculture, approche à grands pas et nous avons hâte d'y être!

La fin d'été nous a offert un magnifique dimanche ce 15 septembre en compagnie des nombreux visiteurs aux **portes ouvertes**. Le repas partagé était copieux, chaleureux et plein de belles rencontres: merci à tous les présents qui y ont contribué.

Ceux qui n'ont finalement pas pu venir sont cordialement invités à venir nous retrouver samedi sur le stand que nous tiendrons au [Petit Bal des Possibles](/post/2019-07-petit-bal-des-possibles/) ou aux prochaines portes ouvertes le [26 octobre](/post/2019-07-journee-portes-ouvertes/) !

Et pour finir les news, nous lançons très prochainement notre **première campagne de financement participatif** sur la plateforme Bluebees. Notre projet s'intitule "_**Saurez-vous résister aux champs MagnyÉthique ?**_" et servira à lancer le projet agricole. Suivez donc nos prochaines nouvelles et merci d'avance pour votre soutien, qu'il soit financier ou simplement en partageant nos publications au plus grand nombre ! Il paraît qu'une bonne campagne est souvent une campagne qui démarre vite et bien. Nous sommes dans les sarting blocks !

À très vite, donc !

Les MagnyÉth'
