---
title: Un Rubik's Cube dans les Urnes ou comment résoudre le casse-tête démocratique
subtitle: Samedi 3 juin à 20h - Conférence gesticulée de Jérôme Noir
date: '2023-05-11'
tags:
  - spectacle
  - culture
  - société
---
![](https://cloud.magnyethique.org/s/P7rs7mPZXGXadM2/preview)

Conférence gesticulée sur la démocratie
=======================================

samedi 3 juin à 20h, sous chapiteau
-----------------------------------

«&nbsp;On n’est pas nés sous la même étoile&nbsp;» nous chantaient IAM en 1997.

Constat implacable : notre système politique et de production engendre et entretient des inégalités de toutes sortes. Or, on nous apprend depuis l’enfance que nous sommes en démocratie. Mais si, rappelez vous les élections de délégués de classe et les cours d’éducation civique… Dès lors, la logique voudrait que ça soit le peuple qui ait décidé démocratiquement de ce système inégalitaire. Vraiment ?

Mais dans le fond, savons-nous réellement ce qu’est la démocratie ? Comment la pratiquons nous ? En votant ? En manifestant ? En nous engageant dans des assos ? Des syndicats ? Suffit-il d’élire nos chef·fe·s pour pouvoir se dire en démocratie ? Comment décider collectivement à 10, 1000, 100 000, des millions?

C’est à travers ses multiples engagements et expériences que Jérome se rend compte que c’est un sacré casse-tête. C’est sur un fond sonore teinté de Hip Hop des années 90 qu’il se politise et se lance alors en quête de résoudre cette énigme : qu’est ce que la démocratie et comment la pratiquer de manière radicale et entière? Est-ce que la démocratie doit se limiter aux institutions politique ou peut-elle se décliner au travail ou tout autre espace qui implique un collectif?

A force de pratiques et de réflexions, il en a même fait son activité principale : animer la démocratie partout où c’est possible. Car oui la démocratie, ça ne se décrète pas, ça se pense et ça s’anime. Jérome viens ici partager son témoignage, ainsi que les outils et les méthodes qu’il a découvert en s’attelant à résoudre le rubik’s cube démocratique.

Infos pratiques :
-----------------

*   Durée 1h30 environ
*   Participation au chapeau
*   Le chapiteau n'a pas de gradin, amenez votre chaise ou votre coussin pour plus de confort !
