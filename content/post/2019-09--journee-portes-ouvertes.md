---
title: Visite et rencontre
subtitle: "Attention changement de date : samedi 30 novembre 2019"
date: '2019-09-27'
tags:
  - portes-ouvertes
  - rencontre
  - promotion
bigimg:
  - desc: null
    src: /images/uploads/1549622709185_IMG-20181230-WA0047.jpg
---
Nous aurons le plaisir de vous accueillir le **samedi 30 novembre 2019** (et non plus le 1er décembre comme annoncé initialement, désolés !) à 14h30 pour une une rencontre conviviale.

Au programme:

14h30 : présentation du projet

15h15 : visite guidée des lieux

16h00 : goûter partagé

Intéressé.es par l’habitat participatif en général, l’écolieu MagnyÉthique en particulier ou simples curieu.ses, vous êtes les bienvenu.es ! Merci de nous contacter via l’onglet “Contactez-nous!” pour vous inscrire afin que nous puissions nous organiser au mieux !
