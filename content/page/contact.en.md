---
title: Contact
---

Want regular news? You may subscribe to our newsletter [here](https://my.sendinblue.com/users/subscribe/js_id/2w4x5/id/1)!
(The newsletter is in French language; use your skills ;-) or the 
translator of your choice.)

- - -

Questions, requests? Contact us directly on our [contact email address](mailto:contact@magnyethique.org) or fill in the form below. We'll answer as best we can as soon as possible.

{{< contact >}}
