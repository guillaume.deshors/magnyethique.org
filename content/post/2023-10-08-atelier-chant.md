---
title:  Atelier chant avec Benjamin Campagna
subtitle: mardi 7 novembre à 20h
date: '2023-10-08'
tags:
  - atelier
  - culture
---
Mardi 7 novembre à 20h, Benjamin Campagna vient proposer un atelier intitulé "libérer la voix". Chanteur et pédagogue expérimenté, il vient en voisin puisqu'il habite Meaux-la-Montagne !
![](https://cloud.magnyethique.org/s/qfamPWAzjEoqPw4/preview)  




