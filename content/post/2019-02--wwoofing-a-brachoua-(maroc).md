---
title: Wwoofing à Brachoua (Maroc)
date: '2017-07-02'
tags:
  - permaculture
  - wwoofing
bigimg:
  - desc: Brachoua
    src: /images/uploads/1549556665511_IMGP1881.JPG
---
Katia, Pascal et leurs enfants sont partis passer deux semaines au Maroc, à Brachoua.

  

Ce village au milieu du désert des contreforts de l'Atlas est devenu une référence au Maroc car il a su amorcer un changement irrémédiable vers le mieux pour sa population en créant une coopérative permacole qui forme et accompagne les habitants à la création de jardins de subsistance, voire plus. En parallèle, un écovillage accueille des écotouristes, de Rabat à 50 km ou d'ailleurs (Europe, États-Unis). Quant aux femmes, elles ne sont pas en reste puisqu'à Brachoua est née la toute première coopérative gérée entièrement par des femmes et qui permet à 12 villageoises de gagner leur vie mieux que la plupart en étant en plus leurs propres patrones! Un bel exemple d'écolieu mélant emploi, échange, solidarité, accueil, protection de l'environnement et avancée sociale!

  

Pour en savoir plus, regardez cette belle vidéo :

{{% youtube "ZjQZgKz1QYI" %}}  

La petite famille a vécu chez Larbi (créateur de l'écolieu), Bouchra (présidente de la coopérative de femmes) et leurs trois enfants. Pascal a participé à la confection et livraison des paniers bio, Katia a pu aider les femmes à la fabrication de diverses semoules et les enfants ont beaucoup joué autour du grand arbre de l'écovillage et appris quelques bribes de marocain!
