---
title: Tatou juste
subtitle: Samedi 24 et dimanche 25 novembre 2018
tags:
  - échanges
  - écologie
  - société
date: '2018-11-24'
bigimg: []
---
![](/images/uploads/image18.jpg)

Certains se sont rendus ce week-end à la 13ème édition du salon [Tatou Juste](http://www.tatoujuste.org "http://www.tatoujuste.org") de St Etienne pour prendre des contacts, piocher des idées et se laisser inspirer.

D'intéressantes conférences cette année encore: permaculture, Framasoft (que nous utilisons déjà en partie - on ne peut que vous encourager à faire de même!)…
