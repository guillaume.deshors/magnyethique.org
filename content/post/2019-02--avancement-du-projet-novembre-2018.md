---
title: Avancement du projet - novembre 2018
date: '2018-11-17'
tags:
  - avancement
---
Les réunions sont toujours aussi nombreuses, tant en plénières qu'en commissions puisqu'avec l'intégration en cours de deux nouveaux foyers, nous sommes suffisamment nombreux pour qu'elles puissent reprendre de l'activité. Plus d'efficacité et ça chauffe les méninges!

Au programme:

*   Nous avons cette semaine validé notre nouveau nom de projet. Nous sommes désormais "MagnyÉthique"! Magny fait bien sûr référence au lieu sur lequel nous nous installerons, le mot éthique dans cette définition trouvée sur internet nous correspond tout à fait:

"_L'éthique peut également être définie comme une réflexion sur les comportements à adopter pour rendre le monde humainement habitable. En cela, l'éthique est une recherche d'idéal de société et de conduite de l'existence_" ([Toupie.org](http://www.toupie.org/Dictionnaire/Ethique.htm)).  

Sans oublier le fait que nous espérons pouvoir attirer "tel un aimant" de nombreuses personnes, curieuses ou engagées à nous rejoindre ponctuellement ou pour plus longtemps!

*   **Tous**: Négociations des paragraphes du compromis de vente qui doit être signé à la fin du mois, réflexion au montage de certains dossiers de subvention
    
*   **Commission statuts et finances**: définir nos futurs statuts en tenant compte de toutes les nécessités (pour les ménages, conditions d'obtention de subventions et de prêts…), les conditions financières pour que le projet soit viable tout en laissant si possible la porte ouverte à l'individualisation des apports
    
*   **Commission aménagements**: diagnostiquer les nécessités de rénovation du bâtiment, chercher les solutions les plus écologiques et économiques, contacter les artisans + travailler sur la répartition future des espaces entre privé, privé-commun, professionnel et public
    
*   **Commission gouvernance**: préparer la session de 5 jours de fin d'année, dont notre réveillon commun et mettant un soin particulier à la cohésion de groupe
    
*   **Commission communication**: préparer les deux rencontres publiques de décembre, travailler à l'élaboration du futur site du projet "MagnyÉthique" (celui-ci sera donc prochainement abandonné), répondre aux multiples prises de contact suite à la publication de notre projet dans la lettre des Colibris du 10 novembre dernier.
