---
title: Chantier participatif de plantation du verger conservatoire
subtitle: Le samedi 25 et le dimanche 26 novembre 2023
date: '2023-11-05'
tags:
  - permaculture
  - biodiversité
  - écologie
  - wwoofing
  - chantier participatif
---
Voilà déjà plusieurs années à présent que l'idée du projet de **verger conservatoire** a germé dans la planification du design en permaculture ! Et depuis plus d'un an, nous avons travaillé à préparer le terrain, grâce à l'aide précieuse de nos [wwoofeureuses](https://wwoof.fr/fr/host/6494) successif·ves.

**Et voici l'étape la plus marquante qui approche à grands pas !**

![](https://cloud.magnyethique.org/s/HzzdAzdiXk924Wx/preview)

**Le week-end du 25 et 26 novembre 2023**, nous planterons enfin plus de 600 des végétaux les plus grands prévus pour le futur verger. Il s'agira principalement de variétés anciennes et/ou locales que nous nous sommes procurées chez nos partenaires locaux ou régionaux. Et pour cela, nous allons avoir besoin d'aide !

Chaque matin, l'équipe de MagnyÉths accueillera les volontaires à partir de 09h00 pour le café et thé avant de prendre brouettes et outils à 09h30 pour descendre sur le terrain. Les repas sont évidemment offerts par l'écolieu et s'organiseront sur place. Nous terminerons la journée avec un goûter à 16h30. Pour les personnes souhaitant dormir sur place, il suffit de nous le préciser lors de l'inscription.

Pour toute **information complémentaire**, écrivez-nous à plantation.verger@magnyethique.org

Pour **vous inscrire**, merci de remplir [ce formulaire](https://forms.gle/nAac8A2Qpt2UwZK18), nous reprendrons contact avec vous quelques jours avant votre venue avec les dernières informations logistiques.

Par avance, merci de votre aide précieuse qui nous porte à chaque fois !

Nous rappelons que la réalisation de ce projet est rendue possible [grâce à la généreuse donation attribuée par le Fonds Germe](https://magnyethique.org/post/2021-12-23-un-verger-conservatoire-a-l-ecolieu/) il y a bientôt 2 ans.

<img src="https://cloud.magnyethique.org/s/ipm3Ho4Jzd8nox6/preview" width=50% alt="Fonds Germes: Le Don au cœur de l'économie">
