---
title: Formation permaculture à l'écolieu
date: '2019-06-27'
tags:
  - permaculture
  - formation
bigimg:
  - desc: Jardin en permaculture à la Ferme du Suchel
    src: /images/uploads/1561739503867_ferme du suchel.jpg
---
Nous avons l’honneur de vous annoncer que la première formation organisée par notre nouvel écolieu sera une formation de permaculture. Elle pourra avoir lieu grâce à l’intervention du [Permalab](http://permalab.fr) et en partenariat avec la [Ferme du Suchel](https://www.tumblr.com/privacy/consent?redirect=http%3A%2F%2Flesuchel.tumblr.com%2F).

Quand?
======

Du samedi 28 septembre au vendredi 4 octobre 2019 de 9 à 18h chaque jour.

Où?
===

Les contenus théoriques auront lieu au Château de Magny à Cublize (69), la pratique sera divisée entre les jardins de Magny où la permaculture débute et ceux de la Ferme du Suchel déjà installés en permaculture depuis plusieurs années.

Intervenants
============

Chan Sac Balam, formateur/designer en permaculture (PermaLab) et Samuel Bonvoisin, agronome (PermaLab), co-fondateurs de l'Oasis de Serendip (26).

Mais aussi Édouard Ribatto, naturaliste écologue et permaculteur amateur à la Ferme du Suchel, et des membres du projet MagnyÉthique.

Contenu de la formation
=======================

Il est possible de s'inscrire uniquement pour l'initiation qui correspond aux deux premiers jours de la formation donnée sur le week-end. Le contenu est centré sur la découverte de l'éthique et des principes de la permaculture, ainsi que des méthodes de conception utilisées. Une grande part des contenus de ce module d'initiation est théorique.

Les cinq jours d'approfondissement de la semaine alterneront moments de théorie et de pratique. À l'issue de ce stage vous saurez :

*   Ce qu'est la permaculture, et ce qu'elle n'est pas (initiation) !
*   Manipuler les principes de la permaculture, et les utiliser dans votre propre contexte (approfondissement).
*   Comment débuter un projet de production vivrière sur un terrain, quelle que soit sa taille.
*   Comment maximiser les ressources en eau, et les utiliser sur votre terrain.
*   Quand et comment installer et gérer des arbres sur votre terrain.
*   Comment favoriser la vie d'un sol, et sa fertilité.
*   Comment produire vos propres purins et autres préparations permettant de soigner vos plantations.
*   Comment s'inspirer des motifs présents dans la nature pour concevoir des (éco-)systèmes.
*   Comment utiliser les outils de la permaculture humaine pour organiser votre vie.
*   Comment utiliser les outils de la permaculture humaine pour favoriser le vivre ensemble.

Hébergement
===========

Possibilité d’hébergement en camping ou en dortoir sur place, ou dans des gîtes à proximité selon le confort souhaité (n’hésitez pas à nous contacter pour en savoir plus!).

Repas
=====

L’organisation du stage prend en charge les repas pendant la durée de la formation. Les repas de midi seront préparés par un cuisinier, ceux du soir en gestion collective. Ils seront végétariens et préparés à base de produits locaux et/ou bio.

Tarif
=====

Notre groupe MagnyÉthique a choisi de faire bénéficier à certains de ses membres de cette formation dirigée par le Permalab afin de pouvoir ensuite mettre en place une gestion responsable et raisonnée des 8 hectares de l’écolieu. Nous avons décidé d’en faire profiter des personnes extérieures sur la base d’un prix libre et conscient. Chaque participant sera libre de donner la somme qu’il pensera être adaptée en son âme et conscience à la formation et à l’accueil qu’il aura reçus. Un temps dédié à l’explication de ce processus et des coûts du stage aura lieu le dernier jour de stage. Si vous le désirez, nous pouvons vous envoyer un document qui présente de façon plus complète notre démarche pour le prix libre et conscient ainsi que le prévisionnel des frais engendrés par la formation.

Places et Inscriptions
======================

La formation initiale (2 jours) peut accueillir en tout 15 personnes extérieures au groupe.

La formation pratique (2+5 jours) peut en accueillir 10. Les personnes s'inscrivant à la semaine complète seront prioritaires.

Formulaire d’inscription à télécharger [ici](https://drive.google.com/open?id=15jRz8UDdij6vIlWgkPsKPqsX19N9nZzL).
