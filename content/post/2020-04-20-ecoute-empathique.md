---
title: Écoute empathique
date: '2020-04-17'
tags:
  - vie de groupe
  - CNV
---
C’est dans une ambiance confinée que se sont mises en place des séances d’écoute empathique à l'écolieu.

Sur la proposition de Caroline, une douzaine de MagnyÉths et quelques extérieurs se réunissent depuis début avril (en visio pour celles et ceux qui n’habitent pas encore sur place). Nous réalisons cette pratique chaque semaine sur un créneau d’une heure, ou en cas de besoin à tout autre moment plus convenant.

C’est lors de notre [formation en CNV suivie en mars](/post/2020-04-16-formation-cnv/) que nous avons abordé cette posture et l’avons expérimentée, sous l'oeil attentif d'Edith Tavernier et Benjamin Pont. Le contexte actuel, difficile pour tous, a impulsé le déclic pour cette mise en place de l’écoute bienveillante au sein du groupe, où chacun peut participer à sa guise. Il s'agit pour les membres qui sont loin de l'écolieu de trouver un espace pour exprimer leurs émotions difficiles face à la situation (angoisse, solitude, frustration…); pour les membres présent.es à l'écolieu, ces séances permettent d'exprimer certaines difficultés d'une vie commune certes harmonieuse dans l'ensemble, mais le confinement ne permet pas d'aller évacuer certaines tensions inhérentes à l'humain ailleurs. Dans les deux cas, il est important de laisser un espace d'expression pour que ces émotions négatives ne gagnent trop en importance. Écouter et être écouté dans le respect de ce qui se vit pour chacun, s’aider mutuellement à mettre la lumière sur nos ressentis et besoins, afin de clarifier la relation à l’autre, aux autres : c'est tout cela que permet l'écoute empathique.

**S’écouter et écouter, pour une meilleure compréhension de la relation**

Chacun est conscients que les émotions négatives accumulées vont produire du stress, de la tristesse, de l’anxiété, des comportements de lassitude ou de fuite,..

Et en tant que groupe, si nous voulons fonctionner, cocréer, vivre ensemble et générer des dynamiques collectives résilientes et pérennes, il est intéressant de pouvoir se munir d'outils tels que l'écoute empathique, fort utiles pour désamorcer les mal-être occasionnés par les difficultés que chacun peut vivre dans son quotidien.

L’écoute de l’autre se réalise par une pleine présence à la relation, en toute bienveillance et sans attente personnelle. C’est un espace dédié à une présence réciproque où l’un exprime ses difficultés, ses émotions, en toute simplicité et authenticité, et l’autre accueille sans jugement, sans passer par ses propres filtres. Accepter ce que son interlocuteur peut vivre dans le moment, et écouter au-delà des mots.

C’est une posture que nous cherchons à rejoindre le mieux que nous pouvons, selon notre capacité dans l’instant à accueillir nos propres émotions, ressentis, vécus, et d'être le plus disponible possible à l’autre.

De cette écoute découle la reconnaissance qui accompagne l'état dans lequel la personne se sent, favorisant la fortification du lien.​

Si se reconnaître mutuellement dans nos difficultés et nos valeurs pouvait simplifier nos capacités à vivre ensemble, alors **soyons l’or que le silence fait naître**.
