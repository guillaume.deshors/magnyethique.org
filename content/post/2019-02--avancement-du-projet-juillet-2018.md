---
title: Avancement du projet - juillet 2018
date: '2018-07-30'
tags:
  - avancement
---
Après plusieurs réunions de travail, voici le tour que prend le projet. Nous travaillons plusieurs pistes:

*   Trouver un nouveau lieu peu cher et suffisamment grand pour pouvoir accueillir assez vite nos 4 foyers et d'autres pour faire vivre le projet initial. Ce lieu se doit d'être dans la mesure du possible à environ 1h de Lyon, en région Rhône-Alpes-Auvergne (Nord-Est de l'Ardèche, Isère, moitié Nord de la Drôle, Loire, Rhône, Ain). Nous cherchons un lieu avec des dépendances et au moins 2 hectares pour le projet d'agroécologie.  
    

Nous avons déjà repéré plusieurs lieux potentiels que nous pourrons peut-être visiter dans le courant de l'été.

Toutes les personnes intéressées sont les bienvenues pour s'associer dès maintenant à nos recherches de lieux et de participants ainsi qu'à nos réflexions. Plus nous serons nombreux au moment de l'achat, plus celui-ci et l'entrée dans le concret sera facilité.

*   Fusionner avec un autre groupe existant permettant l'intégration des foyers impliqués actuellement.
