---
title: Espaces pros
comments: false
---
Au sein de l'écolieu, quelques espaces vont être réhabilités pour accueillir des professionnels dont les activités et valeurs sont compatibles avec notre projet.

Certains espaces du lieu mutualisables se prêtent parfaitement à la création d'un espace professionnel ou artisanal : boulangerie, travail du bois, poterie, épicerie participative et zéro déchet, AMAP, micro-brasserie, espaces de bureaux partagés… Encore à l'état d'envies : poterie, atelier de réparation vélos, ludothèque, et plein d'autres !

Parce que le partage d'espaces nous semble aller dans le bon sens, que la mixité des profils professionnels peut être un levier d'échanges important dans la pratique et la créativité, nous souhaitons offrir aux habitants comme à des personnes extérieures la possibilité de travailler sur place de façon régulière ou ponctuelle. Ces espaces mutualisés peuvent également être utilisés par les habitants qui souhaiteraient développer sur place une activité professionnelle, avoir un atelier et/ou disposer d'un local dédié pour leurs heures de paperasse, histoire que l'espace privé reste vraiment privé.

![](/images/uploads/illustration-site-coworking-atelier.jpg)

Capacités
=========

À disposition dès à présent : bureaux partagés, cuisine et sanitaires communs, atelier partagé avec diverses machines à bois, atelier électronique. À moyen terme, envie d'espaces dédiés au soin, pour des consultations de médecines douces, massages…

Vous avez une envie, un projet ? Contactez-nous : ce sera peut-être l'impulsion qui nous lancera dans l'investissement, la mise en route d'une nouvelle activité !
