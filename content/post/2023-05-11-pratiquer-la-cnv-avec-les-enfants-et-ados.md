---
title: Pratiquer la CNV avec les enfants et ados
subtitle: 14 et 15 octobre 2023
date: '2023-05-11'
tags:
  - formation
  - CNV
---

![](https://cloud.magnyethique.org/s/aPsek2pPaWgfNrL/preview)

# Pratiquer la CNV avec les enfants et ados #
## Stage avec Paul-Georges Crismer, formateur certifié du CNVC ##
## 14 et 15 octobre 2023 ##

### Comment concilier : ###
* l’énergie de vie que nos enfants et ados manifestent
* nos besoins personnels
* nos rôles d’ « éducateur » ou de « parent »

### Comment : ###
* canaliser l’énergie de ceux qui découvrent le monde
* maintenir ou rétablir le contact avec ceux qui se taisent ou s’opposent
* poser des limites claires
* faire des demandes « efficaces »
* accueillir la fureur, l’hystérie (y compris la nôtre)

### Bénéfices de cette formation: ###
* aligner sa posture et son intention
* pratiquer l’empathie et l’auto-empathie

Cet atelier se veut une co-construction afin de nous nourrir de ce dont nous avons faim.

## Prérequis : ###
 Au moins 4 jours de CNV (demande négociable) – soit M1+M2.


Logistique :
------------

Le stage commencera à 9h30 et se terminera à 17h30. L'accueil se fera à partir de 9h15 samedi, et les horaires sont susceptibles de varier un peu.
Nous proposons de mettre en place un baby-sitting pour les enfants à partir de 4 ans par une personne expériementée et formée à la CNV, pour permettre ax parents de venir. Ce service nécessite une contribution financière supplémentaire (voir plus bas et formulaire d'inscription)

### Repas : ###

Pour profiter au mieux du stage, nous proposerons les repas du samedi midi et dimanche midi. Ils seront végétariens à tendance biolocale. Le samedi soir se fera en auberge espagnole, avec ce que vous aimez partager. Le repas du vendredi soir n'est pas prévu.

### Logement : ###

Il est possible de dormir sur place, dans des conditions de confort sommaire : dortoir de 6 à 8 lits, camping ou véhicule aménagé. Si vous avez besoin de plus de confort, nous pouvons vous proposer une liste de chambres d'hôte à proximité. Si vous souhaitez dormir sur place avec vos enfants, prévenez-nous enamont pour qu'on vérifie la faisabilité technique de la chose !

### Transport : ###
Nous vous invitons à favoriser les transports en communs. Pour les derniers kilomètres, nous mettrons en place à partir de la gare d'Amplepuis une navette le vendredi soir vers 20h45 et le samedi matin en fonction de l'horaire de début de stage.

Inscriptions, tarif : 
------------
Frais pédagogiques : 250 € (professionnels et entreprises cf. [site](www.conforit.be) pour plus de détails)

À ajouter et à règler sur place : 
* repas du samedi midi et dimanche midi 2x11€ = 22€ (repas du samedi soir en auberge espagnole)
* hébergement (dortoir "sommaire" de 6 à 8 lits, camping ou véhicule aménagé  ) et petit-déjeuner : participation libre
* utilisation des salles, soutien au projet MagnyÉthique : participation libre
* baby-sitting : participation libre, pour rémunérer la ou les personnes qui s'occuperont des enfants. Somme totale en fonction du nombre d'enfants.


Réservez votre place directement auprès du formateur pour le stage [sur ce lien](https://www.conforit.be/formations-cnv/demande-dinscription-fr/) et remplissez [ce questionnaire](https://framaforms.org/logistique-stage-a-magnyethique-pratiquer-la-cnv-avec-les-enfants-et-ados-1686319986) pour la logistique de votre accueil à MagnyÉthique.


Pour toute question, contactez-nous à [2023cnv@magnyethique.org](mailto:2023cnv@magnyethique.org)






