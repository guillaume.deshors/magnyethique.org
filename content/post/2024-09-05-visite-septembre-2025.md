---
title: Présentation du projet, échanges et visite
subtitle: Dimanche 15 septembre à 15h
date: '2024-09-05'
tags:
  - échanges
  - rencontre
---

Nous organisons une journée de visite et rencontre “Portes ouvertes” le dimanche 15 septembre de 15h à 17h. 

À 17h nous aurons le plaisir d'accueillir Benjamin Campagna pour son [spectacle "SATIE *de travers*"](../2024-09-05-concert-satie-de-travers).

Cet événement s’inscrit dans le cadre des [journées européennes de l'habitat participatif](https://www.habitatparticipatif-france.fr/?HPFJPO).

![](https://www.habitatparticipatif-france.fr/files/HPFJPO_JPOHPF22a_20240603155150_20240605155325.jpg)