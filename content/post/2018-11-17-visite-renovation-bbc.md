---
title: Visite rénovation BBC
tags:
  - formation
  - aménagements
date: '2018-11-17'
---
![](/images/uploads/image8.png)

Noémie, Charlotte et Grégory se sont rendus à la [visite](http://www.infoenergie69-grandlyon.org/am_event/visite-de-site-renovation-globale-bbc-dun-vieux-batiment-en-pierre-samedi-17-novembre-a-saint-forgeux-69-a-10h/ "http://www.infoenergie69-grandlyon.org/am_event/visite-de-site-renovation-globale-bbc-dun-vieux-batiment-en-pierre-samedi-17-novembre-a-saint-forgeux-69-a-10h/") de site organisée par le service Info énergie de la région Auvergne-Rhône-Alpes à St Forgeux.

Il s'agissait de découvrir la rénovation globale BBC d'un vieux bâtiment en pierre: de l'isolation aux questions de chauffage, ce fut l'occasion pour nous de piocher des idées et de prendre des contacts pour la rénovation qui nous attend au Château de Magny.
