document.addEventListener('DOMContentLoaded', function() {
    let updateInterval;
    let originalEvents = [];  // Stocker les données originales

    function formatDate(dateStr, timeStr, dateFormat) {
        const date = new Date(dateStr);
        const options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        let formatted = date.toLocaleDateString('fr-FR', options);
        if (timeStr) {
            formatted += ` à ${timeStr}`;
        }
        return formatted;
    }

    function formatTimeSpan(dateStart, dateEnd, showTime) {
        const start = new Date(dateStart);
        const end = new Date(dateEnd);
        
        const startTime = showTime ? start.toLocaleTimeString('fr-FR', {hour: '2-digit', minute:'2-digit'}).replace(':', 'h') : '';
        const endTime = showTime ? end.toLocaleTimeString('fr-FR', {hour: '2-digit', minute:'2-digit'}).replace(':', 'h') : '';
        
        const sameDay = start.toDateString() === end.toDateString();
        const sameMonth = start.getMonth() === end.getMonth() && start.getFullYear() === end.getFullYear();
        const sameYear = start.getFullYear() === end.getFullYear();
        
        let dateFormat = {};
        if (sameDay) {
            if (showTime) {
                return `le ${formatDate(dateStart)} de ${startTime} à ${endTime}`;
            }
            return `le ${formatDate(dateStart)}`;
        }
        
        if (sameMonth) {
            dateFormat = { weekday: 'long', day: 'numeric' };
        } else if (sameYear) {
            dateFormat = { weekday: 'long', day: 'numeric', month: 'long' };
        } else {
            dateFormat = { weekday: 'long', day: 'numeric', month: 'long', year: 'numeric' };
        }
        
        const startFormatted = start.toLocaleDateString('fr-FR', dateFormat);
        const endFormatted = end.toLocaleDateString('fr-FR', { weekday: 'long', day: 'numeric', month: 'long', year: 'numeric' });
        
        if (showTime) {
            return `du ${startFormatted}, ${startTime}, au ${endFormatted}, ${endTime}`;
        }
        return `du ${startFormatted} au ${endFormatted}`;
    }

    // Capture initiale des données
    function captureOriginalEvents() {
        const container = document.getElementById('liste-evenements');
        if (!container) return false;

        const evenements = Array.from(container.getElementsByClassName('evenement'));
        originalEvents = evenements.map(evt => ({
            date: evt.dataset.date,
            dateFin: evt.dataset.dateFin,
            afficheHeure: evt.dataset.afficheHeure === 'true',
            titre: evt.querySelector('.titre-content').innerHTML,
            description: evt.querySelector('.description-content')?.innerHTML || ''
        }));

        return true;
    }

    function updateEvenements() {
        const container = document.getElementById('liste-evenements');
        if (!container) {
            if (updateInterval) {
                clearInterval(updateInterval);
                updateInterval = null;
            }
            return;
        }

        const now = new Date();
        now.setHours(0, 0, 0, 0);

        const avenir = [];
        const passe = [];

        originalEvents.forEach(evt => {
            const date = new Date(evt.date);
            date.setHours(0, 0, 0, 0);
            
            const timetext = evt.dateFin
                ? formatTimeSpan(evt.date, evt.dateFin, evt.afficheHeure)
                : formatDate(evt.date, 
                    evt.afficheHeure
                        ? new Date(evt.date).toLocaleTimeString('fr-FR', {hour: '2-digit', minute:'2-digit'}).replace(':', 'h')
                        : null
                );

            const html = `
                <li>
                    <strong>${timetext} :</strong>
                    ${evt.titre}
                    ${evt.description ? ` — ${evt.description}` : ''}
                </li>
            `;

            if (date >= now) {
                avenir.push({ date: date, html: html });
            } else {
                passe.push({ date: date, html: html });
            }
        });

        avenir.sort((a, b) => a.date - b.date);
        passe.sort((a, b) => b.date - a.date);

        const parts = [];
        
        if (avenir.length > 0) {
            parts.push(`
                <h2>${container.dataset.titreAvenir} :</h2>
                <ul>${avenir.map(e => e.html).join('')}</ul>
            `);
        }
        
        if (passe.length > 0) {
            parts.push(`
                <h2>${container.dataset.titrePasses} :</h2>
                <ul>${passe.map(e => e.html).join('')}</ul>
            `);
        }

        const outputContainer = container.querySelector('.output') || container;
        outputContainer.innerHTML = parts.join('');
    }

    // Initialisation
    if (captureOriginalEvents()) {
        // Créer un conteneur pour la sortie si nécessaire
        const container = document.getElementById('liste-evenements');
        if (!container.querySelector('.output')) {
            const originalContent = container.innerHTML;
            const outputDiv = document.createElement('div');
            outputDiv.className = 'output';
            container.innerHTML = originalContent;  // Préserver les données originales
            container.appendChild(outputDiv);
        }

        // Mettre à jour immédiatement
        updateEvenements();

        // Configurer l'intervalle de mise à jour
        updateInterval = setInterval(() => {
            if (document.getElementById('liste-evenements')) {
                updateEvenements();
            } else {
                clearInterval(updateInterval);
                updateInterval = null;
            }
        }, 60000);

        // Nettoyer l'intervalle quand la page est déchargée
        window.addEventListener('unload', () => {
            if (updateInterval) {
                clearInterval(updateInterval);
                updateInterval = null;
            }
        });
    }
});
