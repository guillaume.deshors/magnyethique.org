---
title: Pacifier sa relation à l'argent
subtitle: 9 et 10 septembre 2023
date: '2023-05-11'
tags:
  - formation
  - CNV
---

![](https://cloud.magnyethique.org/s/iBstksZwSNcbLSY/preview)

## Stage avec Paul-Georges Crismer, formateur en Communication NonViolente ##

### Et votre relation à l’argent&nbsp;? ###

Est-elle agile, consciente, paisible&nbsp;?
Que dit-elle de vous, de vos besoins, de vos croyances&nbsp;?
Qu’est-ce que vous projetez de positif ou de négatif sur l’argent&nbsp;?
Quelles conséquences cela a-t-il sur vos comportements avec l’argent&nbsp;?

Lors de cet atelier vous travaillerez sur votre propre relation à l’argent.

### Nous explorerons notamment: ###

* Comment réviser nos projections sur l’argent afin que l’argent puisse contribuer joyeusement au monde dans lequel nous voulons vivre&nbsp;?
* Comment faire de l’argent une stratégie pour prendre soin de nos besoins et de ceux des autres&nbsp;?
* Comment transformer ce qui nous empêche de vivre en harmonie et en paix, voire en amour avec l’argent&nbsp;?

### Sources d’inspiration : ###

Ce stage s’inspire des travaux de

* Marshall Rosenberg (CNV)
* Peter Koenig (Relation à l’argent, Peter Koenig System)
* l’enseignement reçu avec Christian Junod, auteur du livre “Ce que l’argent dit de vous”.
    * Il m’a initié au “Peter Koenig System” et m’a encouragé à proposer ce stage:

> «&nbsp;Ayant suivi de très nombreux ateliers de CNV et ayant été inspiré par cette philosophie de vie, je vois aujourd’hui, toujours plus finement, l’intérêt de croiser ces deux approches. Le travail identitaire proposé par le thème de la relation à l’argent permet d’être moins stimulé négativement par le comportement des autres et cela vous soutiendra à communiquer de manière plus paisible. En suivant un tel atelier avec Paul-Georges, je suis sûr que vous êtes entre de bonnes mains pour de riches prises de consciences.&nbsp;»
Christian Junod – 20 avril 2017

### Prérequis : ###

Le stage est ouvert à toutes les personnes, sans prérequis.

Si vous avez suivi au moins 4 jours de CNV avec un/e formateur/trice certifié/e du CNVC, vous approfondirez la distinction entre besoin et stratégie.  Avec la CNV vous avez gagné en agilité relationnelle. Vous êtes conscient.e que vous pouvez mieux être au service de votre vie ainsi que contribuer avec joie à celle des autres.

Logistique :
------------

Le stage commencera à 9h30 et se terminera à 17h30. L'accueil se fera à partir de 9h15 samedi, et les horaires sont susceptibles de varier un peu.

### Repas : ###

Pour profiter au mieux du stage, nous proposerons les repas du samedi midi et dimanche midi. Ils seront végétariens à tendance biolocale. Le samedi soir se fera en auberge espagnole, avec ce que vous aimez partager. Le repas du vendredi soir n'est pas prévu.

### Logement : ###

Il est possible de dormir sur place, dans des conditions de confort sommaire : dortoir de 6 à 8 lits, camping ou véhicule aménagé. Si vous avez besoin de plus de confort, nous pouvons vous proposer une liste de chambres d'hôte à proximité.

### Transport : ###
Nous vous invitons à favoriser les transports en communs. Pour les derniers kilomètres, nous mettrons en place à partir de la gare d'Amplepuis une navette le vendredi soir vers 20h45 et le samedi matin en fonction de l'horaire de début de stage.

Inscriptions, tarif : 
------------
Frais pédagogiques : 250 € (professionnels et entreprises cf. [site](www.conforit.be) pour plus de détails)

À ajouter et à règler sur place : 
* repas du samedi midi et dimanche midi 2x11€ = 22€ (repas du samedi soir en auberge espagnole)
* hébergement (dortoir "sommaire" de 6 à 8 lits, camping ou véhicule aménagé  ) et petit-déjeuner : participation libre
* utilisation des salles, soutien au projet MagnyÉthique : participation libre


Réservez votre place directement auprès du formateur pour le stage [sur ce lien](https://www.conforit.be/formations-cnv/demande-dinscription-fr/) et remplissez [ce questionnaire](https://framaforms.org/logistique-stage-a-magnyethique-relation-a-largent-1686318870) pour la logistique de votre accueil à MagnyÉthique.


Pour toute question, contactez-nous à [2023cnv@magnyethique.org](mailto:2023cnv@magnyethique.org)






