---
title: Formation CNV - Module 1
subtitle: Dimanche 29 avril et vendredi 11 mai 2018
tags:
  - formation
  - gouvernance
  - CNV
date: '2018-05-19'
---
![](/images/uploads/image5.png)

Le groupe, accompagné en médiation en Communication Non Violente par Pierre-Yves Claudin, a décidé de poursuivre la collaboration. Accompagné d'un collègue, il est intervenu une journée complète pour initier le groupe à la CNV et mener un bref atelier sur les non-dits.
