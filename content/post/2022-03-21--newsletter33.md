---
title: "Newsletter 33 : Les uns lézardent, les autres s'activent... Le printemps est là !"
date: 2022-03-21
tags:
  - newsletter
  - avancement
---

### Bonjour ami·es MagnyÉthiques
­
Les journées débutent dans la fraîcheur et blancheur des gelées nocturnes, mais il est des signes qui ne trompent pas : les oiseaux chantent à plein gosiers, les lézards et les couleuvres lézardent, les tritons plongent pour leur période aquatique, les fleurs pointent leurs premiers pétales colorés, et les arbres (dont nos jolis petits fruitiers) bourgeonnent !   
Le printemps est  là et nous remplit, nous aussi, d’une belle énergie de renouveau.

### Save the dates

<img src="/images/uploads/2021-12-16--newsletter31_img1#floatleft" height="50" />

### Les prochains ateliers, stages, événements…

{{< clear >}}

*   **Samedi 21 mai 2022** :   
    Visite “La Biodiversité au jardin” dans le cadre des Journées de la Nature  
     
*   **Du jeudi 26 au dimanche 29 mai 2022** :   
    [Stage Danse-contact-impro et Axis Syllabus](https://magnyethique.org/post/2022-03-01-26-au-29-mai-2022-stage-danse-contact-impro-et-axys-syllabus/)  
     
*   **Samedi 15 et dimanche 16 octobre 2022**   
    Atelier formation "Poser les bases d'un collectif impliqué et durable"  
    Informations et inscriptions par e-mail : [\[email protected\]](http://2022collectif@magnyethique.org)

<img src="/images/uploads/2022-01-22--newsletter32_img1#floatleft" height="50" />

### Envie d'aider ? Calendrier des prochains chantiers participatifs

{{< clear >}}

*   Du **lundi 25 avril** au **vendredi 6 mai 2022**  
     
*   Du **lundi 4** au **vendredi 22 juillet 2022**  
     
*   Du **lundi 8** au **vendredi 19 août 2022**

[\+ d'infos sur les modalités d'inscription](/page/wwoofing_chantiers/)

### De poutres et de planches : Isolation phonique en pratique

Du 17 au 28 janvier, nous avons réalisé un chantier animé par Benjamin et Jérémy, deux charpentiers professionnels. Le but ? **Isoler phoniquement** le sol de quatre pièces dans deux appartements, et au passage refaire un plancher dans deux des pièces qui n’en avaient pas. L’isolation a été faite selon les préconisations de Micha des Choux lents, autre habitat groupé à St Germain au mont d’Or, qui est acousticien. 

Nous avons fait des trous dans le parquet et glissé de la **ouate de cellulose** en vrac pour éviter un effet caisse de résonance, puis posé des **panneaux de laine de bois rigide**, et enfin des **plaques de Fermacell de sol**, désolidarisées du sol par une mousse. 

L’idée : de la masse (le Fermacell) et du ressort (la laine de bois) pour disperser les différentes longueurs d’onde des bruits aériens, et tout ça désolidarisé de la structure pour éviter la transmission des bruits solidiens (notamment impacts de pas). Nous portons une grande attention à cette isolation phonique entre appartements, car d’une part nous savons que pour l’instant elle est mauvaise et d’autre part nous imaginons que ça peut générer de nombreux conflits de voisinage. 

Là où on le pourra, on isolera plutôt les plafonds, c’est encore plus efficace.  On vous expliquera comment !    

Merci à Benjamin, Jérémy, Jan, Dimitri, Turi, Marie, Micha, Maxime J. et Maxime B.

### Entrée en scène de la Carapoule

Voilà un projet pour lequel nous avons bien procrastiné !   
D’abord longtemps à l’état d’idée, nous avons fini par trouver une vieille caravane l’été dernier. Les participants à _l’Université d’Été Décontractée_ d’Avenir Climatique ont été les premiers à y travailler en août pour renforcer la structure et débuter le torchis en intérieur. Mais c’est grâce à la présence prolongée de Jan que nos heureuses poulettes ont à présent un poulailler mobile fantastique bien réel et voient du pays… euh, du terrain, n’exagérons rien !

![](/images/uploads/2022-03-21--newsletter33_img1)

![](/images/uploads/2022-03-21--newsletter33_img2)

­
### Faire rimer maraîchage et biodiversité

Après plus de deux ans d’observation de nos terrains sur bien des plans dans le cadre du [**design**](https://magnyethique.org/post/2021-10-20-design-permacole/) [**permacole**](https://magnyethique.org/post/2021-10-20-design-permacole/) et les premiers essais en maraîchage plutôt concluants l’an dernier, le groupe a donné carte blanche à la commission agricole pour pérenniser ce pan du projet.

**Grâce aux deux** [**wwoofeur·se·s**](https://wwoof.fr/fr/) qui nous prêtent main forte depuis mi-janvier, nous peaufinons donc les aménagements des terrains. Nos objectifs : viser petit à petit la **résilience** pour le groupe et ses hôtes, tout en prenant soin de la **biodiversité** déjà riche à notre arrivée. Nous comptons non seulement la conserver, mais même la favoriser par quelques aménagements longuement élaborés.

Ainsi, nous mettons cette année l’accent sur la **plantation de vivaces** de toutes sortes (aromatiques, médicinales, légumières) pour inscrire le maraîchage sur du long terme, comme avec les vergers. La mise en place des asperges nous régalera dès 2023 et pour une quinzaine d’années, par exemple. Ceci valait bien les deux jours et demi de tranchées pour leur plantation ! ­

Et en parallèle, nous profitons de ces aménagements pour penser aux auxiliaires précieux du maraîchage que sont les insectes, passereaux, lézards, hermines, couleuvres, rapaces… Les trois derniers seront notamment nos alliés dans la lutte contre la multiplication des rats taupiers qui se régalent un peu trop dans nos jardins. **La construction de deux pierriers** centraux nous a pris, elle aussi, du temps et de l'énergie, mais elle devrait nous permettre de faire une place de choix aux prédateurs des rongeurs et nous éviter l'utilisation d’autres méthodes moins douces que celles du simple cycle de la chaîne alimentaire.­

![](/images/uploads/2022-03-21--newsletter33_img3)

![](/images/uploads/2022-03-21--newsletter33_img4)

_Une couleuvre n'a pas tardé à investir le pierrier !_
­

Et pour finir, nous avons adopté fin février pas moins de 5000 [**larves de mouches “soldats noirs”**](https://wiki.lowtechlab.org/wiki/Elevage_de_Mouches_Soldats_Noires) pour lancer un nouveau compost qui devrait nous fournir un excellent terreau très rapidement pour nos semis à venir : deux semaines contre les six mois du lombricompost et les au moins douze mois des composts à froid, que nous continuerons malgré tout à nourrir. La diversité jusqu’aux composts !

### Des poteries hautes en couleurs 

_**Interview d’Agnès, notre potière en herbe, par Louise :**_ 

![](/images/uploads/2022-01-22--newsletter32_img6)

\[Louise\] **Qu’est-ce qui t’a donné envie de faire de la poterie ? :** 

\[Agnès\] J’ai toujours aimé avoir les mains dans la terre. Et puis j’ai eu de la chance, ma maman était cuisinière dans un centre aéré et il y avait une vraie potière. Je faisais de la poterie avec elle tous les mercredis et les vacances scolaires, ça a duré une ou deux années scolaires puis la poterie a disparu de ma vie.

\[L\] : **Qu’est-ce qui t’a donné envie de le partager à Magny ?**  
\[A\] : En fait c’est Magny qui a eu envie que je le partage. Je ne savais pas qu’autant de personnes auraient envie de mettre les mains dans la terre, de la modeler. J’ai été très touchée et très intimidée d’avoir autant de personnes intéressées.   
  
\[L\] : **Est-ce que tu peux nous raconter un bon souvenir à l’atelier de poterie ?**  
\[A\] : Au 1er atelier proposé, tous les enfants étaient là. Ils ont été très respectueux les uns envers les autres. Et la consigne essentielle a été retenue comme quelque chose d’important : on ne touche pas les œuvres des autres et les enfants relaient cette consigne aux nouvelles personnes. Un autre souvenir où j’ai repoussé mes limites parce qu’Alice voulait absolument faire une lapine, je ne savais pas faire une lapine et nous l’avons finalement modelée et elle ressemble à une lapine. J’apprécie beaucoup le sérieux et le jeu avec lequel les enfants viennent faire l’atelier poterie.  
  
\[L\] : **Qu’est-ce qu’il y a eu comme objets modelés lors des ateliers poterie ?**  
\[A\] : Ils ne sont pas encore cuits donc il va encore y avoir de potentielles surprises. Il y a eu des fleurs, beaucoup de pots, des cabanes à oiseaux adaptées à chaque oiseau. Tout ne sera pas cuit mais cela n’enlève pas le plaisir du modeleur, on n’est pas obligé de modeler pour avoir un résultat.  
Les œuvres ont été faites à partir de 2 techniques : la boule et la plaque.   
Et je vous montrerai aussi la technique du colombin pour faire des pots évasés.  
  
\[L\] : Merci pour notre échange.

### Un week-end à l’unisson et un concert surprise 

Nicolas Mahnich est venu les 26 et 27 février pour animer **un week-end d’improvisation vocale et de circle songs** (cercles de chansons). Nous étions une douzaine de participants, moitié du lieu et moitié des alentours, ce qui nous a permis de rencontrer et de sympathiser avec de nouveaux voisins.  
Pendant deux jours, nous avons improvisé des rythmes, des bruits, des notes, mis tout ça ensemble et mélangé. A la clé, des moments étonnants, des moments dissonants, et des moments planants. Merci à Nicolas !

Le 6 mars, **concert impromptu de Juliette et Tom**, deux jeunes artistes aux talents divers qui nous ont offert un concert particulier dans la salle parapluie. Un moment magique et hors du temps. Nous leur souhaitons bonne route et encore de beaux moments de partage dans leur  périple… à vélo s’il vous plaît !

![](/images/uploads/2022-03-21--newsletter33_img5)

### Stage à la maison

Les 12 et 13 mars, Caroline et Bruno ont proposé un premier stage sur la thématique **"Poser les bases d'un collectif impliqué et durable".** 

Les neuf participant·e·s ont pu découvrir et vivre des outils d'intelligence collective, de gouvernance, de prévention/gestion des tensions, et déguster les repas préparés par Agnès et Claire.  
Ce fut un beau week-end, joyeux et profond. Caroline et Bruno ont aussi fort apprécié de donner ce stage "à la maison" : le confort d'être sur place, la joie d'accueillir, la chaleur du poêle... 

Le matériel de collectivité a bien servi : le lave-vaisselle nouvellement acquis a tourné pour la première fois (enfin pour la première fois sans fuite : merci Guillaume pour la réparation !), le four a rendu de fiers services, les deux éviers, le chariot inox... La cuisine "publique" prend forme !  
C'était la première fois que nous proposions ce stage, et les retours enthousiastes nous donnent envie de recommencer rapidement. C'était aussi le premier stage résidentiel animé par des habitant·e·s et avec des repas confectionnés sur place. Prochaine étape : que les repas soient faits avec les légumes du potager ! Au vu de ce que prépare la commission agricole, c'est pour bientôt ...

Laissez-vous porter par les doux rayons du soleil de début de printemps et à bientôt !

![](/images/uploads/2022-03-21--newsletter33_img6)

Un petit bonjour des crapauds du coin.
