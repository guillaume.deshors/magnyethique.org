---
title: Formation permaculture
subtitle: Du 28 septembre au 4 octobre 2019
date: '2019-09-03'
tags:
  - permaculture
  - formation
bigimg:
  - desc: null
    src: /images/uploads/1567541403101_IMGP7748.JPG
---
La formation permaculture approche à grands pas et le contenu se précise !  
Pour les indécis, voici de quoi vous faire envie :

**Samedi 28 et dimanche 29 septembre 2019** : Introduction à la permaculture  
- Samedi : Éthiques et principes, posture et méthodes de design.  
- Dimanche : concepts et thématiques de design, défrichage projet MagnyÉthique.

**Lundi 30 septembre 2019** : Journée de l'eau  
- Matin : Gestion de l'eau  
- Aprem : Orologie

**Mardi 1er octobre 2019** : Journée des sols  
- Matin : Fertilité des sols (théorie)  
- Aprem : Atelier pratique

**Mercredi 2 octobre 2019** : Journée Design  
- Matin : Cueillette des données  
- Aprem : Projections Design MagnyÉthique

**Jeudi 3 octobre 2019** : Agroforesterie  
- Matin : Atelier théorique  
- Aprem : Atelier pratique

**Vendredi 4 octobre 2019**
- Matin : Culture des champignons  
- Aprem : Évaluation

Il reste encore quelques places !!! [Ne trainez pas](/post/2019-06--formation-permaculture-a-magny/) !
