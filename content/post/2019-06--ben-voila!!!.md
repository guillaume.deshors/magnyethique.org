---
title: Ben voilà !!!
subtitle: À nous Magny !
date: '2019-06-06'
tags:
  - avancement
---
-   \[x\] Acte d'achat signé : nous sommes les heureux propriétaires d'un écolieu MagnyÉthique en construction.

**Magny, c'est parti !!**

Prise de possession des lieux dès aujourd'hui et planification des travaux : enfin du super concret !

![](/images/uploads/1559855311940_Ouverture des portes.JPG)
