---
title: Salon Primevère 2019
subtitle: Du vendredi 22 au dimanche 24 février 2019
date: '2019-02-25'
tags:
  - conférence
  - gouvernance
  - société
---
Cette année encore a lieu le Salon Primevère de l'alter-écologie, le 33ème déjà! Nous avons été plusieurs membres du groupe à nous y rendre du vendredi au dimanche avec des objectifs précis en fonction de nos (com)missions.

![](/images/uploads/1550823005975_affiche_primevere2019_web.jpg)  

Beaucoup de professionnels ressources pour nos travaux d'aménagements (isolation, énergies, finitions), des thèmes autour du jardin, de la permaculture, mais aussi des assos pouvant nous conseiller et/ou accompagner dans la gouvernance pour monter notre système restauratif en cours d'élaboration, des idées et impulsions pour la réalisation de l'accueil de public ou d'hôtes... Nous avons pu assister à peu de conférences malheureusement mais les enfants ont adoré les ateliers proposés pour les plus jeunes.

L'énergie de ce beau salon nous a regonflés à bloc et a de quoi redonner confiance aux plus pessimistes!
