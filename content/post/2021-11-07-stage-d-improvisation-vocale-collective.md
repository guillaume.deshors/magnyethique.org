---
title: Stage d'improvisation vocale collective
subtitle: 26 et 27 février 2022
date: '2021-11-07'
tags:
  - formation
  - stage
  - culture
  - musique
---
**Stage d'improvisation vocale collective les 26 et 27 février 2022**

**Ouvert à tous·tes**

Après le [pianiste Marc Vella accueilli en octobre dernier](//post/2021-10-27-quintes-et-sens/post/2021-10-27-quintes-et-sens/), nous sommes enchantés de recevoir Nicolas Mahnich pour un stage d’improvisation, vocale et collective cette fois. Ça promet à nouveau un beau moment !

Ce stage et proposé en participation libre et consciente, tant pour la partie pédagogique pour la partie logistique et accueil. Nous prendrons un petit temps le dimanche pour en parler ; n’hésitez pas à nous contacter si vous souhaitez des renseignements plus détaillés.

Un hébergement en dortoir est possible. Pour les repas, les déjeuners seront fournis au prix de 10€ l'un, et pour celui du samedi soir nous vous demandons d’amener quelque chose à partager.

**Inscriptions** : par courriel à 2022chant@magnyethique.org

Nombre de places limité à 12 personnes : dépêchez-vous !

**Le programme :**

Ce "Lab" est un espace-temps d’exploration autour du chant improvisé collectif au sein duquel Nicolas Mahnich nous propose de la matière à travailler de façon intensive ! Les stagiaires malaxeront et pétriront ainsi la musique avec leur voix, tantôt pour la contempler d’un point de vue large et global, tantôt pour en décortiquer tous les concepts et détails. 

Cette exploration est une invitation à enrichir notre conscience et notre compréhension de la musique, et en conséquence à nourrir nos improvisations et notre façon de les vivre.

![](/images/uploads/1636298612746_PhotoNicolasMahnich.JPG)  

**[Nicolas Mahnich](http://nicolasmahnich.com) :**

Passionné par la diversité des facettes de la musique et de la connexion humaine, Nicolas aime créer des espace-temps collectifs de jeu autour de l’improvisation vocale et des Circle Songs. Aborder la musique de façon joyeuse et légère, tout en développant rigueur, précision et concentration, tel est le cadre proposé. Il s’est formé principalement sur le terrain, en animant de nombreux _[Chants pour Tous](http://nicolasmahnich.com/chant-pour-tous/)_ puis des stages, et a suivi des formations proposées par Gaël Aubrit et David Eskenazy.
