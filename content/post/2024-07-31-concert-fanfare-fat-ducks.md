---
title: Concert de la fanfare lyonnaise les Fat Ducks
date: '2024-07-31'
tags:
  - culture
  - concert
  - rencontre
subtitle: jeudi 8 août 2024 à 20h30
---
Venez guincher !

La Fanfare Fat Ducks rassemble une quinzaine de joyeux-ses drilles qui prennent plaisir à osciller entre funk, electro, disco, house. Constituée de cuivres, de bois, de percussions et de chant amplifié, les Fat Ducks proposent des reprises funkadéliques de tous les continents et diversifient leur style avec quelques compositions originales, toutes avec le point commun de faire déhancher le public.

![](https://cloud.magnyethique.org/s/wxTKaEy75L7cd9Z/preview)

Iels font cet été une tournée en vélo, qui passe dans la région ; comme à ce moment se déroulera à MagnyÉthique l'[Université d'Été Décontractée de l'association Avenir Climatique](https://avenirclimatique.org/luniversite-decontractee-davenir-climatique-2/), tout est réuni pour une belle soirée festive !

Participation libre au chapeau.

Plus d'infos sur les Fat Ducks : [ici](https://www.youtube.com/watch?v=GOe-g9FbyHo https://www.youtube.com/watch?v=Sm0P8K-909U https://www.youtube.com/watch?v=xoon8wco5Y0).

Buvette gérée par la troupe d'[Avenir Climatique](https://avenirclimatique.org).
