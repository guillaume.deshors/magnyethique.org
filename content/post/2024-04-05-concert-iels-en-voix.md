---
title: Concert "Iels S'en Marrent" suivi d'une conférence sur l'eau
date: '2024-04-30'
tags:
  - concert
  - culture
  - échanges
subtitle: Dimanche 26 mai à 15h
---

"Iels S’En Marrent", c’est un spectacle de chant polyphonique *a capella* chanté par  l’ensemble [Iels En Voix](https://www.laveli.fr/nos-projets/iels-en-voix/) (les plus grands choristes de tout l'étang&nbsp;!) sous la direction de [Maude Georges](https://www.laveli.fr/maude-georges/).

![](https://cloud.magnyethique.org/apps/files_sharing/publicpreview/FGakat9wom6gKPG?file=/Affiche%20concert%20iels%20s%27en%20marrent.jpg&fileId=38831&x=1920&y=1080&a=true)

Un programme varié et joyeux pour vous faire rire et inviter petits et grands à (re)découvrir l'eau et ses usagers, porté par des musiques tantôt classiques, tantôt inattendues, mais toujours jouissives&nbsp;! Le concert est suivi d'une pause avec buvette, puis à 16h30 de **la conférence *"L'eau, un élément haut en couleurs"*** donnée par Katia Stachowicz, membre de Magnyethique.

![](https://www.laveli.fr/wp-content/uploads/2023/12/PXL_20231202_143103832.MP-Copie-1980x835.jpg)

Extrait du répertoire :

  -  To Be Sung on Water (Samuel Barber)
  -  Sommes-nous des grenouilles ? (Chanson Plus Bifluorée)
  -  Il pleut dans mon cœur (Pascal Jugy)
  -  Les petits poissons (Romain Bulteau)
  -  Pase el agua (Anonyme)
  -  Wade in the water (gospel traditionnel)

Et si vous arrivez à comprendre pourquoi 🙂 :

  -  Le pudding à l’arsenic (film d'animation *Astérix et Cléopâtre*)

![](https://www.laveli.fr/wp-content/uploads/2023/12/PXL_20231202_143812498.MP-1-1980x692.jpg)

Le concert est à prix libre, avec un tarif suggéré de 12€. Vous pouvez prendre vos billets sur place ou préférentiellement à l'avance sur la plateforme suivante : **https://www.kocoriko.fr/fr/projects/iels-s-en-marrent** 

Si vous ne pouvez pas venir nous voir le 26 mai, rendez-vous le 8 mai à Ambierle ou le 9 mai à Lyon. Attention, cette offre est à durée limitée : le chœur s'auto-détruira à la fin des trois concerts&nbsp;!

![](https://www.laveli.fr/wp-content/uploads/2023/12/PXL_20231202_144345851.MP_-1024x768.jpg)
