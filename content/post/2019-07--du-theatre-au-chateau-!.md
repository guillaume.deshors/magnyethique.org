---
title: Du théatre au château !
date: '2019-07-23'
tags:
  - culture
  - échanges
  - résidence
---

  
Pour notre premier événement culturel, nous accueillerons les 23 et 24 août à 19h30 et 21h15 quatre représentations de théatre. 

Adhésion à l'association obligatoire (sur place, à partir de 2€). Places limitées, merci de [vous inscrire](/page/contact).

![](/images/uploads/1563908373640_George-Dandin-Cublize.png)

> George Dandin, riche paysan qui a réussi, rêve de devenir gentilhomme. Pour y parvenir, il épouse Angélique, la fille d'une famille aristocratique ruinée qui en échange de sa particule verra ses caisses renflouées.
Cependant, Angélique ne l'entend pas de cette oreille. Avec la complicité de sa suivante et d'un courtisan bellâtre, elle fera subir à son infortuné mari moult humiliations amenant ce dernier à commettre l'irréparable...


![](/images/uploads/1563908365238_Affiche compagnie sans commandeur Cublize.png)

> Une petite compagnie de théâtre contemporain découvre qu’elle perdra sa subvention de l’État si elle ne monte pas une pièce de Molière dans son intégralité. Alci, le metteur en scène, rebelle invétéré, va devoir, avec les comédiens qui lui sont restés fidèles et ceux qui lui sont imposés, répondre à cette exigence. On assiste à la recomposition d’une troupe à travers les coups de gueule idéologiques, les batailles d’ego, les intrigues amoureuses, les rancoeurs et les jalousies. Un hymne au vivre-ensemble, au-delà des différences sociales, sexuelles et intellectuelles.

