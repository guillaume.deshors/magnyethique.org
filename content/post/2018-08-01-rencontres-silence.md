---
title: Rencontres de silence
subtitle: Été 2018
tags:
  - échanges
  - gouvernance
  - écologie
date: '2018-08-20'
---
![](/images/uploads/image17.jpg)

Yves s'est rendu à Paulianne aux [rencontres des amis de la revue "Silence!"](http://amies.revuesilence.net/index.php/rencontre-2018-la-ferme-de-paulianne "http://amies.revuesilence.net/index.php/rencontre-2018-la-ferme-de-paulianne"). Les rencontres s'étalait du 26 juillet au 11 août. Il s'agissait de partager et de vivre les principes de l'écologie, la décroissance, l'autonomie et la non-violence à travers des ateliers et un vivre-ensemble intense.
