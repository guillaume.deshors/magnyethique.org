---
title: Danse contact impro
subtitle: du 27 Août au 1er Septembre
date: '2020-07-27'
tags:
  - rencontre
  - atelier
  - culture
  - danse
---
Bruno propose avec la danseuse Geneviève Cron une rencontre de [danse-contact-impro](https://fr.wikipedia.org/wiki/Contact_improvisation) du 27 août au 1er septembre 2020 : quelques jours pour réunir une douzaine de danseureuses expérimenté·e·s et danser, échanger, réfléchir ensemble autour de cette pratique. Les repas seront préparés par Clément et Chloé du Quartier Métisseur, et il est possible de loger sur place sous tente ou en chambres de deux à quatre lits.

![](/images/uploads/2020.07.27_DanseContact.jpg)

Nous demandons une participation financière qui sera composée d'une partie libre et consciente, pour contribuer au lieu et soutenir le projet dans sa vocation à organiser des événements de ce type (et nous encourager à persévérer dans ce modèle économique du prix libre), et d'une part fixe pour les repas de 80€/personne. Réservation indispensable avant mi-août auprès de brunolabouret@protonmail.com (avec une présentation succincte de vos envies) ou au 06 62 28 36 08.
