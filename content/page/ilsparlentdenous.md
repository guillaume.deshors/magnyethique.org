---
title: Ils parlent de nous
---

**09.2024 :** Article paru dans la revue **_La vie en Clunisois_**, intitulé "Un habitat participatif : "MagnyÉthique"

{{< load-photoswipe >}}
{{< gallery hover-effect="none" caption-effect="none" >}}
{{< figure link="https://cloud.magnyethique.org/s/zdyMPQL3DrcaSPT/preview" caption="La Vie en Clunisois" >}}
{{< /gallery >}}


**04-05.2024 :** Article de Maïlys Belliot, paru dans le n°140 de **_La Maison écologique_,** intitulé **"[1 château, 10 foyers et beaucoup d'invités](https://lamaisonecologique.com/articles/1-chateau-10-foyers-et-beaucoup-d-invites)"**


{{< load-photoswipe >}}
{{< gallery hover-effect="none" caption-effect="none" >}}
{{< figure link="https://cloud.magnyethique.org/s/5RAttGdTFirdrDX/preview" caption="La Maison écologique 1">}} {{< figure link="https://cloud.magnyethique.org/s/92Qyme9moBAN6j9/preview" caption="La Maison écologique 2">}} {{< figure link="https://cloud.magnyethique.org/s/mBpENPnL2r3xXCL/preview" caption="La Maison écologique 3">}} {{< figure link="https://cloud.magnyethique.org/s/b32fBrDfMHMDAnW/preview" caption="La Maison écologique 4">}}
{{< /gallery >}}

**18.12.2023 :** Article "[**Le rôle de la permaculture dans la création de haies**](https://www.leprogres.fr/environnement/2023/12/18/le-role-de-la-permaculture-dans-la-creation-de-haies)" paru dans le journal Le Progrès

**Du 23.11.2023 à décembre 2023 :** Expo du Collectif La Clé _En Chantiers_ à Villeurbanne. Une invitation artistique à (re)découvrir les chantiers participatifs mais aussi plusieurs lieux de la région qui ont pu les accueillir, tels que MagnyÉthique.

{{< load-photoswipe >}}
{{< gallery hover-effect="none" caption-effect="none" >}}
{{< figure link="https://cloud.magnyethique.org/s/8naSfTebyCCPNzT/preview" >}}
{{< figure link="https://cloud.magnyethique.org/s/tdHCDRmWBidDPJb/preview" >}}
{{< /gallery >}}

**07.2023 :** Article "[**Organiser des chantiers participatifs, c’est aussi une manière de sensibiliser à l’habitat participatif**](https://www.habitatparticipatif-france.fr/?OrganiserDesChantiersParticipatifsCest)", paru sur la plateforme d'Habitat Participatif France.

**06.2023 :** Article "[**Écolieu : Écologie et partage à MagnyÉthique**](https://www.le-pays.fr/cublize-69550/actualites/ecologie-et-partage-a-magnyethique_14325919/)", paru dans Le Pays de Tarare.

**05.2023 :** Article du [**blog** d'Iris et Steve](https://iristisserand.wixsite.com/csurenpal/post/en-route-pour-le-nord?postId=370c16be-fa38-4c79-813c-c6e955159f57&utm_campaign=03b744ef-8166-43cd-89aa-bb7811077cdd&utm_source=so&utm_medium=mail&utm_content=edd5862d-f68a-4e4f-b7f3-228d7596dada&cid=49b6444d-8fd1-46d6-aed5-0bbdf28475ef), nos premiers wwoofeur·euse·s de l'année 2023.

**10.2022** : Mémoire de recherche de Master 2 de Science politique à l'Université Lumière Lyon II, "**Monographie d’un lieu de vie alternatif - Explication et conséquence de l’engagement à MagnyÉthique**" par Ophélie VERVACKE ; Faculté d’Anthropologie, de Sociologie et de Science politique, Parcours Enquêtes et Analyse des Processus Politiques

**06.2022** : parution du volume "**[Vous avez dit espace commun](https://www.peterlang.com/document/1171462) ?**" édité sous la direction de Silvana Segapeli aux éditions Peter Lang, comprenant une étude de cas de l'écolieu MagnyÉthique corédigée par Charlotte Limonne et Katia Stachowicz et intitulée "L’Habitat participatif : une solution pour dynamiser la ruralité ?"

**01.2022** : article "MagnyÉthique : créer un écosystème résilient" paru dans le [magazine **n°60 de Kaizen**](https://boutique.kaizen-magazine.com/bimestriels/688-kaizen-60-ecologie-des-raisons-d-esperer.html) et rédigé par Gabrielle Paoli

**03.01.2022** : article de blog du **[Road trip des tiers lieux](https://roadtriptierslieux.com/magnyethique-ecolieu-des-papillons/)** en attendant leur film !

**03.12.2021** : [Article](https://cooperative-oasis.org/oasis/magnyethique/) du site de la **Coopérative Oasis** ([archive](https://cloud.magnyethique.org/s/wgddBTb3z8E42FK?dir=undefined&openfile=2306))

**23.09.2021** : reportage du magazine **Vivre Lyon n°22** (23 septembre 2021)

{{< load-photoswipe >}}

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="https://cloud.magnyethique.org/apps/files_sharing/publicpreview/q4HXtcZLTA45BTL?file=/ils%20parlent%20de%20nous/VL22-GREEN_1.jpg&fileId=2134&x=1920&y=1200&a=true" caption="Vivre Lyon 1/3" >}}

{{< figure link="https://cloud.magnyethique.org/apps/files_sharing/publicpreview/q4HXtcZLTA45BTL?file=/ils%20parlent%20de%20nous/VL22-GREEN_2.jpg&fileId=2135&x=1920&y=1200&a=true" caption="Vivre Lyon 2/3" >}}

{{< figure link="https://cloud.magnyethique.org/apps/files_sharing/publicpreview/q4HXtcZLTA45BTL?file=/ils%20parlent%20de%20nous/VL22-GREEN_3.jpg&fileId=2133&x=1920&y=1200&a=true" caption="Vivre Lyon 3/3" >}}

{{< /gallery >}}

**20.02.2021** : [Article du site **La Fête des Possibles**](https://fete-des-possibles.org/temoignages-createurices/magnyethique/) revenant sur l'organisation 2020.

**24.07.2020** : [Article du blog de l'**Altertour**](https://www.altercampagne.net/?p=19275), de passage chez nous.

**16.04.2020** : [Newsletter d'**Eurotopia**](https://eurotopia.de/newsletters/eurotopia-Newsletter_April2020.html) sur le confinement en habitat participatif (en allemand et en anglais seulement)

**07.06.2019** : [article du **Progrès**](https://www.leprogres.fr/rhone-69-edition-tarare/2019/06/07/un-ecolieu-participatif-s-installe-au-chateau-de-magny)
