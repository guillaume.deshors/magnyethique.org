---
title: 'Journée autour du piano '
subtitle: proposée par Léna Kollmeier et Bruno Labouret le dimanche 28 janvier 2024
date: '2023-11-28'
tags:
  - formation
  - musique
  - rencontre
  - culture
---

Après avoir accueilli deux stages avec Marc Vella, l'envie est de réunir des "anciens stagiaires" et d'autres personnes intéressées par l'improvisation au piano, pour jouer à improviser et expérimenter ensemble.

![](https://cloud.magnyethique.org/s/dXMbRwwpYEne2Jm/preview)

### Les intervenant·e·s :

Bruno Labouret est artiste du spectacle vivant et musicien multi-instrumentiste. Après un parcours classique en clarinette, il a dû "batailler" pour sortir des partitions et oser improviser. Il en a tiré des outils simples à transmettre et efficaces pour se libérer des peurs et blocages parfois liés à l'improvisation.
Habitant à MagnyÉthique, c'est un premier stage avec Marc Vella qui lui a donné envie de l'y inviter, et de l'y faire revenir encore et encore depuis trois ans maintenant.
Plus d'info : www.blabouret.eu

Léna Kollmeier : Pianiste classique et fondatrice de plusieurs ensembles, elle a obtenu de différents prix depuis l'âge de dix-sept ans et mène une carrière de concertiste. Habitant les environs, elle vient sur un stage de Marc Vella et y rencontre Bruno en tant que presque voisine. Son approche de l'improvisation l'inspire énormément : elle fait un second stage qui lui donne la confiance pour à son tour accompagner des personnes à improviser au clavier.
Plus d'info : https://www.lenakollmeier.com/

### Logistique 

La journée est limitée à une douzaine de personnes. Elle se déroulera de 10h à 17h (accueil à partir de 9h30) avec une pause déjeuner en mode "auberge espagnole" : apportez ce que vous aimez savourer et partager !
Le stage aura lieu à MagnyÉthique, 213 chemin du château de Magny - 69550 Cublize.

### Tarif

Nous demandons pour cette journée une participation libre et consciente : une partie servira à indemniser les intervenant·e·s pour leur temps de préparation, de communication, trajets, et l journée proprement dite ; une autre part sera versée à MagnyÉthique pour les frais liés à l'accueil de cette journée (chauffage, fluides...), l'entretien du piano, et le soutien au projet MagnyÉthique.

### Inscriptions

Pour vous inscrire, renseignez [ce formulaire](https://framaforms.org/inscription-journee-autour-du-piano-28-janvier-2024-a-magnyethique-1700576618) en remplissant un formulaire par personne.

N'hésitez pas à nous contacter si vous avez des questions à l'adresse suivante : [2024_piano@magnyethique.org](mailto:2024_piano@magnyethique.org)
