---
title: Newsletter 2
date: '2019-01-19'
tags:
  - newsletter
  - avancement
---

Bonjour ami.es MagnyÉthiques,

**2019 est là, année de la concrétisation de bien des projets !** Nous vous la souhaitons riche en rencontres et relations humaines qui font les petits et les grands bonheurs ! Parce que partagé, le bonheur est toujours plus beau et plus fort, n’est-ce pas !?

C’est sous le signe du **partage** que nous avons justement effectué notre passage à la nouvelle année, grâce à une session de travail et de cohésion riche en perspectives, en émotions et en convivialité et une folle après-midi **portes-ouvertes**, la première sur notre futur écolieu, à Magny.

### Journée portes-ouvertes du samedi 29 décembre 2018

Commençons par cette fameuse rencontre : vous étiez 42 adultes et une quinzaine d’enfants à nous avoir fait l’honneur de votre visite ! Nous n’en espérions pas tant et cette affluence nous donne beaucoup d’espoir en l’avenir ! Certain·es sont presque des voisin·es, d’autres en revanche sont venu·es parfois de bien loin découvrir notre projet, le lieu et le groupe. Merci à toutes et tous pour votre présence, pour les échanges ouverts, chaleureux et instructifs, pour avoir pris part au premier goûter partagé autour de la belle cheminée de la salle parapluie qui a complété la chaleur humaine heureusement au rendez-vous. Merci également pour les retours que nous avons eu le plaisir de recevoir suite à cette demi-journée ensemble.


{{< load-photoswipe >}}

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/newsletters/2/porte_1200.jpg" caption="Porche d'entrée" >}}

{{< figure link="/images/newsletters/2/cheminée_1200.jpg" caption="Un bon feu dans la salle parapluie" >}}

{{< /gallery >}}

Pour les malchanceux·ses que la maladie ou le calendrier chargé de fin d’année ont empêché·es de nous rendre visite, pas d’inquiétude, d’autres occasions se présenteront. Nous communiquerons le moment venu sur le site et dans la prochaine newsletter un calendrier prévisionnel avec la ou les prochaines dates de rencontre à retenir. 

### Vie de groupe

Notre session de fin d’année s’est déroulée sur l’écolieu **Roulottes & Cie, à Bessenay**. Cet endroit atypique regroupe un habitat participatif et le siège de plusieurs associations, dont celle organisant le fameux salon Primevère que les habitants de la région sensibles aux alternatives écologiques connaissent bien. Rencontrer quelques acteurs de ce lieu était un plaisir ! Nous les remercions pour leur accueil et leur disponibilité !

Lors de cette session de travail, nous avons abordé principalement les thèmes de la **gouvernance partagée** et des **aménagements**, et l’**ouverture** était à l’honneur, fil rouge de notre séjour ! Elle a d’abord été vécue au sein du groupe qui a pu approfondir sa connaissance des autres dans le cadre du travail commun. Un atelier a été mené pour lever les mauvaises interprétations basées sur une méconnaissance des fonctionnements des autres. Elle s’est aussi traduite par l’accueil de nouveaux membres avec qui nous avons eu plaisir à faire quelques jeux de connaissance et d’introduction aux dynamiques de groupe. 

La journée du dimanche 30 décembre a été un moment particulier grâce à l’accompagnement pertinent de Joris Darphin, intervenant au sein de l’association Scicabulle. Le travail pour lequel nous lui avions demandé de nous encadrer ce jour-là a été riche en pistes de réflexion, en échanges constructifs et en connaissance de soi et des autres - à la fois travail et cohésion, donc. La commission gouvernance en ressort avec le défi de finaliser un **système restauratif** propre au groupe.
 

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/newsletters/2/masques_1200.jpg" caption="Masques" >}}

{{< figure link="/images/newsletters/2/yourte_1200.jpg" caption="Le groupe sous la yourte" >}}

{{< /gallery >}}

Nous nous sommes projetés le lundi sur le lieu en plaçant nos espaces communs et privatifs sur le plan des bâtiments, en rêvant et en concrétisant certains rêves en leur prêtant vie sur le papier.


{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/newsletters/2/jeu_1200.jpg" caption="Masques" >}}

{{< /gallery >}}

Enfin, nous avons terminé la session en beauté avec une soirée « **M comme MagnyÉthique** » où petits et grands ont fait preuve de créativité pour venir déguisés en : 
Mouton, Minet, Montagnards, Momie multicolore, Mystère Masqué, Médecin et sa Malade imaginaire, Mariachi, Mexicaine et Mongole, Marmitons, Mécanicien, Menuisier… 
Un brin de Mystère, une pincée de Malice, une dose de Magie, une bonne cuillère de Musique, une prise de Moments de découvertes européennes… et le résultat était tout simplement **Magnyfique** ! 
Nous avons bien ri, chanté, dansé, profité du temps passé ensemble dans la chaleureuse yourte de Roulottes et Cie où nous avons même lancé la création d’une œuvre d’art commune et évolutive initiée par Charlotte Limonne ! 2019 s’annonce sous les meilleurs auspices et nous avons hâte de la vivre ensemble !

### Perspectives

Le mois de janvier à présent bien entamé et va nous permettre de placer nos futures échéances sur le **calendrier**. Pendant l’année, nous vous inviterons à de nouvelles journées portes-ouvertes, à des chantiers participatifs, mais aussi, dans un second temps peut-être à nos premiers événements culturels et aux premières formations.

D’ici la fin du mois, nous aurons un nouveau **site internet**, et un nouveau **logo** pour l’orner grâce à vous! Nous avons déjà quelques retours de très belles créations, peut-être d’autres à venir d’ici le 20 janvier, date limite de participation. Le choix sera probablement difficile mais nous vous remercions d’avoir commencer à répondre à notre appel et nous réjouissons d’accueillir les créateurs à Magny dans quelques temps!

Alors nous vous souhaitons encore une fois nos **meilleurs voeux 2019** et vous disons à très bientôt! 

MagnyÉthique