---
title: Concert folk-rock-poétique
subtitle: Atapa & Stuart O'Connor
date: '2022-07-27'
tags:
  - culture
  - concert
---
L'écolieu MagnyÉthique a le plaisir d'accueillir le 19 août 2022 Anaïs Di Pasquali et Stuart O'Connor pour un duo vocal et multi-instrumental entrainant, envoutant et parfois dépaysant.

De quoi terminer en beauté les chantiers participatifs estivaux et ainsi remercier nos volontaires, mais aussi faire résonner une nouvelle fois de beaux sons dans le château.

![](https://cloud.magnyethique.org/s/2ozzn3r25a5xEL2/preview) 


