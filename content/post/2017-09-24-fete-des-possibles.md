---
title: Fête des possibles
tags:
  - échanges
  - promotion
date: '2017-09-24'
---
![](/images/uploads/image1.png)

Une de nos journées portes-ouvertes s'inscrivait cette année dans le cadre de la Fête des Possibles qui nous a offert une visibilité plus grande par la venue, entre autre, de quelques médias.

Plus de 2000 rendez-vous étaient attendus aux quatre coins de la France et de la Belgique pour rendre visible les milliers d'initiatives locales qui embellissent la société et construisent un avenir durable et solidaire. Et nous faisons partie de cette incroyable dynamique!

Ce mouvement citoyen global, dont nous sommes parties prenantes, et qui construit jour après jour une société juste et durable grandit. Les initiatives citoyennes se multiplient. Il nous suffit de regarder autour de nous pour nous en apercevoir. En organisant des rendez-vous près de chez eux, les acteurs locaux (association, coopérative, citoyens concernés…) donneront un aperçu de cette révolution citoyenne qui s'amplifie de jour en jour.

Rendez-vous en septembre prochain pour renouveler cette belle expérience, pour découvrir et agir dans toute la France et en Belgique!

http://fete-des-possibles.org

facebook/fêtedespossibles - @fetedespossible - #cestpossible

{{< youtube LvpPsFOwHlo >}}
