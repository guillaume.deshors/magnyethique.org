---
title: Stage d'électricité avec un pro
subtitle: Samedi 14 et dimanche 15 décembre 2024
date: '2024-11-06'
tags:
  - stage
  - accompagnement pro
---
![](https://cloud.magnyethique.org/s/SGKw6zAFc2d5JMa/preview)

  

Inscriptions : 2024elec@magnyethique.org
