---
title: Voyage au cœur du piano
date: '2024-04-29'
tags:
  - culture
  - échanges
subtitle: Dimanche 05 mai à 15h
---

Benjamin Charrière, accordeur professionnel, nous invite à découvrir les entrailles et le fonctionnement d'un piano. Ce sera l'occasion de découvrir l'histoire et le fonctionnement de cet instrument présenté par un fin connaisseur, lors d'un temps convivial et participatif.

![](https://cloud.magnyethique.org/s/dXMbRwwpYEne2Jm/preview)

Cela se passera à MagnyÉthique à 15h. Cette rencontre est accessible aux enfants à partir de 7 ans.

Nous vous demanderons une participation financière du montant de votre choix pour ce temps. 
Merci de signaler votre intention de venir en renseignant [ce formulaire](https://framaforms.org/voyage-au-coeur-du-piano-1714412234).


Pour tout renseignement, contactez-nous à [2024_piano@magnyethique.org](mailto:2024_piano@magnyethique.org)

