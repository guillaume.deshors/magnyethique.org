---
title: Concilier permaculture et biodiversité
subtitle: Le 13 mars 2019 sur Lyon
date: '2019-02-26'
tags:
  - conférence
  - permaculture
  - écologie
  - biodiversité
---
Le mercredi 13 mars au campus René Cassin de Lyon a eu lieu une conférence sur le thème de la permaculture et de la biodiversité. L'objectif annoncé est de "_permettre au grand public de mieux comprendre les enjeux liés à la sauvegarde de la biodiversité tout en décrivant quelques aspects pratiques de cette méthode agricole simple et productive_".

![](/images/uploads/1552038175459_Permaculture 13.03.19.jpg)

Bastien a pu découvrir la portée systémique de la permaculture : aussi bien l'adaptation au terroir présent sur place, l'interaction entre les plantes, l'aménagements des espaces (design), la prise en compte des rythmes différenciés (récoltes régulières ou annuelles), l'importance d'utiliser plusieurs strates pour bénéficier au mieux de l'énergie du soleil.

Le fermier qui l'a présenté a aussi partagé plusieurs points qui m'ont semblé importants et inattendues : lâcher prise et accepter de ne pas faire parfaitement, tolérer un peu de perte (donner à la nature) plutôt que de lutter contre toutes les maladies/insectes, ... planter des fleurs pour attirer les insectes mais les canaliser dans ces zones fleuries, laisser les limaces propager les champignons qui contribuent à la régulation de l'eau et diffusent des informations et des minéraux entre les plantes, ...

En résumé : expérimenter, adapter, lâcher prise

[http://fermedelaplagne.blogspot.com/p/de-nombreuses-annees-dans-une-vie.html](https://fermedelaplagne.blogspot.com/p/de-nombreuses-annees-dans-une-vie.html?fbclid=IwAR0Ur59uSIEhXmq68gu2Z03UU-ShBk4Qq4iSV1ASvSyWgyUPaDBFmCrF2W4)

[https://terredeliens.org/](https://terredeliens.org/?fbclid=IwAR3xZx0MR8bi4al7vOavYcWv3JodHyGlBTkJMD_M0ZbBah4YeZaoNsSXCmI)

{{% youtube LQaKbHd1ejU %}}
