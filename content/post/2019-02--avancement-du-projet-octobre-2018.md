---
title: Avancement du projet - octobre 2018
date: '2018-10-28'
tags:
  - avancement
---
Les réunions s'enchaînent et pour cause!

Nous avons fait une offre d'achat pour le Château de Magny, situé à Cublize dans le Beaujolais vert: offre qui vient d'être acceptée par le vendeur! Nous devrions donc nous installer à deux pas du Lac des Sapins courant 2019!

Alors l'urgence est de finaliser au plus vite et au mieux les statuts. En parallèle, nous nous projetons et faisons venir des artisans pour lancer certains travaux dès que possible en vue d'une installation confortable en 2019, et même si c'est moins urgent, nous commençons à réfléchir à un nouveau logo, nouveau site, nouvelle page FB etc. et surtout… à rêver!
