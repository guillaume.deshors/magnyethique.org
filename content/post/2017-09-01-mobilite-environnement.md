---
title: 'Formation ''mobilité, environnement & développement durable'''
sutitle: Semaine du 28 août au 1er septembre 2017
tags:
  - formation
  - écologie
date: '2017-09-01'
---
![](/images/uploads/image6.jpg)

Pascal a participé à la formation de l'Académie Frankenwarte, à Würzburg en Allemagne sur le thème "[mobilité, environnement & développement durable](https://www.frankenwarte.de/unser-bildungsangebot/rueckblicke/detail/mobilitaet-umwelt-und-nachhaltigkeit.html "https://www.frankenwarte.de/unser-bildungsangebot/rueckblicke/detail/mobilitaet-umwelt-und-nachhaltigkeit.html")". Cette formation propose de rendre visibles des réalités et alternatives sociopolitiques par le vélo.

Le groupe a visité chaque jour une ou plusieurs installations écologiques et sociales en s'y rendant à la force des mollets. Planification durable d'un aménagement urbain, gestion forestière, accès à l'eau, aménagement de jardins écologiques, développement durable en entreprise étaient au programme.
