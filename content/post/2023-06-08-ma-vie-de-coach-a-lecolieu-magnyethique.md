---
title: Ma vie (de coach) à l’écolieu MagnyÉthique
subtitle: Une réflexion de Caroline Dufau-Seguin
date: '2023-06-06'
tags:
  - gouvernance
  - vie de groupe
---
Ma vie (de coach) à l’écolieu [MagnyÉthique](https://www.facebook.com/MagnyEthique?__cft__[0]=AZVxjmAob-c7Ao8I0z4SEwfme1MMNo2Ei8IbrzmDjR_6Djdca3f8LmLe5A3y2TVEDHuv1hsVBMqtkJqizYqNpMIBtfDWoSEQzQILWthk-5cc0jrBiuOSSGNDPTLO1ogHnwbPHVh7AJzu0ifpBWpOrzlRUF32VtKuBKSfF1ccDINNSa-4dEoPumt-xJX14yjllwASGVuHdJ_ALOaUuwQDf1q4&__tn__=-]K-y-R)
======================================================================================================================================================================================================================================================================================================================================

  

![💥](https://static.xx.fbcdn.net/images/emoji.php/v9/t40/1/16/1f4a5.png)Conflits - tensions : comment nous prenons soin des relations à l’écolieu Magnyétique ![💞](https://static.xx.fbcdn.net/images/emoji.php/v9/t98/1/16/1f49e.png)
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  

Aujourd'hui, je voudrais vous partager une réflexion sur la façon dont nous gérons les tensions et les conflits à l'écolieu Magnyéthique.

Nous avons fait le choix de considérer ces situations comme des opportunités d'apprentissage et de développement personnel, plutôt que de les voir comme des obstacles.

  

L'écolieu Magnyéthique est un endroit pas ordinaire ![🙂](https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/1f642.png) où nous vivons, travaillons et partageons ensemble. Cependant, comme dans toute vie de groupe, il arrive parfois que des tensions émergent entre les personnes. Plutôt que d'ignorer ou de réprimer ces tensions, nous avons décidé de les aborder de front, avec ouverture et bienveillance.

  

Nous croyons fermement que les conflits sont le reflet de nos différences et de nos besoins individuels. Plutôt que de les éviter, nous les considérons comme une occasion de grandir et de mieux nous comprendre les uns les autres. C'est un chemin parfois ardu, mais ô combien nourrissant.

  

Dans notre approche, nous mettons en pratique plusieurs principes clés :
------------------------------------------------------------------------

  

![1️⃣](https://static.xx.fbcdn.net/images/emoji.php/v9/t7a/1/16/31_20e3.png) L'écoute active : Nous nous efforçons d'écouter attentivement et respectueusement les différents points de vue, en accordant à chacun la possibilité de s'exprimer librement.

![2️⃣](https://static.xx.fbcdn.net/images/emoji.php/v9/t99/1/16/32_20e3.png) La communication non violente : Nous utilisons les principes de la communication non violente pour exprimer nos sentiments et nos besoins de manière constructive, sans jugement ni critique.

![4️⃣](https://static.xx.fbcdn.net/images/emoji.php/v9/td7/1/16/34_20e3.png) La recherche de solutions communes : Plutôt que de chercher à imposer notre point de vue, nous encourageons la recherche de solutions qui répondent aux besoins de tous, dans le respect de nos valeurs collectives.

![5️⃣](https://static.xx.fbcdn.net/images/emoji.php/v9/tf6/1/16/35_20e3.png) L'apprentissage continu : Nous considérons chaque conflit comme une opportunité d'apprendre, de grandir et de renforcer notre compréhension mutuelle. Nous en tirons des leçons précieuses qui nous guident vers une meilleure harmonie et une collaboration plus fructueuse.

  

![🌱](https://static.xx.fbcdn.net/images/emoji.php/v9/t69/1/16/1f331.png) Nous aspirons à un environnement où chacun se sent entendu, respecté et soutenu. En embrassant les défis que les tensions et les conflits peuvent nous offrir, nous créons une communauté plus solide, plus unie et plus résiliente.

  

C’est sur cette base que nous avons créé notre système restauratif qui met en mots tous ces principes avec des processus sur lesquels nous nous entrainons hors périodes de conflits.

  

![❓](https://static.xx.fbcdn.net/images/emoji.php/v9/t4c/1/16/2753.png)Je vous invite donc à réfléchir à votre propre approche face aux tensions et conflits. Comment pourriez-vous les considérer comme des opportunités de croissance et de compréhension mutuelle, que ce soit dans votre vie personnelle ou professionnelle ?

  

![💪](https://static.xx.fbcdn.net/images/emoji.php/v9/t6c/1/16/1f4aa.png)Ensemble, nous pouvons transformer nos différences en une force qui nourrit.

  

![⭐](https://static.xx.fbcdn.net/images/emoji.php/v9/tb4/1/16/2b50.png)Pour aller un peu plus loin sur le sujet,

  

RDV pour le webinaire “Qui a peur des conflits ?” avec [Céline Meunier](https://www.facebook.com/profile.php?id=100013434135766&__cft__[0]=AZVxjmAob-c7Ao8I0z4SEwfme1MMNo2Ei8IbrzmDjR_6Djdca3f8LmLe5A3y2TVEDHuv1hsVBMqtkJqizYqNpMIBtfDWoSEQzQILWthk-5cc0jrBiuOSSGNDPTLO1ogHnwbPHVh7AJzu0ifpBWpOrzlRUF32VtKuBKSfF1ccDINNSa-4dEoPumt-xJX14yjllwASGVuHdJ_ALOaUuwQDf1q4&__tn__=-]K-y-R) et moi :

![📆](https://static.xx.fbcdn.net/images/emoji.php/v9/tff/1/16/1f4c6.png)mardi 6 juin 2023

![⌚](https://static.xx.fbcdn.net/images/emoji.php/v9/tfa/1/16/231a.png)12h30 à 13h45

  

Lien pour s’inscrire : [https://www.billetweb.fr/webinaire-qui-a-peur-des-conflits](https://www.billetweb.fr/webinaire-qui-a-peur-des-conflits?fbclid=IwAR31pV60EGsdhX7HTPMz5QMBZmBvwBkl2eDPGmDdKL5JxzAGAkzqOAH4X-0)

  

![](https://cloud.magnyethique.org/s/9XfE2MJDY5pJ7dy/preview)  

Illustration de Mathieu BERTRAND [http://matisme.com/](https://l.facebook.com/l.php?u=http%3A%2F%2Fmatisme.com%2F%3Ffbclid%3DIwAR17hjrzqEk0zMTceVTuBRBVFkyYR9-Z9mkX0MrRwRjJye8rbSUrITfgb8k&h=AT0FjqaOfc8SavF4yxhQpDwKdWYcDkXCDxxa1PEI44VDwNw7voEyGRmnh8rLLx406PjMazXe83w2hKZaaxSCdsWo0COIHzLzme7GxBfXRgcSCE-fRbJl-205LC_SKQbB1u4H4zYiKQ&__tn__=-UK-y-R&c[0]=AT1B385K3cXv5HuvRwrMEKz7fhLijmKRSpu-olJuiO2xnLHlU6inlZ-XLOQAxsqu6pqgzO9k5vlJhO5yHfgB0wonmy4zK8QlUVj8jrTgifuuWpcfgLzkcjCqQfXyRXN2n6gWHkVTSZoagt8qzYUFA_A9mRDhcsnI14DijvYBYdEWGnHeALhUIgQY0Ubcy8xSZX6hQyR4Oj-JdGHIJU9ttXV6L4Fh_0Mzry-g)

[MATi](https://www.facebook.com/Mathieu.Bertrand.BD.illustration?__cft__[0]=AZVxjmAob-c7Ao8I0z4SEwfme1MMNo2Ei8IbrzmDjR_6Djdca3f8LmLe5A3y2TVEDHuv1hsVBMqtkJqizYqNpMIBtfDWoSEQzQILWthk-5cc0jrBiuOSSGNDPTLO1ogHnwbPHVh7AJzu0ifpBWpOrzlRUF32VtKuBKSfF1ccDINNSa-4dEoPumt-xJX14yjllwASGVuHdJ_ALOaUuwQDf1q4&__tn__=-]K-y-R)
