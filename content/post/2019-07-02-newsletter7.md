---
title: Newsletter 7
date: '2019-07-02'
tags:
  - newsletter
  - avancement
---

Bonjour Ami.es MagnyÉthique 

Une newsletter (un peu) tardive mais avec tellement d’annonces et de bonnes nouvelles que ça valait bien la peine d’attendre un peu. Nous vous laissons juger par vous-mêmes !

### On débarque !
Ça y est, c’est officiel : depuis le 6 juin, nous sommes les heureux propriétaires du château ! 

![](/images/newsletters/7/porte.jpg)

Les travaux de nettoyage extérieur et intérieur ont commencé aussitôt pour pouvoir accueillir tout le monde : les premiers plants de tomates et autres légumes ont trouvé une petite place sur  la prairie sud ; les premières poulettes nous offrent déjà leurs oeufs dans la cour intérieure en attendant la construction imminente de leur poulailler.

![](/images/newsletters/7/poule.jpg)

Pour nous humains, il s’agit encore de gérer les urgences eau et électricité un peu laborieuses à régler dans ce grand château si longtemps délaissé. 

Nous allons tous lui offrir une deuxième jeunesse et lui rendre joie et animation comme à l’occasion du week-end des 22 et 23 juin où nous nous sommes retrouvés tous ensemble pour fêter deux anniversaires et tenir notre toute première plénière en présence entre cour intérieure, salles “parapluie” et “panoramique”.

![](/images/newsletters/7/cour.jpg)

Parmi les nouveaux arrivants, la joyeuse tribu de la [transition ride](https://transitionride.home.blog/) a bravé les intempéries, et est arrivée à bon port le 13 juin après 1350 km en selle, des séjours dans 4 wwoofing et 3 communautés. Nos 5 franco-allemands ont commencé à poser leurs meubles et cartons dans un appartement temporaire en attendant la réalisation des travaux d’aménagements.



### “Si j’avais un marteau…” 
Parlons travaux justement ! Le sujet est vaste tant il y a à faire ! Cet été nous allons principalement nous atteler à résoudre les problèmes d’humidité des façades et aménager les extérieurs. Si l’aventure vous tente, nous avons planifié 5 sessions de chantiers participatifs tout au long de l’été. Ce peut être une belle occasion de joindre l’utile à l’agréable pour ceux qui souhaitent découvrir la vie en habitat participatif, rencontrer le groupe et bien sûr offrir de leur temps pour donner un gros coup de main ! Plus d’infos, RDV sur le site de chantiers participatifs Twiza lien twiza ou directement sur notre site lien site (À noter : la dernière session se terminera par des représentations théatrales, mais chut… nous développerons cette surprise dans notre prochain courrier !)

![](/images/newsletters/7/chantier.jpg)

### Bienvenue chez nous !
Après une matinée d’échanges très sympathiques autour de notre projet à la MJC d’Amplepuis début juin, nous continuons la reprise des intégrations avec notre première session portes ouvertes en tant que propriétaires samedi 6 juillet. Rendez-vous à partir de 10h30 pour les intéressés. (Pensez à [vous inscrire](/page/contact) avant afin de nous aider dans l’organisation). Les simples curieux désireux de faire notre connaissance ou de voir les lieux pourront se joindre à nous pour un pique-nique participatif à partir de midi. 

![](/images/newsletters/6/porte-ouverte.jpg)

Et pour ceux qui ne sont pas disponibles à cette date, rassurez-vous : des portes ouvertes seront organisées très régulièrement à partir de septembre.


### Permaculture, dites-vous !?
Enfin si l’âme d’un permaculteur sommeille en vous et que vous voulez en apprendre davantage sur le sujet et/ou mettre la main à la pâte, réservez votre semaine à cheval sur septembre et octobre. En effet nous sommes heureux de vous annoncer que la première formation organisée par notre nouvel écolieu sera une formation de permaculture ! 

![](/images/newsletters/7/perma.jpg)

Elle peut avoir lieu grâce à l’intervention du Permalab (26) et en partenariat avec la Ferme du Suchel située à Valsonne, à 20 minutes de chez nous. Le Permalab est constitué de pros de la permaculture qui mènent des formations depuis plusieurs années à présent. Ils seront responsables de tous les contenus de formation délivrés pendant cette semaine. Le Suchel gère les 27 hectares qui entourent leur jolie ferme en rénovation grâce aux principes permacoles et d’agroforesterie. C’est en grande partie chez eux qu’auront lieu les observations et mises en pratique du stage. 

Il est possible de s’inscrire uniquement pour le week end des 28 et 29 septembre pour la partie théorique qui aura lieu chez nous, où pour la semaine complète (du samedi 28 septembre au vendredi 4 octobre) durant laquelle la partie pratique sera divisée entre les jardins de Magny où la permaculture débute et ceux de [la ferme du Suchel](https://lesuchel.tumblr.com/) déjà installés en permaculture depuis plusieurs années.


Bon début d’été à tou.te.s et à bientôt !

MagnyÉthique


{{< load-photoswipe >}}

{{< gallery hover-effect="none" caption-effect="none" >}}

{{< figure link="/images/newsletters/7/groupe.jpg" caption="Le groupe" >}}
{{< figure link="/images/newsletters/7/msv.jpg" caption="Maraîchage sur sol vivant" >}}
{{< figure link="/images/newsletters/7/poules.jpg" caption="Les poules" >}}
{{< figure link="/images/newsletters/7/suchel.jpg" caption="Le Suchel" >}}

{{< /gallery >}}