---
title: Premier gros chantier participatif
date: '2019-07-15'
tags:
  - chantier participatif
bigimg:
  - desc: null
    src: /images/20190714_ChantierParticipatif/p_IMGP7722.JPG 
---

Un immense MERCI aux 8 volontaires Lyonnais.es d'Éclats de voix, 
2 Cublizard.es, aux voisin.es, à la famille et aux ami.es venu.es 
nous aider ce week-end pour avancer sur les dossiers:

- cuisines à boue pour débuter la fabrication de l'aire de jeux des enfants
- toilettes sèches
- début de la coupe des thuyas de la cour intérieure
- enclos et poulailler pour les 15 collocs à plumes
- débroussaillage des descentes au jardin 
- et last but not least vu l'état des alentours: ramassage des ordures qui jonchaient encore les terrains


Résultats: 
des cuisines prises d'assaut avant peinture, 
des enfants sales mais heureux, 
des toilettes qui n'attendent que de la sciure pour accueillir les premières fesses, 
une cour intérieure tellement grande et claire qu'on peine à la reconnaître, 
des poulettes ravies et en sécurité, 
des mollets de jardiniers aussi et Dame Nature qui dit merci d'être moins lourde de dizaines de sacs poubelle de détritus en tout genre.

![Résultat](/images/20190714_ChantierParticipatif/p_IMGP7722.JPG)
![Résultat](/images/20190714_ChantierParticipatif/p_IMGP7723.JPG)

Nous sommes ressorti.es fatigué.es mais heureu.ses d'avoir enfin vu une vraie différence en deux jours et d'avoir en outre rencontré de très belles personnes !

Vous serez toujours les bienvenu.es chez nous !!! Et sinon, vos dédicaces sur le poulailler resteront comme preuve de votre précieux passage !

![La forêt de Thuyas a disparu.](/images/20190714_ChantierParticipatif/p_IMGP7714.JPG)

![La cuisine à boue.](/images/20190714_ChantierParticipatif/p_IMGP7715.JPG)

![La cuisine à boue.](/images/20190714_ChantierParticipatif/p_IMGP7716.JPG)

![Allez, venez, on rentre.](/images/20190714_ChantierParticipatif/p_IMGP7720.JPG)

![Où ça? À notre nouvelle maison.](/images/20190714_ChantierParticipatif/p_IMGP7717.JPG)

![Qu'est-ce qu'on y est bien.](/images/20190714_ChantierParticipatif/p_IMGP7721.JPG)

![Il ne manque plus que le journal.](/images/20190714_ChantierParticipatif/p_IMGP7725.JPG)

Merci et à bientôt !
