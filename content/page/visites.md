---
title: Visites et présentations
sections:
  - >-
    Voici les prochaines dates prévues et les liens vers de plus amples
    informations
  - Rencontres passées
tags: []
---
{{< calendar urls="https://cloud.magnyethique.org/remote.php/dav/public-calendars/FbymRCJoMt8rPg7p/?export" colors="#3CB371" tiny="true" >}}

Des visites de découvertes pour curieux et intéressés sont organisées sur l'écolieu chaque année en septembre pour venir nous rencontrer et discuter. Elles se font en général sur inscription pour nous permettre une organisation optimale.

<div class="row">
<div class="col-xs-0 col-md-3"></div>
<div class="col-xs-12 col-md-6"><img src="/images/uploads/1549622709185_IMG-20181230-WA0047.jpg" width="100%"/></div>
<div class="col-xs-0 col-md-3"></div>
</div>

{{< liste_evenements titre_avenir="Voici les prochaines dates prévues et les liens vers de plus amples informations" titre_passes="Rencontres passées" >}}
{{< evenement
    date="2025-04-12T14:30:00.000Z"
    dateFin="2025-04-12T17:00:00.000Z"
    titre="[Rencontre Twiza de l'habitat écologique n°39](https://airtable.com/appsyhKsI2T4UwUmw/shrki8WBiB698moku/tblQJwZqJ8FbwnLX4), suivie d'un concert à 17h00"
>}}
{{< evenement 
    date="2024-09-15T14:30:00.000Z"
    dateFin="2024-09-15T17:00:00.000Z"
    titre="[Journée Visite et Rencontres](/post/2024-09-05-visite-septembre-2025), suivi d'un [concert à 17h00](/post/2024-09-05-concert-satie-de-travers)"
>}}
{{< evenement 
    date="2024-05-05T14:30:00.000Z"
    dateFin="2024-05-05T16:00:00.000Z"
    titre="Écolieu en habitat participatif"
    description="Présentation de l'écolieu et son fonctionnement au [__Forum Nature et Printemps__](https://www.facebook.com/groups/1267145733367864/permalink/7275511325864578/) organisé par OSCAR à Régny"
>}}
{{< evenement 
    date="2023-09-23T14:00:00.000Z"
    dateFin="2023-09-23T17:00:00.000Z"
    titre="Journée Visite et Rencontres"
>}}
{{< evenement 
    date="2023-03-12T14:00:00.000Z"
    afficheHeure=true
    titre="\"les Solydaires\" (St Clément sur Valsonne) : présentation du projet MagnyÉthique et échange sur l'habitat participatif dans le cadre du [Festival Écologie - Économie](http://festival-ecologie-economie.net)"
>}}
{{< evenement 
    date="2022-09-10T14:30:00.000Z"
    dateFin="2022-06-10T16:30:00.000Z"
    titre="dans le cadre de la [Fête des Possibles](https://fete-des-possibles.org/) et des [Journées européennes de l'Habitat participatif](https://www.habitatparticipatif-france.fr/?HPFJPO)"
>}}
{{< evenement 
    date="2022-07-02T00:00:00.000Z"
    titre="présentation de notre projet à [Univoyage](https://univoyage.co/), association d'université populaire en voyage, de passage chez nous"
>}}
{{< evenement 
    date="2022-06-26T00:00:00.000Z"
    titre="dans le cadre du festival [Du Foin sur les Planches](http://courantdartbeaujolaisvert.fr/du-foin-sur-les-planches-2022/), table ronde sur le thème « [Les nouvelles manières d'habiter le Beaujolais Vert](http://courantdartbeaujolaisvert.fr/du-foin-sur-les-planches-2022/#jp-carousel-2283) » à [l'Atelier](https://www.ateliertierslieu.org/) (Amplepuis)."
>}}
{{< evenement 
    date="2021-10-16T00:00:00.000Z"
    titre="une journée sur le « vivre ensemble » organisée par l'association « [CALME](https://asso-calme.fr) » (Comme À La Maison Ensemble) à Cublize"
>}}
{{< evenement 
    date="2021-09-26T00:00:00.000Z"
    titre="[visite et rencontre dans le cadre des Journées européennes de l'Habitat participatif et de la Fête des Possibles](/post/2021-08-27-presentation-du-projet-echanges-et-visite/)"
>}}
{{< evenement 
    date="2021-08-08T00:00:00.000Z"
    titre="présentation du projet à l'[université d'été décontractée d'Avenir Climatique](https://avenirclimatique.org/luniversite-decontractee-davenir-climatique-2/) dans le cadre de son accueil à l'écolieu"
>}}
{{< evenement 
    date="2021-07-09T11:00:00.000Z"
    dateFin="2021-07-09T12:30:00.000Z"
    titre="conférence-atelier lors des [Rencontres Nationales de l'Habitat Participatif](https://www.rnhp2021.fr/billetterie) \"Organiser un chantier participatif heureux\"."
>}}
{{< evenement 
    date="2021-04-15T00:00:00.000Z"
    titre="rencontre en visioconférence"
>}}
{{< evenement 
    date="2021-01-26T00:00:00.000Z"
    titre="rencontre en visioconférence"
>}}
{{< evenement 
    date="2020-10-15T00:00:00.000Z"
    titre="rencontre en visioconférence"
>}}
{{< evenement 
    date="2020-09-12T00:00:00.000Z"
    titre="[visite précédée d'une projection de documentaire et d'un débat dans le cadre des Journées européennes de l'Habitat participatif et de la Fête des Possibles](/post/2020-08-08-quotun-ecolieu-pourquoi-et-comment-quot/)"
>}}
{{< evenement 
    date="2020-07-24T14:00:00.000Z"
    afficheHeure=true
    titre="présentation du projet à l'[Altertour](http://www.altercampagne.net) et à son [Échappée Belle](http://www.altercampagne.net/?page_id=18511) lors de son passage à l'écolieu"
>}}
{{< evenement 
    date="2020-06-27T00:00:00.000Z"
    titre="[présentation du projet puis atelier autour de la gouvernance et des conflits](/post/2020-07-11-quotjournee-eco-relions-nousquot/) lors de la première journée [Éco-relions-nous](https://www.facebook.com/events/184500639585283/) à Lyon"
>}}
{{< evenement 
    date="2020-03-06T00:00:00.000Z"
    dateFin="2020-03-08T00:00:00.000Z"
    titre="MagnyÉthique au [Salon Primevère](http://salonprimevere.org/)&nbsp;: nous donnerons une mini-conférence sur le thème [\"la gouvernance… et le conflit\"](http://salonprimevere.org/salon_programme_rencontre-398_la-gouvernance-et-les-conflits) en habitat participatif le samedi 7 mars à 13h"
>}}
{{< evenement 
    date="2020-02-15T00:00:00.000Z"
    titre="[Visite et Rencontres](/post/2019-12-04-visite-et-rencontre/)"
>}}
{{< evenement 
    date="2019-11-30T00:00:00.000Z"
    titre="[Portes ouvertes](/post/2019-09-journee-portes-ouvertes/)"
>}}
{{< evenement 
    date="2019-10-26T00:00:00.000Z"
    titre="[Portes ouvertes](/post/2019-07-journee-portes-ouvertes/)"
>}}
{{< evenement 
    date="2019-09-21T00:00:00.000Z"
    titre="à Lamure-sur-Azergues dans le cadre du [Petit Bal des Possibles](/post/2019-07-petit-bal-des-possibles/)"
>}}
{{< evenement 
    date="2019-09-15T00:00:00.000Z"
    titre="[Journée Portes Ouvertes et Fête des Possibles](/post/2019-07-journee-portes-ouvertes-and-fete-des-possibles/)"
>}}
{{< evenement 
    date="2019-07-06T00:00:00.000Z"
    titre="[Journée Portes Ouvertes](/post/2019-06-journee-portes-ouvertes/)"
>}}
{{< evenement 
    date="2019-06-01T00:00:00.000Z"
    titre="[Café-débat à la MJC d'Amplepuis](/post/2019-06-cafe-debat/)"
>}}
{{< /liste_evenements >}}
