---
title: 'Sortie de résidence vocale, chansons en avant-première'
subtitle: samedi 15 avril 2023 à 20h30
date: '2023-04-07'
tags:
  - spectacle
  - culture
  - résidence
  - concert
---

Nous vous proposons ce samedi une soirée musicale autour de deux projets tous frais : la sortie de résidence "mêlons les mots" avec un quartet composé de Florence Délu, Laura Arnod, Lucie Puigblanque et Gwen Mathais, et un concert du chanteur Alexis Provot ré-arrangé en duo avec le clarinettiste Bruno Labouret.
Venez découvrir et vous laisser surprendre !

Entrée libre, chapeau pour les artistes.



## En Mêlant les Mots ##

![](https://cloud.magnyethique.org/s/jRE2ESReteDJBzB/preview)
Après une première résidence à MagnyÉthique, Le quartet "En Mêlant les Mots" propose un concert de sortie de résidence. Les textes écrits par chacune vont se mêler le temps d'une soirée : tu es le et la bienvenu.e pour venir découvrir un moment de chant a capella au coeur d'une floraison improvisée !

Avec Florence Délu, Laura Arnold, Lucie Puigblanque et Gwen Mathais

## Alexis Provot en duo ##

![](https://cloud.magnyethique.org/s/bCENWQodnC99T62/preview)
(Crédit Photo : Tarentinoïz)

Alexis Provot aime les jeux de mot, mais pas au point d'ouvir un salon de coiffure (d'autant qu'il n'y a pas de "s" à son nom de famille). Il préfère les mettre dans ses chansons en donnant naissance à des personnages un peu farfelus : un gaucher qui se lève du pied droit, un autre personnage qui est ravi de se lever avant la sonnerie du réveil, un rencard avec un râteau, ... Et comme il n'est pas acteur, ses personnages il les chante guitare en main, bodhràn au pied. Il sera ce soir accompagné pour la première fois en public par Bruno Labouret qui vient colorer ses chansons de clarinette (parfois basse, parfois non) voire de trombone coulissant plus ou moins ...
[https://alexisprovot.com/](https://alexisprovot.com/)
