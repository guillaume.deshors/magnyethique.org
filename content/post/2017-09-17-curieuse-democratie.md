---
title: Festival Curieuses Démocraties
sutitle: Week-end des 16 et 17 septembre 2017
tags:
  - conférence
  - gouvernance
date: '2017-09-17'
---
![](/images/uploads/image7.jpg)

Yves s'est rendu au [Festival CurieuseS Démocratie](http://curieusesdemocraties.org/ "http://curieusesdemocraties.org/")s à Saillans (Drôme).

L'évènement avait des accents politiques mais nous espérions en tirer quelques enseignements intéressants pour le fonctionnement du groupe en participant aux différents ateliers proposés sur les systèmes de gouvernances partagées.

Nous n'avons pas été déçus! Au contraire, nos trois amis sont revenus emballés, touchés par la bienveillance générale ambiante et riches de nouveaux outils qu'ils ont découverts activement et dont ils vont faire bénéficier tout le groupe à l'avenir: holacratie, gestion d'une réunion participative (de la collecte d'idées à la mise en œuvre de solutions sur un objectif prédéfini). Et bien d'autres sujets à creuser en parallèle…

Étaient présents parmi 250 participants de tous horizons et dans le désordre:

*   Le parti Pirate
*   Charlotte Marchandise, de [LaPrimaire.org](https://articles.laprimaire.org/ "https://articles.laprimaire.org/")
*   La marea Atlantica, "parti" créé pour gérer la commune de La Corogne (Galice-Espagne) autogérée de 250 000 habitants SVP
*   Isabelle Attard, anciene députée EELV
*   [Super Châtaigne](http://www.superchataigne2017.fr/projet/ "http://www.superchataigne2017.fr/projet/")
*   [La Belle Démocratie](http://labelledemocratie.fr/ "http://labelledemocratie.fr/")
*   [Ma voix](https://www.mavoix.info/ "https://www.mavoix.info/") \- L'observatoire national des Démocraties
*   [Pouvoir citoyen en marche](http://eg-pouvoir-citoyen.org/wakka.php?wiki=PagePrincipale "http://eg-pouvoir-citoyen.org/wakka.php?wiki=PagePrincipale").
