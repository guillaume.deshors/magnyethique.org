---
title: C'est bon de se retrouver sur place!
date: '2019-06-23'
tags:
  - avancement
  - habitat participatif
  - vie de groupe
---
Le 23 juin, nous étions tous présents: papillons, chrysalides et chenilles (à l'exception d'un seul membre), réunis pour notre toute première plénière en présence sur notre écolieu. 

  

Quel plaisir de nous retrouver et de prendre le temps sur place pour continuer à nous apprivoiser et apprivoiser le lieu en même temps! Le soleil radieux (sans canicule encore) était bien sûr aussi très plaisant et nous avons hâte de nous installer sur l'été, d'accueillir encore de nouveaux venus, les chantiers participatifs, et de passer à nouveau du temps ensemble autour d'un repas partagé ou du feu de camp à la belle étoile comme la veille de la plénière.

  

Merci les MagnyÉth' pour ces chouettes moments partagés!
