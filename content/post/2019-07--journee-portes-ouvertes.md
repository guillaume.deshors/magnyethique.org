---
title: Visite et rencontre
subtitle: 26 octobre 2019
date: '2019-07-26'
tags:
  - portes-ouvertes
  - rencontre
  - promotion
---
Nous aurons le plaisir de vous accueillir le samedi 26 octobre 2019 à 14h30 pour une rencontre conviviale.

Au programme:

14h30 : présentation du projet

15h15 : visite guidée des lieux

16h00 : goûter partagé

Intéressé.es par l’habitat participatif en général, l’écolieu MagnyÉthique en particulier ou simples curieu.ses, vous êtes les bienvenu.es ! Merci de nous contacter via l’onglet “Contactez-nous!” pour vous inscrire afin que nous puissions nous organiser au mieux !
