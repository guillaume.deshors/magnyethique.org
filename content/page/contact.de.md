---
title: Kontakt
---

Regelmäßige Neuigkeiten? Abonniert unseren monatlichen 
[Newsletter](https://my.sendinblue.com/users/subscribe/js_id/2w4x5/id/1).
(Der Newsletter ist auf Französisch. Nutzt eure Skills oder den Übersetzer 
eures Vertrauens. ;-) ).

* * *

Folgt Artikeln und Events, die uns gefallen, auf unserer Facebookseite: [@MagnyÉthique](https://www.facebook.com/MagnyEthique/)

* * *

Fragen oder Wünsche? Füllt das unten stehende Kontaktformular aus. Wir werden uns bemühen, so schnell wie möglich zu antworten.

{{< contact >}}
