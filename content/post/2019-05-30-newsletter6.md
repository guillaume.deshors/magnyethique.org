---
title: Newsletter 6
date: '2019-05-30'
tags:
  - newsletter
  - avancement
---

Bonjour Ami.es MagnyÉthique 


Que de belles annonces à vous faire dans ce courrier !

### Recherche graines de futurs papillons
Les diverses études d’aménagements que nous souhaitions avancer ce printemps sont en cours et nous serons très bientôt les heureux propriétaires de Magny. Nous avons donc le plaisir de pouvoir vous annoncer la reprise immédiate des intégrations ! Nous les avions en effet mises en suspens en février afin de concentrer nos forces aux dossiers urgents et d’être en mesure de répondre mieux aux personnes intéressées concernant les espaces encore disponibles. 

### Les prochaines dates pour nous rencontrer :
Si l’habitat participatif en général vous intéresse et le projet MagnyÉthique en particulier attise votre curiosité, retenez bien les dates suivantes : 

Nous présenterons notre projet à la MJC d’Amplepuis le 8 juin prochain de 10h00 à 11h30, suivi d’un repas partagé (renseignement et réservations : contact@mjc-amplepuis.fr ou 04 74 89 42 71).

Afin de pouvoir également découvrir le lieu, vous êtes chaleureusement invités à notre prochaine journée portes ouvertes qui aura lieu le 6 juillet à partir de 10h30. Pour connaître les détails du programme et vous inscrire, c’est [par ici](/page/contact/) ! (Si aucune date ne vous convient n’hésitez pas à nous contacter quand même !) 
Nous avons hâte de vous rencontrer nombreux et de partager de beaux moments comme de riches conversations !


![](/images/newsletters/6/porte-ouverte.jpg)


### Du boulot… et des moments partagés !
Chose promise, chose due ! Nous vous parlions de chantiers participatifs dès cet été… Et bien l’été approchant, nous avons adhéré à Twiza et avons le plaisir de vous annoncer que les chantiers à Magny débuteront en juillet ! Au programme pour commencer mais en vrac : nettoyage, destruction de cloisons pour réorganiser les espaces, cloisonnement, installation de toilettes sèches et douches solaires, débroussaillage, construction d’une aire de jeux extérieurs pour les jeunes magnyeth’, construction d’un poulailler et pose de clotûre pour les premiers habitants à poils, élagage et fabrication de broyat pour débuter l’enrichissement des terres et les jardins, début des travaux pour l’installation de la phytoépuration etc. Il y en a pour tous les goûts, en intérieur comme en extérieur. Nous serons donc heureux de vous retrouver nombreux à partager ces moments avec nous. L’occasion aussi de découvrir le coin en s’échappant pour une rando ou un temps de baignade au Lac des Sapins… Alors, ça vous dit ? Réservez vite votre place en nous contactant, par Twiza ou via notre site !

![](/images/newsletters/6/travaux.jpg)

### Signature en vue et éclosions 
Après ces longs mois d’attente, la date de signature a enfin été fixée :  ce sera le 6 juin ! L’achat sera fait par la SCI qui compte actuellement 7 sociétaires mais qui va bientôt s’agrandir avec 2 nouveaux papillons fraîchement sortis de leur chrysalide.

![](/images/pictogrammes/picto-papillon.jpg)

### Et pendant ce temps, sur la route de Magny
Mais alors, où en est l'épopée de nos 5 futurs habitants ? Partis d’Allemagne à vélo, ils se rendent jusqu’à Cublize. Ils ont parcouru à ce jours près de 1 000 km et continuent leur route vers le sud. Ils sont ces jours-ci en Alsace et s’arrêtent au gré des habitats et des chantiers participatifs. Ils n’ont plus que 400 km à parcourir pour entrer dans les lieux ! Pour plus de détails sur leurs visites, vous pouvez lire leur progression et leurs rencontres ici https://transitionride.home.blog/ en français et en allemand.

Pour faire un don : 
{{< helloasso "Faire un don" "https://www.helloasso.com/associations/les-magnyethiques/formulaires/1" >}}

À bientôt !

MagnyÉthique
