---
title: Petit Bal des Possibles
subtitle: Fête des possibles
date: '2019-07-25'
tags:
  - rencontre
  - promotion
---
Le Quartier Métisseur nous a fait l'honneur de nous proposer de tenir un stand lors de leur RDV annuel dans le cadre de la Fête des Possibles : [le Petit Bal des Possibles](/images/uploads/1564149500105_Programme%20-%20Petit%20Bal%20des%20possibles.pdf). 

  

Vous pourrez donc venir nous rencontrer chez eux, à Lamure-sur-Azergues, toute la journée du samedi 21 septembre, et profiter aussi des nombreuses conférences et activités organisées ce jour-là, mais aussi le vendredi et le dimanche ! Laissez-vous inspirer par un programme inspirant[ ](/images/uploads/1564149500105_Programme%20-%20Petit%20Bal%20des%20possibles.pdf)!
