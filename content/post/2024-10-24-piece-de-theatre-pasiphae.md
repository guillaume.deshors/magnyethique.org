---
title: Pièce de théâtre Pasiphae
subtitle: Dimanche 1er décembre 2024 de 16 à 17h30
date: '2024-10-24'
tags:
  - culture
  - théâtre
  - spectacle
---
Dimanche 1er décembre, la Compagnie Arnold Schmürz viendra jouer chez nous, la tragi-comédie _Pasiphaé_ de Fabrice Hadjadj qui revisite le mythe antique de la mère du Minotaure.

_Rien ne va plus dans la Crète antique ! Le roi Minos ne peut plus courir les jupons sans être empêché par un sortilège baroque et la reine Pasiphaé est follement éprise d'un taureau destiné au sacrifice de Poséidon. L'ingénieur en chef Dédale et la nourrice de la reine ont beau déployer toute leur ingéniosité et soutien, rien n'y fait. Le mal est fait, les dieux s'en sont mêlés. Dès lors, comment vivre avec un monstre dans le ventre ? Comment accepter la pire des cocufications ?"_

![](https://cloud.magnyethique.org/s/RMwDtj7tqY6jnGp/preview)

Représentation à 16h.

1h30 de spectacle, adapté aux jeunes à partir de 15 ans.

Participation au chapeau pour les artistes.

Buvette payante en soutien à l'asso MagnyÉthique

Goûter-apéro partagé : vous pouvez apporter de quoi grignoter.
