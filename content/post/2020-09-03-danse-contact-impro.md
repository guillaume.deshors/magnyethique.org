---
title: 'Danse-contact-impro '
subtitle: Session août-septembre 2020
date: '2020-09-03'
tags:
  - rencontre
  - atelier
  - culture
  - danse
bigimg:
  - desc: null
    src: /images/uploads/1599165637565_Danse-contact2.jpg
---
Sur l'invitation de Bruno et de Geneviève Cron, danseuse parisienne, nous avons réunis sur quelques jours une petite dizaine de danseuses et danseurs confirmé·e·s pour un temps d'échange et de pratique de la danse-contact-impro. 

  

Ce fut une rencontre riche, passionnée et passionnante, soutenue par les excellents repas préparés par Clément et Chloé du [Quartier Métisseur](http://quartiermetisseur.mystrikingly.com), et qui a permis à quelques habitant·e·s de découvrir cette pratique. 

  

Tout le monde en est sorti enchanté, tant par le lieu que par la fluidité de l'organisation auto-gérée et la profondeur des contenus : portés, équilibre, tango-contact, danse et méditation en nature, danser en trio et plus, fluidité et élasticité, voilà quelques uns des thèmes que nous avons partagés dans une ambiance à la fois joueuse, joyeuse et studieuse. Le contact-improvisation est maintenant dans les murs pour de bon et d'autres propositions vont suivre !  

  

![](/images/uploads/1599165583361_Danse-contact1.jpg)
