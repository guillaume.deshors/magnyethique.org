---
title: Marc Vella - "Rendre belles les fausses notes de la Vie"
subtitle: Formation des samedi 23 et dimanche 24 octobre 2021
date: '2021-07-18'
tags:
  - formation
  - rencontre
  - culture
---
_"Qui n’a pas été maladroit, défaillant ?_

_N’a pas été blessé, offensé ?_

_Qui, parfois, n’a pas manqué de discernement ?_

_Et si toutes ces fausses notes de la vie étaient une chance ?_

_Et si elles étaient notre plus grande aventure donnant un sens à notre existence ?"_

C'est ainsi que s'ouvre la page d'accueil de l'[École de la fausse note](https://ecoledelafaussenote.com) : tout un programme !

C'est dans ce cadre et avec cette intention d'accepter et d'accepter, voire même de s'enrichir des fausses notes de la vie que [Marc Vella](https://ecoledelafaussenote.com/marc-vella/) interviendra les 23 et 24 octobre 2021 à l'Écolieu MagnyÉthique pour partager cette philosophie et des techniques d'improvisation au piano avec les stagiaires.

Si vous êtes intéressé·e, suivez les instructions de l'affiche !

![](/images/uploads/1620568167925_MagnyéthiqueStageMVella.jpg)


Le stage est limité à une quinzaine de personnes.

Nous proposons à celleux qui le désireraient la possibilité de dormir sur place en dortoir. Pour profiter au mieux du temps de stage, nous proposons des repas végétariens à tendance biolocale pour samedi et dimanche midi et vous demandons de prévoir un pique-nique pour celui du samedi soir (attention pas de micro-onde disponible !)

En ce qui concerne le prix, nous vous demanderons 240€ pour les frais pédagogiques, 20€ pour les repas du midi, et une contribution libre aux frais engendrés par l'accueil&nbsp;: utilisation des salles, éventuellement logement et petit-déjeuner, soutien au projet MagnyÉthique et à la participation libre et consciente.

Si vous souhaitez confirmer votre inscription, contactez-nous à 202110_marcvella@magnyethique.org en précisant également votre ville de départ et mode de transport pour des éventuels covoiturages, en indiquant si vous souhaitez dormir sur place, et faites parvenir un chèque d’arrhes de 80€ à :
MagnyÉthique - stage Marc Vella - 213 chemin du château de Magny - 69550 Cublize

N'hésitez pas à nous contacter si vous avez des questions !

