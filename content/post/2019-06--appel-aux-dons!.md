---
title: Appel aux dons!
date: '2019-06-10'
tags:
  - avancement
  - générosité
---
"Oyez oyez bonnes gens!"

  
Nous emménageons tout doucement et en prévision des chantiers participatifs qui vont bientôt débuter, nous lançons un appel aux dons de matériel!

Nous recherchons:  
\- nécessaire de cuisine (collective si possible)  
\- assiettes, verres, couverts en grand nombre  
\- outils en tout genre (marteaux et tourne vis, fourches, bêches, pelles & co mais aussi brouettes et charrettes en tout genre)

Vous en avez qui trainent dans un coin en mal d'utilisation ou qui s'amoncèlent désespérément au grenier ou dans un vieil abri de jardin chez vous ou chez un parent dans l'espoir de retrouver un jour un peu de son éclat d'antan ou juste de l'activité?

Contactez-nous pour voir si vous préférez nous emmener tout ça et au passage prendre l'apéro et faire une petite visite guidée du lieu ou si vous préférez que l'on passe récupérer tout ça!

Merci d'avance!
