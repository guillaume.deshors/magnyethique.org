---
title: Conférence gesticulée - Un jongleur se déballe
subtitle: samedi 11 mars 2023 à 20h
date: '2023-01-17'
tags:
  - spectacle
  - culture
---

![](https://cloud.magnyethique.org/s/MdgN3J4NJi5Gr26/preview)



Bruno Labouret, jongleur et artiste pluridisciplinaire, vous partage ses réflexions inspirées par son parcours. Quels parallèles entre le sens de la vie et la trajectoire d’une balle ? Combien faut-il d’échecs pour en faire une réussite ?
Un moment rare d’échange et d’humanité, à la fois profond et convivial, avec des vrais morceaux de spectacle dedans !

* à 20h à MagnyÉthique
* Durée 2h15 avec entracte
* Proposition peu appropriée avant l’âge d’une quinzaine d’année
* Prix libre, conscient et solidaire
* Réservation indispensable par SMS au 06 62 28 36 08 : nombre de places limité
* Après le spectacle, nous partagerons les boissons et grignotages que vous aurez apportés.
