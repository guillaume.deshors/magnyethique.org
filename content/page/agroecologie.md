---
title: Agroécologie
comments: false
---
L'écolieu a vocation à **favoriser l'écologie et la biodiversité** sur ses presque 8 hectares de terrain. Les objectifs sont divers mais complémentaires et visent , outre l'usage vivrier des terrains, à conserver la richesse faunique et floristique trouvée à notre arrivée. Une longue phase de plus d'une année d'observation nous a d'abord permis d'identifier la diversité présente. Nous cherchons à présent à mettre en place des aménagements humains et pour la biodiversité qui favoriseraient son développement, dans le cadre d'un design en permaculture pour lequel nous sommes accompagnés par un bureau d'étude.

*   Les habitants commencent doucement à aménager un **espace agroécologique** sur les 8 hectares de terrain entourant le château, qui permettra peut-être d’atteindre une certaine autosuffisance alimentaire d'une part et de pouvoir tester et partager diverses méthodes de culture sur place (agroforesterie, Maraîchage sur Sol Vivant…) d'autre part. Les enseignements tirés de nos expériences pourront permettre leur mise en réseau afin de servir la recherche agroécologique qui nécessite des recoupements, mais aussi offrir un lieu global de formation à la permaculture.

![](https://cloud.magnyethique.org/s/p9gYsyeokKYWgSt/preview)

*   Le projet MagnyÉthique s'engage également dans la **conservation d'espèces anciennes et/ou locales** de végétaux afin de contribuer à en assurer la sauvegarde et à perpétuer la **variété des espèces**, plus précieuse que jamais. C'est dans le respect de ces deux principes de **diversité et de conservation** que le choix des végétaux du verger-maraîcher a été fait en 2020 et que nous plantons un verger partiellement conservatoire. C'est grâce aux conseils des Croqueurs de Pommes du [Verger Saint Genois](http://vergersaintgenois.com) et surtout à l'[aide précieuse et généreuse du **Fonds Germe**](https://magnyethique.org/post/2021-12-23-un-verger-conservatoire-a-l-ecolieu/) que nous allons pouvoir concrétiser ce verger étape par étape dès 2022.
    
*   Nous avons le même engagement en faveur de la biodiversité pour la faune. Depuis juillet 2020, l'écolieu est **[refuge LPO](https://www.lpo.fr/la-lpo-en-actions/mobilisation-citoyenne/refuges-lpo/presentation)** (Ligue de Protection des Oiseaux). Le long travail de reconnaissance floristique et faunique que nous avons mené sur nos terrains nous a permis d'identifier les nombreuses espèces présentes, d'apprendre à mieux les connaître et de mettre en oeuvre des actions de préservation, voire même de favoriser l'installation sur les terrains d'espèces en danger (oiseaux, insectes, reptiles, batraciens etc.). Nous souhaitons profiter du lieu et de sa richesse pour y animer des ateliers de sensibilisation à la biodiversité.
    
*   Il peut être envisageable de mettre à disposition d’un professionnel (horticulteur·rice, arboriculteur·rice, maraîcher·ère, éleveur·se, apiculteur·rice, champignonnier·ère, vannier·ère…) ou d'une association des espaces extérieurs ou des caves dans le respect des principes du projet et en concertation afin d'intégrer cette activité dans le design global des terrains.
    

![](https://cloud.magnyethique.org/s/S7naQq54CfHmDsr/preview)

**Pour en savoir plus sur le Fonds Germe :**

*   [Choix de banque](https://cloud.magnyethique.org/s/X5ZYn5egnECwayo)
*   [Fonds Germe](https://cloud.magnyethique.org/s/afQSYTHPz7Xo28w)

![](/images/uploads/illustration-site-agricole.jpg)

Capacités
=========

Un terrain de 8 hectares de landes et prairies autour du château, présence de sources offrant la possibilité d’une irrigation des cultures si nécessaire ou de culture de végétaux hydrophiles.
